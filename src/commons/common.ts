export const common = {
  tipoDato: [
    { value: "", label: "Seleccionar tipo de dato" },
    { value: "ALFANUMERICO", label: "ALFANUMERICO" },
    { value: "NUMERICO", label: "NUMERICO" },
    { value: "FECHA", label: "FECHA" },
    { value: "PORCENTAJE", label: "PORCENTAJE" },
    { value: "LISTADO", label: "LISTADO" },
    { value: "JSON", label: "JSON" },
    { value: "XML", label: "XML" }],

  tipoOrigenValor: [
    { value: "", label: "Seleccionar tipo de origen valor" },
    { value: "SISTEMA_TABLA", label: "SISTEMA TABLA" },
    { value: "SISTEMA_CONSULTA", label: "SISTEMA CONSULTA" },
    { value: "SISTEMA_SESION", label: "SISTEMA SESION" },
    { value: "SUMINISTRADO_POR_USUARIO", label: "SUMINISTRADO POR USUARIO" }],

  tiposEstado: [
    { value: "ACTIVO", label: "Activo" }, { value: "INACTIVO", label: "Inactivo" }],

  tiposEstadoVigenciaParametro: [
    { value: "ACTIVO", label: "Activo" }, { value: "INACTIVO", label: "Inactivo" }, { value: "PROGRAMADO", label: "Programado" }],

  siNo: [
    { value: "TRUE", label: "Sí" }, { value: "FALSE", label: "No" }],

  getFuenteDato: [
    { value: "", label: "Seleccionar fuente dato" }, { value: "1", label: "Fuente Dato 1" }],

  opcionesEstado: [
    { value: "ACTIVO", label: "Activo" }, { value: "INACTIVO", label: "Inactivo" }],

  opcionesSiNo: [
    { value: "TRUE", label: "Si" }, { value: "FALSE", label: "No" }],

  cols: [
    { name: 'codigo' }, { name: 'nombre' }],

  consultaEstadoIdentificacionActivoPageable: { estado: ["VIGENTE", "EXPIRADO"], page: 0, size: 100 },

  consultaEstadoIdentificacionInactivoPageable: { estado: ["VIGENTE", "EXPIRADO", "INACTIVO"], page: 0, size: 100 },

  consultaEstadoEstandarPageable: { estado: ['ACTIVO', 'INACTIVO'], page: 0, size: 100 },

  consultaEstadoActivoPageable: { estado: ['ACTIVO'], page: 0, size: 100 },

  consultaEstadoEstandarCompletoPageable: { estado: ["ACTIVO", "INACTIVO", "ELIMINADO"], page: 0, size: 100 },

  consultaEstadoActivo: {estado:"ACTIVO"},

  seleccioneValorEstandar: { value: "", label: "Seleccione un valor" },

  estadoNombreOpciones: ['estado', 'nombre'],

  codigoNombreOpciones: ['codigo', 'nombre'],

  nombreOpcion: ['nombre'],

};