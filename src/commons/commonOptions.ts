export const commonOptions = {
  tipoDato: [
    { value: "", label: "Seleccionar tipo de dato" },
    { value: "ALFANUMERICO", label: "ALFANUMERICO" },
    { value: "NUMERICO", label: "NUMERICO" },
    { value: "FECHA", label: "FECHA" },
    { value: "PORCENTAJE", label: "PORCENTAJE" },
    { value: "LISTADO", label: "LISTADO" },
    { value: "JSON", label: "JSON" },
    { value: "XML", label: "XML" }],

  tipoOrigenValor: [
    { value: "", label: "Seleccionar tipo de origen valor" },
    { value: "SISTEMA_TABLA", label: "SISTEMA TABLA" },
    { value: "SISTEMA_CONSULTA", label: "SISTEMA CONSULTA" },
    { value: "SISTEMA_SESION", label: "SISTEMA SESION" },
    { value: "SUMINISTRADO_POR_USUARIO", label: "SUMINISTRADO POR USUARIO" }],

  tiposEstado: [
    { value: "ACTIVO", label: "Activo" }, { value: "INACTIVO", label: "Inactivo" }],

  tiposEstadoVigenciaParametro: [
    { value: "ACTIVO", label: "Activo" }, { value: "INACTIVO", label: "Inactivo" }, { value: "PROGRAMADO", label: "Programado" }],

  siNo: [
    { value: "TRUE", label: "Sí" }, { value: "FALSE", label: "No" }],

  getFuenteDato: [
    { value: "", label: "Seleccionar fuente dato" }, { value: "1", label: "Fuente Dato 1" }],

  opcionesEstado: [
    { value: "ACTIVO", label: "Activo" }, { value: "INACTIVO", label: "Inactivo" }],

  opcionesSiNo: [
    { value: "TRUE", label: "Si" }, { value: "FALSE", label: "No" }],

  cols: [
    { name: 'codigo' }, { name: 'nombre' }],

  opcionesJndi: [
    { value: "SI", label: "Si" }, { value: "NO", label: "No" }],

  consultaEstadoIdentificacion: { estado: ["VIGENTE", "EXPIRADO", "INACTIVO"], page: 0, size: 100 },

  consultaEstadoEstandar: { estado: ['ACTIVO', 'INACTIVO'], page: 0, size: 100 },

  consultaEstadoActivo: { estado: ['ACTIVO'], page: 0, size: 100 },

  consultaEstadoEstandarCompleto: { estado: ["ACTIVO", "INACTIVO", "ELIMINADO"], page: 0, size: 100 },

  seleccioneValorEstandar: { value: "", label: "Seleccione un valor" },

  opcionesLog: [
    { value: 0, label: "Off" },
    { value: 10, label: "Fatal" },
    { value: 20, label: "Error" },
    { value: 30, label: "Warn" },
    { value: 40, label: "Info" },
    { value: 50, label: "Debug" }
  ],

  estadosGeneral: ["ACTIVO", "INACTIVO", "ELIMINADO"],

  estadosActivoInactivo: ["ACTIVO", "INACTIVO"],

  valoresEstado: [
    { label: "Activo", value: "ACTIVO" },
    { label: "Inactivo", value: "INACTIVO" }
  ],

  valoresTipoLista: [
    { label: "Seleccione un tipo", value: "" },
    { label: "Tipo Consulta", value: "TIPOCONSULTA" },
    { label: "Tipo Dominio", value: "TIPODOMINIO" },
    { label: "Tipo Rango", value: "TIPORANGO" }
  ],

  TIPO_CONSULTA: "TIPOCONSULTA",
  TIPO_DOMINIO: "TIPODOMINIO",
  TIPO_RANGO: "TIPORANGO",

  valoresTipoDatoRango: [
    { label: "Seleccione tipo dato", value: "" },
    { label: "Numérico", value: "TIPONUMERICO" },
    { label: "Alfanumérico", value: "TIPOALFANUMERICO" },
    { label: "Fecha", value: "TIPOFECHA" }
  ],

  TIPOD_NUMERICO: "TIPONUMERICO",
  TIPOD_ALFANUM: "TIPOALFANUMERICO",
  TIPOD_FECHA: "TIPOFECHA",

  tiposObjetos:[
    { label: "Referenciado", value: "REFERENCIADO" },
    { label: "Dinamico", value: "DINAMICO" },
    { label: "Estatico", value: "ESTATICO" }
  ],

  opcioneSINO: [
    { value: "SI", label: "SI" }, { value: "NO", label: "NO" }
  ],

  tipoDatoObjeto:[
    { label: "Alfanumerico", value: "ALFANUMERICO" },
    { label: "Numerico", value: "NUMERICO" },
    { label: "Fecha", value: "FECHA" },
    { label: "Objeto De Negocio", value: "OBJETONEGOCIO" },
    { label: "Binario", value: "BINARIO" },
  ],

  tiposCardinalidad:[
    { label: "Unitaria", value: "UNITARIA" },
    { label: "Multiple", value: "MULTIPLE" },
  ],


};