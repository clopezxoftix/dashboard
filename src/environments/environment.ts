// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  urlBase: 'http://127.0.0.1:8080',
  basicToken: 'eG9mdGl4LWNsaWVudDp4MEZ0MXg=',
  urlAuthentication: 'http://127.0.0.1:8080/auth/login',
  production: false,
  codigo: 'codigo',
  slash: '/',


  cuentasUsuario: '/cuentas-usuario',
  fuentedatos: '/datasources',
  nivelesSeguridad: '/niveles-seguridad',
  recursos:'/recursos',
  modulos: '/modulos',
  parametros: '/parametros',
  parametrosVigencia: '/parametros-vigencia',
  privilegios:'/privilegios',
  reportes:'/reportes',
  perfilPrivilegio:'/perfiles-privilegios',
  eliminarPerfilPrivilegio: "eliminar-por-perfil-privilegio",
  editarPerfilPrivilegio: "editar-por-perfil-privilegio",
  perfiles:'/perfiles',
  perfilCuenta:'/perfiles-cuenta',
  eliminarPerfilCuenta: "eliminar-por-perfil-cuenta",
  editarPerfilCuenta: "editar-por-perfil-cuenta",
  recursoAuditoria:'/recursos-auditorias',
  identificaciones: '/identificaciones',
  personas: '/personas',
  id_identificacion: '/id_identificacion',
  idiomas: '/idiomas',
  regiones: '/regiones',
  tiposUnidades: '/tipos-unidades',
  listaValores: '/lista-valores',
  listaAtributo: '/atributo',
  guardarAtributo: 'asociarAtributos',
  consultar: 'consultar',
  listaElemento: '/elemento',
  guardarElemento: 'asociarElementos',
  fuenteDato: '/fuentes_dato',
  fuenteDatoAtributo: '/fuente_dato_atributo',
  objeto:'/objetos',
  atributo:'/objeto-atributos',
  subir:'subir',
  bajar:'bajar',
  guardar_todos:'guardar_todos',
  objetivoEstrategico: '/objetivos-estrategicos',
  objetivoOperativo: '/objetivos-operativos',
  organizaciones: '/organizaciones',
  unidadesOrganizacionales: '/unidades-organizacionales',

  firebase: {},


  debug: true,
  log: {
    auth: false,
    store: false,
  },

  smartadmin: {
    api: null,
    db: 'smartadmin-angular'
  }

};
