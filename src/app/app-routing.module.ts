import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthLayoutComponent } from "./views/app-layouts/auth-layout.component";
import { MainLayoutComponent } from "./views/app-layouts/main-layout.component";
import { SampleComponent } from "./shared/components/general-form/sample/sample.component";

const routes: Routes = [
  {
    path: "",
    component: MainLayoutComponent,
    data: { pageTitle: "Home" },
    children: [
      {
        path: "",
        redirectTo: 'modules/home',
        pathMatch: 'full'
      },
      {
        path: "sample",
        component: SampleComponent,
        pathMatch: 'full'
      },
      {
        path: "modules",
        loadChildren: "./views/views.module#ViewsModule",
        data: { pageTitle: "Views" }
      }
    ]
  },

  {
    path: "auth",
    component: AuthLayoutComponent,
    loadChildren: "./core/auth/auth.module#AuthModule"
  },
  { path: "**", redirectTo: "miscellaneous/error404" }




];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {}
