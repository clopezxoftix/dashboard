import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlogRoutingModule } from './blog-routing.module';
import { BlogComponent } from './blog.component';
import { StatsModule } from '@app/shared/components/stats/stats.module';
import { SmartadminLayoutModule } from '@app/views/layout';


@NgModule({
  imports: [
    CommonModule,
    BlogRoutingModule,
    SmartadminLayoutModule,
    StatsModule,
  ],
  declarations: [BlogComponent]
})
export class BlogModule { }
