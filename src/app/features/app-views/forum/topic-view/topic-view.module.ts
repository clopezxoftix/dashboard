import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TopicViewRoutingModule} from './topic-view-routing.module';
import {TopicViewComponent} from './topic-view.component';
import { StatsModule } from '@app/shared/components/stats/stats.module';
import { SmartadminLayoutModule } from '@app/views/layout';

@NgModule({
  imports: [
    CommonModule,
    TopicViewRoutingModule,
    SmartadminLayoutModule,
    StatsModule,
  ],
  declarations: [TopicViewComponent]
})
export class TopicViewModule {
}
