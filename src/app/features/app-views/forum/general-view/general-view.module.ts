import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GeneralViewRoutingModule} from './general-view-routing.module';
import {GeneralViewComponent} from './general-view.component';
import { StatsModule } from '@app/shared/components/stats/stats.module';
import { SmartadminLayoutModule } from '@app/views/layout';

@NgModule({
  imports: [
    CommonModule,
    GeneralViewRoutingModule,
    SmartadminLayoutModule,
    StatsModule,
  ],
  declarations: [GeneralViewComponent]
})
export class GeneralViewModule {
}
