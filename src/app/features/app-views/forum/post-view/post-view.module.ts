import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PostViewRoutingModule} from './post-view-routing.module';
import {PostViewComponent} from './post-view.component';
import { StatsModule } from '@app/shared/components/stats/stats.module';
import { SmartadminLayoutModule } from '@app/views/layout';

@NgModule({
  imports: [
    CommonModule,
    PostViewRoutingModule,

    SmartadminLayoutModule,
    StatsModule,
  ],
  declarations: [PostViewComponent]
})
export class PostViewModule {
}
