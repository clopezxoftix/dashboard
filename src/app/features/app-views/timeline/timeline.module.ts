import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TimelineRoutingModule} from './timeline-routing.module';
import {TimelineComponent} from './timeline.component';
import { StatsModule } from '@app/shared/components/stats/stats.module';
import { SmartadminLayoutModule } from '@app/views/layout';

@NgModule({
  imports: [
    CommonModule,
    SmartadminLayoutModule,
    StatsModule,
    TimelineRoutingModule
  ],
  declarations: [TimelineComponent]
})
export class TimelineModule {
}
