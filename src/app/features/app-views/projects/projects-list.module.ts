import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProjectsListRoutingModule} from './projects-list-routing.module';
import {ProjectsListComponent} from './projects-list.component';
import { StatsModule } from '@app/shared/components/stats/stats.module';
import { SmartadminDatatableModule } from '@app/shared/components/ui/datatable/smartadmin-datatable.module';
import { SmartadminWidgetsModule } from '@app/shared/components/widgets/smartadmin-widgets.module';
import { SmartadminLayoutModule } from '@app/views/layout';

@NgModule({
  imports: [
    CommonModule,
    ProjectsListRoutingModule,
    SmartadminLayoutModule,
    StatsModule,
    SmartadminDatatableModule,

    SmartadminWidgetsModule,
  ],
  declarations: [ProjectsListComponent]
})
export class ProjectsListModule {
}
