import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CkeditorRoutingModule } from './ckeditor-routing.module';
import { CkeditorComponent } from './ckeditor.component';
import { StatsModule } from '@app/shared/components/stats/stats.module';
import { SmartadminWidgetsModule } from '@app/shared/components/widgets/smartadmin-widgets.module';
import { SmartadminLayoutModule } from '@app/views/layout';

@NgModule({
  imports: [
    CommonModule,
    CkeditorRoutingModule,


    SmartadminLayoutModule,
		StatsModule,
    SmartadminWidgetsModule,
  ],
  declarations: [CkeditorComponent]
})
export class CkeditorModule { }
