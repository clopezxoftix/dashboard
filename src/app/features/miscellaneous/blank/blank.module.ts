import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlankRoutingModule } from './blank-routing.module';
import { BlankComponent } from './blank.component';
import { StatsModule } from '@app/shared/components/stats/stats.module';
import { SmartadminWidgetsModule } from '@app/shared/components/widgets/smartadmin-widgets.module';
import { SmartadminLayoutModule } from '@app/views/layout';

@NgModule({
  imports: [
    CommonModule,
    BlankRoutingModule,

    SmartadminLayoutModule,
		StatsModule,
    SmartadminWidgetsModule,
  ],
  declarations: [BlankComponent]
})
export class BlankModule { }
