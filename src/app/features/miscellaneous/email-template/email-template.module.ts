import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmailTemplateRoutingModule } from './email-template-routing.module';
import { EmailTemplateComponent } from './email-template.component';
import { StatsModule } from '@app/shared/components/stats/stats.module';
import { SmartadminLayoutModule } from '@app/views/layout';

@NgModule({
  imports: [
    CommonModule,
    EmailTemplateRoutingModule,

    SmartadminLayoutModule,
    StatsModule,
  ],
  declarations: [EmailTemplateComponent]
})
export class EmailTemplateModule { }
