import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PricingTablesRoutingModule } from './pricing-tables-routing.module';
import { PricingTablesComponent } from './pricing-tables.component';
import { StatsModule } from '@app/shared/components/stats/stats.module';
import { SmartadminLayoutModule } from '@app/views/layout';


@NgModule({
  imports: [
    CommonModule,
    PricingTablesRoutingModule,


    SmartadminLayoutModule,
		StatsModule,
  ],
  declarations: [PricingTablesComponent]
})
export class PricingTablesModule { }
