import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchRoutingModule } from './search-routing.module';
import { SearchComponent } from './search.component';
import { StatsModule } from '@app/shared/components/stats/stats.module';
import { SmartadminLayoutModule } from '@app/views/layout';

@NgModule({
  imports: [
    CommonModule,
    SearchRoutingModule,


    SmartadminLayoutModule,
		StatsModule,
  ],
  declarations: [SearchComponent]
})
export class SearchModule { }
