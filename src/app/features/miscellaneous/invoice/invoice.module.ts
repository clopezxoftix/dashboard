import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoiceRoutingModule } from './invoice-routing.module';
import { InvoiceComponent } from './invoice.component';
import { StatsModule } from '@app/shared/components/stats/stats.module';
import { SmartadminWidgetsModule } from '@app/shared/components/widgets/smartadmin-widgets.module';
import { SmartadminLayoutModule } from '@app/views/layout';


@NgModule({
  imports: [
    CommonModule,
    InvoiceRoutingModule,

    SmartadminLayoutModule,
		StatsModule,
    SmartadminWidgetsModule,
  ],
  declarations: [InvoiceComponent]
})
export class InvoiceModule { }
