export default class normalizeUtil {

    static normalizar(palabra): string {
        return palabra.normalize('NFD')
            .replace(/([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+/gi, "$1")
            .normalize().toLowerCase();
    }


}