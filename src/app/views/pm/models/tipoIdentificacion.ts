export class TipoIdentificacion{
    id: number;
    estado: string;
    codigo: string;
    nombre: string;
    documentacion: string;
}