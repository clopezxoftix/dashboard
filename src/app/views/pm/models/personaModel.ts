import { Identificacion } from "./identificacion";

export class Persona{
    id: number;
    estado: string;
    aliasSigla: string;
    clase: string;    
    direccionCorrespondencia: string;
    direccionResidencia: string;
    emailAlterno: string;
    emailPrincipal: string;
    fechaNacimiento: Date;
    fotoLogo: string;
    idGenero: number; //cambiar a objeto cuando se tenga el back
    pmtIdentificacion: Identificacion;
    idUbicacionCorrespondencia: number;
    idUbicacionNacimiento: number;
    ubicacionPaisResidencia: number;
    nombre: string;
    primerApellido: string;
    segundoApellido: string;
    telefonoAlterno: string;
    telefonoPrincipal: string;
    documentacion: string;
}