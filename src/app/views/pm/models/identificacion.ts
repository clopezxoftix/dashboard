import { TipoIdentificacion } from "./tipoIdentificacion";

export class Identificacion{
    id: number;
    estado: string;
    digitoVerificacion: string;
    fechaExpedicion: Date;
    fechaVencimiento: Date;
    idPais: number; //cambiar a objeto cuando se tenga el back
    pmtTipoIdentificacion: TipoIdentificacion;
    identificacion: string;
    documentacion: string;
}