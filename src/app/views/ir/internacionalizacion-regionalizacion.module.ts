import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IdiomaComponent } from './idioma/idioma.component';
import { RegionComponent } from './region/region.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [IdiomaComponent, RegionComponent]
})
export class InternacionalizacionRegionalizacionModule { }
