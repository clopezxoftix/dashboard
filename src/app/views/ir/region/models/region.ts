import { Idioma } from "../../idioma/models/idioma";

export class Region {
    constructor() {
    }
    id: number;
    idioma: Idioma;
    codigo: string;
    nombre: string;
    estado: string;
    documentacion: string;
 
}