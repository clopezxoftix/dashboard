import { Component, OnInit, ViewChild, Output, Input, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { GeneralFormComponent } from '@app/shared/components/general-form/general-form.component';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ParametroVigencia } from './models/parametroVigencia';
import { commonOptions } from '../../../../../commons/commonOptions';
import { ParametrosVigenciaService } from '@app/core/services/ge/parametros-vigencia.service';
import { ModulosService } from '@app/core/services/ge/modulos.service';
import { ParametrosVigencia } from './models/parametrosParametroVigencia';
import { Parametro } from '../models/parametro';
import { TypeElement } from '@app/shared/components/general-form/types/TypeElement';
import { NotificationService } from '@app/core/services';
import * as moment from 'moment'
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';

@Component({
  selector: 'parametro-vigencia',
  templateUrl: './parametro-vigencia.component.html',
  styleUrls: ['./parametro-vigencia.component.css']
})
export class ParametroVigenciaComponent implements OnInit, OnChanges {

  parametro2: any = null;
  @Output() agregarVigencias = new EventEmitter();
  @Input()
  set parametro(p: any) {
    this.parametro2 = p;
  }
  @Input() disableEdit;

  ngOnChanges(changes: SimpleChanges) {

    this.parametro2 = changes.parametro.currentValue;
    this.consultar();
    this.deshabilitarFechas(this.parametro2.id);
  }

  campo = "codigo";
  criterio = "";
  estado = "ACTIVO";
  est = "ACTIVO";
  parametroVigencia = new ParametroVigencia;
  valorPaginado = 10;
  formParametroVigencia: any;
  formParametroVigenciaDate: any;
  formParametroVigenciaTime: any;
  mensaje: String;
  accion: string;
  bool = "ACTIVO";
  searching = true;
  disable = false;
  confirmDialog = false;
  codigoRecorded = false;
  suscritoAComponenteCodigo = false;
  booleanos = "FALSE";
  public modulos = [{ value: "", label: "Seleccionar módulo" }];
  public tiposDato = commonOptions.tipoDato;
  public origenesValor = commonOptions.tipoOrigenValor;
  public fuentesDato = commonOptions.getFuenteDato;
  opcionesEstado = commonOptions.tiposEstadoVigenciaParametro;
  opcionesSN = commonOptions.siNo;

  //variables de calendario
  invalidDates: Date[] = [];
  minDate: Date;
  maxDate: Date;
  today: any = new Date();
  fecha: Date;
  visible: boolean = true;
  value: any;
  rangeDates: any;

  @ViewChild('formParametroVigenciaId', { 'static': false }) formParametroVigenciaId: GeneralFormComponent;
  @ViewChild('formParametroVigenciaDateId', { 'static': false }) formParametroVigenciaDateId: GeneralFormComponent;
  @ViewChild('formParametroVigenciaTimeId', { 'static': false }) formParametroVigenciaTimeId: GeneralFormComponent;

  @ViewChild(DatatableComponent, { 'static': false }) table: DatatableComponent;

  /* Variables relacionadas a la tabla de consulta */
  controls: any = {
    pageSize: 10,
    filter: '',
    rowCount: 0, //total elementos
    curPage: 0, //numero pagina
    selectedCount: 0,
    offset: 0
  }
  cols = [{ name: 'codigo' }, { name: 'nombre' }];
  temp = [];
  rows = [];
  rowsT = [];
  activo: any;
  loadingIndicator = false;
  reorderable = true;
  messages = {
    emptyMessage: 'No existe informacion relacionada',
    totalMessage: 'Registros totales',
  };

  constructor(private _ModulosService: ModulosService, private _ParametrosVigenciaService: ParametrosVigenciaService, private notificationService: NotificationService) { }

  ngOnInit() {
    this.today = new Date();
    this.formParametroVigencia = this.createForm();
    this.formParametroVigenciaDate = this.createFormDate();
    this.formParametroVigenciaTime = this.createFormTime();
    this.criterio = "%";
  }

  myDatePickerOptions: INgxMyDpOptions = {
    dayLabels: { su: "Do", mo: "Lu", tu: "Ma", we: "Mi", th: "Ju", fr: "Vi", sa: "Sa" },
    monthLabels: { 1: "Ene", 2: "Feb", 3: "Mar", 4: "Abr", 5: "May", 6: "Jun", 7: "Jul", 8: "Ago", 9: "Sep", 10: "Oct", 11: "Nov", 12: "Dic" },
    dateFormat: 'yyyy/mm/dd',
    firstDayOfWeek: 'mo',
    sunHighlight: true,
    selectorHeight: "290px",
    selectorWidth: "260px",
    disableDateRanges: []
  };

  createFormDate(): any {
    return {
      header: {
        label: "Vigencia Parámetros",
      },
      fields: {
        id: {},
        row1: {
          row: [
            {
              key: "fechaDesde",
              label: "Fecha desde",
              typeElement: TypeElement.DATE2,
              validation: {
                required: "El campo es obligatorio"
              },
              myDatePickerOptions: this.myDatePickerOptions,
              modelo: this.rangeDates
            },
            {
              key: "fechaHasta",
              label: "Fecha hasta",
              typeElement: TypeElement.DATE2,
              validation: {
                required: "El campo es obligatorio"
              },
              myDatePickerOptions: this.myDatePickerOptions,
              modelo: this.rangeDates
            }
          ]
        }

      }
    }
  }
  createFormTime(): any {
    return {
      fields: {
        id: {},
        row1: {
          row: [
            {
              key: "horaDesde",
              label: "Hora desde",
              typeElement: TypeElement.TIME
            },
            {
              key: "horaHasta",
              label: "Hora hasta",
              typeElement: TypeElement.TIME
            }
          ]
        },

      }
    }
  }
  createForm(): any {
    return {
      fields: {
        id: {},
        row1: {
          row: [
            {
              key: "valor",
              label: "Valor",
              typeElement: TypeElement.TEXT,
              validation: {
                required: "El campo es obligatorio"
              },
              placeholder: "Ingrese un valor"
            },
            {
              key: "estado",
              label: "Estado",
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesEstado,
              default: this.opcionesEstado[0],
              validation: {
                required: "El campo es obligatorio"
              }
            }
          ]
        },
        documentacion: {
          label: "Documentación",
          typeElement: TypeElement.TEXTAREA

        },

      }
    }
  }

  nuevo() {
    this.parametroVigencia = new ParametroVigencia;
  }

  mostrar() {
    if (this.visible == true) {
      this.visible = false;
    } else if (this.visible == false) {
      this.visible = true;
    }
  }

  openModal(id) {
    if (id != null) {
      this.cargarDatosParametroVigencia(id);
    }
  }

  confirm(): void {
    this.eliminar(this.parametroVigencia.id);
    this.nuevo();
    this.confirmDialog = false;
  }

  decline(): void {
    this.confirmDialog = false;
    this.nuevo();
  }


  editando() {
    return !this.searching
  }

  consultando() {
    return this.searching
  }

  verTabla() {
    this.searching = true
  }

  verFormulario(s: any) {
    this.searching = false
    this.nuevo();
    this.suscritoAComponenteCodigo = false;
    this.disable = false;
    this.deshabilitarFechas(this.parametro2.id);
    if (s == 'A') {
      this.getMaximaVigenciaHasta();
    }
  }

  /**
   * 
   * @param idParametro método que trae la última vigencia por idParametro
   */
  getMaximaVigenciaHasta() {
    this._ParametrosVigenciaService.maxVigencia(this.parametro2.id).subscribe(
      response => {
        this.parametroVigencia = response;
        if (this.parametroVigencia != null) {
          let d = moment(this.parametroVigencia.vigenciaHasta).add('day', 1);
          let h = moment(this.parametroVigencia.vigenciaHasta).add('day', 1);
          let desde = moment(d).format('MM-DD-YYYY,hh:mm');
          let hasta = moment(h).format('MM-DD-YYYY,hh:mm');
          let dx = new Date(desde);
          dx.setHours(1);
          dx.setMinutes(0);
          let hx = new Date(hasta);
          hx.setHours(23);
          hx.setMinutes(59);
          this.formParametroVigenciaDateId.form.controls['fechaDesde'].setValue({ date: { year: +moment(desde).format('YYYY'), month: +moment(desde).format('M'), day: +moment(desde).format('D') }, jsdate: dx });
          this.formParametroVigenciaDateId.form.controls['fechaHasta'].setValue({ date: { year: +moment(hasta).format('YYYY'), month: +moment(hasta).format('M'), day: +moment(hasta).format('D') }, jsdate: hx });
          this.formParametroVigenciaTimeId.form.controls['horaDesde'].setValue(dx);
          this.formParametroVigenciaTimeId.form.controls['horaHasta'].setValue(hx);
        }
        else {
          let f = new Date();
          f.setHours(1);
          f.setMinutes(0);
          this.formParametroVigenciaDateId.form.controls['fechaDesde'].setValue({ jsdate: f });
          this.formParametroVigenciaDateId.form.controls['fechaHasta'].setValue({ jsdate: f });
          this.formParametroVigenciaTimeId.form.controls['horaDesde'].setValue(f);
          this.formParametroVigenciaTimeId.form.controls['horaHasta'].setValue(f);
        }
      },
      error => {
        this.notificationService.errorMessage("No se pudo encontrar la vigencia con el parámetro");
      }
    );
  }

  /**
   * Método deshabilitarFechas por parámetros
   */
  deshabilitarFechas(idParametro) {
    this.myDatePickerOptions.disableDateRanges.length = 0;
    if (idParametro != null) {
      let p = new Parametro;
      p.id = idParametro;
      const x = new ParametrosVigencia;


      x.estado = ["ACTIVO", "INACTIVO", "PROGRAMADO"];
      this.criterio = "%";
      x.id_parametro = idParametro;

      this._ParametrosVigenciaService.listar(x).subscribe(
        response => {
          this.invalidDates.length = 0;
          for (let i = 0; i < response.content.length; i++) {
            let desde = new Date(moment(response.content[i].vigenciaDesde).format('MM-DD-YYYY,hh:mm:ss')).getTime();
            let hasta = new Date(moment(response.content[i].vigenciaHasta).format('MM-DD-YYYY,hh:mm:ss')).getTime();
            if (response.content[i].vigenciaDesde != null && response.content[i].vigenciaHasta == null) {
              this.fecha = new Date(moment(response.content[i].vigenciaDesde).format('MM-DD-YYYY,hh:mm:ss'));
              this.minDate = this.today;
              this.maxDate = this.today;

              break;
            }
            else if (response.content[i].vigenciaDesde != null && response.content[i].vigenciaHasta != null) {
              if (this.parametroVigencia.vigenciaDesde != response.content[i].vigenciaDesde) {
                this.myDatePickerOptions.disableDateRanges.push({ begin: { year: +moment(desde).format('YYYY'), month: +moment(desde).format('M'), day: +moment(desde).format('D') }, end: { year: +moment(hasta).format('YYYY'), month: +moment(hasta).format('M'), day: +moment(hasta).format('D') } });
              }
            }

          }

        },
        error => {
          console.log("No se pudo consultar", error);
        }
      );
    }
  }

  /**
* Description: Metodo para el filtro de la tabla de consulta
* responde al evento keyup del elemento ngx-dataTable
*
* @param {*} event Valor del input del filtro del dataTable
* @memberof ModulosComponent
*/
  updateFilter(event) {
    let val = event.target.value.toLowerCase();
    let colsAmt = this.cols.length;
    let keys = ['codigo', 'nombre'];
    this.rows = this.temp.filter(function (item) {
      for (let i = 0; i < colsAmt; i++) {
        if (item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 || !val) {
          return true;
        }
      }
    });
    this.table.offset = 0;
  }

  /**
   * Description: Metodo que ejecuta la paginacion del ngx-dataTable responde
   * al evento ngModelChange del componente select
   *
   * @param {*} value Valor del select del componente paginacion del dataTable
   * @memberof ModulosComponent
   */
  updatePageSize(value) {
    this.controls.pageSize = parseInt(value, 10);
    this.table.limit = this.controls.pageSize;
    this.table.offset = 0;
    this.controls.curPage = 0;
    this.consultar();
  }

  setPage(event) {
    this.controls.curPage = event.offset;
    this.controls.pageSize = event.pageSize;
    this.consultar();
  }

  onChanges(): void {

    if (this.formParametroVigenciaId && !this.suscritoAComponenteCodigo) {
      this.suscritoAComponenteCodigo = true;

    }
  }

  get desactivar() {
    return this.disable;
  }

  /**
   * Método que se ejecuta al enviar un nivel de seguridad guardar, editar
   */
  onSubmit() {
    let encontrado = false;
    if (this.activo.length > 0) {
      encontrado = true;
    }
    for (let i = 0; i < this.rowsT.length && !encontrado; i++) {
      if (this.rowsT[i].estado == "ACTIVO") {
        encontrado = true;
      }
    }

    this.parametroVigencia = this.formParametroVigenciaId.form.value;

    let desde = moment(this.formParametroVigenciaDateId.form.controls['fechaDesde'].value.jsdate).format('MM-DD-YYYY,hh:mm');
    let hasta = moment(this.formParametroVigenciaDateId.form.controls['fechaHasta'].value.jsdate).format('MM-DD-YYYY,hh:mm');
    let horaDesde = moment(this.formParametroVigenciaTimeId.form.controls['horaDesde'].value).format('HH');
    let horaHasta = moment(this.formParametroVigenciaTimeId.form.controls['horaHasta'].value).format('HH');
    let minDesde = moment(this.formParametroVigenciaTimeId.form.controls['horaDesde'].value).format('mm');
    let minHasta = moment(this.formParametroVigenciaTimeId.form.controls['horaHasta'].value).format('mm');
    let dx = new Date(desde);
    dx.setHours(+horaDesde);
    dx.setMinutes(+minDesde);
    let hx = new Date(hasta);
    hx.setHours(+horaHasta);
    hx.setMinutes(+minHasta);

    this.parametroVigencia.vigenciaDesde = dx;
    this.parametroVigencia.vigenciaHasta = hx;
    this.parametroVigencia.estado = this.parametroVigencia.estado == "" ? this.est : this.parametroVigencia.estado;
    this.parametroVigencia.idParametro = this.parametro2;
    if (dx < hx) {
      //MS: Si la condición retorna true, entonces va a editar una vigencia
      if (this.parametroVigencia.id > 0) {
        let p = new Parametro;
        p.id = this.parametro2.id;
        this.parametroVigencia.idParametro = p;
        if ((this.parametroVigencia.estado != "ACTIVO" && encontrado) || (this.parametroVigencia.estado == "ACTIVO" && !encontrado) || this.parametroVigencia.id == this.activo[0].id && this.parametroVigencia.estado == "ACTIVO") {
          this._ParametrosVigenciaService.guardar(this.parametroVigencia).subscribe(
            response => {
              this.parametroVigencia = response;

              this.searching = true;
              this.nuevo();
              this.notificationService.successMessage("El registro se modificó satisfactoriamente");
              this.consultar();
            },
            error => {
              this.notificationService.errorMessage("No se pudo modificar las vigencias con el parámetro");
            }
          );
        } else {
          this.notificationService.errorMessage("No pueden existir dos vigencias activas en el parámetro");
        }
      } else {
        //Si el id del parámetro es > 0, se agregan vigencias a un parámetro existente
        if (this.parametroVigencia.idParametro.id > 0) {
          let p = new Parametro;
          p.id = this.parametro2.id;
          this.parametroVigencia.idParametro = p;
          if (this.parametroVigencia.estado != "ACTIVO" || !encontrado) {
            //push a rows visual
            this.rows.push(
              {
                "estado": this.parametroVigencia.estado,
                "idParametro": {

                  "codigo": this.parametro2.codigo,
                  "nombre": this.parametro2.nombre,
                  "id": this.parametroVigencia.idParametro.id,
                },
                "valor": this.parametroVigencia.valor,
                "vigenciaDesde": moment(this.parametroVigencia.vigenciaDesde).format('YYYY-MM-DD,HH:mm:ss'),
                "vigenciaHasta": moment(this.parametroVigencia.vigenciaHasta).format('YYYY-MM-DD,HH:mm:ss')
              }
            );

            //rowsT= array temporal para almacenar vigencias
            this.rowsT.push(
              {
                "estado": this.parametroVigencia.estado,
                "idParametro": {
                  "codigo": this.parametro2.codigo,
                  "nombre": this.parametro2.nombre,
                  "id": this.parametroVigencia.idParametro.id,
                },
                "valor": this.parametroVigencia.valor,
                "vigenciaDesde": this.parametroVigencia.vigenciaDesde,
                "vigenciaHasta": this.parametroVigencia.vigenciaHasta
              }
            );

          } else {

            this.notificationService.errorMessage("Sólo puede haber una vigencia activa");
          }

        } else {
          //Agrega vigencias junto con un nuevo parámetro
          if ((this.parametroVigencia.estado != "ACTIVO" && encontrado) || (this.parametroVigencia.estado == "ACTIVO" && !encontrado) || (this.activo.length == 0 && !encontrado)) {
            this.rows.push(
              {
                "estado": this.parametroVigencia.estado,
                "idParametro": {
                  "id": 0,
                },
                "valor": this.parametroVigencia.valor,
                "vigenciaDesde": moment(this.parametroVigencia.vigenciaDesde).format('YYYY-MM-DD,HH:mm:ss'),
                "vigenciaHasta": moment(this.parametroVigencia.vigenciaHasta).format('YYYY-MM-DD,HH:mm:ss')
              }
            );
            this.rowsT.push(
              {
                "estado": this.parametroVigencia.estado,
                "idParametro": {
                  "id": 0,
                },
                "valor": this.parametroVigencia.valor,
                "vigenciaDesde": this.parametroVigencia.vigenciaDesde,
                "vigenciaHasta": this.parametroVigencia.vigenciaHasta
              }
            );
          } else {

            this.notificationService.errorMessage("Sólo puede haber una vigencia activa para el nuevo parámetro");
          }
        }
        this.agregarVigencias.emit(this.rowsT);
        this.searching = true;
        this.nuevo();
        this.temp = this.rows;
        this.loadingIndicator = false;
        this.controls.rowCount += this.rows.length;
      }
    } else {
      this.notificationService.warningMessage("La fecha desde debe ser inferior a la fecha hasta");
    }
  }

  get isInvalid() {
    if (this.formParametroVigenciaId) {
      this.onChanges();
    }
    return this.formParametroVigenciaId || this.formParametroVigenciaDateId || this.formParametroVigenciaTimeId ? this.formParametroVigenciaId.form.invalid : true;
  }

  /**
   * Método consultar por parámetros
   */
  consultar() {
    this.loadingIndicator = true;
    let p = new Parametro;
    p.id = this.parametro2;
    const x = new ParametrosVigencia;
    x.size = this.controls.pageSize;
    x.page = this.controls.curPage;

    if (this.criterio != "") {
      if (this.estado != "TODOS") {
        x.estado = this.estado.split(",");
      }
      if (this.criterio != "%") {
        if (this.campo == "codigo") {
          x.codigo = this.criterio;
        } else if (this.campo == "nombre") {
          x.nombre = this.criterio;
        }
      }
      if (this.parametro2 != null) {
        x.id_parametro = this.parametro2.id;
      }

      this._ParametrosVigenciaService.listar(x).subscribe(
        response => {
          this.rows = response.content;
          this.temp = this.rows;
          this.loadingIndicator = false;
          this.controls.rowCount = response.totalElements;
          this.controls.curPage = response.number;
          this.existenVigenciasActivas();
        },
        error => {
          console.log("No se pudo consultar", error);
        }
      );
    }
    this.loadingIndicator = false;
  }

  /**
   * 
   * @param id 
   * Este método muestra un nivel de seguridad con el id dado por parámetro
   */
  visualizar(id) {
    this.verFormulario('V');
    this.buscar(id, true);

    this.disable = true;
  }

  /**
   * Busca un nivel de seguridad según id pasado por parámetro
   * @param id 
   */
  buscar(id, deshabilitar) {
    this._ParametrosVigenciaService.consultar(id).subscribe(
      response => {
        this.parametroVigencia = response;
        this.deshabilitarFechas(this.parametroVigencia.idParametro.id);
        let desde = moment(this.parametroVigencia.vigenciaDesde).format('MM-DD-YYYY');
        let hasta = moment(this.parametroVigencia.vigenciaHasta).format('MM-DD-YYYY');
        let horaDesde = moment(this.parametroVigencia.vigenciaDesde).format('kk:mm:ss');
        let horaHasta = moment(this.parametroVigencia.vigenciaHasta).format('kk:mm:ss');
        let fechaDesde = desde + "T" + horaDesde;
        let fechaHasta = hasta + "T" + horaHasta;
        this.formParametroVigenciaDateId.form.controls['fechaDesde'].setValue({ date: { year: +moment(desde).format('YYYY'), month: +moment(desde).format('M'), day: +moment(desde).format('D') }, jsdate: new Date(fechaDesde.replace(/-/g, '\/').replace(/T.+/, '')) });
        this.formParametroVigenciaDateId.form.controls['fechaHasta'].setValue({ date: { year: +moment(hasta).format('YYYY'), month: +moment(hasta).format('M'), day: +moment(hasta).format('D') }, jsdate: new Date(fechaHasta.replace(/-/g, '\/').replace(/T.+/, '')) });
        this.formParametroVigenciaTimeId.form.controls['horaDesde'].setValue(new Date(this.parametroVigencia.vigenciaDesde));
        this.formParametroVigenciaTimeId.form.controls['horaHasta'].setValue(new Date(this.parametroVigencia.vigenciaHasta));
        this.formParametroVigenciaId.form.controls['documentacion'].setValue(this.parametroVigencia.documentacion);
        this.formParametroVigenciaId.form.controls['estado'].setValue(this.parametroVigencia.estado);
        this.formParametroVigenciaId.form.controls['valor'].setValue(this.parametroVigencia.valor);
        this.formParametroVigenciaId.form.controls['id'].setValue(this.parametroVigencia.id);
        this.formParametroVigenciaTimeId.form.controls['']


        if (deshabilitar) {
          this.formParametroVigenciaId.form.disable();
          this.formParametroVigenciaDateId.form.disable();
          this.formParametroVigenciaTimeId.form.disable();
        }
      },
      error => {
        console.log("No se pudo consultar", error);
      }
    );
  }

  /**
   * Carga los datos del parametro vigencia
   * @param id 
   */
  cargarDatosParametroVigencia(id) {
    this._ParametrosVigenciaService.consultar(id).subscribe(
      response => {
        this.parametroVigencia = response;
        this.confirmDialog = true;
        this.mensaje = "Eliminar";
      },
      error => {
        console.log("No se pudo cargar", error);
      }
    );
  }

  /**
   * Edita un nivel de seguridad según id pasado por parametro
   * @param id 
   */
  editar(id) {
    this.verFormulario('E');
    this.buscar(id, false);
    this.disable = false;

  }

  /**
   * 
   * @param id Método que elimina un nivel de seguridad con id pasado por parámetro (eliminado lógiso, cambio de estado)
   */
  eliminar(id) {
    this._ParametrosVigenciaService.eliminar(id).subscribe(
      response => {
        this.notificationService.successMessage("El registro se elimino satisfactoriamente");
        this.consultar();
      },
      error => {
        this.notificationService.errorMessage("El registro no se pudo eliminar");
      }
    );

    this.consultar();
  }

  /**
   * Método que verifica si existen vigencias activas para el parametro
   */
  existenVigenciasActivas() {
    const x = new ParametrosVigencia;
    x.id_parametro = this.parametro2.id;
    x.size = this.controls.pageSize;
    x.page = this.controls.curPage;
    x.estado = "ACTIVO".split(",");

    this._ParametrosVigenciaService.listar(x).subscribe(
      response => {
        this.activo = response.content;
      },
      error => {
        console.log("No se pudo consultar", error);
      }
    );
  }

}
