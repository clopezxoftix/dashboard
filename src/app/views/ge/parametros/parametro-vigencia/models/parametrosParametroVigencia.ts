import { Parametro } from "../../models/parametro";

export class ParametrosVigencia {
    constructor(){

    }
      page : number;
      size: number;
      estado: string[];
      codigo: string;
      nombre: string;
      id_parametro: number;
}
