import { Parametro } from "../../models/parametro";

export class ParametroVigencia{
    constructor(){}
    id: number;
    estado: string;
    idParametro: Parametro;
    vigenciaDesde: Date;
    vigenciaHasta: Date;
    valor: string;
    documentacion: string;
}