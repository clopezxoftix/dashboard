import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParametroVigenciaComponent } from './parametro-vigencia.component';

describe('ParametroVigenciaComponent', () => {
  let component: ParametroVigenciaComponent;
  let fixture: ComponentFixture<ParametroVigenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParametroVigenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParametroVigenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
