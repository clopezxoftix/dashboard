import { Modulo } from "../../modulos/models/modulo";

export class Parametro{
    constructor(){}
    id: number;
    codigo: string;
    nombre: string;
    estado: string;
    modulo: Modulo;
    etiqueta: string;
    tipoDato: string;
    valorCifrado: string;
    origenValor: string;
    controlVigencia: string;
    idFuenteDato: number;
    valor: string;
    documentacion: string;
}