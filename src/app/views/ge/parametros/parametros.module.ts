import { GeneralFormComponentModule } from '../../../shared/components/general-form/general-form-component.module';
import { NgModule } from "@angular/core";
import { FormsModule } from '@angular/forms';
import { ParametrosComponent } from './parametros.component';
import { CommonModule } from '@angular/common';
import { DataTableHeaderCellComponent, NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ParametrosRoutingModule } from './parametros.routing.module';

@NgModule({
    declarations: [ ParametrosComponent ],
    imports: [ParametrosRoutingModule, GeneralFormComponentModule, CommonModule, FormsModule,,NgxDatatableModule,DataTableHeaderCellComponent],
    exports: [GeneralFormComponentModule]
})
export class ParametroModule {}