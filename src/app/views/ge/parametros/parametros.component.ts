import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Parametro } from './models/parametro';
import { GeneralFormComponent } from '@app/shared/components/general-form/general-form.component';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NotificationService } from '@app/core/services';
import { TypeElement } from '@app/shared/components/general-form/types/TypeElement';
import { ModulosService } from '@app/core/services/ge/modulos.service';
import { commonOptions } from '../../../../commons/commonOptions';
import { Modulo } from '../modulos/models/modulo';
import { Parametros } from './models/parametros';
import { ParametrosService } from '@app/core/services/ge/parametros.service';
import { ParametroVigenciaComponent } from './parametro-vigencia/parametro-vigencia.component';
import { ParametrosVigenciaService } from '@app/core/services/ge/parametros-vigencia.service';
import { ParametrosVigencia } from './parametro-vigencia/models/parametrosParametroVigencia';
import { commonValues } from '@commons/commonValues';
import { commonMessage } from '@commons/commonMessages';

@Component({
  selector: 'parametros',
  templateUrl: './parametros.component.html',
  styleUrls: ['./parametros.component.css']
})
export class ParametrosComponent implements OnInit {

  @ViewChild(GeneralFormComponent, { 'static': false }) formParametroId: GeneralFormComponent;
  @ViewChild(DatatableComponent, { 'static': false }) table: DatatableComponent;
  @ViewChild(ParametroVigenciaComponent, { 'static': false }) formParametroVigencia: ParametroVigenciaComponent;

  @Input() vigencias: any;
  vigenciasParam: any;
  nuevoP = false;
  campo = commonValues.CODIGO_VALUE;
  criterio = "";
  estado = commonValues.ACTIVO_VALUE;
  est = commonValues.ACTIVO_VALUE;
  parametro = new Parametro;
  valorPaginado = 10;
  formParametro: any;
  mensaje: String;
  accion: string;
  bool = commonValues.ACTIVO_VALUE;
  searching = true;
  disable = false;
  confirmDialog = false;
  codigoRecorded = false;
  suscritoAComponenteCodigo = false;
  booleanos = commonValues.FALSE_VALUE;
  public modulos = [{ value: "", label: commonValues.SELECCIONE_MODULO_LABEL }];
  public tiposDato = commonOptions.tipoDato;
  public origenesValor = commonOptions.tipoOrigenValor;
  public fuentesDato = commonOptions.getFuenteDato;
  tabVigencia = false;
  opcionesEstado = commonOptions.tiposEstado;
  opcionesSN = commonOptions.siNo;
  currentTab = commonValues.PARAMETROS_VALUE;

  //Labels vista
  lblParametros = commonValues.PARAMETROS_LABEL;
  lblCancelar = commonValues.CANCELAR_LABEL;
  lblGuardar = commonValues.GUARDAR_LABEL;
  lblAgregar = commonValues.AGREGAR_LABEL;
  lblCriteriosBusqueda = commonValues.CRITERIOS_BUSQUEDA_LABEL;
  lblCampo = commonValues.CAMPO_LABEL;
  lblCodigo = commonValues.CODIGO_LABEL;
  lblNombre = commonValues.NOMBRE_LABEL;
  lblUrl = commonValues.URL_LABEL;
  lblEstado = commonValues.ESTADO_LABEL;
  lblActivo = commonValues.ACTIVO_LABEL;
  lblInactivo = commonValues.INACTIVO_LABEL;
  lblTodos = commonValues.TODOS_LABEL;
  lblCriterioBusqueda = commonValues.CRITERIO_BUSQUEDA_LABEL;
  lblBuscar = commonValues.BUSCAR_LABEL;
  lblFiltroTabla = commonValues.FILTRO_TABLA_LABEL;
  lblAcciones = commonValues.ACCIONES_LABEL;
  lblVer = commonValues.VER_LABEL;
  lblEditar = commonValues.EDITAR_LABEL;
  lblEliminar = commonValues.ELIMINAR_LABEL;
  lblVigencias = commonValues.VIGENCIAS_LABEL;
  lblSi = commonValues.SI_LABEL;
  lblNo = commonValues.NO_LABEL;
  msgDeseaEliminar = commonMessage.DESEA_ELIMINAR_REGISTRO;
  msgNoSePuedeEliminar = commonMessage.NO_SE_PUEDE_ELIMINAR;

  selectTab(moduleCode: string) {
    this.currentTab = moduleCode;
  }

  /* Variables relacionadas a la tabla de consulta */
  controls: any = {
    pageSize: 10,
    filter: '',
    rowCount: 0, //total elementos
    curPage: 0, //numero pagina
    selectedCount: 0,
    offset: 0
  }
  cols = [{ name: 'codigo' }, { name: 'nombre' }];
  temp = [];
  rows = [];
  loadingIndicator = false;
  reorderable = true;
  messages = {
    emptyMessage: commonMessage.NO_EXISTE_INFO,
    totalMessage: commonMessage.REGISTROS_TOTALES,
  };

  constructor(private _ModulosService: ModulosService, private _ParametrosService: ParametrosService, private _ParametrosVigenciaService: ParametrosVigenciaService, private notificationService: NotificationService) { }

  ngOnInit() {
    this.formParametro = this.createForm();
    const x = new Parametros;
    x.estado = commonValues.ACTIVO_VALUE;
    x.page = 0;
    x.size = 1000;
    this._ModulosService.listar(x).subscribe(
      response => {
        for (const key of response.content) {
          this.modulos.push({
            label: '[' + key.codigo + ']' + ' ' + key.nombre,
            value: key.id
          });
        }
        this.modulos = Array.from(this.modulos);
      }
    )
  }

  createForm(): any {
    return {
      header: {
        label: commonValues.PARAMETROS_LABEL,
      },
      fields: {
        id: {},
        modulo: {
          // key: "modulo",
          label: commonValues.MODULO_LABEL,
          typeElement: TypeElement.SELECT,
          options: this.modulos,
          default: this.modulos[0],
          validation: {
            required: commonMessage.CAMPO_OBLIGATORIO
          }
        },
        row2: {
          row: [
            {
              key: "codigo",
              label: commonValues.CODIGO_LABEL,
              typeElement: TypeElement.TEXT,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
              placeholder: commonMessage.INGRESE_CODIGO
            },
            {
              key: "nombre",
              label: commonValues.NOMBRE_LABEL,
              typeElement: TypeElement.TEXT,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },

              placeholder: commonMessage.INGRESE_NOMBRE

            }
          ]
        },
        row3: {
          row: [
            {
              key: "etiqueta",
              label: commonValues.ETIQUETA_LABEL,
              typeElement: TypeElement.TEXT,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
              placeholder: commonMessage.INGRESE_ETIQUETA
            },
            {
              key: "estado",
              label: commonValues.ESTADO_LABEL,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesEstado,
              default: this.opcionesEstado[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            }
          ]
        },
        row4: {
          row: [

            {
              key: "tipoDato",
              label: commonValues.TIPO_DATO_LABEL,
              typeElement: TypeElement.SELECT,
              options: this.tiposDato,
              default: this.tiposDato[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
            },
            {
              key: "valorCifrado",
              label: commonValues.VALOR_CIFRADO_LABEL,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesSN,
              default: this.opcionesSN[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {
              key: "origenValor",
              label: commonValues.ORIGEN_VALOR_LABEL,
              typeElement: TypeElement.SELECT,
              options: this.origenesValor,
              default: this.origenesValor[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
            }
          ]
        },
        row5: {
          row: [
            {
              key: "controlVigencia",
              label: commonValues.CONTROL_VIGENCIA_LABEL,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesSN,
              default: this.opcionesSN[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {
              key: "idFuenteDato",
              label: commonValues.FUENTE_DATOS_LABEL,
              typeElement: TypeElement.SELECT,
              options: this.fuentesDato,
              default: this.fuentesDato[0],
            },
            {
              key: "valor",
              label: commonValues.VALOR_LABEL,
              typeElement: TypeElement.TEXT,
              placeholder: commonMessage.INGRESE_VALOR
            }
          ]
        },
        documentacion: {
          label: commonValues.DOCUMENTACION_LABEL,
          typeElement: TypeElement.TEXTAREA

        },

      }
    }
  }

  nuevo() {
    this.parametro = new Parametro;
  }

  openModal(id) {
    if (id != null) {
      this.cargarDatosParametro(id);
    }
  }

  confirm(): void {
    this.eliminar(this.parametro.id);
    this.nuevo();
    this.confirmDialog = false;
  }

  decline(): void {
    this.confirmDialog = false;
    this.nuevo();
  }


  editando() {
    return !this.searching
  }

  consultando() {
    return this.searching
  }

  verTabla() {
    this.searching = true
  }

  verFormulario(nuevo) {
    this.currentTab = commonValues.PARAMETROS_VALUE;
    this.searching = false
    this.nuevo();
    this.suscritoAComponenteCodigo = false;
    this.disable = false;
    if (nuevo == true) {
      this.nuevoP = true;
      this.parametro = new Parametro;
      this.parametro.id = 0;
    }
  }

  /**
* Description: Metodo para el filtro de la tabla de consulta
* responde al evento keyup del elemento ngx-dataTable
*
* @param {*} event Valor del input del filtro del dataTable
* @memberof ModulosComponent
*/
  updateFilter(event) {
    let val = event.target.value.toLowerCase();
    let colsAmt = this.cols.length;
    let keys = ['codigo', 'nombre'];
    this.rows = this.temp.filter(function (item) {
      for (let i = 0; i < colsAmt; i++) {
        if (item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 || !val) {
          return true;
        }
      }
    });
    this.table.offset = 0;
  }

  /**
   * Description: Metodo que ejecuta la paginacion del ngx-dataTable responde
   * al evento ngModelChange del componente select
   *
   * @param {*} value Valor del select del componente paginacion del dataTable
   * @memberof ModulosComponent
   */
  updatePageSize(value) {
    this.controls.pageSize = parseInt(value, 10);
    this.table.limit = this.controls.pageSize;
    this.table.offset = 0;
    this.controls.curPage = 0;
    this.consultar();
  }

  setPage(event) {
    this.controls.curPage = event.offset;
    this.controls.pageSize = event.pageSize;
    this.consultar();
  }

  onChanges(): void {
    if (this.formParametroId && !this.suscritoAComponenteCodigo) {
      this.suscritoAComponenteCodigo = true;
      this.formParametroId.form.get('codigo').valueChanges.subscribe(val => {
        this.onKeyupCodigo(val);
      });
    }

    this.formParametroId.form.get('controlVigencia').valueChanges.subscribe(val => {
      this.mostrarVigencias(val);

    });
  }

  get desactivar() {
    return this.disable;
  }

  /**
   * 
   * @param valorCodigo Método que búsca sí el código ingresado en el input ya existe en la bd
   */
  onKeyupCodigo(valorCodigo) {

    let p = new Parametros;
    if ((valorCodigo && (this.parametro.id && valorCodigo != this.parametro.codigo)) || !this.parametro.id && valorCodigo) {
      p.codigo = valorCodigo;
      p.estado = "ACTIVO,INACTIVO,ELIMINADO";
      this._ParametrosService.listar(p)
        .subscribe(data => {

          data = data.content;
          this.codigoRecorded = data != null && data.length > 0;
          if (this.codigoRecorded) {
            this.formParametroId.form.controls['codigo'].setErrors({ 'codigoRecorded': true });
          } else {
            this.formParametroId.form.controls['codigo'].setErrors({ 'codigoRecorded': false });
            const errores = this.formParametroId.form.controls['codigo'].errors;
            delete errores.codigoRecorded;
            this.formParametroId.form.controls['codigo'].setErrors(Object.keys(errores).length === 0 ? null : errores);
          }
        },
          error => console.error(error))
    }

  }

  /**
   * Método que se ejecuta al enviar un nivel de seguridad guardar, editar
   */
  onSubmit() {
    let m = new Modulo;
    m.id = this.formParametroId.form.controls['modulo'].value;
    this.parametro = this.formParametroId.form.value;
    this.parametro.estado = this.parametro.estado == "" ? this.est : this.parametro.estado;
    this.parametro.modulo = m;
    this.parametro.idFuenteDato = this.parametro.idFuenteDato;
    this.parametro.tipoDato = this.parametro.tipoDato;
    this.parametro.origenValor = this.parametro.origenValor;
    this._ParametrosService.guardar(this.parametro).subscribe(
      response => {
        this.parametro = response;
        this.searching = true;
        if (this.vigenciasParam != null && this.vigenciasParam != undefined) {
          for (let i = 0; i < this.vigenciasParam.length; i++) {
            this.vigenciasParam[i].idParametro.id = this.parametro.id;

            this._ParametrosVigenciaService.guardar(this.vigenciasParam[i]).subscribe(
              response => {
                this.vigenciasParam[i] = response;

                this.searching = true;
                this.nuevo();
                this.notificationService.successMessage(commonMessage.VIGENCIAS_PARAMETRO_REGISTRADAS);
                this.consultar();
              },
              error => {
                this.notificationService.errorMessage(commonMessage.VIGENCIAS_PARAMETRO_NO_REGISTRADAS);
              }
            );
          }
        }
        this.vigenciasParam = null;
        this.nuevo();
        this.notificationService.successMessage(commonMessage.REGISTRO_SATISFACTORIO);
        this.consultar();
      },
      error => {
        this.notificationService.errorMessage(commonMessage.REGISTRO_NO_EXITOSO);
      }
    );
    this.consultar();
  }

  get isInvalid() {
    if (this.formParametroId) {
      this.onChanges();
    }
    return this.formParametroId ? this.formParametroId.form.invalid : true;
  }

  /**
   * Método consultar por parámetros
   */
  consultar() {
    this.loadingIndicator = true;
    const x = new Parametros;
    x.size = this.controls.pageSize;
    x.page = this.controls.curPage;
    if (this.criterio != "") {
      if (this.estado != commonValues.TODOS_VALUE) {
        x.estado = this.estado;
      }
      if (this.criterio != commonValues.CRITERIO_VALUE) {
        if (this.campo == commonValues.CODIGO_VALUE) {
          x.codigo = this.criterio;
        } else if (this.campo == commonValues.NOMBRE_VALUE) {
          x.nombre = this.criterio;
        }
      }

      this._ParametrosService.listar(x).subscribe(
        response => {
          this.rows = response.content;
          this.temp = this.rows;
          this.loadingIndicator = false;
          this.controls.rowCount = response.totalElements;
          this.controls.curPage = response.number;
        },
        error => {
          console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
        }
      );
    } else {
      this.rows = null;
      this.temp = null;
      this.loadingIndicator = false;
      this.controls.rowCount = 0;
      this.controls.curPage = 0;
    }
  }

  /**
   * 
   * @param id 
   * Este método muestra un nivel de seguridad con el id dado por parámetro
   */
  visualizar(id) {

    this.currentTab = commonValues.PARAMETROS_VALUE;
    this.verFormulario(false);
    this.buscar(id, true);
    this.disable = true;
  }

  /**
   * Busca un nivel de seguridad según id pasado por parámetro
   * @param id 
   */
  buscar(id, deshabilitar) {
    this._ParametrosService.consultar(id).subscribe(
      response => {
        this.parametro = response;
        this.formParametroId.form.setValue(response);
        response.modulo != null ? this.formParametroId.form.controls['modulo'].setValue(response.modulo.id) : 0;

        if (deshabilitar) {
          this.formParametroId.form.disable();
        }
      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );
  }

  /**
   * Edita un nivel de seguridad según id pasado por parametro
   * @param id 
   */
  editar(id) {
    this.currentTab = commonValues.PARAMETROS_VALUE;
    this.verFormulario(false);
    this.buscar(id, false);
    this.disable = false;

  }

  /**
   * 
   * @param id Método que elimina un nivel de seguridad con id pasado por parámetro (eliminado lógiso, cambio de estado)
   */
  eliminar(id) {
    this._ParametrosService.eliminar(id).subscribe(
      response => {
        this.notificationService.successMessage(commonMessage.REGISTRO_ELIMINADO);
        this.eliminarVigencias(id);
        this.consultar();
      },
      error => {
        this.notificationService.errorMessage(commonMessage.REGISTRO_NO_ELIMINADO);
      }
    );

    this.consultar();
  }

  eliminarVigencias(id) {
    if (this.parametro != null) {
      let p = new Parametro;
      p.id = id;
      const x = new ParametrosVigencia;


      x.estado = commonOptions.estadosGeneral;
      this.criterio = commonValues.CRITERIO_VALUE;
      x.id_parametro = p.id;

      this._ParametrosVigenciaService.listar(x).subscribe(
        response => {
          for (let i = 0; i < response.content.length; i++) {
            this._ParametrosVigenciaService.eliminar(response.content[i].id).subscribe(
              response => {


              },
              error => {
                console.log(commonMessage.REGISTRO_NO_ELIMINADO, error);
              }
            );

          }

        },
        error => {
          console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
        }
      );
    }
  }

  agregarVigencias(vigencias: any) {
    this.vigenciasParam = vigencias;
  }

  /**
 * Carga los datos del parametro vigencia
 * @param id 
 */
  cargarDatosParametro(id) {
    this._ParametrosService.consultar(id).subscribe(
      response => {
        this.parametro = response;
        this.confirmDialog = true;
        this.mensaje = commonValues.ELIMINAR_LABEL;
      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CARGAR, error);
      }
    );
  }

  /**
   * Método para ocultar o mostrar el tab de vigencias
   */
  mostrarVigencias(valor) {
    this.tabVigencia = valor == commonValues.TRUE_VALUE ? true : false;
  }
}
