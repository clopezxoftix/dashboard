import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
    {
        path: '', redirectTo: '/', pathMatch: 'full'
      },
      {
          path: 'modulos', loadChildren: './modulos/modulos.module#ModulosModule'
      }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GeneralRoutingModule {}