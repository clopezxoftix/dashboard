import { SharedModule } from '@app/shared/shared.module';
import { ModulosComponent } from './modulos/modulos.component';
import { NgModule } from "@angular/core";
import { GeneralRoutingModule } from "./general.routing.module";
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ReactiveFormsModule } from '@angular/forms';
import { ParametrosComponent } from './parametros/parametros.component';
import { ParametroVigenciaComponent } from './parametros/parametro-vigencia/parametro-vigencia.component';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { AccordionModule } from 'primeng/accordion';
import { CalendarModule } from 'primeng/calendar';
import { ListaValoresComponent } from './lista-valores/lista-valores.component';
import { ListaAtributosComponent } from './lista-valores/lista-atributos/lista-atributos.component'
import { ListaElementosComponent } from './lista-valores/lista-elementos/lista-elementos.component'
import { BlockUIModule } from 'ng-block-ui';
import { FuenteDatoComponent } from './fuente-dato/fuente-dato.component';
import { FuenteDatoAtributoComponent } from './fuente-dato/fuente-dato-atributo/fuente-dato-atributo.component';
import { ObjetoNegocioComponent } from './objeto-negocio/objeto-negocio.component';
import { ObjetoAtributoComponent } from './objeto-negocio/objeto-atributo/objeto-atributo.component';


@NgModule({
    declarations: [ModulosComponent, ParametrosComponent, ParametroVigenciaComponent, ListaValoresComponent, ListaAtributosComponent, ListaElementosComponent,FuenteDatoComponent,FuenteDatoAtributoComponent,ObjetoNegocioComponent,ObjetoAtributoComponent],
    imports: [GeneralRoutingModule,SharedModule,NgxDatatableModule,ReactiveFormsModule, CalendarModule, NgxMyDatePickerModule, AccordionModule,BlockUIModule.forRoot()],
    exports: [ModulosComponent, ParametrosComponent, ParametroVigenciaComponent, ListaValoresComponent, ListaAtributosComponent, ListaElementosComponent,FuenteDatoComponent,FuenteDatoAtributoComponent,ObjetoNegocioComponent,ObjetoAtributoComponent],
    entryComponents: [ModulosComponent, ParametrosComponent, ParametroVigenciaComponent, ListaValoresComponent, ListaAtributosComponent, ListaElementosComponent,FuenteDatoComponent,FuenteDatoAtributoComponent,ObjetoNegocioComponent,ObjetoAtributoComponent]
})
export class GeneralModule {}
