
import { Component, OnInit, ViewChild } from '@angular/core';
import { commonValues } from '@commons/commonValues';
import { commonMessage } from '@commons/commonMessages';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NotificationService } from '@app/core/services/notification.service';
import { GeneralFormComponent } from '@app/shared/components/general-form/general-form.component';
import { TypeCaseText } from '@app/shared/components/general-form/types/TypeCaseText';
import { TypePatternMask } from '@app/shared/components/general-form/types/TypePatternMask';
import { TypeElement } from '@app/shared/components/general-form/types/TypeElement';
import { commonOptions } from '@commons/commonOptions';
import { RecursosService } from '@app/core/services/as/recursos.service';
import { FormBuilder, Validators } from '@angular/forms';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ObjetoNegocioService } from '../../../core/services/ge/objeto-negocio.service';
import { Objeto } from './models/objeto-negocio';
import { ParametrosObjetoNegocio } from './models/parametro-objeto-negocio';
import { ModulosService } from '../../../core/services/ge/modulos.service';
import { common } from '@commons/common';
import { ObjetoAtributoService } from '@app/core/services/ge/objeto-atributo.service';
import { Atributo } from './objeto-atributo/models/objeto-atributo';

@Component({
  selector: 'objeto-negocio',
  templateUrl: './objeto-negocio.component.html',
  styleUrls: ['./objeto-negocio.component.css']
})
export class ObjetoNegocioComponent implements OnInit {

  @ViewChild(DatatableComponent, { 'static': false }) table: DatatableComponent;
  @ViewChild(GeneralFormComponent, { 'static': false }) gfObjetoNegocio: GeneralFormComponent;

  @BlockUI('formConsulta') blockUIConsulta: NgBlockUI;
  @BlockUI('formRegistro') blockUIRegistro: NgBlockUI;

  //Labels vista
  lblConsulta = commonValues.CONSULTA_LABEL;
  lblObjetoNegocio = commonValues.OBJETO_NEGOCIO_LABEL;
  lblCampos = commonValues.CAMPOS_LABEL;
  lblEstado = commonValues.ESTADO_LABEL;
  lblActivo = commonValues.ACTIVO_LABEL;
  lblInactivo = commonValues.INACTIVO_LABEL;
  lblTodos = commonValues.TODOS_LABEL;
  lblCriterioBusqueda = commonValues.CRITERIO_BUSQUEDA_LABEL;
  lblBuscar = commonValues.BUSCAR_LABEL;
  lblAgregar = commonValues.AGREGAR_LABEL;
  lblFiltroTabla = commonValues.FILTRO_TABLA_LABEL;
  lblAcciones = commonValues.ACCIONES_LABEL;
  lblVer = commonValues.VER_LABEL;
  lblEditar = commonValues.EDITAR_LABEL;
  lblEliminar = commonValues.ELIMINAR_LABEL;
  lblSi = commonValues.SI_LABEL;
  lblNo = commonValues.NO_LABEL;
  lblCodigo = commonValues.CODIGO_LABEL;
  lblNombre = commonValues.NOMBRE_LABEL;
  lblCancelar = commonValues.CANCELAR_LABEL;
  lblGuardar = commonValues.GUARDAR_LABEL;
  msgCriterioObligatorio = commonMessage.CRITERIO_OBLIGATORIO;
  msgDeseaEliminar = commonMessage.DESEA_ELIMINAR_REGISTRO;
  msgNoSePuedeEliminar = commonMessage.NO_SE_PUEDE_ELIMINAR;

  /* Inicializacion de filtro de busqueda */
  campo = commonValues.CODIGO_VALUE;
  estado = commonValues.ACTIVO_VALUE;
  criterio = "";
  /*Variable para el control de apertura del formulario */
  searching = true;
  criterioRequerido = false;
  opcionesEstado = commonOptions.tiposEstado;
  tiposObjetos = commonOptions.tiposObjetos;
  optionGestionable = commonOptions.opcioneSINO;

  /*Variables de tabla*/
  /* Variables relacionadas a la tabla de consulta */
  controls: any = {
    pageSize: 10,
    filter: '',
    rowCount: 0,
    curPage: 0,
    offset: 0
  }

  temp = [];
  rows = [];
  loadingIndicator = false;
  reorderable = true;
  messages = {
    emptyMessage: commonMessage.NO_EXISTE_INFO,
    totalMessage: commonMessage.REGISTROS_TOTALES,
  };

  confirmDialog = false;
  sePuedeEliminar: boolean;

  suscritoAComponenteCodigo: any = false;
  disable: any;
  formCriterio: any;
  objeto: Objeto;
  formObjetoNegocio: any;
  modulos = new Array<any>();
  idO: any;
  accion = "";
  listaAtributosOutput: Array<Atributo> = new Array();
  auxEditar = false;
  variableMostrarBotones: any;

  constructor(private formBuilder: FormBuilder,
    private _objetoNegocioService: ObjetoNegocioService,
    private notificationService: NotificationService,
    private recursoService: RecursosService,
    private _moduloService: ModulosService,
    private _AtributoService: ObjetoAtributoService) { }

  ngOnInit() {
    this.formCriterio = this.formBuilder.group({
      criterio: ['', Validators.required]
    });
    this.verbotones();
    this.formObjetoNegocio = this.createForm();
  }

  consultando() {
    return this.searching
  }

  editando() {
    return !this.searching
  }

  consultar(initPage: boolean) {
    if (this.formCriterio.invalid) {
      this.criterioRequerido = true;
      return;
    }
    this.criterioRequerido = false;
    if (this.criterio != "") {
      this.controls.filter = "";
      this.loadingIndicator = true;
      if (initPage) {
        this.controls.curPage = 0;
      }
      let params = new ParametrosObjetoNegocio;
      if (this.campo == commonValues.CODIGO_VALUE) {
        params.codigo = this.criterio;
      } else {
        params.nombre = this.criterio;
      }
      params.estados = this.estado;
      params.size = this.controls.pageSize;
      params.page = this.controls.curPage;

      params.listaEstados = this.estado.split(",");
      this._objetoNegocioService.listar(params).subscribe(
        response => {
          this.rows = response.content;
          this.temp = this.rows;
          this.loadingIndicator = false;
          this.controls.rowCount = response.totalElements;
          this.controls.curPage = response.number;
        },
        error => {
          this.loadingIndicator = false;
          console.log(error);
          this.notificationService.errorMessage(commonMessage.NO_SE_REALIZO_CONSULTA);
        }
      );
    }
  }

  updateFilter(event) {
    let val = event.target.value.toLowerCase();
    let colsAmt = 2;
    let keys = ['codigo', 'nombre'];
    this.rows = this.temp.filter(function (item) {
      for (let i = 0; i < colsAmt; i++) {
        if (item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 || !val) {
          return true;
        }
      }
    });
    this.table.offset = 0;
  }

  updatePageSize(value) {
    this.controls.pageSize = parseInt(value, 10);
    this.table.limit = this.controls.pageSize;
    this.controls.curPage = (this.controls.pageSize > this.controls.rowCount) ?
      0 : this.controls.curPage;
    this.consultar(false);
  }

  setPage(event) {
    this.controls.curPage = event.offset;
    this.controls.pageSize = event.pageSize;
    this.consultar(false);
  }

  limpiarCriterio() {
    this.criterio = "";
  }

  verTabla() {
    this.criterioRequerido = false;
    this.searching = true;
    this.disable = false;
    this.idO = null;
    this.listaAtributosOutput.length = 0;
  }

  verFormulario() {
    this.accion = commonValues.REGISTRAR_LABEL;
    this.searching = false;
    this.nuevo();
    this.idO = null;
    this.suscritoAComponenteCodigo = false;
    this.auxEditar = true;
  }

  nuevo() {
    this.objeto = new Objeto();
  }

  get isInvalid() {
    if (this.gfObjetoNegocio) {
      this.onChanges();
    }
    return this.gfObjetoNegocio ? this.gfObjetoNegocio.form.invalid : true;
  }

  get desactivar() {
    return this.disable;
  }

  public state: any = {
    tabs: {
      tab: 'objeto'
    },
  }

  asociarAtributos(event: any) {
    this.listaAtributosOutput.push(event);
  }

  verbotones() {
    return this.variableMostrarBotones = true;
  }

  ocultarbotones() {
    return this.variableMostrarBotones = false;
  }

  onChanges(): void {
    if (this.gfObjetoNegocio && !this.suscritoAComponenteCodigo) {
      this.suscritoAComponenteCodigo = true;
      this.gfObjetoNegocio.form.get('codigo').valueChanges.subscribe(val => {
        this.onKeyupCodigo(val);
      });
    }
  }

  onKeyupCodigo(valorCodigo: string) {
    if ((valorCodigo && (this.objeto.id && valorCodigo != this.objeto.codigo)) || !this.objeto.id && valorCodigo) {
      this._objetoNegocioService.consultarCodigo(valorCodigo).subscribe(
        response => {
          if (response != null && response.id != this.objeto.id) {
            this.gfObjetoNegocio.form.controls['codigo'].setErrors({ 'codigoRecorded': true });
          } else {
            this.gfObjetoNegocio.form.controls['codigo'].setErrors({ 'codigoRecorded': false });
            const errores = this.gfObjetoNegocio.form.controls['codigo'].errors;
            delete errores.codigoRecorded;
            this.gfObjetoNegocio.form.controls['codigo'].setErrors(Object.keys(errores).length === 0 ? null : errores);
          }
        },
        error => {
          console.log(error);
          this.notificationService.errorMessage("Error al consultar código");
        }
      );
    }

  }

  onSubmit() {
    if (this.accion == this.lblEditar) {
      this.editar();
      return
    }
    this.objeto = this.gfObjetoNegocio.form.value;
    this.objeto.estado = (this.objeto.estado == "" ? 'ACTIVO' : this.objeto.estado);//COMPARADOR TERNARIOS
    this.objeto.tipo = (this.objeto.tipo == "" ? 'REFERENCIADO' : this.objeto.tipo);// PARA VALIDAR 
    this.objeto.gestionable = (this.objeto.gestionable == "" ? 'SI' : this.objeto.gestionable);//LA SELECCION DEL FORM
    this._objetoNegocioService.guardar(this.objeto).subscribe(response => {
      //para asociar atributos de una lista que no existe
      if (this.listaAtributosOutput && this.listaAtributosOutput.length) {
        let lista = new Objeto();
        lista.id = response.id;
        this.listaAtributosOutput.forEach((atributo) => {
          atributo.idObjeto = lista;
        });
        this._AtributoService.guardarTodos(this.listaAtributosOutput).subscribe(data => {
        });
      }
      this.verTabla();
      this.searching = true;
      this.nuevo();
      this.notificationService.successMessage(commonMessage.REGISTRO_SATISFACTORIO)
      this.consultar(false);
    },
      error => {
        this.notificationService.errorMessage(commonMessage.REGISTRO_NO_EXITOSO)
      })
  }

  editar() {
    this.objeto = this.gfObjetoNegocio.form.value;
    this.objeto.estado = (this.objeto.estado == "" ? 'ACTIVO' : this.objeto.estado);//COMPARADOR TERNARIOS
    this.objeto.tipo = (this.objeto.tipo == "" ? 'REFERENCIADO' : this.objeto.tipo);// PARA VALIDAR 
    this.objeto.gestionable = (this.objeto.gestionable == "" ? 'SI' : this.objeto.gestionable);//LA SELECCION DEL FORM
    this._objetoNegocioService.editar(this.objeto.id, this.objeto).subscribe(response => {
      this.searching = true;
      this.nuevo();
      this.notificationService.successMessage(commonMessage.REGISTRO_SATISFACTORIO)
      this.consultar(false);
    },
      error => {
        this.notificationService.errorMessage(commonMessage.REGISTRO_NO_EXITOSO)
      })
  }

  //confimr dialog
  openModal(id) {
    if (id != null) {
      this.buscar(id, false, true);
    }
  }

  confirm(): void {
    this.eliminar(this.objeto.id);
    this.nuevo();
    this.confirmDialog = false;
  }

  decline(): void {
    this.confirmDialog = false;
    this.nuevo();
  }

  visualizar(id, deshabilitar) {
    this.verFormulario();
    this.buscar(id, deshabilitar, false);
  }

  buscar(id: number, deshabilitar: boolean, eliminar: boolean) {
    this.idO = id;
    this.blockUIRegistro.start(commonMessage.CARGANDO + '...');
    this.auxEditar = false;
    this._objetoNegocioService.consultar(id).subscribe(
      response => {
        this.objeto = response;
        if (!eliminar) {
          this.gfObjetoNegocio.form.disable();
          this.disable = true;
          this.accion = (!deshabilitar ? commonValues.EDITAR_LABEL : commonValues.VER_LABEL);
          if (!deshabilitar) {
            this.gfObjetoNegocio.form.enable();
            this.disable = false;
          }
          this.gfObjetoNegocio.form.setValue(response);
        } else {
          this.confirmDialog = true; commonMessage.CARGANDO + this.consultarRelaciones(id);
        }
        this.blockUIRegistro.stop();
      },
      error => {
        this.blockUIRegistro.stop();
        this.notificationService.errorMessage(commonMessage.NO_SE_REALIZO_CONSULTA);
      }
    );
  }

  eliminar(id) {
    this.blockUIConsulta.start(commonMessage.CARGANDO + '...');
    this._objetoNegocioService.eliminar(id).subscribe(
      response => {
        this.consultar(false);
        this.notificationService.successMessage(commonMessage.REGISTRO_ELIMINADO);
        this.objeto = new Objeto();
        this.blockUIConsulta.stop();
      },
      error => {
        this.notificationService.errorMessage(commonMessage.REGISTRO_NO_ELIMINADO);
        this.blockUIConsulta.stop();
      }
    );
  }

  consultarRelaciones(id) {
    let arrayTemp = [];
    this.recursoService.listar(commonOptions.consultaEstadoEstandar).subscribe(
      response => {
        for (let item of response.content) {

          if (item.modulo.id == id) {
            arrayTemp.push(item);
          }
        }
        this.sePuedeEliminar = arrayTemp.length > 0;
        this.confirmDialog = true;

      },
      error => {
        this.confirmDialog = false;
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );
  }

  createForm(): any {
    return {
      header: {
        label: commonValues.DATOS_BASICOS_LABEL
      },
      fields: {
        id: {},
        rowTwo: {
          row: [
            {
              key: "idModulo",
              label: commonValues.MODULO_LABEL,
              typeElement: { typeElement: TypeElement.SELECCIONAREGISTRO },
              service: this._moduloService,
              inputsConsulta: common.estadoNombreOpciones,
              camposHeadersTabla: ["Codigo", "Nombre", "Estado"],
              camposTabla: ["codigo", "nombre", "estado"],
              parametrosDefecto: common.consultaEstadoActivo,
              camposInput: ["codigo", "nombre"],
              listarEspecifico: false,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {
              key: "nombre",
              label: commonValues.NOMBRE_LABEL,
              typeElement: { typeElement: TypeElement.TEXT },
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
              placeholder: commonMessage.INGRESE_NOMBRE
            },
            {
              key: "codigo",
              label: commonValues.CODIGO_LABEL,
              typeElement: { typeElement: TypeElement.FORMAT },
              textCase: TypeCaseText.UPPER_CASE,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO,
                expression: TypePatternMask.getPattern(TypePatternMask.LETRAS_NUMEROS_UNDERSCORE)
              },
              placeholder: commonMessage.INGRESE_CODIGO,
              tooltip: commonMessage.CODIGO_IDENTIFICA_REGISTRO
            },
          ]
        },
        rowThree: {
          row: [
            {
              key: "esquema",
              label: commonValues.ESQUEMA_LABEL,
              typeElement: { typeElement: TypeElement.TEXT },
              textCase: TypeCaseText.UPPER_CASE,
              placeholder: commonMessage.INGRESE_ESQUEMA,
            },
            {
              key: "objeto",
              label: commonValues.OBJETO_NEGOCIO_LABEL,
              typeElement: { typeElement: TypeElement.TEXT },
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
              placeholder: commonMessage.INGRESE_OBJETO
            },
            {
              key: "tipo",
              label: commonValues.TIPO_LABEL,
              typeElement: { typeElement: TypeElement.BUTTON_GROUP },
              options: this.tiposObjetos,
              default: this.tiposObjetos[0],
              dynamic: { visible: true }
            }
          ]
        },
        rowFour: {
          row: [
            {
              key: "estado",
              label: commonValues.ESTADO_LABEL,
              typeElement: { typeElement: TypeElement.BUTTON_GROUP },
              options: this.opcionesEstado,
              default: this.opcionesEstado[0],
              dynamic: { visible: true }
            },
            {
              key: "gestionable",
              label: commonValues.GESTIONABLE_LABEL,
              typeElement: { typeElement: TypeElement.BUTTON_GROUP },
              options: this.optionGestionable,
              default: this.optionGestionable[0],
              dynamic: { visible: true }
            }, { key: "id" },
          ]
        },
        documentacion: {
          label: commonValues.DOCUMENTACION_LABEL,
          typeElement: { typeElement: TypeElement.TEXTAREA }
        }
      }
    }
  }



}
