import { ObjetoNegocioRoutingModule } from './objeto-negocio-routing.module';
import { ObjetoNegocioComponent } from './objeto-negocio.component';
import { GeneralFormComponentModule } from '@app/shared/components/general-form/general-form-component.module';
import { FormsModule } from '@angular/forms';
import { NgxDatatableModule, DataTableHeaderCellComponent } from '@swimlane/ngx-datatable';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { BlockUIModule } from 'ng-block-ui';
import { ObjetoAtributoComponent } from './objeto-atributo/objeto-atributo.component';

@NgModule({
  declarations: [ObjetoNegocioComponent, ObjetoAtributoComponent],
  imports: [
    CommonModule,
    ObjetoNegocioRoutingModule,
    FormsModule,
    NgxDatatableModule,
    DataTableHeaderCellComponent,
    GeneralFormComponentModule,
    SharedModule,
    BlockUIModule.forRoot(),
    ReactiveFormsModule
  ],
  exports: [ObjetoNegocioComponent]
})
export class ObjetoNegocioModule { }
