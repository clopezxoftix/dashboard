import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjetoNegocioComponent } from './objeto-negocio.component';

describe('ObjetoNegocioComponent', () => {
  let component: ObjetoNegocioComponent;
  let fixture: ComponentFixture<ObjetoNegocioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObjetoNegocioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjetoNegocioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
