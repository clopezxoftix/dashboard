import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjetoAtributoComponent } from './objeto-atributo.component';

describe('ObjetoAtributoComponent', () => {
  let component: ObjetoAtributoComponent;
  let fixture: ComponentFixture<ObjetoAtributoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObjetoAtributoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjetoAtributoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
