
import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { commonValues } from '@commons/commonValues';
import { commonMessage } from '@commons/commonMessages';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NotificationService } from '@app/core/services/notification.service';
import { GeneralFormComponent } from '@app/shared/components/general-form/general-form.component';
import { TypeCaseText } from '@app/shared/components/general-form/types/TypeCaseText';
import { TypePatternMask } from '@app/shared/components/general-form/types/TypePatternMask';
import { TypeElement } from '@app/shared/components/general-form/types/TypeElement';
import { commonOptions } from '@commons/commonOptions';
import { RecursosService } from '@app/core/services/as/recursos.service';
import { FormBuilder, Validators } from '@angular/forms';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { common } from '@commons/common';
import { ObjetoAtributoService } from '@app/core/services/ge/objeto-atributo.service';
import { ParametrosObjetoAtributo } from './models/parametro-objeto-atributo';
import { Atributo } from './models/objeto-atributo';
import { ModulosService } from '../../../../core/services/ge/modulos.service';
import { ObjetoNegocioService } from '../../../../core/services/ge/objeto-negocio.service';
import { Objeto } from '../models/objeto-negocio';


@Component({
  selector: 'objeto-atributo',
  templateUrl: './objeto-atributo.component.html',
  styleUrls: ['./objeto-atributo.component.css']
})
export class ObjetoAtributoComponent implements OnInit {

  @Input() idObjeto: number;
  @Input('auxLista') variableMemoria: Array<any>;
  @Input() auxEditar: boolean;
  @Output() listaAtributos = new EventEmitter<Atributo>();
  @ViewChild(DatatableComponent, { 'static': false }) table: DatatableComponent;
  @ViewChild(GeneralFormComponent, { 'static': false }) gfAtributo: GeneralFormComponent;

  @BlockUI('formConsulta') blockUIConsulta: NgBlockUI;
  @BlockUI('formRegistro') blockUIRegistro: NgBlockUI;

  //Labels vista
  lblConsulta = commonValues.CONSULTA_LABEL;
  lblObjetoNegocio = commonValues.OBJETO_NEGOCIO_LABEL;
  lblAtributo = commonValues.ATRIBUTO_LABEL;
  lblCampos = commonValues.CAMPOS_LABEL;
  lblEstado = commonValues.ESTADO_LABEL;
  lblActivo = commonValues.ACTIVO_LABEL;
  lblInactivo = commonValues.INACTIVO_LABEL;
  lblTodos = commonValues.TODOS_LABEL;
  lblCriterioBusqueda = commonValues.CRITERIO_BUSQUEDA_LABEL;
  lblBuscar = commonValues.BUSCAR_LABEL;
  lblAgregar = commonValues.AGREGAR_LABEL;
  lblFiltroTabla = commonValues.FILTRO_TABLA_LABEL;
  lblAcciones = commonValues.ACCIONES_LABEL;
  lblVer = commonValues.VER_LABEL;
  lblEditar = commonValues.EDITAR_LABEL;
  lblEliminar = commonValues.ELIMINAR_LABEL;
  lblSi = commonValues.SI_LABEL;
  lblNo = commonValues.NO_LABEL;
  lblCodigo = commonValues.CODIGO_LABEL;
  lblNombre = commonValues.NOMBRE_LABEL;
  lblCancelar = commonValues.CANCELAR_LABEL;
  lblGuardar = commonValues.GUARDAR_LABEL;
  msgCriterioObligatorio = commonMessage.CRITERIO_OBLIGATORIO;
  msgDeseaEliminar = commonMessage.DESEA_ELIMINAR_REGISTRO;
  msgNoSePuedeEliminar = commonMessage.NO_SE_PUEDE_ELIMINAR;
  lblMover = commonMessage.MOVER_INDICE;

  /* Inicializacion de filtro de busqueda */
  campo = commonValues.CODIGO_VALUE;
  estado = commonValues.ACTIVO_VALUE;
  criterio = "";
  /*Variable para el control de apertura del formulario */
  searching = true;
  criterioRequerido = false;
  opcionesEstado = commonOptions.tiposEstado;
  tiposObjetos = commonOptions.tiposObjetos;
  optionGestionable = commonOptions.opcioneSINO;
  opcionTipoDato = commonOptions.tipoDatoObjeto;
  opcionesCardinalidad = commonOptions.tiposCardinalidad;
  opcionAplicaMetadato = commonOptions.opcioneSINO;

  accion = "";

  /*Variables de tabla*/
  /* Variables relacionadas a la tabla de consulta */
  controls: any = {
    pageSize: 10,
    filter: '',
    rowCount: 0,
    curPage: 0,
    offset: 0
  }

  temp = [];
  rows = [];
  loadingIndicator = false;
  reorderable = true;
  messages = {
    emptyMessage: commonMessage.NO_EXISTE_INFO,
    totalMessage: commonMessage.REGISTROS_TOTALES,
  };

  confirmDialog = false;
  sePuedeEliminar: boolean;
  formCriterio: any;
  formAtributo: any;
  suscritoAComponenteCodigo: any = false;
  disable: any;
  atributo: Atributo;
  atributoMemoria: Atributo;
  auxId = 0;
  idEliminar = 0;
  actualizarActivo: boolean;
  confirmDialogAux = false;
  confirmDialogActualizar = false;


  constructor(private formBuilder: FormBuilder,
    private notificationService: NotificationService,
    private recursoService: RecursosService,
    private _AtributoService: ObjetoAtributoService,
    private _objetoService: ObjetoNegocioService) { }

  ngOnInit() {
    this.formCriterio = this.formBuilder.group({
      criterio: ['', Validators.required]
    });
    this.formAtributo = this.createForm();
  }

  consultando() {
    return this.searching
  }

  editando() {
    return !this.searching
  }

  consultar(initPage: boolean) {
    if (this.formCriterio.invalid) {
      this.criterioRequerido = true;
      return;
    }
    this.criterioRequerido = false;
    if (this.criterio != "") {
      this.controls.filter = "";
      this.loadingIndicator = true;
      if (initPage) {
        this.controls.curPage = 0;
      }
      let params = new ParametrosObjetoAtributo;
      if (this.campo == commonValues.CODIGO_VALUE) {
        params.codigo = this.criterio;
      } else {
        params.nombre = this.criterio;
      }
      params.estados = this.estado;
      params.size = this.controls.pageSize;
      params.page = this.controls.curPage;
      params.listaEstados = this.estado.split(",");
      params.idObjeto = this.idObjeto;
      this._AtributoService.listar(params).subscribe(
        response => {
          this.rows = response.content;
          this.temp = this.rows;
          this.loadingIndicator = false;
          this.controls.rowCount = response.totalElements;
          this.controls.curPage = response.number;
        },
        error => {
          this.loadingIndicator = false;
          console.log(error);
          this.notificationService.errorMessage(commonMessage.NO_SE_REALIZO_CONSULTA);
        }
      );

    }

  }

  updateFilter(event) {
    let val = event.target.value.toLowerCase();
    let colsAmt = 2;
    let keys = ['codigo', 'nombre'];
    this.rows = this.temp.filter(function (item) {
      for (let i = 0; i < colsAmt; i++) {
        if (item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 || !val) {
          return true;
        }
      }
    });
    this.table.offset = 0;
  }

  updatePageSize(value) {
    this.controls.pageSize = parseInt(value, 10);
    this.table.limit = this.controls.pageSize;
    this.controls.curPage = (this.controls.pageSize > this.controls.rowCount) ?
      0 : this.controls.curPage;
    this.consultar(false);
  }

  setPage(event) {
    this.controls.curPage = event.offset;
    this.controls.pageSize = event.pageSize;
    this.consultar(false);
  }

  limpiarCriterio() {
    this.criterio = "";
  }

  verFormulario() {
    this.verbotones();
    this.suscritoAComponenteCodigo = false;
    this.accion = commonValues.REGISTRAR_LABEL;
    this.searching = false;
    this.nuevo();
    //this.auxEditar = true;

  }

  nuevo() {
    this.atributo = new Atributo();
  }

  get isInvalid() {
    if (this.gfAtributo) {
      this.onChanges();
    }
    return this.gfAtributo ? this.gfAtributo.form.invalid : true;
  }

  get desactivar() {
    return this.disable;
  }

  public state: any = {
    tabs: {
      tab: 'objeto'
    },
  }

  verbotones() {

  }

  ocultarbotones() {

  }

  //----- Metodo subir indice del objeto de negocio

  subir(id) {
    this._AtributoService.subirIndice(id).subscribe(data => {
      this.consultar(false);
    });

  }

  //------ Metodo para bajar el indice del objeto de negocio

  bajar(id) {
    this._AtributoService.bajarIndice(id).subscribe(data => {
      this.consultar(false);
    });

  }

  onChanges(): void {
    if (this.gfAtributo && !this.suscritoAComponenteCodigo) {
      this.suscritoAComponenteCodigo = true;
      this.gfAtributo.form.get('longitud').valueChanges.subscribe(val => {
        if (val > 0 || val == "") {

        }
        else {
          this.notificationService.errorMessage("El Valor no puede ser menor que 0");
          this.gfAtributo.form.get("longitud").setValue(0);
        }
      });

      this.gfAtributo.form.get('codigo').valueChanges.subscribe(codigo => {
        if (this.auxEditar == false) {
          this.onKeyupCodigo(codigo);
        }
        else {
          this.onKeyUpCodigoMemoria(codigo);
        }
      });

    }


  }

  onKeyupCodigo(valorCodigo: string) {
    if ((valorCodigo && (this.atributo.id && valorCodigo != this.atributo.codigo)) || !this.atributo.id && valorCodigo) {
      this._AtributoService.consultarCodigo(valorCodigo).subscribe(
        response => {
          if (response != null && response.id != this.atributo.id) {
            this.gfAtributo.form.controls['codigo'].setErrors({ 'codigoRecorded': true });
          } else {
            this.gfAtributo.form.controls['codigo'].setErrors({ 'codigoRecorded': false });
            const errores = this.gfAtributo.form.controls['codigo'].errors;
            delete errores.codigoRecorded;
            this.gfAtributo.form.controls['codigo'].setErrors(Object.keys(errores).length === 0 ? null : errores);
          }
        },
        error => {
          console.log(error);
          this.notificationService.errorMessage("Error al consultar código");
        }
      );
    }

  }

  // Metodo que valida si el codigo esta repetido en los registros en memoria
  onKeyUpCodigoMemoria(codigo) {
    this.variableMemoria.forEach(elemento => {
      if (elemento.codigo == codigo) {
        this.gfAtributo.form.controls['codigo'].setErrors({ 'codigoRecorded': true });
      }

    });
  }
  visualizar(id, deshabilitar) {
    this.verFormulario();
    this.buscar(id, deshabilitar, false);


  }

  onSubmit() {
    if (this.accion == this.lblEditar) {
      this.openModalActualizar();
      //this.editar();
      return
    }
    let objetoref = new Objeto();
    let objeto = new Objeto();
    this.atributo = this.gfAtributo.form.value;
    this.atributo.tipoDato = (this.atributo.tipoDato == "" ? 'ALFANUMERICO' : this.atributo.tipoDato);// PARA VALIDAR LA SELECCION DEL FORM
    //asigna las referencias de los objetos de negocio a las que apunta
    if (this.atributo.idObjetoRef) {
      objetoref.id = this.gfAtributo.form.get('idObjetoRef').value.id;
    } else {
      objetoref = null;
    }
    objeto.id = this.idObjeto;
    this.atributo.idObjeto = objeto;
    this.atributo.idObjetoRef = objetoref;
    //guarda atributos en momoria si es el caso
    if (!this.idObjeto) {
      this.auxId = this.auxId + 1;
      this.atributo.id = this.auxId;
      this.atributo.indice = this.auxId;
      this.listaAtributos.emit(this.atributo);
      this.consultarAtributosMemoria();
      //this.ocultarDialogoAgregar();
      this.verTabla();
      this.notificationService.successMessage(commonMessage.REGISTRO_SATISFACTORIO);
      return;
    }
    //
    this._AtributoService.guardar(this.atributo).subscribe(response => {
      this.searching = true;
      this.nuevo();
      this.notificationService.successMessage(commonMessage.REGISTRO_SATISFACTORIO)
      this.consultar(false);
      this.auxEditar = false;
    },
      error => {
        this.notificationService.errorMessage(commonMessage.REGISTRO_NO_EXITOSO)
      })
  }

  editar() {
    this.atributo = this.gfAtributo.form.value;
    if (this.auxEditar == false) {
      this._AtributoService.editar(this.atributo.id, this.atributo).subscribe(response => {
        this.searching = true;
        // this.nuevo();
        this.notificationService.successMessage(commonMessage.REGISTRO_SATISFACTORIO)
        this.consultar(false);
      },
        error => {
          this.notificationService.errorMessage(commonMessage.REGISTRO_NO_EXITOSO)
        })
    } else {
      let pos = 0;
      let posEditar = 0;
      let auxResponseMemoria: Atributo;

      this.variableMemoria.forEach(responseEditMemoria => {
        //this.atributos = responseEditMemoria;
        if (responseEditMemoria.id == this.auxId) {
          responseEditMemoria = this.gfAtributo.form.value;
          auxResponseMemoria = responseEditMemoria;
          posEditar = pos;
          this.notificationService.successMessage(commonMessage.REGISTRO_SATISFACTORIO);
        }
        pos++;
      });
      this.variableMemoria.splice(posEditar, 1, auxResponseMemoria);
      this.verTabla();
      this.consultarAtributosMemoria();
    }
  }

  eliminarEnMemoria() {
    let cont = 0;
    this.variableMemoria.forEach(responseMemoria => {
      if (this.idEliminar == responseMemoria.id) {
        this.variableMemoria.splice(cont, 1);
        this.notificationService.successMessage(commonMessage.REGISTRO_ELIMINADO)
        this.consultarAtributosMemoria();
      }
      cont++;
    },
      error => {
        this.notificationService.errorMessage(commonMessage.REGISTRO_NO_ELIMINADO)
      });
    this.idEliminar = 0;

  }

  consultarAtributosMemoria() {
    this.loadingIndicator = true;
    let atributosTemp: any = JSON.parse(JSON.stringify(this.variableMemoria));
    this.rows = atributosTemp;
    this.temp = this.rows;
    this.controls.rowCount = atributosTemp.length;
    this.controls.curPage = 0;
    this.loadingIndicator = false;

  }


  //ESTE METODO HAY QUE ADAPTAR A LO QUE HAY EN LOCAL
  buscar(id: number, deshabilitar: boolean, eliminar: boolean) {
    this.blockUIRegistro.start(commonMessage.CARGANDO + '...');
    if (this.variableMemoria.length == 0) {
      this.auxEditar = false;
      //this.nuevo();
      this._AtributoService.consultar(id).subscribe(
        response => {
          this.atributo = response;
          this.atributoMemoria = null;
          if (!eliminar) {
            this.gfAtributo.form.disable();
            this.confirmDialogAux = false;
            this.confirmDialog = false;
            this.disable = true;
            this.accion = (!deshabilitar ? commonValues.EDITAR_LABEL : commonValues.VER_LABEL);
            if (!deshabilitar) {
              this.gfAtributo.form.enable();
              this.disable = false;
            }
            this.gfAtributo.form.setValue(response);
          } else {
            this.confirmDialog = true; commonMessage.CARGANDO + this.consultarRelaciones(id);
          }
          this.blockUIRegistro.stop();
        },
        error => {
          this.blockUIRegistro.stop();
          this.notificationService.errorMessage(commonMessage.NO_SE_REALIZO_CONSULTA);
        }
      );
    } else {
      this.buscarMemoria(id, deshabilitar, false);
    }

  }

  buscarMemoria(id, deshabilitar, eliminar?) {
    this.blockUIRegistro.start(commonMessage.CARGANDO + '...');
    this.auxEditar = true;
    if (eliminar == null) {
      this.verFormulario();
    }
    this.promiseFormBuscar(id).then(response => {
      if (response) {
        this.atributo = response;
        if (eliminar == null) {
          this.gfAtributo.form.setValue(this.atributo);
          if (deshabilitar) {
            this.gfAtributo.form.disable();
            this.disable = true;
            this.actualizarActivo = false;
            this.confirmDialogAux = false;
            this.confirmDialog = true;
          }
        }
      } else {
        this.accion = (!deshabilitar ? commonValues.EDITAR_LABEL : commonValues.VER_LABEL);
        this.variableMemoria.forEach(responseMemoria => {
          this.atributo = responseMemoria;
          if (responseMemoria.id == id) {
            if (this.gfAtributo) {
              this.gfAtributo.form.setValue(this.atributo);
            }
            else {
              this.confirmDialogAux = true;
            }
            this.atributoMemoria = responseMemoria;
            this.confirmDialog = false;
          }
          if (deshabilitar) {
            this.gfAtributo.form.disable();
            this.disable = true;
            this.actualizarActivo = false;
            this.blockUIRegistro.stop();
          } else {
            this.auxId = id;
            this.disable = false;
            this.blockUIRegistro.stop();
          }
        });
      }
      this.blockUIRegistro.stop();
    });
  }

  //=================METODO VISUALIZAR CON PROMESA =========================
  promiseFormBuscar(id: any): Promise<any> {
    return new Promise((resolve) => {
      this._AtributoService.consultar(id).subscribe(
        data => {
          this.atributo = data;
          this.actualizarActivo = true;
          resolve(data)
        },
        error => {
          console.log("No se pudo consultar", error);
        }
      );
    });
  }


  eliminar(id) {
    this.blockUIConsulta.start(commonMessage.CARGANDO + '...');
    this._AtributoService.eliminar(id).subscribe(
      response => {
        this.consultar(false);
        this.notificationService.successMessage(commonMessage.REGISTRO_ELIMINADO);
        this.atributo = null;
        this.atributo = new Atributo();
        this.blockUIConsulta.stop();
      },
      error => {
        this.notificationService.errorMessage(commonMessage.REGISTRO_NO_ELIMINADO);
        this.blockUIConsulta.stop();
      }
    );
  }

  consultarRelaciones(id) {
    let arrayTemp = [];
    this.recursoService.listar(commonOptions.consultaEstadoEstandar).subscribe(
      response => {
        for (let item of response.content) {

          if (item.modulo.id == id) {
            arrayTemp.push(item);
          }
        }
        this.sePuedeEliminar = arrayTemp.length > 0;
        this.confirmDialog = true;

      },
      error => {
        this.confirmDialog = false;
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );
  }

  //confimr dialog
  openModal(id) {
    if (id != null) {
      if (this.auxEditar == false) {
        this.buscar(id, false, true);
      } else {
        this.idEliminar = id;
        this.buscarMemoria(id, false, false);
      }
    }
  }

  confirm(): void {
    if (this.auxEditar == false) {
      this.eliminar(this.atributo.id);
      this.confirmDialog = false;
      this.confirmDialogAux = false;
    } else {
      this.nuevo();
      this.eliminarEnMemoria();
      this.confirmDialog = false;
      this.confirmDialogAux = false;

    }
  }

  decline(): void {
    this.confirmDialog = false;
    this.confirmDialogAux = false;
    this.suscritoAComponenteCodigo=false;
    this.nuevo();
  }

  openModalActualizar() {
    this.confirmDialogActualizar = true;
  }

  confirmActualizar(){
    this.editar();
    this.confirmDialogActualizar = false;
  }

  declineActualizar(){
    this.confirmDialogActualizar = false;
  }

  verTabla() {
    this.criterioRequerido = false;
    this.searching = true;
    this.disable = false;
    this.suscritoAComponenteCodigo=false;
  }

  createForm(): any {
    return {
      header: {
        label: commonValues.DATOS_BASICOS_LABEL
      },
      fields: {
        id: {},
        idObjeto: {},
        indice: {},
        rowTwo: {
          row: [

            {
              key: "codigo",
              label: commonValues.CODIGO_LABEL,
              typeElement: { typeElement: TypeElement.FORMAT },
              textCase: TypeCaseText.UPPER_CASE,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO,
                expression: TypePatternMask.getPattern(TypePatternMask.LETRAS_NUMEROS_UNDERSCORE)
              },
              placeholder: commonMessage.INGRESE_CODIGO,
              tooltip: commonMessage.CODIGO_IDENTIFICA_REGISTRO
            },
            {
              key: "nombre",
              label: commonValues.NOMBRE_LABEL,
              typeElement: { typeElement: TypeElement.TEXT },
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
              placeholder: commonMessage.INGRESE_NOMBRE
            },
            {
              key: "idObjetoRef",
              label: commonValues.OBJETO_NEGOCIO_REF_LABEL,
              typeElement: { typeElement: TypeElement.SELECCIONAREGISTRO },
              service: this._objetoService,
              inputsConsulta: common.estadoNombreOpciones,
              camposHeadersTabla: ["Codigo", "Nombre", "Estado"],
              camposTabla: ["codigo", "nombre", "estado"],
              parametrosDefecto: common.consultaEstadoActivo,
              camposInput: ["codigo", "nombre"],
              listarEspecifico: false,
            },

          ]
        },
        rowThree: {
          row: [
            {
              key: "longitud",
              label: commonValues.LONGITUD_LABEL,
              typeElement: { typeElement: TypeElement.NUMBER },
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {
              key: "decimales",
              label: commonValues.DECIMALES_LABEL,
              typeElement: { typeElement: TypeElement.NUMBER },
              default: 0,
            },
            {
              key: "etiqueta",
              label: commonValues.ETIQUETA_LABEL,
              typeElement: { typeElement: TypeElement.TEXT },
              textCase: TypeCaseText.UPPER_CASE,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
              placeholder: commonMessage.INGRESE_ETIQUETA
            },

          ]
        },
        rowFour: {
          row: [
            {
              key: "estado",
              label: commonValues.ESTADO_LABEL,
              typeElement: { typeElement: TypeElement.BUTTON_GROUP },
              options: this.opcionesEstado,
              default: this.opcionesEstado[0],
              dynamic: { visible: true }
            },
            {
              key: "aplicaMetadato",
              label: commonValues.APLICA_METADATO_LABEL,
              typeElement: { typeElement: TypeElement.BUTTON_GROUP },
              options: this.opcionAplicaMetadato,
              default: this.opcionAplicaMetadato[0],
              dynamic: { visible: true }
            },
            {
              key: "cardinalidad",
              label: commonValues.CARDINALIDAD_LABEL,
              typeElement: { typeElement: TypeElement.BUTTON_GROUP },
              options: this.opcionesCardinalidad,
              default: this.opcionesCardinalidad[0],
              dynamic: { visible: true }
            },
          ]
        },
        rowFive: {
          row: [
            {
              key: "tipoDato",
              label: commonValues.TIPO_LABEL,
              typeElement: { typeElement: TypeElement.SELECT },
              options: this.opcionTipoDato,
              default: this.opcionTipoDato[0],
              dynamic: { visible: true }
            },
          ]
        },
        documentacion: {
          label: commonValues.DOCUMENTACION_LABEL,
          typeElement: { typeElement: TypeElement.TEXTAREA }
        },
      }
    }
  }

}
