import { Objeto } from '../../models/objeto-negocio';
 
export class Atributo{
    constructor() {
 
    }
    id:number;
    idObjeto:Objeto;
    codigo: string;
    nombre: string;
    estado: string;
    etiqueta: string;
    indice: number;
    tipoDato: string;
    idObjetoRef: Objeto;
    longitud: number;
    decimales: number;
    cardinalidad: string;
    aplicaMetadato: string;
    documentacion: string;
} 