export class ParametrosObjetoAtributo {
    constructor() {
    }
    codigo: string;
    nombre: string;
    listaEstados: string[];
    page: number;
    size: number;
    estados: string;
    idObjeto: number;
}