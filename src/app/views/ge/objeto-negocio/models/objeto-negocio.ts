import { Modulo } from '../../modulos/models/modulo';


export class Objeto {
    constructor() {
 
    }
    id:number;
    idModulo:Modulo;
    codigo: string;
    nombre: string;
    estado: string;
    tipo: string;
    esquema: string;
    objeto: string;
    gestionable: string;
    documentacion: string;
}