import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { commonValues } from '@commons/commonValues';
import { commonMessage } from '@commons/commonMessages';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { GeneralFormComponent } from '@app/shared/components/general-form/general-form.component';
import { ListaElementos } from '../models/lista-elementos'
import { NotificationService } from '@app/core/services';
import { ListaElementosService } from '@app/core/services/ge/lista-elementos.service';
import { TypeElement } from '@app/shared/components/general-form/types/TypeElement';
import { RecursosService } from '@app/core/services/as/recursos.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ListaValores } from '../models/lista-valores';
import { commonOptions } from '@commons/commonOptions';

@Component({
  selector: 'lista-elementos',
  templateUrl: './lista-elementos.component.html',
  styleUrls: ['./lista-elementos.component.css']
})
export class ListaElementosComponent implements OnInit {

  @Input('auxListaElementos') auxElementosMemoria: Array<any>;
  @Input() varEditarMemoria: boolean;
  @Input() idListavalorElementos: number;
  @Output() adicionarALista = new EventEmitter<ListaElementos>();

  @ViewChild(DatatableComponent, { 'static': false }) table: DatatableComponent;
  @ViewChild(GeneralFormComponent, { 'static': false }) formularioPrincipalElementos: GeneralFormComponent;

  @BlockUI('formConsulta') blockUIConsulta: NgBlockUI;
  @BlockUI('formRegistro') blockUIRegistro: NgBlockUI;

  formularioVisibleElemento = false;
  formularioElementos: any;
  disable: any;
  confirmDialogMod: boolean = false;
  listaElementos: ListaElementos;
  suscrito = false;
  auxContadorMemoria = 0;
  actualizarElemento: boolean;
  idEdit = 0;
  accion = "";
  confirmDialog = false;
  sePuedeEliminar: boolean;
  auxVarEditar: boolean = false;
  invalid: boolean = true;


  //Labels vista
  lblConsulta = commonValues.CONSULTA_LABEL;
  lblElementos = commonValues.ELEMENTOS;
  lblGuardar = commonValues.GUARDAR_LABEL;
  lblAgregar = commonValues.AGREGAR_LABEL;
  lblCancelar = commonValues.CANCELAR_LABEL;
  lblEditar = commonValues.EDITAR_LABEL;
  lblModificar = commonValues.MODIFICAR_LABEL;
  lblAcciones = commonValues.ACCIONES_LABEL;
  lblCampo = commonValues.CAMPO_LABEL;
  lblEliminar = commonValues.ELIMINAR_LABEL;
  lblSi = commonValues.SI_LABEL;
  lblNo = commonValues.NO_LABEL;
  lblVer = commonValues.VER_LABEL;
  lblFiltroTabla = commonValues.FILTRO_TABLA_LABEL;
  lblListaValores = commonValues.LISTA_VALORES_LABEL;
  lblValor = commonValues.VALOR;
  lblEtiqueta = commonValues.ETIQUETA;
  lblOrden = commonValues.ORDEN;
  msgDeseaModificar = commonMessage.DESEA_MODFICAR_REGISTRO;
  msgValoresPositivos = commonMessage.VALORES_POSITIVOS;
  msgDeseaEliminar = commonMessage.DESEA_ELIMINAR_REGISTRO;

  /* Variables relacionadas a la tabla de consulta */
  controls: any = {
    pageSize: 10,
    filter: '',
    rowCount: 0, //total elementos
    curPage: 0, //numero pagina
    selectedCount: 0,
    offset: 0
  }
  cols = [{ name: 'codigo' }, { name: 'nombre' }];
  temp = [];
  rows = [];
  loadingIndicator = false;
  reorderable = true;
  messages = {
    emptyMessage: commonMessage.NO_EXISTE_INFO,
    totalMessage: commonMessage.REGISTROS_TOTALES,
  };

  constructor(private _listaElementoService: ListaElementosService,
    private notificationService: NotificationService,
    private recursoService: RecursosService) { }

  ngOnInit() {
    this.formularioElementos = this.createFormElemento();
    if (this.idListavalorElementos) {
      this.ListaElementosByListaValores();
    }
    this.consultarElementosMemoria();
  }

  /**
* Description: Metodo para el filtro de la tabla de consulta
* responde al evento keyup del elemento ngx-dataTable
*
* @param {*} event Valor del input del filtro del dataTable
* @memberof ListaValoresComponent
*/
  updateFilter(event) {
    let val = event.target.value.toLowerCase();
    let colsAmt = 2;
    let keys = ['codigo', 'nombre'];
    this.rows = this.temp.filter(function (item) {
      for (let i = 0; i < colsAmt; i++) {
        if (item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 || !val) {
          return true;
        }
      }
    });
    this.table.offset = 0;
  }

  /**
   * Description: Metodo que ejecuta la paginacion del ngx-dataTable responde
   * al evento ngModelChange del componente select
   *
   * @param {*} value Valor del select del componente paginacion del dataTable
   * @memberof ListaValoresComponent
   */
  updatePageSize(value) {
    this.controls.pageSize = parseInt(value, 10);
    this.table.limit = this.controls.pageSize;
    this.controls.curPage = (this.controls.pageSize > this.controls.rowCount) ?
      0 : this.controls.curPage;
    //this.consultar(false);
  }

  setPage(event) {
    this.controls.curPage = event.offset;
    this.controls.pageSize = event.pageSize;
    //this.consultar(false);
  }

  createFormElemento() {
    return {
      header: {
        label: commonValues.DATOS_BASICOS_LABEL
      },
      fields: {
        id: {},
        rowOne: {
          row: [
            {
              key: "orden",
              label: commonValues.ORDEN,
              placeholder: commonMessage.INRGESE_ORDEN,
              typeElement: TypeElement.NUMBER,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO,
              }
            },
            {
              key: "valor",
              label: commonValues.VALOR,
              placeholder: commonMessage.INGRESE_VALOR,
              typeElement: TypeElement.TEXT,

              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO,
              }
            },
            {
              key: "etiqueta",
              label: commonValues.ETIQUETA,
              placeholder: commonMessage.INGRESE_ETIQUETA,
              typeElement: TypeElement.TEXT,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO,
              }
            }
          ]
        },
        rowTwo: {
          row: [
            {
              key: "idListaValores",
            }
          ]
        }
      }
    }
  }

  verFormulario() {
    this.accion = commonValues.REGISTRAR_LABEL;
    this.formularioVisibleElemento = true;
    this.auxVarEditar = true;
    this.nuevo();

  }

  nuevo() {
    this.listaElementos = new ListaElementos();
  }

  ocultarFormulario() {
    this.formularioVisibleElemento = false;
    this.listaElementos = null;
    this.suscrito = false;

  }

  visualizar(id, deshabilitar) {
    this.verFormulario();
    this.buscar(id, deshabilitar, false);
  }

  buscar(id: number, deshabilitar: boolean, eliminar: boolean) {
    this.blockUIRegistro.start(commonMessage.CARGANDO + '...');
    this.auxVarEditar = true;
    if (this.auxElementosMemoria.length == 0) {
      this._listaElementoService.consultarId(id).subscribe(
        response => {
          this.listaElementos = response;
          if (!eliminar) {
            this.formularioPrincipalElementos.form.disable();
            this.disable = true;
            this.accion = (!deshabilitar ? commonValues.EDITAR_LABEL : commonValues.VER_LABEL);
            if (!deshabilitar) {
              this.formularioPrincipalElementos.form.enable();
              this.disable = false;
            }
            this.formularioPrincipalElementos.form.setValue(response);
          } else {
            this.confirmDialog = true; commonMessage.CARGANDO +
              this.consultarRelaciones(id);
          }
          this.blockUIRegistro.stop();
        },
        error => {
          this.blockUIRegistro.stop();
          this.notificationService.errorMessage(commonMessage.NO_SE_REALIZO_CONSULTA);
        }
      );
    } else {
      this.buscarMemoria(id, deshabilitar, false);
    }

  }

  buscarMemoria(id, deshabilitar, eliminar?) {
    this.blockUIRegistro.start(commonMessage.CARGANDO + '...');
    if (eliminar == null) {
      this.verFormulario();
    }
    this.promiseFormBuscarMemoria(id).then(response => {
      if (response) {
        this.listaElementos = response;
        if (eliminar == null) {
          this.formularioPrincipalElementos.form.setValue(this.listaElementos);
          if (deshabilitar) {
            this.formularioPrincipalElementos.form.disable();
            this.disable = true;
            this.actualizarElemento = false;
          }
        }
      } else {
        this.auxElementosMemoria.forEach(responseMemoria => {
          this.listaElementos = responseMemoria;
          if (responseMemoria.id == id) {
            this.formularioPrincipalElementos.form.setValue(this.listaElementos);
            this.blockUIRegistro.stop();
          }
          if (deshabilitar) {
            this.formularioPrincipalElementos.form.disable();
            this.disable = true;
            this.actualizarElemento = false;
            this.blockUIRegistro.stop();
          } else {
            this.idEdit = id;
            this.disable = false;
            this.blockUIRegistro.stop();
          }
        });
      }
    });
  }

  promiseFormBuscarMemoria(id: any): Promise<any> {
    return new Promise((resolve) => {
      this._listaElementoService.consultarId(id).subscribe(
        data => {
          this.listaElementos = data;
          this.actualizarElemento = true;
          resolve(data)
        },
        error => {
          this.notificationService.errorMessage(commonMessage.NO_SE_REALIZO_CONSULTA);
        }
      );
    });
  }


  onSubmit() {
    this.listaElementos = this.formularioPrincipalElementos.form.value;
    let listaValoresElementos = new ListaValores();
    listaValoresElementos.id = this.idListavalorElementos;
    if (!this.idListavalorElementos) {
      this.auxContadorMemoria = this.auxContadorMemoria + 1;
      this.listaElementos.id = this.auxContadorMemoria;
      this.adicionarALista.emit(this.listaElementos);
      this.ocultarFormulario();
      this.consultarElementosMemoria();
      return;
    }
    this.listaElementos.idListaValores = listaValoresElementos;
    this._listaElementoService.guardar(this.listaElementos).subscribe(data => {
      this.notificationService.successMessage(commonMessage.REGISTRO_SATISFACTORIO);
      this.ocultarFormulario();
      this.ListaElementosByListaValores();
    }, error => {
      this.notificationService.errorMessage(commonMessage.REGISTRO_NO_EXITOSO);
      this.ocultarFormulario();
    });
  }

  get desactivar() {
    return this.disable;
  }

  get isInvalid() {
    if (this.formularioPrincipalElementos) {
      this.onChange();
      return this.formularioPrincipalElementos.form.invalid;
    }
    if (this.varEditarMemoria == false) {
      this.invalid = true;
    } else {
      this.invalid = false;
    }
    return this.invalid;
  }



  onChange() {
    if (!this.suscrito) {
      this.suscrito = true
      this.formularioPrincipalElementos.form.get('orden').valueChanges.subscribe(ordenData => {
        if (ordenData < 0) {
          this.notificationService.errorMessage(this.msgValoresPositivos);
          this.formularioPrincipalElementos.form.get("orden").setValue("");
        }
      });
    }
  }

  openModalMod(id) {
    this.confirmDialogMod = true
  }
  confirmMod(): void {
    this.editar();
    this.confirmDialogMod = false;
  }

  declineMod(): void {
    this.confirmDialogMod = false;
  }

  editar() {
    this.listaElementos = this.formularioPrincipalElementos.form.value
    if (this.auxElementosMemoria.length == 0) {
      let listaValoresAtributos = new ListaValores();
      listaValoresAtributos.id = this.idListavalorElementos;
      this.listaElementos.idListaValores = listaValoresAtributos;
      this._listaElementoService.editar(this.listaElementos.id, this.listaElementos).subscribe(data => {
        this.listaElementos = data;
        this.ocultarFormulario();
        this.notificationService.successMessage(commonMessage.REGISTRO_SATISFACTORIO);
        this.consultar();
        this.ListaElementosByListaValores();
      }, error => {
        this.notificationService.errorMessage(commonMessage.REGISTRO_NO_EXITOSO);
      });

    } else {
      let pos = 0;
      let posEditar = 0;
      let auxResponseMemoria: ListaElementos;
      this.auxElementosMemoria.forEach(responseEditMemoria => {
        if (responseEditMemoria.id == this.idEdit) {
          responseEditMemoria = this.formularioPrincipalElementos.form.value;
          auxResponseMemoria = responseEditMemoria;
          posEditar = pos;
        }
        pos++;
      });
      this.auxElementosMemoria.splice(posEditar, 1, auxResponseMemoria);
      this.ocultarFormulario();
      this.consultarElementosMemoria();
    }
  }

  consultar() {
    this.loadingIndicator = true;
    let params = {};
    params['size'] = this.controls.pageSize;
    params['page'] = this.controls.curPage;
    this._listaElementoService.listar(params).subscribe(response => {
      this.rows = response['content'];
      this.temp = this.rows;
      this.controls.rowCount = response.totalElements;
      this.controls.curPage = response.number;
      this.loadingIndicator = false;
    }),
      error => {
        this.notificationService.errorMessage(commonMessage.NO_SE_REALIZO_CONSULTA);
      }
  }

  ListaElementosByListaValores() {
    this.blockUIRegistro.start(commonMessage.CARGANDO + '...');
    this._listaElementoService.consultar(this.idListavalorElementos).subscribe(data => {
      this.rows = data['content'];
      this.temp = this.rows;
      this.controls.rowCount = data.totalElements;
      this.controls.curPage = data.number;
      this.loadingIndicator = false;
      this.blockUIRegistro.stop();
    },
      error => {
        this.blockUIRegistro.stop();
        this.notificationService.errorMessage(commonMessage.NO_SE_REALIZO_CONSULTA);
      });
  }

  consultarElementosMemoria() {
    this.blockUIRegistro.start(commonMessage.CARGANDO + '...');
    this.loadingIndicator = true;
    this.rows = this.auxElementosMemoria;
    this.temp = this.rows;
    this.controls.rowCount = this.auxElementosMemoria.length;
    this.controls.curPage = 0;
    this.loadingIndicator = false;
    this.blockUIRegistro.stop();
  }

  openModal(id) {
    if(this.auxElementosMemoria.length == 0){
      if (id != null) {
        this.buscar(id, false, true);
      }
    }else{
      if (id != null) {
        let position = -1;
        let positionToEliminate = 0;
        this.auxElementosMemoria.forEach(element => {
          position++;
          if (id == element.id) {
            positionToEliminate = position;
          }
        });
        this.auxElementosMemoria.splice(positionToEliminate, 1);
        this.consultarElementosMemoria();
      }
    }
    
  }

  confirm(): void {
    this.eliminar(this.listaElementos.id);
    this.nuevo();
    this.confirmDialog = false;
  }

  decline(): void {
    this.confirmDialog = false;
    this.nuevo();
  }

  eliminar(id) {
    this.blockUIConsulta.start(commonMessage.CARGANDO + '...');
      this._listaElementoService.eliminar(id).subscribe(
        response => {
          this.consultar();
          this.notificationService.successMessage(commonMessage.REGISTRO_ELIMINADO);
          this.ListaElementosByListaValores()
          this.blockUIConsulta.stop();
        },
        error => {
          this.notificationService.errorMessage(commonMessage.REGISTRO_NO_ELIMINADO);
          this.blockUIConsulta.stop();
        }
      );
  }

  consultarRelaciones(id) {
    let arrayTemp = [];
    this.recursoService.listar(commonOptions.consultaEstadoEstandar).subscribe(
      response => {
        for (let item of response.content) {

          if (item.modulo.id == id) {
            arrayTemp.push(item);
          }
        }
        this.sePuedeEliminar = arrayTemp.length > 0;
        this.confirmDialog = true;

      },
      error => {
        this.confirmDialog = false;
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );
  }

}
