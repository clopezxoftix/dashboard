import * as moment from 'moment';
export class Validaciones {
    constructor() { }


      convertDate(str) {
        var date = new Date(str),
          mnth = ("0" + (date.getMonth() + 1)).slice(-2),
          day = ("0" + date.getDate()).slice(-2);
          var  hours  = ("0" + date.getHours()).slice(-2);
          var  minutes = ("0" + date.getMinutes()).slice(-2);
          var seg = ("0" + date.getSeconds()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-") + " " + [hours, minutes,seg].join(":");
      }

    convertirStringFecha(fechaString: string): Date {
        return moment(fechaString, 'YYYY-MM-DD HH:mm:ss').toDate();
    }

}