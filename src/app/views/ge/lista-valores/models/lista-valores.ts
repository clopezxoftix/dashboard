import {FuenteDato} from "../../fuente-dato/models/FuenteDato"
export class ListaValores {
    constructor() { }
    
    id:number;
    codigo:string;
    nombre:string;
    estado:string;
    tipo:string;
    seleccionMultiple:string;
    predecesor:ListaValores;
    fuenteDato:FuenteDato;
    tipoDatoRango:any;
    rangoDesde:any;
    rangoHasta:any;
    documentacion:string;
}