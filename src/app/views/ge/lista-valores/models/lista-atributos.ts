import { ListaValores } from './lista-valores';
import { FuenteDatoAtributo } from '../../fuente-dato/models/FuenteDatoAtributo';
export class ListaAtributos {

    constructor() {

    }
    id: number;
    idListaValores: ListaValores;
    idFuenteDatoAtributo: FuenteDatoAtributo;
    indice: number;
    retorno: string;
    visible: string;
    filtro: string;

}