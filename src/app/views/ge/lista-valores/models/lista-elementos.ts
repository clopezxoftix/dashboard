import { ListaValores } from './lista-valores';
export class ListaElementos {
    constructor() {
    }
    id: number;
    idListaValores: ListaValores;
    valor: string;
    etiqueta: string;
    orden: number;
}