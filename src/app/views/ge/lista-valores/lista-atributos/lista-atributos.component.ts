import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { GeneralFormComponent } from '@app/shared/components/general-form/general-form.component';
import { commonMessage } from '@commons/commonMessages';
import { commonValues } from '@commons/commonValues';
import { ListaAtributos } from '../models/lista-atributos'
import { NotificationService } from '@app/core/services';
import { ListaAtributosService } from '@app/core/services/ge/lista-atributos.service';
import { TypeElement } from '@app/shared/components/general-form/types/TypeElement';
import { commonOptions } from '@commons/commonOptions';
import { ListaValores } from '../models/lista-valores';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { RecursosService } from '@app/core/services/as/recursos.service';
import { FuenteDatoAtributoService } from '../../../../core/services/ge/fuente-dato-atributo.service';

@Component({
  selector: 'lista-atributos',
  templateUrl: './lista-atributos.component.html',
  styleUrls: ['./lista-atributos.component.css']
})
export class ListaAtributosComponent implements OnInit {

  @Input('auxListaAtributos') auxAtributosMemoria: Array<any>;
  @Input() varEditarMemoria: boolean;
  @Input() idListavalorAtributos: number;
  @Output() adicionarALista = new EventEmitter<ListaAtributos>();

  @ViewChild(DatatableComponent, { 'static': false }) table: DatatableComponent;
  @ViewChild(GeneralFormComponent, { 'static': false }) formularioPrincipalAtributos: GeneralFormComponent;

  @BlockUI('formConsulta') blockUIConsulta: NgBlockUI;
  @BlockUI('formRegistro') blockUIRegistro: NgBlockUI;

  formularioVisibleAtributo = false;
  formularioAtributos: any;
  listaAtributos: ListaAtributos;
  confirmDialogMod: boolean = false;
  actualizarAtributo: boolean;
  opcionesGenerales = commonOptions;
  disable: any;
  suscrito = false;
  searching = true;
  auxContadorMemoria = 0;
  idEdit = 0;
  accion = "";
  auxVarEditar: boolean = false;
  invalid: boolean = true;
  confirmDialog = false;
  sePuedeEliminar: boolean;

  //Labels vista
  lblConsulta = commonValues.CONSULTA_LABEL;
  lblAtributos = commonValues.ATRIBUTOS;
  lblGuardar = commonValues.GUARDAR_LABEL;
  lblAgregar = commonValues.AGREGAR_LABEL;
  lblCancelar = commonValues.CANCELAR_LABEL;
  lblEditar = commonValues.EDITAR_LABEL;
  lblModificar = commonValues.MODIFICAR_LABEL;
  lblEliminar = commonValues.ELIMINAR_LABEL;
  lblCampo = commonValues.CAMPO_LABEL;
  lblIndice = commonValues.INDICE;
  lblRetorno = commonValues.RETORNO;
  lblVisible = commonValues.VISIBLE;
  lblFiltro = commonValues.FILTRO;
  lblAcciones = commonValues.ACCIONES_LABEL;
  lblReUbicar = commonValues.RE_UBICAR;
  lblSubir = commonValues.SUBIR;
  lblBajar = commonValues.BAJAR;
  lblSi = commonValues.SI_LABEL;
  lblNo = commonValues.NO_LABEL;
  lblVer = commonValues.VER_LABEL;
  lblFiltroTabla = commonValues.FILTRO_TABLA_LABEL;
  msgDeseaModificar = commonMessage.DESEA_MODFICAR_REGISTRO;
  msgValoresPositivos = commonMessage.VALORES_POSITIVOS;
  msgDeseaEliminar = commonMessage.DESEA_ELIMINAR_REGISTRO;

  /* Variables relacionadas a la tabla de consulta */
  controls: any = {
    pageSize: 10,
    filter: '',
    rowCount: 0, //total elementos
    curPage: 0, //numero pagina
    selectedCount: 0,
    offset: 0
  }
  cols = [{ name: 'codigo' }, { name: 'nombre' }];
  temp = [];
  rows = [];
  loadingIndicator = false;
  reorderable = true;
  messages = {
    emptyMessage: commonMessage.NO_EXISTE_INFO,
    totalMessage: commonMessage.REGISTROS_TOTALES,
  };

  constructor(private _listaAtributoService: ListaAtributosService,
    private notificationService: NotificationService,
    private _fuenteDatoAtributoService: FuenteDatoAtributoService,
    private recursoService: RecursosService) { }

  ngOnInit() {
    this.formularioAtributos = this.createFormAtributo();
    if (this.idListavalorAtributos) {
      this.ListaAtributosByListaValores();
    }
    this.consultarAtributosMemoria();
  }

  /**
* Description: Metodo para el filtro de la tabla de consulta
* responde al evento keyup del elemento ngx-dataTable
*
* @param {*} event Valor del input del filtro del dataTable
* @memberof ListaValoresComponent
*/
  updateFilter(event) {
    let val = event.target.value.toLowerCase();
    let colsAmt = 2;
    let keys = ['codigo', 'nombre'];
    this.rows = this.temp.filter(function (item) {
      for (let i = 0; i < colsAmt; i++) {
        if (item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 || !val) {
          return true;
        }
      }
    });
    this.table.offset = 0;
  }

  /**
   * Description: Metodo que ejecuta la paginacion del ngx-dataTable responde
   * al evento ngModelChange del componente select
   *
   * @param {*} value Valor del select del componente paginacion del dataTable
   * @memberof ListaValoresComponent
   */
  updatePageSize(value) {
    this.controls.pageSize = parseInt(value, 10);
    this.table.limit = this.controls.pageSize;
    this.controls.curPage = (this.controls.pageSize > this.controls.rowCount) ?
      0 : this.controls.curPage;
    //this.consultar(false);
  }

  setPage(event) {
    this.controls.curPage = event.offset;
    this.controls.pageSize = event.pageSize;
    //this.consultar(false);
  }

  createFormAtributo() {
    return {
      header: {
        label: commonValues.DATOS_BASICOS_LABEL
      },
      fields: {
        id: {},
        idListaValores: {},
        idFuenteDatoAtributo: {
          key: "idFuenteDatoAtributo",
          label: commonValues.FUENTE_DATO_ATRIBUTO,
          typeElement: { typeElement: "seleccionar_registro" },
          service: this._fuenteDatoAtributoService,
          inputsConsulta: ['codigo', 'nombre'],
          camposHeadersTabla: ["Código", "Nombre", "Estado"],
          camposTabla: ["codigo", "nombre", "estado"],
          parametrosDefecto: { estado: "ACTIVO" },
          camposInput: ["codigo", "nombre"],
          listarEspecifico: false,
          validation: {
            required: commonMessage.CAMPO_OBLIGATORIO,
          }
        },
        rowOne: {
          row: [
            {
              key: "indice",
              label: commonValues.INDICE,
              placeholder: commonMessage.INGRESE_INDICE,
              typeElement: TypeElement.NUMBER,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO,
              }
            },
            {
              key: "retorno",
              label: commonValues.RETORNO,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesGenerales.opcionesSiNo,
              default: this.opcionesGenerales.opcionesSiNo[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO,
              }
            },
            {
              key: "visible",
              label: commonValues.VISIBLE,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesGenerales.opcionesSiNo,
              default: this.opcionesGenerales.opcionesSiNo[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO,
              }

            }
          ]
        },
        rowTwo: {
          row: [
            {
              key: "filtro",
              label: commonValues.FILTRO,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesGenerales.opcionesSiNo,
              default: this.opcionesGenerales.opcionesSiNo[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO,
              }
            }           
          ]
        },
      }
    }

  }

  consultar() {
    this.loadingIndicator = true;
    let params = {};
    params['size'] = this.controls.pageSize;
    params['page'] = this.controls.curPage;
    this._listaAtributoService.listar(params).subscribe(response => {
      this.rows = response['content'];
      this.temp = this.rows;
      this.controls.rowCount = response.totalElements;
      this.controls.curPage = response.number;
      this.loadingIndicator = false;
    }),
      error => {
        this.notificationService.errorMessage(commonMessage.NO_SE_REALIZO_CONSULTA);
      }
  }

  verFormulario() {
    this.accion = commonValues.REGISTRAR_LABEL;
    this.formularioVisibleAtributo = true;
    this.auxVarEditar = true;
    this.nuevo();

  }

  ocultarFormulario() {
    this.formularioVisibleAtributo = false;
    this.listaAtributos = null;
    this.suscrito = false;

  }

  nuevo() {
    this.listaAtributos = new ListaAtributos();
  }

  onSubmit() {
    this.listaAtributos = this.formularioPrincipalAtributos.form.value;
    this.listaAtributos.retorno = (this.listaAtributos.retorno == "" ? 'SI' : this.listaAtributos.retorno);
    this.listaAtributos.visible = (this.listaAtributos.visible == "" ? 'SI' : this.listaAtributos.visible);
    this.listaAtributos.filtro = (this.listaAtributos.filtro == "" ? 'SI' : this.listaAtributos.filtro);

    let listaValoresAtributos = new ListaValores();
    listaValoresAtributos.id = this.idListavalorAtributos;
    if (!this.idListavalorAtributos) {
      this.auxContadorMemoria = this.auxContadorMemoria + 1;
      this.listaAtributos.id = this.auxContadorMemoria;
      this.adicionarALista.emit(this.listaAtributos);
      this.ocultarFormulario();
      this.consultarAtributosMemoria();
      return;
    }
    this.listaAtributos.idListaValores = listaValoresAtributos;
    this._listaAtributoService.guardar(this.listaAtributos).subscribe(data => {
      this.notificationService.successMessage(commonMessage.REGISTRO_SATISFACTORIO);
      this.ocultarFormulario();
      this.ListaAtributosByListaValores();
    }, error => {
      this.notificationService.errorMessage(commonMessage.REGISTRO_NO_EXITOSO);
      this.ocultarFormulario();
    });
  }

  visualizar(id, deshabilitar) {
    this.verFormulario();
    this.buscar(id, deshabilitar, false);
  }

  buscar(id: number, deshabilitar: boolean, eliminar: boolean) {
    this.blockUIRegistro.start(commonMessage.CARGANDO + '...');
    this.auxVarEditar = true;
    if (this.auxAtributosMemoria.length == 0) {
      this._listaAtributoService.consultarId(id).subscribe(
        response => {
          this.listaAtributos = response;
          if (!eliminar) {
            this.formularioPrincipalAtributos.form.disable();
            this.disable = true;
            this.accion = (!deshabilitar ? commonValues.EDITAR_LABEL : commonValues.VER_LABEL);
            if (!deshabilitar) {
              this.formularioPrincipalAtributos.form.enable();
              this.disable = false;
            }
            this.formularioPrincipalAtributos.form.setValue(response);
          } else {
            this.confirmDialog = true; commonMessage.CARGANDO +
              this.consultarRelaciones(id);
          }
          this.blockUIRegistro.stop();
        },
        error => {
          this.blockUIRegistro.stop();
          this.notificationService.errorMessage(commonMessage.NO_SE_REALIZO_CONSULTA);
        }
      );
    } else {
      this.buscarMemoria(id, deshabilitar, false);
    }


  }

  buscarMemoria(id, deshabilitar, eliminar?) {
    this.blockUIRegistro.start(commonMessage.CARGANDO + '...');
    if (eliminar == null) {
      this.verFormulario();
    }
    this.promiseFormBuscarMemoria(id).then(response => {
      if (response) {
        this.listaAtributos = response;
        if (eliminar == null) {
          this.formularioPrincipalAtributos.form.setValue(this.listaAtributos);
          if (deshabilitar) {
            this.formularioPrincipalAtributos.form.disable();
            this.disable = true;
            this.actualizarAtributo = false;
          }
        }
      } else {
        this.auxAtributosMemoria.forEach(responseMemoria => {
          this.listaAtributos = responseMemoria;
          if (responseMemoria.id == id) {
            this.formularioPrincipalAtributos.form.setValue(this.listaAtributos);
            this.blockUIRegistro.stop();
          }
          if (deshabilitar) {
            this.formularioPrincipalAtributos.form.disable();
            this.disable = true;
            this.actualizarAtributo = false;
            this.blockUIRegistro.stop();
          } else {
            this.idEdit = id;
            this.disable = false;
            this.blockUIRegistro.stop();
          }
        });
      }
    });
  }

  promiseFormBuscarMemoria(id: any): Promise<any> {
    return new Promise((resolve) => {
      this._listaAtributoService.consultarId(id).subscribe(
        data => {
          this.listaAtributos = data;
          this.actualizarAtributo = true;
          resolve(data)
        },
        error => {
          this.notificationService.errorMessage(commonMessage.NO_SE_REALIZO_CONSULTA);
        }
      );
    });
  }


  get desactivar() {
    return this.disable;
  }

  get isInvalid() {
    if (this.formularioPrincipalAtributos) {
      this.onChange();
      return this.formularioPrincipalAtributos.form.invalid;
    }
    if (this.varEditarMemoria == false) {
      this.invalid = true;
    } else {
      this.invalid = false;
    }
    return this.invalid;
  }

  onChange() {
    if (!this.suscrito) {
      this.suscrito = true
      this.formularioPrincipalAtributos.form.get('indice').valueChanges.subscribe(indiceData => {
        if (indiceData < 0) {
          this.notificationService.errorMessage(this.msgValoresPositivos);
          this.formularioPrincipalAtributos.form.get("indice").setValue("");
        }
      });
    }

  }

  consultarAtributosMemoria() {
    this.loadingIndicator = true;
    this.rows = this.auxAtributosMemoria;
    this.temp = this.rows;
    this.controls.rowCount = this.auxAtributosMemoria.length;
    this.controls.curPage = 0;
    this.loadingIndicator = false;


  }

  openModalMod(id) {
    this.confirmDialogMod = true
  }
  confirmMod(): void {
    this.editar();
    this.confirmDialogMod = false;
  }

  declineMod(): void {
    this.confirmDialogMod = false;
  }

  editar() {
    this.listaAtributos = this.formularioPrincipalAtributos.form.value;
    if (this.auxAtributosMemoria.length == 0) {
      let listaValoresAtributos = new ListaValores();
      listaValoresAtributos.id = this.idListavalorAtributos;
      this.listaAtributos.idListaValores = listaValoresAtributos;
      this._listaAtributoService.editar(this.listaAtributos.id, this.listaAtributos).subscribe(data => {
        this.listaAtributos = data;
        this.ocultarFormulario();
        this.notificationService.successMessage(commonMessage.REGISTRO_SATISFACTORIO);
        this.consultar();
        this.ListaAtributosByListaValores();
      }, error => {
        this.notificationService.errorMessage(commonMessage.REGISTRO_NO_EXITOSO);
      });

    } else {
      let pos = 0;
      let posEditar = 0;
      let auxResponseMemoria: ListaAtributos;
      this.auxAtributosMemoria.forEach(responseEditMemoria => {
        if (responseEditMemoria.id == this.idEdit) {
          responseEditMemoria = this.formularioPrincipalAtributos.form.value;
          auxResponseMemoria = responseEditMemoria;
          posEditar = pos;
        }
        pos++;
      });
      this.auxAtributosMemoria.splice(posEditar, 1, auxResponseMemoria);
      this.ocultarFormulario();
      this.consultarAtributosMemoria();
    }

  }

  ListaAtributosByListaValores() {
    this.blockUIRegistro.start(commonMessage.CARGANDO + '...');
    this._listaAtributoService.consultar(this.idListavalorAtributos).subscribe(data => {
      this.rows = data['content'];
      this.temp = this.rows;
      this.controls.rowCount = data.totalElements;
      this.controls.curPage = data.number;
      this.loadingIndicator = false;
      this.blockUIRegistro.stop();
    }, error => {
      this.blockUIRegistro.stop();
      this.notificationService.errorMessage(commonMessage.NO_SE_REALIZO_CONSULTA);
    });
  }

  subir(id) {
    this._listaAtributoService.subirIndice(id).subscribe(data => {
      this.ListaAtributosByListaValores();
    });

  }

  bajar(id) {
    this._listaAtributoService.bajarIndice(id).subscribe(data => {
      this.ListaAtributosByListaValores();
    });

  }

  openModal(id) {
    if (this.auxAtributosMemoria.length == 0) {
      if (id != null) {
        this.buscar(id, false, true);
      }
    } else {
      if (id != null) {
        let position = -1;
        let positionToEliminate = 0;
        this.auxAtributosMemoria.forEach(element => {
          position++;
          if (id == element.id) {
            positionToEliminate = position;
          }
        });
        this.auxAtributosMemoria.splice(positionToEliminate, 1);
        this.consultarAtributosMemoria();
      }
    }

  }

  confirm(): void {
    this.eliminar(this.listaAtributos.id);
    this.nuevo();
    this.confirmDialog = false;
  }

  decline(): void {
    this.confirmDialog = false;
    this.nuevo();
  }


  eliminar(id) {
    this.blockUIConsulta.start(commonMessage.CARGANDO + '...');
    if (this.auxAtributosMemoria.length == 0) {
      this._listaAtributoService.eliminar(id).subscribe(
        response => {
          this.consultar();
          this.notificationService.successMessage(commonMessage.REGISTRO_ELIMINADO);
          this.ListaAtributosByListaValores();
          this.blockUIConsulta.stop();
        },
        error => {
          this.notificationService.errorMessage(commonMessage.REGISTRO_NO_ELIMINADO);
          this.blockUIConsulta.stop();
        }
      );
    } else {
      let position = -1;
      let positionToEliminate = 0;
      this.auxAtributosMemoria.forEach(element => {
        position++;
        if (id == element.id) {
          positionToEliminate = position;
        }
      });
      this.auxAtributosMemoria.splice(positionToEliminate, 1);
      this.consultarAtributosMemoria();
    }

  }

  consultarRelaciones(id) {
    let arrayTemp = [];
    this.recursoService.listar(commonOptions.consultaEstadoEstandar).subscribe(
      response => {
        for (let item of response.content) {

          if (item.modulo.id == id) {
            arrayTemp.push(item);
          }
        }
        this.sePuedeEliminar = arrayTemp.length > 0;
        this.confirmDialog = true;

      },
      error => {
        this.confirmDialog = false;
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );
  }


}
