import { Component, OnInit, ViewChild } from '@angular/core';
import { commonOptions } from '@commons/commonOptions';
import { commonValues } from '@commons/commonValues';
import { commonMessage } from '@commons/commonMessages';
import { TypeElement } from '@app/shared/components/general-form/types/TypeElement';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { GeneralFormComponent } from '@app/shared/components/general-form/general-form.component';
import { ListaValores } from './models/lista-valores';
import { FormBuilder, Validators } from '@angular/forms';
import { TypeCaseText } from '@app/shared/components/general-form/types/TypeCaseText';
import { TypePatternMask } from '@app/shared/components/general-form/types/TypePatternMask';
import { Subscription } from 'rxjs';
import { NotificationService } from '@app/core/services/notification.service';
import { ListaValoresService } from '@app/core/services/ge/lista-valores.service';
import { ParametrosListaValores } from './models/parametros-lista-valores';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { RecursosService } from '@app/core/services/as/recursos.service';
import { Validaciones } from './validations/Validaciones';
import { ListaAtributos } from './models/lista-atributos'
import { ListaAtributosService } from '@app/core/services/ge/lista-atributos.service';
import { ListaElementos } from './models/lista-elementos';
import { ListaElementosService } from '@app/core/services/ge/lista-elementos.service';
import { FuenteDatoService } from '../../../core/services/ge/fuente-dato.service';

@Component({
  selector: 'lista-valores',
  templateUrl: './lista-valores.component.html',
  styleUrls: ['./lista-valores.component.css']
})
export class ListaValoresComponent implements OnInit {

  @ViewChild(DatatableComponent, { 'static': false }) table: DatatableComponent;
  @ViewChild(GeneralFormComponent, { 'static': false }) formularioPrincipal: GeneralFormComponent;

  @BlockUI('formConsulta') blockUIConsulta: NgBlockUI;
  @BlockUI('formRegistro') blockUIRegistro: NgBlockUI;

  listaAtributosOutput: Array<ListaAtributos> = new Array();
  listaElementosOutput: Array<ListaElementos> = new Array();
  listaValores: ListaValores;
  idListaValores: any;
  auxVarEditar: boolean = false;
  validaciones: Validaciones = new Validaciones();
  disable: any;
  sePuedeEliminar: boolean;
  searching = true;
  typeElementTipoDato = { typeElement: TypeElement.NUMBER }
  typeElementCodigo = { typeElement: TypeElement.FORMAT };
  valorDinamicoTC = { visible: false };
  valorDinamicoTR = { visible: false };
  opcionesEstado = commonOptions.tiposEstado;
  opcionesGenerales = commonOptions;
  opcionesSeleccionM = commonOptions.opcionesSiNo;
  campo = commonValues.CODIGO_VALUE;
  estado = commonValues.ACTIVO_VALUE;
  criterio = "";
  accion = "";
  formCriterio: any;
  criterioRequerido = false;
  lblCriterioBusqueda = commonValues.CRITERIO_BUSQUEDA_LABEL;
  msgCriterioObligatorio = commonMessage.CRITERIO_OBLIGATORIO;
  lblFiltroTabla = commonValues.FILTRO_TABLA_LABEL;
  lblAcciones = commonValues.ACCIONES_LABEL;
  lblVer = commonValues.VER_LABEL;
  formularioVisible = false;
  formListaValores: any;
  tabActiva: any = "tabListaValores";
  confirmDialog = false;
  confirmDialogMod = false;
  suscritoAComponente: any = false;
  esTipoConsulta: boolean = false;
  esTipoDominio: boolean = false;
  esTipoNumerico: boolean = false;
  ocultarBotones: boolean = true;
  esTipoFecha: boolean = false;
  subsNumDesde: Subscription;
  subsNumHasta: Subscription;
  subsFecDesde: Subscription;
  subsFechHasta: Subscription;


  //Labels vista
  lblConsulta = commonValues.CONSULTA_LABEL;
  lblListaValores = commonValues.LISTA_VALORES_LABEL;
  lblGuardar = commonValues.GUARDAR_LABEL;
  lblAgregar = commonValues.AGREGAR_LABEL;
  lblCancelar = commonValues.CANCELAR_LABEL;
  lblEditar = commonValues.EDITAR_LABEL;
  lblCampo = commonValues.CAMPO_LABEL;
  lblCodigo = commonValues.CODIGO_LABEL;
  lblNombre = commonValues.NOMBRE_LABEL;
  lblEstado = commonValues.ESTADO_LABEL;
  lblActivo = commonValues.ACTIVO_LABEL;
  lblInactivo = commonValues.INACTIVO_LABEL;
  lblTodos = commonValues.TODOS_LABEL;
  lblBuscar = commonValues.BUSCAR_LABEL;
  lblCriteriosBusqueda = commonValues.CRITERIOS_BUSQUEDA_LABEL;
  lblEliminar = commonValues.ELIMINAR_LABEL;
  lblSi = commonValues.SI_LABEL;
  lblNo = commonValues.NO_LABEL;
  lblModificar = commonValues.MODIFICAR_LABEL;
  lblAtributos = commonValues.ATRIBUTOS;
  lblElementos = commonValues.ELEMENTOS;
  msgDeseaEliminar = commonMessage.DESEA_ELIMINAR_REGISTRO;
  msgDeseaModificar = commonMessage.DESEA_MODFICAR_REGISTRO;
  msgValoresPositivos = commonMessage.VALORES_POSITIVOS;
  msgMenorHasta = commonMessage.MENOR_VALOR_HASTA;
  msgMayorDesde = commonMessage.MAYOR_VALOR_HASTA;

  /* Variables relacionadas a la tabla de consulta */
  controls: any = {
    pageSize: 10,
    filter: '',
    rowCount: 0, //total elementos
    curPage: 0, //numero pagina
    selectedCount: 0,
    offset: 0
  }
  cols = [{ name: 'codigo' }, { name: 'nombre' }];
  temp = [];
  rows = [];
  loadingIndicator = false;
  reorderable = true;
  messages = {
    emptyMessage: commonMessage.NO_EXISTE_INFO,
    totalMessage: commonMessage.REGISTROS_TOTALES,
  };

  constructor(private formBuilder: FormBuilder,
    private notificationService: NotificationService,
    private _listaValoresService: ListaValoresService,
    private _listaAtributoService: ListaAtributosService,
    private _listaElementoService: ListaElementosService,
    private _fuenteDatoService: FuenteDatoService,
    private recursoService: RecursosService) { }

  ngOnInit() {
    this.formCriterio = this.formBuilder.group({
      criterio: ['', Validators.required]
    });
    this.formListaValores = this.createForm();
  }


  /**
* Description: Metodo para el filtro de la tabla de consulta
* responde al evento keyup del elemento ngx-dataTable
*
* @param {*} event Valor del input del filtro del dataTable
* @memberof ListaValoresComponent
*/
  updateFilter(event) {
    let val = event.target.value.toLowerCase();
    let colsAmt = 2;
    let keys = ['codigo', 'nombre'];
    this.rows = this.temp.filter(function (item) {
      for (let i = 0; i < colsAmt; i++) {
        if (item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 || !val) {
          return true;
        }
      }
    });
    this.table.offset = 0;
  }

  /**
   * Description: Metodo que ejecuta la paginacion del ngx-dataTable responde
   * al evento ngModelChange del componente select
   *
   * @param {*} value Valor del select del componente paginacion del dataTable
   * @memberof ListaValoresComponent
   */
  updatePageSize(value) {
    this.controls.pageSize = parseInt(value, 10);
    this.table.limit = this.controls.pageSize;
    this.controls.curPage = (this.controls.pageSize > this.controls.rowCount) ?
      0 : this.controls.curPage;
    this.consultar(false);
  }

  setPage(event) {
    this.controls.curPage = event.offset;
    this.controls.pageSize = event.pageSize;
    this.consultar(false);
  }

  createForm() {
    return {
      header: {
        label: commonValues.DATOS_BASICOS_LABEL
      },
      fields: {
        id: {},
        tipo: {
          key: "tipo",
          label: commonValues.TIPOS_LISTA_VALORES,
          typeElement: TypeElement.SELECT,
          options: this.opcionesGenerales.valoresTipoLista,
          default: this.opcionesGenerales.valoresTipoLista[0],
          validation: {
            required: commonMessage.CAMPO_OBLIGATORIO,
          }
        },
        rowOne: {
          row: [
            {
              key: "codigo",
              label: commonValues.CODIGO_LABEL,
              typeElement: { typeElement: TypeElement.FORMAT },
              textCase: TypeCaseText.UPPER_CASE,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO,
                expression: TypePatternMask.getPattern(TypePatternMask.LETRAS_NUMEROS_UNDERSCORE)
              },
              placeholder: commonMessage.INGRESE_CODIGO,
              tooltip: commonMessage.CODIGO_IDENTIFICA_REGISTRO
            },
            {
              key: "nombre",
              label: commonValues.NOMBRE_LABEL,
              typeElement: { typeElement: TypeElement.TEXT },
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
              placeholder: commonMessage.INGRESE_NOMBRE
            },
            {
              key: "predecesor",
              label: commonValues.PREDECESOR_LABEL,
              typeElement: { typeElement: "seleccionar_registro" },
              service: this._listaValoresService,
              inputsConsulta: ['codigo', 'nombre'],
              camposHeadersTabla: ["Código", "Nombre", "Estado"],
              camposTabla: ["codigo", "nombre", "estado"],
              parametrosDefecto: { estado: "ACTIVO" },
              camposInput: ["codigo", "nombre"],
              listarEspecifico: false
            }
          ]
        },
        rowTwo: {
          row: [
            {
              key: "estado",
              label: commonValues.ESTADO_LABEL,
              typeElement: { typeElement: TypeElement.BUTTON_GROUP },
              options: this.opcionesEstado,
              default: this.opcionesEstado[0],
              dynamic: { visible: true },
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO,
              }
            },
            {
              key: "seleccionMultiple",
              label: commonValues.SELECCION_MULTIPLE_LABEL,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesSeleccionM,
              default: this.opcionesSeleccionM[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO,
              }
            },
            {
              key: "fuenteDato",
              label: commonValues.FUENTE_DATOS_LABEL,
              typeElement: { typeElement: "seleccionar_registro" },
              service: this._fuenteDatoService,
              inputsConsulta: ['codigo', 'nombre'],
              camposHeadersTabla: ["Código", "Nombre", "Estado"],
              camposTabla: ["codigo", "nombre", "estado"],
              parametrosDefecto: { estado: "ACTIVO" },
              camposInput: ["codigo", "nombre"],
              listarEspecifico: false,
              dynamic: this.valorDinamicoTC
            }
          ]
        },
        rowFour: {
          row: [
            {
              key: "tipoDatoRango",
              label: commonValues.TIPO_DATO_LABEL,
              typeElement: TypeElement.SELECT,
              options: this.opcionesGenerales.valoresTipoDatoRango,
              default: this.opcionesGenerales.valoresTipoDatoRango[0],
              dynamic: this.valorDinamicoTR
            },
            {
              key: "rangoDesde",
              label: commonValues.DESDE,
              placeholder: commonValues.DESDE,
              typeElement: this.typeElementTipoDato,
              dynamic: this.valorDinamicoTR
            },
            {
              key: "rangoHasta",
              label: commonValues.HASTA,
              placeholder: commonValues.HASTA,
              typeElement: this.typeElementTipoDato,
              dynamic: this.valorDinamicoTR
            }

          ]
        },
        documentacion: {
          label: commonValues.DOCUMENTACION_LABEL,
          typeElement: TypeElement.TEXTAREA
        },
      }

    }

  }

  verFormulario() {
    this.accion = commonValues.REGISTRAR_LABEL;
    this.searching = false;
    this.formularioVisible = true;
    this.auxVarEditar = true;
    this.nuevo();
  }

  ocultarFormulario() {
    this.criterioRequerido = false;
    this.disable = false;
    this.formularioVisible = false;
    this.suscritoAComponente = false;
    this.formularioPrincipal.form.reset();
    this.searching = true;
    this.idListaValores = null;
    this.listaElementosOutput.length = 0;
    this.listaAtributosOutput.length = 0;
    this.auxVarEditar = false;

  }

  nuevo() {
    this.listaValores = new ListaValores();
  }

  onSubmit() {
    this.listaValores = this.formularioPrincipal.form.value;
    this.listaValores.estado = this.listaValores.estado == "" ? commonValues.ACTIVO_VALUE : this.listaValores.estado;
    this.listaValores.seleccionMultiple = this.listaValores.seleccionMultiple == "" ? commonValues.SI_VALUE : this.listaValores.seleccionMultiple

    if (!this.listaValores.predecesor) {
      delete this.listaValores.predecesor
    } else {
      let predecesor: ListaValores = new ListaValores();
      predecesor.id = this.listaValores.predecesor.id;
      this.listaValores.predecesor = predecesor;
    }
    if (!this.listaValores.tipoDatoRango) {
      delete this.listaValores.tipoDatoRango
    }
    if (!this.listaValores.fuenteDato) {
      delete this.listaValores.fuenteDato;
    }
    let idfuenteDato = this.formularioPrincipal.form.get('fuenteDato').value;
    let tipoListaV = this.formularioPrincipal.form.get('tipo').value;
    let tipoDatoRango = this.formularioPrincipal.form.get('tipoDatoRango').value;
    let rangoDesde = this.formularioPrincipal.form.get('rangoDesde').value;
    let rangoHasta = this.formularioPrincipal.form.get('rangoHasta').value;

    if (tipoListaV == this.opcionesGenerales.valoresTipoLista[1].value && idfuenteDato == "") {
      this.notificationService.errorMessage(commonMessage.SELECIONAR_FUENTE_DATO);
      this.listaValores.id = null;
      return
    }

    if (tipoListaV == this.opcionesGenerales.valoresTipoLista[1].value || tipoListaV == this.opcionesGenerales.valoresTipoLista[2].value){
      delete this.listaValores.tipoDatoRango;
      delete this.listaValores.rangoDesde;
      delete this.listaValores.rangoHasta;
    }

    if (tipoListaV == this.opcionesGenerales.valoresTipoLista[3].value && (tipoDatoRango == "" || rangoDesde == "" || rangoHasta == "")) {
      this.notificationService.errorMessage(commonMessage.INGRESE_TODOS_CAMPOS);
      this.listaValores.id = null;
    } else {

      if (tipoListaV == this.opcionesGenerales.valoresTipoLista[3].value && tipoDatoRango == this.opcionesGenerales.valoresTipoDatoRango[3].value) {

        this.listaValores.rangoDesde = this.validaciones.convertDate(this.listaValores.rangoDesde);
        this.listaValores.rangoHasta = this.validaciones.convertDate(this.listaValores.rangoHasta);
        let fechaDesde: Date = new Date(this.listaValores.rangoDesde);
        let fechaHasta: Date = new Date(this.listaValores.rangoHasta)
        this.listaValores.rangoDesde = fechaDesde.getTime().toString();
        this.listaValores.rangoHasta = fechaHasta.getTime().toString();
      }

      this._listaValoresService.guardar(this.listaValores).subscribe(
        response => {
          this.listaValores = response;
          this.searching = true;
          this.nuevo();
          if (this.listaAtributosOutput && this.listaAtributosOutput.length) {
            let lista = new ListaValores();
            lista.id = response.id;
            this.listaAtributosOutput.forEach((atributo) => {
              atributo.id = null;
              atributo.idListaValores = lista;
            });
            this._listaAtributoService.guardarTodo(this.listaAtributosOutput).subscribe(data => {
            });
          }
          if (this.listaElementosOutput.length) {
            let lista = new ListaValores();
            lista.id = response.id;
            this.listaElementosOutput.forEach((elemento) => {
              elemento.id = null;
              elemento.idListaValores = lista;
            });
            this._listaElementoService.guardarTodo(this.listaElementosOutput).subscribe(data => {
            });
          }
          this.notificationService.successMessage(commonMessage.REGISTRO_SATISFACTORIO);
          this.consultar(false);
          this.ocultarFormulario();
          this.listaAtributosOutput = [];
        },
        error => {
          this.notificationService.errorMessage(commonMessage.REGISTRO_NO_EXITOSO);
          this.ocultarFormulario();
        }
      );

    }

  }


  consultar(initPage: boolean) {
    if (this.formCriterio.invalid) {
      this.criterioRequerido = true;
      return;
    }
    this.criterioRequerido = false;
    if (this.criterio != "") {
      this.controls.filter = "";
      this.loadingIndicator = true;
      if (initPage) {
        this.controls.curPage = 0;
      }
      let params = new ParametrosListaValores();
      if (this.campo == commonValues.CODIGO_VALUE) {
        params.codigo = this.criterio;
      } else {
        params.nombre = this.criterio;
      }
      params.size = this.controls.pageSize;
      params.page = this.controls.curPage;
      params.listaEstados = this.estado.split(",");
      this._listaValoresService.listar(params).subscribe(
        response => {
          this.rows = response.content;
          this.temp = this.rows;
          this.loadingIndicator = false;
          this.controls.rowCount = response.totalElements;
          this.controls.curPage = response.number;
        },
        error => {
          this.loadingIndicator = false;
          this.notificationService.errorMessage(commonMessage.NO_SE_REALIZO_CONSULTA);
        }
      );
    }

  }

  visualizar(id, deshabilitar) {
    this.buscar(id, deshabilitar, false);
    this.verFormulario();

  }

  buscar(id: number, deshabilitar: boolean, eliminar: boolean) {
    this.blockUIRegistro.start(commonMessage.CARGANDO + '...');
    this.auxVarEditar = true;
    this._listaValoresService.consultar(id).subscribe(
      response => {
        this.listaValores = response;
        this.idListaValores = this.listaValores.id;
        if (!eliminar) {
          if (this.listaValores.tipo == this.opcionesGenerales.valoresTipoLista[3].value && this.listaValores.tipoDatoRango == this.opcionesGenerales.valoresTipoDatoRango[3].value) {
            this.listaValores.rangoDesde = this.validaciones.convertirStringFecha(this.listaValores.rangoDesde)
            this.listaValores.rangoHasta = this.validaciones.convertirStringFecha(this.listaValores.rangoHasta)
          }
          this.formularioPrincipal.form.disable();
          this.disable = true;
          this.accion = (!deshabilitar ? commonValues.EDITAR_LABEL : commonValues.VER_LABEL);
          if (!deshabilitar) {
            this.formularioPrincipal.form.enable();
            this.disable = false;
            this.auxVarEditar = true;
          } else {
            this.formularioPrincipal.form.disable();
            this.auxVarEditar = false;
          }
          this.formularioPrincipal.form.setValue(response);
        } else {
          this.confirmDialog = true; commonMessage.CARGANDO +
            this.consultarRelaciones(id);
        }
        this.blockUIRegistro.stop();
      },
      error => {
        this.blockUIRegistro.stop();
        this.notificationService.errorMessage(commonMessage.NO_SE_REALIZO_CONSULTA);
      }
    );

  }

  consultarRelaciones(id) {
    let arrayTemp = [];
    this.recursoService.listar(commonOptions.consultaEstadoEstandar).subscribe(
      response => {
        for (let item of response.content) {

          if (item.modulo.id == id) {
            arrayTemp.push(item);
          }
        }
        this.sePuedeEliminar = arrayTemp.length > 0;
        this.confirmDialog = true;
      },
      error => {
        this.confirmDialog = false;
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );
  }

  openModal(id) {
    if (id != null) {
      this.buscar(id, false, true);
    }
  }

  confirm(): void {
    this.eliminar(this.listaValores.id);
    this.nuevo();
    this.confirmDialog = false;
  }

  decline(): void {
    this.confirmDialog = false;
    this.nuevo();
  }

  eliminar(id) {
    this.blockUIConsulta.start(commonMessage.CARGANDO + '...');
    this._listaValoresService.eliminar(id).subscribe(
      response => {
        this.consultar(false);
        this.notificationService.successMessage(commonMessage.REGISTRO_ELIMINADO);
        this.blockUIConsulta.stop();
      },
      error => {
        this.notificationService.errorMessage(commonMessage.REGISTRO_NO_ELIMINADO);
        this.blockUIConsulta.stop();
      }
    );
  }

  openModalMod(id) {
    this.confirmDialogMod = true
  }
  confirmMod(): void {
    this.editar();
    this.confirmDialogMod = false;
  }

  declineMod(): void {
    this.confirmDialogMod = false;
  }

  editar() {
    this.listaValores = this.formularioPrincipal.form.value;
    this.listaValores.estado = this.listaValores.estado == "" ? commonValues.ACTIVO_VALUE : this.listaValores.estado;
    this.listaValores.seleccionMultiple = this.listaValores.seleccionMultiple == "" ? commonValues.SI_VALUE : this.listaValores.seleccionMultiple

    if (!this.listaValores.predecesor) {
      delete this.listaValores.predecesor
    } else {
      let predecesor: ListaValores = new ListaValores();
      predecesor.id = this.listaValores.predecesor.id;
      if (this.listaValores.id == this.listaValores.predecesor.id) {
        this.notificationService.errorMessage(commonMessage.ERROR_PREDECESOR);
        return
      } else {
        this.listaValores.predecesor = predecesor;
      }

    }
    if (!this.listaValores.tipoDatoRango) {
      delete this.listaValores.tipoDatoRango
    }
    let idfuenteDato = this.formularioPrincipal.form.get('fuenteDato').value;
    let tipoListaV = this.formularioPrincipal.form.get('tipo').value;
    let tipoDatoRango = this.formularioPrincipal.form.get('tipoDatoRango').value;
    let rangoDesde = this.formularioPrincipal.form.get('rangoDesde').value;
    let rangoHasta = this.formularioPrincipal.form.get('rangoHasta').value;

    if (tipoListaV == this.opcionesGenerales.valoresTipoLista[1].value && idfuenteDato == "") {
      this.notificationService.errorMessage(commonMessage.SELECIONAR_FUENTE_DATO);
      return
    }

    if (tipoListaV == this.opcionesGenerales.valoresTipoLista[3].value && (tipoDatoRango == "" || rangoDesde == "" || rangoHasta == "")) {
      this.notificationService.errorMessage(commonMessage.INGRESE_TODOS_CAMPOS);
    } else {
      if (tipoListaV == this.opcionesGenerales.valoresTipoLista[3].value && tipoDatoRango == this.opcionesGenerales.valoresTipoDatoRango[3].value) {
        this.listaValores.rangoDesde = this.validaciones.convertDate(this.listaValores.rangoDesde);
        this.listaValores.rangoHasta = this.validaciones.convertDate(this.listaValores.rangoHasta);
        let fechaDesde: Date = new Date(this.listaValores.rangoDesde);
        let fechaHasta: Date = new Date(this.listaValores.rangoHasta)
        this.listaValores.rangoDesde = fechaDesde.getTime().toString();
        this.listaValores.rangoHasta = fechaHasta.getTime().toString();
      }
      this._listaValoresService.editar(this.listaValores.id, this.listaValores).subscribe(
        response => {
          this.listaValores = response;
          this.searching = true;
          this.nuevo();
          this.notificationService.successMessage(commonMessage.REGISTRO_SATISFACTORIO);
          this.consultar(false);
          this.ocultarFormulario();
        },
        error => {
          this.notificationService.errorMessage(commonMessage.REGISTRO_NO_EXITOSO);
        }
      );
    }

  }

  get isInvalid() {
    if (this.formularioPrincipal) {
      this.onChange();
    }
    return this.formularioPrincipal ? this.formularioPrincipal.form.invalid : true;
  }

  get desactivar() {
    return this.disable;
  }

  onChange(): void {
    if (!this.suscritoAComponente) {
      this.suscritoAComponente = true;

      this.formularioPrincipal.form.get("codigo").valueChanges.subscribe(codigoDigitado => {
        if (codigoDigitado != null) this.onKeyupCodigo(codigoDigitado);
      });
      this.formularioPrincipal.form.get("tipo").valueChanges.subscribe(tipo => {
        if (tipo == this.opcionesGenerales.TIPO_CONSULTA) {
          this.valorDinamicoTC.visible = true;
          this.valorDinamicoTR.visible = false;
          this.esTipoConsulta = true;
          this.esTipoDominio = false;
        }
        else if (tipo == this.opcionesGenerales.TIPO_RANGO) {
          this.valorDinamicoTR.visible = true;
          this.valorDinamicoTC.visible = false;
          this.esTipoConsulta = false;
          this.esTipoDominio = false;

        } else if (tipo == this.opcionesGenerales.TIPO_DOMINIO) {
          this.valorDinamicoTC.visible = false;
          this.valorDinamicoTR.visible = false;
          this.esTipoConsulta = false;
          this.esTipoDominio = true;
        } else {
          // this.valorDinamicoTC.visible = false;
          // this.valorDinamicoTR.visible = false;
        }
      });

      this.formularioPrincipal.form.get("tipoDatoRango").valueChanges.subscribe(valueTipoDato => {
        switch (valueTipoDato) {
          case this.opcionesGenerales.valoresTipoDatoRango[1].value:
            this.esTipoNumerico = true;
            this.esTipoFecha = false;
            this.typeElementTipoDato.typeElement = valueTipoDato == this.opcionesGenerales.TIPOD_NUMERICO ? TypeElement.NUMBER : TypeElement.TEXT;
            this.formularioPrincipal.form.get("rangoDesde").valueChanges.subscribe(valorDesde => {
              if ((parseInt(valorDesde) < 0) && this.esTipoNumerico == true) {
                this.notificationService.errorMessage(this.msgValoresPositivos);
                this.formularioPrincipal.form.get("rangoDesde").setValue("");
              } else {
                if (valorDesde == "") { }
              }
              if ((valorDesde > this.formularioPrincipal.form.get("rangoHasta").value) && this.formularioPrincipal.form.get("rangoHasta").value == "") {
              } else {
                if (valorDesde > this.formularioPrincipal.form.get("rangoHasta").value) {
                  if (valorDesde == "") {
                  } else {
                    this.notificationService.errorMessage(this.msgMenorHasta);
                    this.formularioPrincipal.form.get("rangoDesde").setValue("");
                  }
                }
              }
            });
            this.formularioPrincipal.form.get("rangoHasta").valueChanges.subscribe(valorHasta => {
              if ((parseInt(valorHasta) < 0) && this.esTipoNumerico == true) {
                this.notificationService.errorMessage(this.msgValoresPositivos);
                this.formularioPrincipal.form.get("rangoHasta").setValue("");
              } else {
                if (valorHasta == "") {
                }
              }
              if (valorHasta < this.formularioPrincipal.form.get("rangoDesde").value) {
                if (valorHasta == "") {
                } else {
                  this.notificationService.errorMessage(this.msgMayorDesde);
                  this.formularioPrincipal.form.get("rangoHasta").setValue("");
                }
              }
            });
            break;
          case this.opcionesGenerales.valoresTipoDatoRango[2].value:
            this.esTipoNumerico = false;
            this.esTipoFecha = false;
            this.typeElementTipoDato.typeElement = valueTipoDato == this.opcionesGenerales.TIPOD_ALFANUM ? TypeElement.TEXT : TypeElement.TEXT;
            break;
          case this.opcionesGenerales.valoresTipoDatoRango[3].value:
            this.esTipoNumerico = false;
            this.esTipoFecha = true;
            this.typeElementTipoDato.typeElement = valueTipoDato == this.opcionesGenerales.TIPOD_FECHA ? TypeElement.DATE : TypeElement.TEXT;
            this.formularioPrincipal.form.get("rangoDesde").valueChanges.subscribe(valorDesde => {
              if ((valorDesde > this.formularioPrincipal.form.get("rangoHasta").value) && this.formularioPrincipal.form.get("rangoHasta").value == "") {

              } else {
                if (valorDesde > this.formularioPrincipal.form.get("rangoHasta").value) {
                  if (valorDesde == "") {
                  } else {
                    this.notificationService.errorMessage(this.msgMenorHasta);
                    this.formularioPrincipal.form.get("rangoDesde").setValue("");
                  }
                }
              }
            });

            this.formularioPrincipal.form.get("rangoHasta").valueChanges.subscribe(valorHasta => {
              if (valorHasta < this.formularioPrincipal.form.get("rangoDesde").value) {
                if (valorHasta == "") {
                } else {
                  this.notificationService.errorMessage(this.msgMayorDesde);
                  this.formularioPrincipal.form.get("rangoHasta").setValue("");
                }
              }
            });
            break;
        }
      });
    }
  }

  onKeyupCodigo(valorCodigo: string) {
    if ((valorCodigo && (this.listaValores.id && valorCodigo != this.listaValores.codigo)) || !this.listaValores.id && valorCodigo) {
      this._listaValoresService.consultarCodigo(valorCodigo).subscribe(
        response => {
          if (response != null && response.id != this.listaValores.id) {
            this.formularioPrincipal.form.controls['codigo'].setErrors({ 'codigoRecorded': true });
          } else {
            this.formularioPrincipal.form.controls['codigo'].setErrors({ 'codigoRecorded': false });
            const errores = this.formularioPrincipal.form.controls['codigo'].errors;
            delete errores.codigoRecorded;
            this.formularioPrincipal.form.controls['codigo'].setErrors(Object.keys(errores).length === 0 ? null : errores);
          }
        },
        error => {
          console.log(error);
          this.notificationService.errorMessage(commonMessage.ERROR_CONSULTAR_CODIGO);
        }
      );
    }
  }

  ocultarBoton() {
    this.ocultarBotones = false
  }

  mostrarBoton() {
    this.ocultarBotones = true
  }

  setActiva(tabActiva: string) {
    this.tabActiva = tabActiva;
  }

  asociarAtributo(event: any) {
    this.listaAtributosOutput.push(event);
  }

  asociarElemento(event: any) {
    this.listaElementosOutput.push(event);

  }


}
