import { ModulosComponent } from './modulos.component';
import { CommonModule } from '@angular/common';
import { GeneralFormComponentModule } from '../../../shared/components/general-form/general-form-component.module';

import { NgModule } from "@angular/core";

import { FormsModule } from '@angular/forms';
import { NgxDatatableModule, DataTableHeaderCellComponent} from '@swimlane/ngx-datatable';
import { ModulosRoutingModule } from './modulos.routing.module';



@NgModule({
    declarations: [ ModulosComponent ],
    imports: [ModulosRoutingModule,GeneralFormComponentModule,CommonModule,FormsModule,NgxDatatableModule,
        DataTableHeaderCellComponent],
    exports: [GeneralFormComponentModule]
})
export class ModulosModule {}