import { Modulo } from './models/modulo';
import { FormGroup } from '@angular/forms';
import { ModulosService } from './../../../core/services/ge/modulos.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ParametrosModulo } from './models/parametrosModulo';
import { NotificationService } from '@app/core/services';
import { GeneralFormComponent } from '@app/shared/components/general-form/general-form.component';
import { TypeElement } from '@app/shared/components/general-form/types/TypeElement';
import { RecursosService } from '@app/core/services/as/recursos.service';
import { commonOptions } from '../../../../commons/commonOptions';
import { commonValues } from '@commons/commonValues';
import { commonMessage } from '@commons/commonMessages';
import { TypeCaseText } from '@app/shared/components/general-form/types/TypeCaseText';
import { TypePatternMask } from '@app/shared/components/general-form/types/TypePatternMask';


@Component({
  selector: 'app-modulos',
  templateUrl: './modulos.component.html',
  styleUrls: ['./modulos.component.css']
})
export class ModulosComponent implements OnInit {

  @ViewChild(GeneralFormComponent, { 'static': false }) formMoudule: GeneralFormComponent;
  @ViewChild(DatatableComponent, { 'static': false }) table: DatatableComponent;

  codigoRecorded: boolean;
  suscritoAComponenteCodigo: any = false;
  disable: any;
  formModulo: any;
  formLogin: any;
  searching = true;
  campo = commonValues.CODIGO_VALUE;
  criterio = "";
  estado = commonValues.ACTIVO_VALUE;
  valorPaginado = 10;
  formularioModulo: FormGroup;
  modulo: Modulo;
  mensaje: string;
  confirmDialog = false;
  opcionesEstado = commonOptions.tiposEstado;

  //Labels vista
  lblModulos = commonValues.MODULOS_LABEL;
  lblCancelar = commonValues.CANCELAR_LABEL;
  lblGuardar = commonValues.GUARDAR_LABEL;
  lblAgregar = commonValues.AGREGAR_LABEL;
  lblCriteriosBusqueda = commonValues.CRITERIOS_BUSQUEDA_LABEL;
  lblCampo = commonValues.CAMPO_LABEL;
  lblCodigo = commonValues.CODIGO_LABEL;
  lblNombre = commonValues.NOMBRE_LABEL;
  lblUrl = commonValues.URL_LABEL;
  lblEstado = commonValues.ESTADO_LABEL;
  lblActivo = commonValues.ACTIVO_LABEL;
  lblInactivo = commonValues.INACTIVO_LABEL;
  lblTodos = commonValues.TODOS_LABEL;
  lblCriterioBusqueda = commonValues.CRITERIO_BUSQUEDA_LABEL;
  lblBuscar = commonValues.BUSCAR_LABEL;
  lblFiltroTabla = commonValues.FILTRO_TABLA_LABEL;
  lblAcciones = commonValues.ACCIONES_LABEL;
  lblVer = commonValues.VER_LABEL;
  lblEditar = commonValues.EDITAR_LABEL;
  lblEliminar = commonValues.ELIMINAR_LABEL;
  lblSi = commonValues.SI_LABEL;
  lblNo = commonValues.NO_LABEL;
  msgDeseaEliminar = commonMessage.DESEA_ELIMINAR_REGISTRO;
  msgNoSePuedeEliminar = commonMessage.NO_SE_PUEDE_ELIMINAR;
  typeElementCodigo = { typeElement: TypeElement.FORMAT };
  typeElementNombre = { typeElement: TypeElement.TEXT };
  typeElementEstado = { typeElement: TypeElement.BUTTON_GROUP };
  typeElementDocumento = { typeElement: TypeElement.TEXTAREA };
  estadoDynamic = { visible: true };

  /* Variables relacionadas a la tabla de consulta */
  controls: any = {
    pageSize: 10,
    filter: '',
    rowCount: 0,
    curPage: 0,
    offset: 0
  }
  cols = [{ name: 'codigo' }, { name: 'nombre' }];
  temp = [];
  rows = [];
  loadingIndicator = false;
  reorderable = true;
  messages = {
    emptyMessage: commonMessage.NO_EXISTE_INFO,
    totalMessage: commonMessage.REGISTROS_TOTALES,
  };
  sePuedeEliminar: boolean;


  constructor(private moduloService: ModulosService,
    private notificationService: NotificationService,
    private recursoService: RecursosService) {
  }
  //confimr dialog
  openModal(id) {
    if (id != null) {
      this.buscar(id, false, true);
      this.consultarRelaciones(id);
    }
    // this.confirmDialog = true;
    this.mensaje = commonValues.ELIMINAR_LABEL;
  }

  confirm(): void {
    this.eliminar(this.modulo.id);
    this.nuevo();
    this.confirmDialog = false;
  }

  decline(): void {
    this.confirmDialog = false;
    this.nuevo();
  }
  //confimr dialog

  ngOnInit() {
    this.formModulo = this.createForm();
  }

  createForm(): any {
    return {
      header: {
        label: commonValues.DATOS_BASICOS_LABEL
      },
      fields: {
        id: {},
        row: {
          row: [
            {
              key: "codigo",
              label: commonValues.CODIGO_LABEL,
              typeElement: this.typeElementCodigo,
              textCase: TypeCaseText.UPPER_CASE,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO,
                expression: TypePatternMask.getPattern(TypePatternMask.LETRAS_NUMEROS_UNDERSCORE)
              },
              placeholder: commonMessage.INGRESE_CODIGO,
              tooltip: commonMessage.CODIGO_IDENTIFICA_REGISTRO
            },
            {
              key: "nombre",
              label: commonValues.NOMBRE_LABEL,
              typeElement: this.typeElementNombre,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
              placeholder: commonMessage.INGRESE_NOMBRE
            },
            {
              key: "estado",
              label: commonValues.ESTADO_LABEL,
              typeElement: this.typeElementEstado,
              options: this.opcionesEstado,
              default: this.opcionesEstado[0],
              dynamic: this.estadoDynamic
            }
          ]
        },
        documentacion: {
          label: commonValues.DOCUMENTACION_LABEL,
          typeElement: this.typeElementDocumento,
          icon: "fa-search",
        }
      }
    }
  }

  get isInvalid() {
    if (this.formMoudule) {
      this.onChanges();
    }
    return this.formMoudule ? this.formMoudule.form.invalid : true;
  }

  get desactivar() {
    return this.disable;
  }

  onChanges(): void {
    if (this.formMoudule && !this.suscritoAComponenteCodigo) {
      this.suscritoAComponenteCodigo = true;
      this.formMoudule.form.get('codigo').valueChanges.subscribe(val => {
        this.onKeyupCodigo(val);
        console.log(this.formMoudule.form.controls);

      });
    }
  }

  onKeyupCodigo(valorCodigo) {

    if ((valorCodigo && (this.modulo.id && valorCodigo != this.modulo.codigo)) || !this.modulo.id && valorCodigo) {
      this.moduloService.listar({ codigo: valorCodigo, estado: commonOptions.estadosGeneral })
        .subscribe(data => {
          data = data.content;
          this.codigoRecorded = data != null && data.length > 0;
          if (this.codigoRecorded) {
            this.formMoudule.form.controls['codigo'].setErrors({ 'codigoRecorded': true });
          } else {
            this.formMoudule.form.controls['codigo'].setErrors({ 'codigoRecorded': false });
            const errores = this.formMoudule.form.controls['codigo'].errors;
            delete errores.codigoRecorded;
            this.formMoudule.form.controls['codigo'].setErrors(Object.keys(errores).length === 0 ? null : errores);
          }
        },
          error => console.error(error))
    }

  }

  editando() {
    return !this.searching
  }

  consultando() {
    return this.searching
  }

  verTabla() {
    this.searching = true;
    this.disable = false;

  }

  verFormulario() {
    this.searching = false;
    this.nuevo();
    this.suscritoAComponenteCodigo = false;
  }

  onSubmit() {
    this.modulo = this.formMoudule.form.value;
    this.modulo.estado = this.modulo.estado == "" ? commonValues.ACTIVO_VALUE : this.modulo.estado;
    this.moduloService.guardar(this.modulo).subscribe(
      response => {
        this.modulo = response;
        this.searching = true;
        this.nuevo();
        this.notificationService.successMessage(commonMessage.REGISTRO_SATISFACTORIO);
        this.consultar();
      },
      error => {
        this.notificationService.errorMessage(commonMessage.REGISTRO_NO_EXITOSO);
      }
    );
    this.consultar();
  }

  updateFilter(event) {
    let val = event.target.value.toLowerCase();
    let colsAmt = this.cols.length;
    let keys = ['codigo', 'nombre'];
    this.rows = this.temp.filter(function (item) {
      for (let i = 0; i < colsAmt; i++) {
        if (item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 || !val) {
          return true;
        }
      }
    });
    this.table.offset = 0;
  }
  updatePageSize(value) {

    this.controls.pageSize = parseInt(value, 10);
    this.table.limit = this.controls.pageSize;
    this.table.offset = 0;
    this.controls.curPage = 0;
    this.consultar();
  }

  setPage(event) {
    this.controls.curPage = event.offset;
    this.controls.pageSize = event.pageSize;
    this.consultar();
  }

  nuevo() {
    this.modulo = new Modulo;
  }

  editar(id) {
    this.verFormulario();
    this.buscar(id, false);

  }
  consultar() {
    this.loadingIndicator = true;
    const x = new ParametrosModulo;
    x.size = this.controls.pageSize;
    x.page = this.controls.curPage;
    x.estado = this.estado.split(",");
    if (this.criterio != "") {
      if (this.criterio != commonValues.CRITERIO_VALUE) {
        if (this.campo == commonValues.CODIGO_VALUE) {
          x.codigo = this.criterio;
        } else if (this.campo == commonValues.NOMBRE_VALUE) {
          x.nombre = this.criterio;
        }
      }

      this.moduloService.listar(x).subscribe(
        response => {

          this.rows = response.content;
          this.temp = this.rows;
          this.loadingIndicator = false;
          this.controls.rowCount = response.totalElements;
          this.controls.curPage = response.number;
        },
        error => {
          this.notificationService.errorMessage(commonMessage.NO_SE_REALIZO_CONSULTA);
        }
      );
    }
  }
  buscar(id, deshabilitar, eliminar?) {
    this.moduloService.consultar(id).subscribe(
      response => {

        this.modulo = response;
        if (eliminar == null) {
          this.formMoudule.form.setValue(response);
          if (deshabilitar) {
            this.formMoudule.form.disable();
          }
        }
      },
      error => {
        this.notificationService.errorMessage(commonMessage.NO_SE_REALIZO_CONSULTA);
      }
    );
  }
  visualizar(id) {

    this.verFormulario();
    this.buscar(id, true);
    this.disable = true;
  }

  eliminar(id) {
    this.moduloService.eliminar(id).subscribe(
      response => {
        this.notificationService.successMessage(commonMessage.REGISTRO_ELIMINADO);
        this.consultar();
      },
      error => {
        this.notificationService.errorMessage(commonMessage.REGISTRO_NO_ELIMINADO);
      }
    );
    this.consultar();
  }

  consultarRelaciones(id) {
    let arrayTemp = [];

    this.recursoService.listar(commonOptions.consultaEstadoEstandar).subscribe(
      response => {
        for (let item of response.content) {

          if (item.modulo.id == id) {
            arrayTemp.push(item);
          }
        }
        this.sePuedeEliminar = arrayTemp.length > 0;
        this.confirmDialog = true;

      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );

  }

}
