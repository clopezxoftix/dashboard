import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FuenteDatoComponent } from './fuente-dato.component';

describe('FuenteDatoComponent', () => {
  let component: FuenteDatoComponent;
  let fixture: ComponentFixture<FuenteDatoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FuenteDatoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FuenteDatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
