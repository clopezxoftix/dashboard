import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FuenteDatoAtributoComponent } from './fuente-dato-atributo.component';

describe('FuenteDatoAtributoComponent', () => {
  let component: FuenteDatoAtributoComponent;
  let fixture: ComponentFixture<FuenteDatoAtributoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FuenteDatoAtributoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FuenteDatoAtributoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
