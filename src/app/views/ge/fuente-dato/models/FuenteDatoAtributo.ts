import { FuenteDato } from './FuenteDato';
export class FuenteDatoAtributo{
    id:number;
    fuenteDato: FuenteDato;
    codigo:string;
    nombre:string;
    estado:string;
    tipoDato:string;
    identificaRegistro:string;
    visible:string;
    permiteFiltrar:string;
    indice:number;
    documentacion:string;

    constructor(){

    }
  
}