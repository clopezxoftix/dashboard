import { Modulo } from "../../modulos/models/modulo";

export class FuenteDato{
 
    id:number;
    modulo:Modulo;
    codigo:string;
    nombre:string;
    estado:string;
    tipo:string;
    esquema:string;
    objeto:string;
    dataJson:string;
    requiereParametro:string;
    documentacion:string;
    
    constructor(){

    }

}
