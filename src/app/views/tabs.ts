import { ModulosComponent } from './ge/modulos/modulos.component';
import { FuentedatosComponent } from "./as/fuentedatos/fuentedatos.component";
import { ParametrosComponent } from './ge/parametros/parametros.component';
import { RecursosComponent } from "@app/views/as/recursos/recursos.component";
import { PrivilegiosComponent } from './as/privilegios/privilegios.component';
import { CuentasUsuarioComponent } from './as/cuentas-usuario/cuentas-usuario.component';
import { PerfilesComponent } from './as/perfiles/perfiles.component';
import { OpcionesMenuComponent } from './as/opciones-menu/opciones-menu.component';
import { ListaValoresComponent } from './ge/lista-valores/lista-valores.component';
import { NivelSeguridadComponent } from './as/nivel-seguridad/nivel-seguridad.component';
import { ObjetoNegocioComponent } from './ge/objeto-negocio/objeto-negocio.component';
import { TableroControlComponent } from './tc/tablero-control.component';

export const TABS: any = [
    
      {
        code: 'FD',
        component: FuentedatosComponent,
        title: "Fuente de datos"
      },
      {
        code: 'NS',
        component: NivelSeguridadComponent,
        title: "Niveles de seguridad"
      },
      {
        code:'RS',
        component: RecursosComponent,
        title:"Recursos"
        
      },{
        code: 'MS',
        component: ModulosComponent,
        title: "Módulos"
      },{
        code: 'PS',
        component: PrivilegiosComponent,
        title: "Privilegios"
      },
      {
        code: 'P',
        component: ParametrosComponent,
        title: "Parámetros"
      },
      {
        code: 'CU',
        component: CuentasUsuarioComponent,
        title: "Cuentas Usuario"
      },
      {
        code: 'PE',
        component: PerfilesComponent,
        title: "Perfiles"
      },
      {
        code: 'MO',
        component: OpcionesMenuComponent,
        title: "Menú de opciones"
	  },
      {
        code: 'LV',
        component: ListaValoresComponent,
        title: "Lista de Valores"
      },
      {
        code: 'ON',
        component: ObjetoNegocioComponent,
        title: "Objetos De Negocio"
      },
      {
        code: 'TC',
        component: TableroControlComponent,
        title: "Tablero de Control"
      }
     
]

export class TabUtil {
    static getComponent (tabs: any) {
        console.log("logging")
        return tabs.map(tab => tab.component)
    }
}
