import { Component, OnInit } from '@angular/core';
import { TABS } from '@app/views/tabs'
@Component({
  selector: 'navigation-tab',
  templateUrl: './navigation-tab.component.html',
  styleUrls: ['./navigation-tab.component.css']
})
export class NavigationTabComponent implements OnInit {
  currentTab = 'HOME';
   tabs = TABS;

  constructor() { }

  ngOnInit() {
  }


  selectTab(moduleCode: string) {
    this.currentTab = moduleCode
  }

}
