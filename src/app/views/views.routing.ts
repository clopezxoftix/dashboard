import { Routes, RouterModule } from "@angular/router";

export const routes: Routes = [
    {
        path: '', redirectTo: 'home', pathMatch: 'full'
      },
      {
          path: 'home',
          loadChildren: './home/home.module#HomeModule'
      },
    {
          path: 'as',
          loadChildren: './as/administracion.seguridad.module#AdministracionSeguridadModule'
      },/*,
      {
          path: 'ge',
          loadChildren: './ge/general.module#GeneralModule'
      },
      {
          path: 'go',
          loadChildren: './go/gestion-organizacional.module#GestionOrganizacionalModule'
      },*/
      {
          path: 'tc',
          loadChildren: './tc/tablero-control.module#TableroControlModule'
      }
      
];
export const routing = RouterModule.forChild(routes);
