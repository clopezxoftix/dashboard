import { NgModule } from "@angular/core";
import { routing } from "./views.routing";
import { CommonModule } from "@angular/common";
import { HeaderModule, NavigationModule, FooterComponent, RibbonComponent, ShortcutComponent, LayoutSwitcherComponent } from "@app/views/layout";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { UtilsModule } from "@app/shared/components/utils/utils.module";
import { TooltipModule, BsDropdownModule } from "ngx-bootstrap";
import { SharedModule } from "@app/shared/shared.module";
import { HomeModule } from "./home/home.module";
import { AdministracionSeguridadModule } from "./as/administracion.seguridad.module";
import { MenuBarModule } from "@app/shared/components/menu-bar/menu-bar.module";
import { NavigationTabComponent } from "./navigation-tab/navigation-tab.component";
import { MainLayoutComponent } from "./app-layouts/main-layout.component";
import { EmptyLayoutComponent } from "./app-layouts/empty-layout.component";
import { AuthLayoutComponent } from "./app-layouts/auth-layout.component";
import {PickListModule} from 'primeng/picklist';
import {TableroControlModule} from "./tc/tablero-control.module";


@NgModule({
    declarations: [
        NavigationTabComponent,
        MainLayoutComponent,
        EmptyLayoutComponent,
        AuthLayoutComponent
    ],
    imports: [
        routing,
        CommonModule,
        HeaderModule,
        NavigationModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        UtilsModule,
        TooltipModule,
        BsDropdownModule,
        SharedModule,
        MenuBarModule,
        NavigationModule,
        HomeModule,
        //GeneralModule,
        AdministracionSeguridadModule,
        PickListModule,
        //GestionOrganizacionalModule
        TableroControlModule

    ],
    exports: [
        NavigationTabComponent,
        MainLayoutComponent,
        EmptyLayoutComponent,
        AuthLayoutComponent,
        RouterModule,
        HeaderModule,
        NavigationModule,
        FooterComponent,
        RibbonComponent,
        ShortcutComponent,
        LayoutSwitcherComponent
    ]
})
export class ViewsModule{}
