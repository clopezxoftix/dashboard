import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter'
})

export class FilterPipe implements PipeTransform {

    transform(value: any, arg: any): any {
        const resultWidget = [];

        if(!arg || arg.length < 3) return value;

        for(const widget of value){
            if(widget.nombre.toLowerCase().indexOf(arg.toLowerCase()) > -1){
                resultWidget.push(widget);
            };
        };
        return resultWidget;
    }

}