import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableroControlComponent } from './tablero-control.component';
import { PaletaHerramientasComponent } from './paleta-herramientas/paleta-herramientas.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FilterPipe } from './pipes/filter.pipe'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop'
import { GraficasComponent } from './paleta-herramientas/graficas/graficas.component'
import { SharedModule } from '@app/shared/shared.module';
import { MorrisGraphModule } from '@app/shared/components/graphs/morris-graph/morris-graph.module';
import { ChartsModule } from 'ng2-charts';
import { TimeagoModule } from 'ngx-timeago';

@NgModule({
  declarations: [
    TableroControlComponent, 
    PaletaHerramientasComponent, 
    FilterPipe,
    GraficasComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    DragDropModule,
    SharedModule,
    MorrisGraphModule,
    ChartsModule,
    ReactiveFormsModule,
    TimeagoModule.forRoot()
  ],
  exports: [
    TableroControlComponent
  ],
  entryComponents: [
    TableroControlComponent,
  ],
  bootstrap: [TableroControlComponent]
})
export class TableroControlModule { }
