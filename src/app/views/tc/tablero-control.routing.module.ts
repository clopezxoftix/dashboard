import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { TableroControlComponent } from './tablero-control.component';

const routes: Routes = [

    {
        path: '',  pathMatch: 'full',
        component:TableroControlComponent
	}
	
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TableroControlRoutingModule {}
