import { Component, OnInit, ElementRef } from '@angular/core';


@Component({
  selector: 'tablero-control',
  templateUrl: './tablero-control.component.html',
  styleUrls: ['./tablero-control.component.css']
})
export class TableroControlComponent implements OnInit {
  
  public escalaLienzo: String;
  public contenidoLienzo: ElementRef;

  constructor() { }

  onDropped(event){
    console.log(event);
  }

  ngOnInit() {
  }

}
