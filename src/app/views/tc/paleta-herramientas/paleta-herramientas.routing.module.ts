import { NgModule } from "@angular/core";
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from "@angular/router";
import { PaletaHerramientasComponent } from './paleta-herramientas.component';

const routes: Routes = [

    {
        path: '',  pathMatch: 'full',
        component:PaletaHerramientasComponent
    }
	
]

@NgModule({
    imports: [RouterModule.forChild(routes), FormsModule],
    exports: [RouterModule]
})
export class PaletaHerramientasRoutingModule {}