import {
  Component,
  OnInit,
  Input,
  ElementRef,
  EventEmitter,
  ViewChild,
  QueryList,
} from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { NgbAccordionConfig } from "@ng-bootstrap/ng-bootstrap";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { ResizedEvent } from "angular-resize-event";

import * as printJS from "print-js";
import * as timeago from "timeago.js";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";

@Component({
  selector: "paleta-herramientas",
  templateUrl: "./paleta-herramientas.component.html",
  styleUrls: ["./paleta-herramientas.component.css"],
})
export class PaletaHerramientasComponent implements OnInit {
  /* Array de imagenes subidas */
  form: FormGroup;
  // Formulario
  formConfGraph: FormGroup;
  public user: any;
  // JSON de elementos
  public categorias: Array<any>;
  public imagenes = [];
  // Pipes busqueda de elementos
  public filterWidget: String;
  public optConf: boolean;
  // Array: contiene los elementos y graficos.
  public charts: Array<any> = [];
  public chartActual: any;
  public nombreActual: String;
  // Redimensionar
  public width: number;
  public height: number;
  //configuracion zoom de lienzo
  @Input() escalaLienzo: String;
  /* @Output() contenidoLienzo = new EventEmitter<ElementRef>(); */
  @ViewChild(ElementRef, { static: false }) contenidoLienzo: ElementRef;
  @ViewChild(ElementRef, { static: false }) pdf: ElementRef;
  // Estado vistas modal
  public optionConfGraph = false;
  public optionConfDate = false;
  public optionConfFormt = false;
  //JSON gráficos
  public configuracion: any;
  /* Lista de imagenes */
  public imageSrc = [];
  /* Info imagen seleccionada */
  public infoImgSelect: any;
  /* API */
  private urlapi = "https://api.covid19api.com/summary";
  public apiObject: any;
  public listKey: any = [
    "Country",
    "CountryCode",
    "NewConfirmed",
    "TotalConfirmed",
  ];
  public listSeries: any = [];
  public listValores: any = [];

  constructor(
    private _config: NgbAccordionConfig,
    private formBuilder: FormBuilder,
    private httpClient: HttpClient
  ) {
    this.buildFormGraph();
    this.getDataApi();

    this.filterWidget = "";
    this.categorias = [
      {
        id: "1",
        nombre: "Gráficos",
        icono: "https://image.flaticon.com/icons/svg/897/897459.svg",
        elementos: [
          {
            id: "1",
            nombre: "Linea",
            icono: "https://image.flaticon.com/icons/png/512/559/559039.png",
            parametros: {
              chartData: [
                { data: [10, 72, 12, 55, 60, 75], label: "Default A" },
                { data: [85, 55, 50, 88, 15, 32], label: "Default B" },
                { data: [15, 25, 90, 12, 25, 85], label: "Default C" },
              ],
              chartLabels: [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
              ],
              chartOptions: {
                responsive: true,
                title: {
                  display: true,
                  fontColor: "rgb(0,0,0,0.6)",
                  text: "Title Default",
                },
              },
              chartColors: [
                {
                  borderColor: "#e63946",
                  backgroundColor: "rgba(230,57,70,0.7)",
                  borderWidth: "1",
                },
                {
                  borderColor: "#f1faee",
                  backgroundColor: "rgba(241,250,238,0.7)",
                  borderWidth: "1",
                },
                {
                  borderColor: "#a8dadc",
                  backgroundColor: "rgba(168,218,220,0.7)",
                  borderWidth: "1",
                },
              ],
              chartLegend: true,
              chartPlugins: [],
              chartType: "line",
              fondo: "white",
            },
          },
          {
            id: "2",
            nombre: "Barrra",
            icono: "https://image.flaticon.com/icons/svg/900/900772.svg",
            parametros: {
              chartData: [
                { data: [65, 59, 80, 81, 56, 55, 40], label: "Series A" },
                { data: [28, 48, 40, 19, 86, 27, 90], label: "Series B" },
              ],
              chartLabels: [
                "2006",
                "2007",
                "2008",
                "2009",
                "2010",
                "2011",
                "2012",
              ],
              chartOptions: {
                responsive: true,
                scales: { xAxes: [{}], yAxes: [{}] },
                plugins: { datalabels: { anchor: "end", algin: "end" } },
              },
              chartColors: [
                {
                  borderColor: "black",
                  backgroundColor: "rgba(255,255,0,0.28)",
                },
              ],
              chartLegend: true,
              chartPlugins: [],
              chartType: "bar",
              fondo: "white",
            },
          },
          {
            id: "3",
            nombre: "Dona",
            icono: "https://image.flaticon.com/icons/svg/481/481818.svg",
            parametros: {
              chartData: [
                { data: [59, 80, 81] },
                { data: [28, 48, 40] },
                { data: [65, 59, 80] },
              ],
              chartLabels: [
                "Download Sales",
                "In-Store Sales",
                "Mail-Order Sales",
              ],
              chartOptions: { responsive: true },
              chartColors: [],
              chartLegend: true,
              chartPlugins: [],
              chartType: "doughnut",
            },
          },
          {
            id: "4",
            nombre: "Radar",
            icono: "https://image.flaticon.com/icons/png/512/571/571745.png",
            parametros: {
              chartData: [
                { data: [65, 59, 90, 81, 56, 55, 40], label: "Series A" },
                { data: [28, 48, 40, 19, 96, 27, 100], label: "Series B" },
              ],
              chartLabels: [
                "Eating",
                "Drinking",
                "Sleeping",
                "Designing",
                "Coding",
                "Cycling",
                "Running",
              ],
              chartOptions: { responsive: true },
              chartColors: [],
              chartLegend: true,
              chartPlugins: [],
              chartType: "radar",
            },
          },
          {
            id: "5",
            nombre: "Circular",
            icono: "https://image.flaticon.com/icons/svg/886/886170.svg",
            parametros: {
              chartData: [{ data: [300, 500, 100] }],
              chartLabels: [
                ["Download", "Sales"],
                ["In", "Store", "Sales"],
                "Mail Sales",
              ],
              chartOptions: { responsive: true, legend: { position: "top" } },
              chartColors: [],
              chartLegend: true,
              chartPlugins: [],
              chartType: "pie",
            },
          },
          {
            id: "6",
            nombre: "Área Polar",
            icono: "https://image.flaticon.com/icons/svg/554/554585.svg",
            parametros: {
              chartData: [{ data: [300, 500, 100, 40, 120] }],
              chartLabels: [
                "Download Sales",
                "In-Store Sales",
                "Mail Sales",
                "Telesales",
                "Corporate Sales",
              ],
              chartOptions: { responsive: true, legend: { position: "top" } },
              chartColors: [],
              chartLegend: true,
              chartPlugins: [],
              chartType: "polarArea",
            },
          },
          {
            id: "7",
            nombre: "Burbujas",
            icono: "https://image.flaticon.com/icons/svg/734/734531.svg",
            parametros: {
              chartData: [
                {
                  data: [
                    { x: 10, y: 10, r: 10 },
                    { x: 15, y: 5, r: 15 },
                    { x: 26, y: 12, r: 23 },
                    { x: 7, y: 8, r: 8 },
                  ],
                  label: "Series A",
                  backgroundColor: "green",
                  borderColor: "blue",
                  hoverBackgroundColor: "purple",
                  hoverBorderColor: "red",
                },
              ],
              chartLabels: [],
              chartOptions: {
                responsive: true,
                scales: {
                  xAxes: [{ ticks: { min: 0, max: 30 } }],
                  yAxes: [{ ticks: { min: 0, max: 30 } }],
                },
              },
              chartColors: [
                {
                  backgroundColor: [
                    "red",
                    "green",
                    "blue",
                    "purple",
                    "yellow",
                    "brown",
                    "magenta",
                    "cyan",
                    "orange",
                    "pink",
                  ],
                },
              ],
              chartLegend: true,
              chartPlugins: [],
              chartType: "bubble",
            },
          },
          {
            id: "8",
            nombre: "Dispersión",
            icono:
              "https://www.flaticon.com/premium-icon/icons/svg/2272/2272127.svg",
            parametros: {
              chartData: [
                {
                  data: [
                    { x: 1, y: 1 },
                    { x: 2, y: 3 },
                    { x: 3, y: -2 },
                    { x: 4, y: 4 },
                    { x: 5, y: -3 },
                  ],
                  label: "Series A",
                  pointRadius: 10,
                },
              ],
              chartLabels: [
                "Eating",
                "Drinking",
                "Sleeping",
                "Designing",
                "Coding",
                "Cycling",
                "Running",
              ],
              chartOptions: { responsive: true },
              chartColors: [],
              chartLegend: true,
              chartPlugins: [],
              chartType: "scatter",
            },
          },
        ],
      },
      {
        id: "2",
        nombre: "Widgets",
        icono: "https://image.flaticon.com/icons/svg/3022/3022220.svg",
        elementos: [
          {
            id_cat: "9",
            nombre: "widget",
            nombre_corto: "line-basic",
            icono: "https://image.flaticon.com/icons/svg/897/897459.svg",
          },
          {
            id_cat: "10",
            nombre: "widget",
            nombre_corto: "line-basic",
            icono: "https://image.flaticon.com/icons/svg/886/886170.svg",
          },
        ],
      },
      {
        id: "3",
        nombre: "Tablas",
        icono: "https://image.flaticon.com/icons/svg/1827/1827163.svg",
        elementos: [
          {
            id: "13",
            nombre: "widget",
            icono: "https://image.flaticon.com/icons/svg/897/897459.svg",
          },
          {
            id: "14",
            nombre: "widget",
            icono: "https://image.flaticon.com/icons/svg/886/886170.svg",
          },
        ],
      },
    ];

    this.user = {
      nombre: "",
      tema: "",
      paletaColor: "",
    };

    if (this.user.paletaColor == "Dolphin") {
      console.log("ewreffdf");
    }
  }

  ngOnInit() {}

  agregarWidget(elemento) {
    console.log(this.charts);
    this.charts.push({
      codigo: this.charts.length + 1,
      nombre: "Grafico #" + (1 + this.charts.length),
      parametros: Object.assign({}, elemento.parametros),
      zIndex: this.charts.length + 1,
    });
    this.chartActual = this.charts[this.charts.length - 1];
  }

  onResized(event: ResizedEvent) {
    this.width = event.newWidth;
    this.height = event.newHeight;
  }

  obtenerElementoActual(elemento_actual) {
    this.nombreActual = elemento_actual.nombre;
    this.chartActual = elemento_actual;
  }

  modalConfGraph() {
    this.optionConfGraph = true;
    this.optionConfDate = false;
    this.optionConfFormt = false;
  }

  modalConfData() {
    this.optionConfGraph = false;
    this.optionConfDate = true;
    this.optionConfFormt = false;
  }

  modalConfFormt() {
    this.optionConfGraph = false;
    this.optionConfDate = false;
    this.optionConfFormt = true;
  }

  onSubmit() {}

  copiaGrafico(elemento) {
    this.charts.push({
      codigo: this.charts.length + 1,
      nombre: "Grafico #" + (1 + this.charts.length),
      parametros: Object.assign({}, elemento.parametros),
      zIndex: elemento.zIndex + 1,
    });
  }

  borrarGrafico(elemento) {
    this.charts.splice(this.charts.indexOf(elemento), 1);
    this.charts.forEach((chart, index) => {
      chart.zIndex = index + 1;
    });
    console.log(this.charts);
  }

  configGrafico(color) {
    console.log(this.chartActual);
    if (color == "Dolphin") {
      this.charts[this.chartActual.codigo - 1].parametros.chartColors = [
        {
          borderColor: "#023e8a",
          backgroundColor: "rgba(2,62,138,0.7)",
          borderWidth: "1",
        },
        {
          borderColor: "#0096c7",
          backgroundColor: "rgba(0,150,199,0.7)",
          borderWidth: "1",
        },
        {
          borderColor: "#48cae4",
          backgroundColor: "rgba(72,202,228,0.7)",
          borderWidth: "1",
        },
      ];
    }
    if (color == "Desert") {
      this.charts[this.chartActual.codigo - 1].parametros.chartColors = [
        {
          borderColor: "#f77f00",
          backgroundColor: "rgba(247,127,0,0.7)",
          borderWidth: "1",
        },
        {
          borderColor: "#fcbf49",
          backgroundColor: "rgba(252,191,73,0.7)",
          borderWidth: "1",
        },
        {
          borderColor: "#eae2b7",
          backgroundColor: "rgba(234,226,183,0.7)",
          borderWidth: "1",
        },
      ];
    }
    if (color == "Fabric") {
      this.charts[this.chartActual.codigo - 1].parametros.chartColors = [
        {
          borderColor: "#9b5de5",
          backgroundColor: "rgba(155,93,189,0.7)",
          borderWidth: "1",
        },
        {
          borderColor: "#f15bb5",
          backgroundColor: "rgba(241,91,229,0.7)",
          borderWidth: "1",
        },
        {
          borderColor: "#fee440",
          backgroundColor: "rgba(254,228,64,0.7)",
          borderWidth: "1",
        },
      ];
    }
    if (color == "Dark") {
      this.charts[this.chartActual.codigo - 1].parametros.chartOptions = {
        responsive: true,
        title: { display: true, fontColor: "white", text: "Title Default" },
        legend: { labels: { fontColor: "rgba(255,255,255,0.6)" } },
        scales: {
          xAxes: [
            {
              gridLines: { color: "black" },
              ticks: { fontColor: "rgba(255,255,255,0.6)" },
            },
          ],
          yAxes: [
            {
              gridLines: { color: "black" },
              ticks: { fontColor: "rgba(255,255,255,0.6)" },
            },
          ],
        },
      };
      this.charts[this.chartActual.codigo - 1].parametros.fondo = "#212529";
    }
    if (color == "Light") {
      this.charts[this.chartActual.codigo - 1].parametros.chartOptions = {
        responsive: true,
        title: { display: true, fontColor: "black", text: "Title Default" },
      };
      this.charts[this.chartActual.codigo - 1].parametros.fondo = "white";
    }
  }

  posGrafico(elemento) {
    this.charts.forEach((chart, index) => {
      if (index > this.charts.indexOf(elemento)) {
        [elemento.zIndex, chart.zIndex] = [chart.zIndex, elemento.zIndex];
      }
    });
    this.charts.sort(function (a, b) {
      return a.zIndex - b.zIndex;
    });
  }

  mostrarConf() {
    this.optConf = true;
  }

  ocultarConf() {
    this.optConf = false;
  }

  exportarPDF() {
    printJS({
      printable: "pdf",
      type: "html",
      documentTitle: "Prueba - Tablero de Control",
      targetStyles: ["*"],
    });
  }

  zoomLienzo(escala: String) {
    this.escalaLienzo = escala;
  }

  onFileChange(event: { target: { files: any[] } }): void {
    const uploadedFiles = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(uploadedFiles);
    reader.onload = (e) =>
      this.imageSrc.push({
        src: reader.result,
        name: uploadedFiles.name,
        size: uploadedFiles.size,
        type: uploadedFiles.type,
        lastModified: timeago.format(uploadedFiles.lastModified),
      });
    console.log(this.imageSrc);
  }
  /* Insertando imagenes */
  insertInfotImg(img) {
    this.infoImgSelect = { src: img.src, name: img.name };
  }

  insertImg() {
    this.imagenes.push({
      src: this.infoImgSelect.src,
      name: this.infoImgSelect.name,
    });
  }

  /* Formulario de configuración de grafica. */
  private buildFormGraph() {
    this.formConfGraph = this.formBuilder.group({
      nombre: ["", [Validators.maxLength(200)]],
      fondo: [""],
      colores: [""],
    });

    this.formConfGraph.valueChanges.subscribe((value) => {
      if (value.fondo !== "") {
        this.configGrafico(value.fondo);
      }
      if (value.colores !== "") {
        this.configGrafico(value.colores);
      }
    });
  }

  save(event: Event) {
    event.preventDefault();
    const value = this.formConfGraph.value;
    console.log(value);
  }

  /* Datos de la API */
  private getDataApi() {
    this.httpClient.get(this.urlapi).subscribe((apiData) => {
      this.apiObject = apiData["Countries"];
      console.log(this.apiObject);
    });
  }

  /* Eventos arrastrar y soltar datos  */
  dragoverRow(e) {
    e.preventDefault();
  }

  dropRow(event, cnt) {
    const row = event.dataTransfer.getData("row");
    if (cnt == "series") {
      this.listSeries.push(row);
      this.updateDataTabLine("series", row);
    }
    if (cnt == "valores") {
      this.listValores.push(row);
      this.updateDataTabLine("valores", this.listValores);
    }
  }

  dragRow(event) {
    event.dataTransfer.setData("row", event.target.innerText);
  }

  /* modificar datos de graficos */
  updateDataTabLine(camp, row) {
    let series = [];
    let upData = [];
    let valores = [];
    let count = 0;

    if (camp == "series") {
      console.log("entrraa");
      this.apiObject.forEach((element) => {
        if (count < 20) {
          series.push(element[row]);
        }
        count += 1;
      });
      this.charts[this.chartActual.codigo - 1].parametros.chartLabels = series;
    }
    if (camp == "valores") {
      row.forEach((element) => {
        this.apiObject.forEach((element2) => {
          if (count < 20) {
            valores.push(element2[element]);
          }
          count += 1;
        });
        upData.push({ data: valores, label: element });
      });

      this.charts[this.chartActual.codigo - 1].parametros.chartData = upData;
    }
  }

  deleteRow(item) {
    if (this.listSeries.indexOf(item) != -1) {
      this.listSeries.splice(this.listSeries.indexOf(item), 1);
    }
    if (this.listValores.indexOf(item) != -1) {
      this.listValores.splice(this.listValores.indexOf(item), 1);
    }
    console.log(
      "delete row:",
      this.listValores.indexOf(item),
      this.listSeries.indexOf(item)
    );
  }
}
//comment
