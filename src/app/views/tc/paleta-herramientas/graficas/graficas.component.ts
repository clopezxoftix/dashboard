import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label, BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'grafica',
  templateUrl: './graficas.component.html',
  styleUrls: ['./graficas.css']
})
export class GraficasComponent implements OnInit {
  @ViewChild(BaseChartDirective,  {static: false}) chart: BaseChartDirective;

  // Parametros de Widget
  @Input()
  datasets: ChartDataSets[];
  @Input()
  labels: Label[];
  @Input()
  options: any;
  @Input()
  colors: Color[];
  @Input()
  legend: boolean;
  @Input()
  plugins: Array<any>;
  @Input()
  chartType: String;
  @Input()
  fondo: String;

  constructor() {
    
  }

  ngOnInit() {
    
  }



}
