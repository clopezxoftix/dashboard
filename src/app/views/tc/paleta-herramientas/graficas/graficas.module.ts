import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { GraficasComponent } from './graficas.component';
import { NgbAccordionModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { SharedModule } from '@app/shared/shared.module';
import { MorrisGraphModule } from '@app/shared/components/graphs/morris-graph/morris-graph.module';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [GraficasComponent],
  imports: [
    CommonModule,
    NgbAccordionModule,
    NgbModule,
    BrowserModule,
    DragDropModule,
    SharedModule,
    MorrisGraphModule,
    ChartsModule
  ],
  exports: [
    GraficasComponent
  ],
  entryComponents: [
    GraficasComponent
  ],
  bootstrap: [GraficasComponent]
})
export class GraficasModule { }
