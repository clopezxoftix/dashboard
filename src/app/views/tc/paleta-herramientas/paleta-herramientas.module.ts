import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { PaletaHerramientasComponent } from './paleta-herramientas.component';
import { NgbAccordionModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { GraficasComponent } from './graficas/graficas.component'
import { SharedModule } from '@app/shared/shared.module';
import { MorrisGraphModule } from '@app/shared/components/graphs/morris-graph/morris-graph.module';
import { ChartsModule } from 'ng2-charts';
import { FilterPipe } from '../pipes/filter.pipe'
import {  FormsModule, ReactiveFormsModule } from '@angular/forms'; 
import { UploadService } from '../service/upload.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [PaletaHerramientasComponent, GraficasComponent, FilterPipe],
  imports: [
    CommonModule,
    NgbAccordionModule,
    NgbModule,
    BrowserModule,
    DragDropModule,
    SharedModule,
    MorrisGraphModule,
    ChartsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  exports: [
    PaletaHerramientasComponent
  ],
  entryComponents: [
    PaletaHerramientasComponent,
  ],
  bootstrap: [PaletaHerramientasComponent], 
  providers: [UploadService]
})
export class PaletaHerramientaslModule { }
