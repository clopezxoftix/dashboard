import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { GenericObject } from './models/genericObject';
import { commonValues } from '@commons/commonValues';
import { PickList } from 'primeng/picklist';

@Component({
  selector: 'picklist',
  templateUrl: './picklist.component.html',
  styleUrls: ['./picklist.component.css']
})
export class PicklistComponent implements OnInit {

  @ViewChild('picklist', { 'static': false }) pickList: PickList;

  arrayFirst: GenericObject[] = new Array;
  arraySecond: GenericObject[] = new Array;
  suscritoAcomponente = false;
  lblActivo = commonValues.ACTIVO_LABEL;
  lblInactivo = commonValues.INACTIVO_LABEL;
  lblElementosDisponibles = commonValues.ELEMENTOS_DISPONIBLES_LABEL;
  lblElementosAsociados = commonValues. ELEMENTOS_ASOCIADOS_LABEL;

  @Input()
  set arrayUno(array) {
    this.arrayFirst = array;
  }

  @Input()
  set arrayDos(array) {
    this.arraySecond = array;
  }

  constructor() { }

  ngOnInit() {
    this.suscritoAcomponente = false;
  }

}
