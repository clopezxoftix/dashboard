export class OpcionesMenu {
    constructor() { }
    id: number;
    idPredecesor:number;
    codigo: String;
    nombre: String;
    posicionHermanos:number;
    lanzaEjecutable:String;
    documentacion: String;
}