import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OpcionesMenuComponent } from './opciones-menu.component';
import { GeneralFormComponentModule } from '@app/shared/components/general-form/general-form-component.module';
import { FormsModule } from '@angular/forms';
import { NgxDatatableModule, DataTableHeaderCellComponent } from '@swimlane/ngx-datatable';
import {TreeModule} from 'primeng/tree';

@NgModule({
  imports: [
    CommonModule, GeneralFormComponentModule, FormsModule, NgxDatatableModule, DataTableHeaderCellComponent, TreeModule
  ],
  declarations: [OpcionesMenuComponent],
  exports: [GeneralFormComponentModule]
  
})
export class OpcionesMenuModule { }
