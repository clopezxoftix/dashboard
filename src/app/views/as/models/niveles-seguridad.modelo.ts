export class NivelSeguridad{
    constructor(){}
    id: number;
    codigo: String;
    nombre: String;
    longitudMinima: number;
    controlVencimiento: String;
    factorVencimiento: number;
    controlReutilizacion: String;
    factorReutilizacion: number;
    controlSimilitud: String;
    factorSimilitud: number;
    controlReintentos: String;
    factorReintentos: number;
    controlCaracteres: String;
    requiereMayusculas: String;
    requiereMinusculas: String;
    requiereNumeros: String;
    requiereEspeciales: String;
    documentacion: String;
}