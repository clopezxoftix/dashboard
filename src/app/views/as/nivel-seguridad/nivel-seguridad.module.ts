import { GeneralFormComponentModule } from '@app/shared/components/general-form/general-form-component.module';
import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { NivelSeguridadRoutingModule } from './nivel-seguridad.routing.module';
import { SharedModule } from 'primeng/components/common/shared';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BlockUIModule } from 'ng-block-ui';
import { NgxDatatableModule, DataTableHeaderCellComponent } from '@swimlane/ngx-datatable';
import { NivelSeguridadComponent } from './nivel-seguridad.component';

@NgModule({

    imports: [
        NivelSeguridadRoutingModule,
        GeneralFormComponentModule, 
        CommonModule,
        FormsModule,
        NgxDatatableModule,
        DataTableHeaderCellComponent,
        SharedModule, 
        BlockUIModule.forRoot(),
        ReactiveFormsModule
    
    ],
    declarations: [ NivelSeguridadComponent ],
    exports: [GeneralFormComponentModule]
})
export class NivelSeguridadModule {}