
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { NivelSeguridadComponent } from './nivel-seguridad.component';


const routes: Routes = [
    
    {
        path: '', component: NivelSeguridadComponent , pathMatch: 'full'
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class NivelSeguridadRoutingModule {}