export class NivelSeguridad{
    constructor(){}
    id: number;
    codigo: String;
    nombre: String;
    estado: string;
    longitudMinima: number;
    controlReutilizacion: String;
    factorReutilizacion: number;
    controlSimilitud: String;
    factorSimilitud: number;
    controlCaracteres: String;
    requiereMayusculas: String;
    requiereMinusculas: String;
    requiereNumeros: String;
    requiereEspeciales: String;
    controlVencimiento: String;
    factorVencimiento: number
    controlReintentos: String;
    factorReintentos: number;;
    factorBloqueo: number;
    documentacion: String;
}