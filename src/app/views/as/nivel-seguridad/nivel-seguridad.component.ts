import { Component, OnInit, ViewChild } from '@angular/core';
import { commonValues } from '@commons/commonValues';
import { commonMessage } from '@commons/commonMessages';
import { GeneralFormComponent } from '@app/shared/components/general-form/general-form.component';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NivelSeguridad } from './models/nivel-seguridad';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { NotificationService } from '@app/core/services';
import { CuentasUsuarioService } from '@app/core/services/as/cuentas-usuario.service';
import { FormBuilder, Validators } from '@angular/forms';
import { NivelSeguridadService } from '@app/core/services/as/nivel-seguridad.service';
import { TypeElement } from '@app/shared/components/general-form/types/TypeElement';
import { commonOptions } from '@commons/commonOptions';
import { ParametrosNivelSeguridad } from './models/paramentros-nivel-seguridad';


@Component({
  selector: 'nivel-seguridad',
  templateUrl: './nivel-seguridad.component.html',
  styleUrls: ['./nivel-seguridad.component.css']
})
export class NivelSeguridadComponent implements OnInit {

  @ViewChild(GeneralFormComponent, { 'static': false }) afNivelSeguridad: GeneralFormComponent
  @ViewChild(DatatableComponent, { 'static': false }) table: DatatableComponent;

  suscritoAComponenteCodigo: any = false;
  disable: any;
  formNivelSeguridad: any;
  formCriterio: any;
  nivelSeguridad: NivelSeguridad;
  accion = "";
  suscrito = false;
  @BlockUI('formConsulta') blockUIConsulta: NgBlockUI;
  @BlockUI('formRegistro') blockUIRegistro: NgBlockUI;

  /* Inicializacion de filtro de busqueda */
  campo = commonValues.CODIGO_VALUE;
  estado = commonValues.ACTIVO_VALUE;
  criterio = "";

  /*Variable para el control de apertura del formulario */
  searching = true;
  criterioRequerido = false;
  opcionesEstado = commonOptions.tiposEstado;
  opcionesSiNo = commonOptions.siNo;
  estadoMensaje= "";

  /* Inicializacion de filtro de busqueda */
  lblConsulta = commonValues.CONSULTA_LABEL;
  lblNivelesSeguridad = commonValues.NIVELES_SEGURIDAD_LABEL;
  lblCampos = commonValues.CAMPOS_LABEL;
  lblEstado = commonValues.ESTADO_LABEL;
  lblActivo = commonValues.ACTIVO_LABEL;
  lblInactivo = commonValues.INACTIVO_LABEL;
  lblTodos = commonValues.TODOS_LABEL;
  lblCriterioBusqueda = commonValues.CRITERIO_BUSQUEDA_LABEL;
  lblBuscar = commonValues.BUSCAR_LABEL;
  lblAgregar = commonValues.AGREGAR_LABEL;
  lblFiltroTabla = commonValues.FILTRO_TABLA_LABEL;
  lblAcciones = commonValues.ACCIONES_LABEL;
  lblVer = commonValues.VER_LABEL;
  lblEditar = commonValues.EDITAR_LABEL;
  lblEliminar = commonValues.ELIMINAR_LABEL;
  lblSi = commonValues.SI_LABEL;
  lblNo = commonValues.NO_LABEL;
  lblCodigo = commonValues.CODIGO_LABEL;
  lblNombre = commonValues.NOMBRE_LABEL;
  lblNivelSeguriadad = commonValues.NIVEL_SEGURIDAD_LABEL;
  lblCancelar = commonValues.CANCELAR_LABEL;
  lblGuardar = commonValues.GUARDAR_LABEL;
  msgCriterioObligatorio = commonMessage.CRITERIO_OBLIGATORIO;
  msgDeseaEliminar = commonMessage.DESEA_ELIMINAR_REGISTRO;
  msgNoSePuedeEliminar = commonMessage.NO_SE_PUEDE_ELIMINAR;


  /* Variables relacionadas a la tabla de consulta */
  controls: any = {
    pageSize: 10,
    filter: '',
    rowCount: 0,
    curPage: 0,
    offset: 0
  }

  temp = [];
  rows = [];
  loadingIndicator = false;
  reorderable = true;
  messages = {
    emptyMessage: commonMessage.NO_EXISTE_INFO,
    totalMessage: commonMessage.REGISTROS_TOTALES,
  };

  confirmDialog = false;
  sePuedeEliminar: boolean;

  constructor(private nivelSeguridadService: NivelSeguridadService,
    private notificationService: NotificationService,
    private cuentaUsuarioService: CuentasUsuarioService,
    private formBuilder: FormBuilder) {

  }

  ngOnInit() {
    this.formCriterio = this.formBuilder.group({
      criterio: ['', Validators.required]
    });
    this.formNivelSeguridad = this.createForm();

  }
  createForm(): any {
    return {
      header: {
        label: commonValues.DATOS_BASICOS_LABEL
      },
      fields: {
        id: {},
        row: {
          row: [
            {
              key: "codigo",
              label: commonValues.CODIGO_LABEL,
              typeElement: TypeElement.TEXT,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
              placeholder: commonMessage.INGRESE_CODIGO
            },
            {
              key: "nombre",
              label: commonValues.NOMBRE_LABEL,
              typeElement: TypeElement.TEXT,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
              placeholder: commonMessage.INGRESE_NOMBRE
            },
            {
              key: "estado",
              label: commonValues.ESTADO_LABEL,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesEstado,
              default: this.opcionesEstado[0]
            }
          ]
        },
        row2: {
          row: [
            {
              key: "longitudMinima",
              label: commonValues.LONGITUD_MINIMA_LABEL,
              typeElement: TypeElement.NUMBER,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO,
              },
              placeholder: commonMessage.INGRESE_VALOR,
            },
            {
              key: "controlReutilizacion",
              label: commonValues.CONTROL_REUTILIZACION_LABEL,
              typeElement: { typeElement: TypeElement.BUTTON_GROUP },
              options: this.opcionesSiNo,
              default: this.opcionesSiNo[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO,
              },
            },
            {
              key: "factorReutilizacion",
              label: commonValues.FACTOR_REUTILIZACION_LABEL,
              typeElement: TypeElement.NUMBER,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO,
              },
              placeholder: commonMessage.INGRESE_VALOR
            }
          ]
        },
        row3: {
          row: [
            {
              key: "controlSimilitud",
              label: commonValues.CONTROL_SIMILITUD_LABEL,
              typeElement: { typeElement: TypeElement.BUTTON_GROUP },
              options: this.opcionesSiNo,
              default: this.opcionesSiNo[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {
              key: "factorSimilitud",
              label: commonValues.FACTOR_SIMILITUD_LABEL,
              typeElement: TypeElement.NUMBER,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO,
              },
              placeholder: commonMessage.INGRESE_VALOR
            },
            {
              key: "controlCaracteres",
              label: commonValues.CONTROL_CARACTERES_LABEL,
              typeElement: { typeElement: TypeElement.BUTTON_GROUP },
              options: this.opcionesSiNo,
              default: this.opcionesSiNo[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
          ]
        },
        row4: {
          row: [
            {
              key: "requiereMayusculas",
              label: commonValues.MAYUSCULAS_LABEL,
              typeElement: { typeElement: TypeElement.BUTTON_GROUP },
              options: this.opcionesSiNo,
              default: this.opcionesSiNo[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {
              key: "requiereMinusculas",
              label: commonValues.MINUSCULAS_LABEL,
              typeElement: { typeElement: TypeElement.BUTTON_GROUP },
              options: this.opcionesSiNo,
              default: this.opcionesSiNo[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {
              key: "requiereNumeros",
              label: commonValues.NUMEROS_LABEL,
              typeElement: { typeElement: TypeElement.BUTTON_GROUP },
              options: this.opcionesSiNo,
              default: this.opcionesSiNo[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
          ]
        },
        row5: {
          row: [
            {
              key: "requiereEspeciales",
              label: commonValues.ESPECIALES_LABEL,
              typeElement: { typeElement: TypeElement.BUTTON_GROUP },
              options: this.opcionesSiNo,
              default: this.opcionesSiNo[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {
              key: "controlVencimiento",
              label: commonValues.CONTROL_VENCIMIENTO_LABEL,
              typeElement: { typeElement: TypeElement.BUTTON_GROUP },
              options: this.opcionesSiNo,
              default: this.opcionesSiNo[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {
              key: "factorVencimiento",
              label: commonValues.FACTOR_VENCIMIENTO_LABEL,
              typeElement: TypeElement.NUMBER,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO,
              },
              placeholder: commonMessage.INGRESE_VALOR
            },
          ]
        },
        row6: {
          row: [

            {
              key: "controlReintentos",
              label: commonValues.CONTROL_REINTENTOS_LABEL,
              typeElement: { typeElement: TypeElement.BUTTON_GROUP },
              options: this.opcionesSiNo,
              default: this.opcionesSiNo[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }

            },
            {
              key: "factorReintentos",
              label: commonValues.FACTOR_REINTENTOS_LABEL,
              typeElement: TypeElement.NUMBER,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO,
              },
              placeholder: commonMessage.INGRESE_VALOR

            },
            {
              key: "factorBloqueo",
              label: commonValues.FACTOR_BLOQUEO_LABEL,
              typeElement: TypeElement.NUMBER,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO,
              },
              placeholder: commonMessage.INGRESE_VALOR

            },
          ]
        },
        documentacion: {
          label: commonValues.DOCUMENTACION_LABEL,
          typeElement: TypeElement.TEXTAREA

        },
      }
    }
  }

  consultando() {
    return this.searching
  }

  editando() {
    return !this.searching
  }


  
  consultar(initPage: boolean) {
    if (this.formCriterio.invalid) {
      this.criterioRequerido = true;
      return;
    }
    this.criterioRequerido = false;
    if (this.criterio != "") {
      this.controls.filter = "";
      this.loadingIndicator = true;
      if (initPage) {
        this.controls.curPage = 0;
      }
      let params = new ParametrosNivelSeguridad();
      if (this.campo == commonValues.CODIGO_VALUE) {
        params.codigo = this.criterio;
      } else {
        params.nombre = this.criterio;
      }
      params.size = this.controls.pageSize;
      params.page = this.controls.curPage;
      params.listaEstados = this.estado.split(",");
      this.nivelSeguridadService.listar(params).subscribe(
        response => {
          this.rows = response.content;
          this.temp = this.rows;
          this.loadingIndicator = false;
          this.controls.rowCount = response.totalElements;
          this.controls.curPage = response.number;
        },
        error => {
          this.loadingIndicator = false;
          console.log(error);
          this.notificationService.errorMessage(commonMessage.NO_SE_REALIZO_CONSULTA);
        }
      );
    }
  }


  updateFilter(event) {
    let val = event.target.value.toLowerCase();
    let colsAmt = 2;
    let keys = ['codigo', 'nombre'];
    this.rows = this.temp.filter(function (item) {
      for (let i = 0; i < colsAmt; i++) {
        if (item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 || !val) {
          return true;
        }
      }
    });
    this.table.offset = 0;
  }

  updatePageSize(value) {
    this.controls.pageSize = parseInt(value, 10);
    this.table.limit = this.controls.pageSize;
    this.controls.curPage = (this.controls.pageSize > this.controls.rowCount) ?
      0 : this.controls.curPage;
    this.consultar(false);
  }


  setPage(event) {
    this.controls.curPage = event.offset;
    this.controls.pageSize = event.pageSize;
    this.consultar(false);
  }

  limpiarCriterio() {
    this.criterio = "";
  }

  verTabla() {
    this.criterioRequerido = false;
    this.searching = true;
    this.disable = false;
  }

  verFormulario() {
    this.accion = commonValues.REGISTRAR_LABEL;
    this.searching = false;
    this.nuevo();
    this.suscritoAComponenteCodigo = false;
  }

  nuevo() {
    this.nivelSeguridad = new NivelSeguridad();
  }

  visualizar(id,deshabilitar) {
    this.verFormulario();
    this.buscar(id,deshabilitar,false);
  }

  buscar(id: number, deshabilitar: boolean, eliminar: boolean) {
    this.blockUIRegistro.start(commonMessage.CARGANDO + '...');
    this.nivelSeguridadService.consultar(id).subscribe(
      response => {
        this.nivelSeguridad = response;
        if (!eliminar) {
          this.afNivelSeguridad.form.disable();
          this.disable = true;
          this.accion = (!deshabilitar ? commonValues.EDITAR_LABEL : commonValues.VER_LABEL);
          if (!deshabilitar) {
            this.afNivelSeguridad.form.enable();
            this.disable = false;
          }
          this.afNivelSeguridad.form.setValue(response);
        } else {
          this.confirmDialog = true; commonMessage.CARGANDO +
            this.consultarRelaciones(id);
        }
        this.blockUIRegistro.stop();
      },
      error => {
        this.blockUIRegistro.stop();
        this.notificationService.errorMessage(commonMessage.NO_SE_REALIZO_CONSULTA);
      }
    );
  }

  get isInvalid() {
    if (this.afNivelSeguridad) {
      this.onChanges();

    }
    return this.afNivelSeguridad ? this.afNivelSeguridad.form.invalid : true;
  }

  get desactivar() {
    return this.disable;
  }

  onChanges(): void {
    if (!this.suscrito) {
      this.suscrito = true;
      this.afNivelSeguridad.form.get("factorReutilizacion").valueChanges.subscribe(valor => {
        if (valor < 0) {
          this.afNivelSeguridad.form.get("factorReutilizacion").setValue("");
          this.notificationService.errorMessage(commonMessage.REGISTRO_NO_EXITOSO);
        }
      })
      this.afNivelSeguridad.form.get("factorSimilitud").valueChanges.subscribe(valor => {
        if (valor < 0) {
          this.afNivelSeguridad.form.get("factorSimilitud").setValue("");
          this.notificationService.errorMessage(commonMessage.REGISTRO_NO_EXITOSO);
        }
      })
      this.afNivelSeguridad.form.get("factorVencimiento").valueChanges.subscribe(valor => {
        if (valor < 0) {
          this.afNivelSeguridad.form.get("factorVencimiento").setValue("");
          this.notificationService.errorMessage(commonMessage.REGISTRO_NO_EXITOSO);
        }
      })
      this.afNivelSeguridad.form.get("factorReintentos").valueChanges.subscribe(valor => {
        if (valor < 0) {
          this.afNivelSeguridad.form.get("factorReintentos").setValue("");
          this.notificationService.errorMessage(commonMessage.REGISTRO_NO_EXITOSO);
        }
      })
      this.afNivelSeguridad.form.get("factorBloqueo").valueChanges.subscribe(valor => {
        if (valor < 0) {
          this.afNivelSeguridad.form.get("factorBloqueo").setValue("");
          this.notificationService.errorMessage(commonMessage.REGISTRO_NO_EXITOSO);
        }
      })
    }
  }

  onSubmit() {
    this.nivelSeguridad = this.afNivelSeguridad.form.value;
    this.nivelSeguridad.estado = this.nivelSeguridad.estado == "" ? commonValues.ACTIVO_VALUE : this.nivelSeguridad.estado;
    this.nivelSeguridadService.guardar(this.nivelSeguridad).subscribe(
      response => {
        this.nivelSeguridad = response;
        this.searching = true;
        this.nuevo();
        this.notificationService.successMessage(commonMessage.REGISTRO_SATISFACTORIO);
        this.consultar(false);
      },
      error => {
        this.notificationService.errorMessage(commonMessage.REGISTRO_NO_EXITOSO);
      }
    );
  }
  eliminar(id) {
    this.blockUIConsulta.start(commonMessage.CARGANDO + '...');
    this.nivelSeguridadService.eliminar(id).subscribe(
      response => {
        this.consultar(false);
        this.notificationService.successMessage(commonMessage.REGISTRO_ELIMINADO);
        this.blockUIConsulta.stop();
      },
      error => {
        this.notificationService.errorMessage(commonMessage.REGISTRO_NO_ELIMINADO);
        this.blockUIConsulta.stop();
      }
    );
  }
  //confimr dialog
  openModal(id, estado) {
    this.estadoMensaje= estado;
    if (id != null) {
      this.buscar(id, false, true);
    }
    
  }

  confirm(): void {
    if(this.estadoMensaje== commonValues.ELIMINAR_LABEL)
    { 
      this.eliminar(this.nivelSeguridad.id);
      this.nuevo();
    }
    else if(this.estadoMensaje == commonValues.EDITAR_LABEL){
      this.onSubmit();
    }

    this.confirmDialog = false;
  }

 
  decline(): void {
    if(this.estadoMensaje== commonValues.ELIMINAR_LABEL)
    { 
      this.nuevo();
    }
    this.confirmDialog = false;
  }


  consultarRelaciones(id) {
    let arrayTemp = [];
    this.cuentaUsuarioService.listar(commonOptions.consultaEstadoEstandar).subscribe(
      response => {
        console.log(response.content);
        for (let item of response.content) {
          if ((item.Fuentedatos != null)) {
            if (item.Fuentedatos.id == id) {
              arrayTemp.push(item);
            }
          }
        }
      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );
  }

}
