import { Component, OnInit, ViewChild, Input, OnChanges, SimpleChanges } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { PrivilegiosService } from '@app/core/services/as/privilegios.service';
import { NotificationService } from '@app/core/services';
import { RecursosService } from '@app/core/services/as/recursos.service';
import { ParametrosPrivilegio } from './models/parametrosPrivilegio';
import { TypeElement } from '@app/shared/components/general-form/types/TypeElement';
import { GeneralFormComponent } from '@app/shared/components/general-form/general-form.component';
import { Recurso, Privilegio } from './models/privilegio';
import { PerfilPrivilegioService } from '@app/core/services/as/perfil-privilegio.service';
import { commonValues } from '@commons/commonValues';
import { commonMessage } from '@commons/commonMessages';
import { commonOptions } from '@commons/commonOptions';

@Component({
  selector: 'privilegios',
  templateUrl: './privilegios.component.html',
  styleUrls: ['./privilegios.component.css']
})
export class PrivilegiosComponent implements OnInit, OnChanges {

  @ViewChild('formPrivileges', { 'static': false }) formPrivileges: GeneralFormComponent;
  @ViewChild(DatatableComponent, { 'static': false }) table: DatatableComponent;
  @Input() datos: { llamado: boolean, id: number } = { llamado: false, id: 0 };

  //Labels vista
  lblFuentesDatos = commonValues.FUENTES_DATOS_LABEL;
  lblCancelar = commonValues.CANCELAR_LABEL;
  lblGuardar = commonValues.GUARDAR_LABEL;
  lblAgregar = commonValues.AGREGAR_LABEL;
  lblVolver = commonValues.VOLVER_LABEL;
  lblCriteriosBusqueda = commonValues.CRITERIOS_BUSQUEDA_LABEL;
  lblCampo = commonValues.CAMPO_LABEL;
  lblCodigo = commonValues.CODIGO_LABEL;
  lblNombre = commonValues.NOMBRE_LABEL;
  lblRecurso = commonValues.RECURSO_LABEL;
  lblEstado = commonValues.ESTADO_LABEL;
  lblActivo = commonValues.ACTIVO_LABEL;
  lblInactivo = commonValues.INACTIVO_LABEL;
  lblTodos = commonValues.TODOS_LABEL;
  lblCriterioBusqueda = commonValues.CRITERIO_BUSQUEDA_LABEL;
  lblBuscar = commonValues.BUSCAR_LABEL;
  lblFiltroTabla = commonValues.FILTRO_TABLA_LABEL;
  lblAcciones = commonValues.ACCIONES_LABEL;
  lblVer = commonValues.VER_LABEL;
  lblEditar = commonValues.EDITAR_LABEL;
  lblEliminar = commonValues.ELIMINAR_LABEL;
  lblPerfilesAsociado = commonValues.PERFILES_ASOCIADOS_LABEL;
  lblPerfiles = commonValues.PERFILES_LABEL;
  lblPrivilegios = commonValues.PRIVILEGIOS_LABEL;
  lblSi = commonValues.SI_LABEL;
  lblNo = commonValues.NO_LABEL;
  msgDeseaEliminar = commonMessage.DESEA_ELIMINAR_REGISTRO;
  msgNoSePuedeEliminar = commonMessage.NO_SE_PUEDE_ELIMINAR;

  informacion = { llamado: true, id: 0 };
  sePuedeEliminar: boolean;
  searching = true;
  estado = commonValues.ACTIVO_VALUE;
  campo = commonValues.CODIGO_VALUE;
  suscritoAComponenteCodigo = false;
  codigoRecorded = false;
  criterio: string = "";
  confirmDialog: boolean;
  currentTab = commonValues.PRIVILEGIOS_VALUE;
  operaciones = [
    { value: "", label: commonValues.SELECCIONE_TIPO_LABEL },
    { value: 1, label: "Creación" },
    { value: 2, label: "Actualización" },
    { value: 3, label: "Borrado" }
  ];
  recursos = [{ value: "", label: commonValues.SELECCIONE_RECURSO_LABEL }];
  opcionesEstado = commonOptions.tiposEstado;
  opcionesTipo = [
    { value: "", label: commonValues.SELECCIONE_TIPO_LABEL },
    { value: "CREACION", label: "Creación" },
    { value: "RECUPERACION", label: "Recuperación" },
    { value: "ACTUALIZACION", label: "Actualización" },
    { value: "BORRADO", label: "Borrado" },
    { value: "EJECUCION", label: "Ejecución" }
  ]

  controls: any = {
    pageSize: 10,
    filter: '',
    rowCount: 0, //total elementos
    curPage: 0, //numero pagina
    selectedCount: 0,
    offset: 0
  }
  cols = [{ name: 'codigo' }, { name: 'nombre' }];
  temp = [];
  rows = [];
  loadingIndicator = false;
  reorderable = true;
  messages = {
    emptyMessage: 'No existe informacion relacionada',
    totalMessage: 'Registros totales',
  };
  formPrivilegios: any;
  privilegio: any = new Privilegio;
  disable: any;
  pestanias: boolean;
  estadoFormulario: boolean;

  constructor(private privilegioService: PrivilegiosService,
    private notificationService: NotificationService,
    private RecursoService: RecursosService,
    private perfilPrivilegioService: PerfilPrivilegioService) {

    this.formPrivilegios = this.createForm();
  }

  ngOnInit() {
    this.actualizarListaRecursos();
  }

  openModal(id) {
    if (id != null) {
      this.estadoFormulario = false;
      this.buscar(id, true);
      this.consultarRelaciones(id);
    }

  }
  limpiarCriterio() {
    this.criterio = "";
  }
  confirm(): void {
    this.eliminar(this.privilegio.id);
    this.nuevo();
    this.confirmDialog = false;
  }
  decline(): void {
    this.confirmDialog = false;
    this.nuevo();
  }

  consultarRelaciones(id) {
    let arrayTemp = [];

    this.perfilPrivilegioService.listar(commonOptions.consultaEstadoEstandarCompleto).subscribe(
      response => {
        for (let item of response.content) {
          if (item.privilegio.id == id) {
            arrayTemp.push(item);
          }
        }
        this.sePuedeEliminar = arrayTemp.length > 0;
        this.confirmDialog = true;
      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );

  }

  actualizarListaRecursos(): any {
    this.RecursoService.listar(commonOptions.consultaEstadoEstandarCompleto).subscribe(
      response => {
        this.recursos.length = 0;
        this.recursos.push({ value: "", label: commonValues.SELECCIONE_RECURSO_LABEL });
        for (let item of response.content) {
          this.recursos.push({ value: item.id, label: '[' + item.codigo + '] ' + item.nombre });
        }
      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );
  }
  nuevo() {
    this.privilegio = new Privilegio;
  }

  createForm(): any {
    return {
      header: {
        label: commonValues.DATOS_BASICOS_LABEL,
      },
      fields: {
        id: {},
        row: {
          row: [
            {
              key: "codigo",
              label: commonValues.CODIGO_LABEL,
              typeElement: TypeElement.TEXT,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
              placeholder: commonMessage.INGRESE_CODIGO
            },
            {
              key: "nombre",
              label: commonValues.NOMBRE_LABEL,
              typeElement: TypeElement.TEXT,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
              placeholder: commonMessage.INGRESE_NOMBRE
            },
            {
              key: "estado",
              label: commonValues.ESTADO_LABEL,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesEstado,
              default: this.opcionesEstado[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            }
          ]
        },
        row2: {
          row: [
            {
              key: "recurso",
              label: commonValues.RECURSO_LABEL,
              typeElement: TypeElement.SELECT,
              options: this.recursos,
              default: this.recursos[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {
              key: "operacion",
              label: commonValues.OPERACION_LABEL,
              typeElement: TypeElement.SELECT,
              options: this.operaciones,
              default: this.operaciones[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {
              key: "tipo",
              label: commonValues.TIPO_LABEL,
              typeElement: TypeElement.SELECT,
              options: this.opcionesTipo,
              default: this.opcionesTipo[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            }
          ]
        },
        documentacion: {
          label: commonValues.DOCUMENTACION_LABEL,
          typeElement: TypeElement.TEXTAREA
        }

      }
    }
  }
  selectTab(moduleCode: string) {
    this.currentTab = moduleCode
    window.scroll(0, 0);
  }

  editando() {
    return !this.searching
  }

  consultando() {

    //this.pestanias=false;
    return this.searching
  }

  verTabla() {
    this.searching = true;
    this.disable = false;

  }

  verFormulario(estado = false) {
    this.selectTab(commonValues.PRIVILEGIOS_VALUE);
    this.estadoFormulario = estado;
    this.searching = false;
    this.pestanias = false;
    this.nuevo();
    this.selectTab(commonValues.PRIVILEGIOS_VALUE);
    this.suscritoAComponenteCodigo = false;
    this.actualizarListaRecursos();
  }

  get isInvalid() {
    if (this.formPrivileges) {
      this.onChanges();
    }
    return this.formPrivileges ? this.formPrivileges.form.invalid : true;
  }

  updateFilter(event) {
    let val = event.target.value.toLowerCase();
    let colsAmt = this.cols.length;
    let keys = ['codigo', 'nombre'];
    this.rows = this.temp.filter(function (item) {
      for (let i = 0; i < colsAmt; i++) {
        if (item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 || !val) {
          return true;
        }
      }
    });
    this.table.offset = 0;
  }
  updatePageSize(value) {

    this.controls.pageSize = parseInt(value, 10);
    this.table.limit = this.controls.pageSize;
    this.table.offset = 0;
    this.controls.curPage = 0;
    this.consultar();
  }
  setPage(event) {
    this.controls.curPage = event.offset;
    this.controls.pageSize = event.pageSize;
    this.consultar();
  }

  validarExistencia() {
    this.privilegio = this.formPrivileges.form.value;
    this.privilegioService.listar(commonOptions.consultaEstadoEstandarCompleto).subscribe(
      response => {
        for (let item of response.content) {
          if (item.operacion == this.privilegio.operacion && item.recurso.id == this.privilegio.recurso && this.privilegio.id != item.id) {
            this.notificationService.errorMessage(commonMessage.EXISTE_PRIVILEGIO_RECURSO_OPERACION);
            return null;
          }
        }
        this.onSubmit();
        error => {
          this.notificationService.errorMessage(commonMessage.REGISTRO_NO_EXITOSO);
        }
      }
    );

  }

  onSubmit() {

    this.privilegio.estado = this.privilegio.estado == "" ? commonValues.ACTIVO_VALUE : this.privilegio.estado;
    let miRecurso = new Recurso;
    miRecurso.id = this.privilegio.recurso;
    this.privilegio.recurso = miRecurso;
    this.privilegio.operacion = parseInt(this.privilegio.operacion);
    this.privilegioService.guardar(this.privilegio).subscribe(
      response => {
        this.privilegio = response;
        this.searching = true;
        this.nuevo();
        this.notificationService.successMessage(commonMessage.REGISTRO_SATISFACTORIO);
        this.consultar();
      },
      error => {
        this.notificationService.errorMessage(commonMessage.REGISTRO_NO_EXITOSO);
      }
    );

  }

  consultar() {
    if (this.datos.llamado) {
      this.listarPrivilegiosPorRecurso(this.datos.id);
    } else {
      this.loadingIndicator = true;
      const x = new ParametrosPrivilegio;
      x.size = this.controls.pageSize;
      x.page = this.controls.curPage;
      x.estado = this.estado.split(",");
      if (this.criterio != "") {
        if (this.criterio != commonValues.CRITERIO_VALUE) {
          if (this.campo != commonValues.RECURSO_VALUE) {
            if (this.campo == commonValues.CODIGO_VALUE) {
              x.codigo = this.criterio;
            } else if (this.campo == commonValues.NOMBRE_VALUE) {
              x.nombre = this.criterio;
            }
          } else {
            x.id_recurso = parseInt(this.criterio);
          }
        }
        this.privilegioService.listar(x).subscribe(
          response => {
            this.rows = response.content;
            this.temp = this.rows;
            this.loadingIndicator = false;
            this.controls.rowCount = response.totalElements;
            this.controls.curPage = response.number;
          },
          error => {
            console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
          }
        );
      }

    }
  }

  visualizar(id) {
    this.buscar(id);
    this.verFormulario(true);
    this.disable = true;
    this.pestanias = true;
  }

  editar(id) {
    this.buscar(id);
    this.verFormulario();
    this.pestanias = true;

  }

  get desactivar() {
    return this.disable;
  }

  onChanges(): void {
    if (this.formPrivileges && !this.suscritoAComponenteCodigo) {
      this.suscritoAComponenteCodigo = true;
      this.formPrivileges.form.get('codigo').valueChanges.subscribe(val => {
        this.onKeyupCodigo(val);
      });
    }
  }

  onKeyupCodigo(valorCodigo) {
    let parametros = new ParametrosPrivilegio;
    parametros.codigo = valorCodigo;
    parametros.estado = [commonValues.ACTIVO_VALUE, commonValues.INACTIVO_VALUE, commonValues.ELIMINADO_VALUE];
    if ((valorCodigo && (this.privilegio.id && valorCodigo != this.privilegio.codigo)) || !this.privilegio.id && valorCodigo) {
      this.privilegioService.listar(parametros).subscribe(data => {

        data = data.content;
        this.codigoRecorded = data != null && data.length > 0;
        if (this.codigoRecorded) {
          this.formPrivileges.form.controls['codigo'].setErrors({ 'codigoRecorded': true });
        } else {
          this.formPrivileges.form.controls['codigo'].setErrors({ 'codigoRecorded': false });
          const errores = this.formPrivileges.form.controls['codigo'].errors;
          delete errores.codigoRecorded;
          this.formPrivileges.form.controls['codigo'].setErrors(Object.keys(errores).length === 0 ? null : errores);
        }
      },
        error => console.error(error))
    }
  }

  buscar(id, eliminar?) {
    this.informacion.id = id;
    this.privilegioService.consultar(id).subscribe(
      response => {
        if (this.estadoFormulario) {
          this.formPrivileges.form.disable();
        }
        this.privilegio = response;
        if (eliminar == null) {
          this.formPrivileges.form.setValue(response);
          this.formPrivileges.form.controls['operacion'].setValue(response.operacion != null ? response.operacion : "");
          this.formPrivileges.form.controls['recurso'].setValue(response.recurso != null ? response.recurso.id : "");
        }
      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );

  }

  eliminar(id) {
    this.privilegioService.eliminar(id).subscribe(
      response => {
        this.notificationService.successMessage(commonMessage.REGISTRO_ELIMINADO);
        this.consultar();
      },
      error => {
        this.notificationService.errorMessage(commonMessage.REGISTRO_NO_ELIMINADO);
      }
    );

    this.consultar();
  }

  listarPrivilegiosPorRecurso(id) {
    this.privilegioService.listar({ id_recurso: id, estado: [commonValues.ACTIVO_VALUE, commonValues.INACTIVO_VALUE] }).subscribe(
      response => {
        this.rows = response.content;
        this.temp = this.rows;
        this.loadingIndicator = false;
        this.controls.rowCount = response.totalElements;
        this.controls.curPage = response.number;
      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );

  }

  ngOnChanges(changes: SimpleChanges) {
    this.datos = changes.datos.currentValue
    this.listarPrivilegiosPorRecurso(this.datos.id);
  }

}
