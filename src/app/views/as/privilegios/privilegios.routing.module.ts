import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PrivilegiosComponent } from "./privilegios.component";

const routes: Routes = [
    
    {
        path: '', component: PrivilegiosComponent , pathMatch: 'full'
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PrivilegiosRoutingModule {}