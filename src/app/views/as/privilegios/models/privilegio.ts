export class Privilegio {
    constructor() {
    }
    codigo: string;
    documentacion: string;
    estado: string;
    id: number;
    nombre: string;
    operacion: number;
    recurso: Recurso;
    tipo: string;
   
}
export class Recurso {
    constructor() {
    }
    id: number;
}


