import { CommonModule } from '@angular/common';
import { GeneralFormComponentModule } from './../../../shared/components/general-form/general-form-component.module';
import { NgModule } from "@angular/core";
import { FormsModule } from '@angular/forms';
import { NgxDatatableModule, DataTableHeaderCellComponent } from '@swimlane/ngx-datatable';
import { PrivilegiosComponent } from './privilegios.component';
import { PrivilegiosRoutingModule } from './privilegios.routing.module';
import { PerfilesComponent } from '../perfiles/perfiles.component';

@NgModule({
    declarations: [ PrivilegiosComponent,PerfilesComponent ],
    imports: [PrivilegiosRoutingModule,GeneralFormComponentModule,CommonModule,FormsModule,NgxDatatableModule,DataTableHeaderCellComponent,PerfilesComponent],
    exports: [GeneralFormComponentModule]
})
export class PrivilegiosModule {}