
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { FuentedatosComponent } from './fuentedatos/fuentedatos.component';

const routes: Routes = [
    {
        path: '', redirectTo: '/', pathMatch: 'full'
    },
    {
        path: 'fuentedatos',  loadChildren: './fuentedatos/fuentedatos.module#FuentedatosModule', 
        component:FuentedatosComponent
	},
	{
        path: 'nivel-seguridad', loadChildren: './nivel-seguridad/nivel-seguridad.module#NivelSeguridadModule'
    },
    {
      path:'recursos',loadChildren:'./recursos/recursos.module#RecursosModule'
    },
    {
        path:'privilegios',loadChildren:'./privilegios/privilegios.module#PrivilegiosModule'
    },
    {
        path: 'Menú de opciones', loadChildren: './opciones-menu/opciones-menu.module#OpcionesMenuModule'
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdministracionSeguridadRoutingModule {}
