import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Parametros } from './models/parametros';
import { RecursosService } from '@app/core/services/as/recursos.service';
import { TypeElement } from '@app/shared/components/general-form/types/TypeElement';
import { ModulosService } from '@app/core/services/ge/modulos.service';
import { GeneralFormComponent } from '@app/shared/components/general-form/general-form.component';
import { Recurso, Modulo } from './models/recurso';
import { NotificationService } from '@app/core/services';
import { PrivilegiosService } from '@app/core/services/as/privilegios.service';
import { ReportesService } from '@app/core/services/as/reportes.service';
import { FuentedatosService } from '@app/core/services/as/fuentadatos.service';
import { Datasource, Reporte } from './models/reporte';
import { RecursoAuditoriaService } from '@app/core/services/as/recurso-auditoria.service';
import { commonValues } from '@commons/commonValues';
import { commonMessage } from '@commons/commonMessages';
import { commonOptions } from '@commons/commonOptions';


@Component({
  selector: 'recursos',
  templateUrl: './recursos.component.html',
  styleUrls: ['./recursos.component.css']
})
export class RecursosComponent implements OnInit {

  @ViewChild(GeneralFormComponent, { 'static': false }) formResource: GeneralFormComponent;
  @ViewChild('formResourceExtend', { 'static': false }) formResourceExtend: GeneralFormComponent;
  @ViewChild('formResourceAuditory', { 'static': false }) formResourceAuditory: GeneralFormComponent;
  @ViewChild(DatatableComponent, { 'static': false }) table: DatatableComponent;

  //Labels vista
  lblRecursos = commonValues.RECURSOS_LABEL;
  lblCancelar = commonValues.CANCELAR_LABEL;
  lblGuardar = commonValues.GUARDAR_LABEL;
  lblAgregar = commonValues.AGREGAR_LABEL;
  lblCriteriosBusqueda = commonValues.CRITERIOS_BUSQUEDA_LABEL;
  lblCampo = commonValues.CAMPO_LABEL;
  lblCodigo = commonValues.CODIGO_LABEL;
  lblNombre = commonValues.NOMBRE_LABEL;
  lblModulo = commonValues.MODULO_LABEL;
  lblEstado = commonValues.ESTADO_LABEL;
  lblActivo = commonValues.ACTIVO_LABEL;
  lblInactivo = commonValues.INACTIVO_LABEL;
  lblTodos = commonValues.TODOS_LABEL;
  lblCriterioBusqueda = commonValues.CRITERIO_BUSQUEDA_LABEL;
  lblBuscar = commonValues.BUSCAR_LABEL;
  lblFiltroTabla = commonValues.FILTRO_TABLA_LABEL;
  lblAcciones = commonValues.ACCIONES_LABEL;
  lblVer = commonValues.VER_LABEL;
  lblEditar = commonValues.EDITAR_LABEL;
  lblEliminar = commonValues.ELIMINAR_LABEL;
  lblDocumentacion = commonValues.DOCUMENTACION_LABEL;
  lblPrivilegiosAsociados = commonValues.PRIVILEGIOS_ASOCIADOS_LABEL;
  lblPrivilegios = commonValues.PRIVILEGIOS_LABEL;
  lblSi = commonValues.SI_LABEL;
  lblNo = commonValues.NO_LABEL;
  msgDeseaEliminar = commonMessage.DESEA_ELIMINAR_REGISTRO;
  msgNoSePuedeEliminar = commonMessage.NO_SE_PUEDE_ELIMINAR;

  sePuedeEliminar: boolean;
  reporte = new Reporte;
  estadoReporte = false;
  estadoAuditoria = false;
  pestanias = false;
  disable = false;
  searching = true;
  criterio: string = "";
  documentacion = "";
  estado = commonValues.ACTIVO_VALUE;
  campo = commonValues.CODIGO_VALUE;
  currentTab = commonValues.RECURSOS_VALUE;
  formRecursos: any;
  formRecursoAuditoria: any;
  formRecursoExtentido: any;
  modulos = [];
  recursos = [];
  reportes = [];
  datasources = [{ value: "", label: commonValues.SELECCION_FUENTE_DATOS_LABEL }];
  opcionesEstado = commonOptions.opcionesEstado;
  opcionesParametro = commonOptions.siNo;
  opcionesTipo = [{ value: "FORMA", label: commonValues.FORMA_LABEL }, { value: "REPORTE", label: commonValues.REPORTE_LABEL }, { value: "SERVICIO", label: commonValues.SELECCIONE_RECURSO_LABEL }]
  opcionesOrigen = [{ value: "PLANTILLA", label: commonValues.PLANTILLA_LABEL }, { value: "CONSULTA", label: commonValues.CONSULTA_LABEL }]
  opcionesLog = commonOptions.opcionesLog;
  suscritoAComponenteCodigo = false;
  codigoRecorded = false;
  recurso: any;

  /* Variables relacionadas a la tabla de consulta */
  controls: any = {
    pageSize: 10,
    filter: '',
    rowCount: 0, //total elementos
    curPage: 0, //numero pagina
    selectedCount: 0,
    offset: 0
  }
  cols = [{ name: 'codigo' }, { name: 'nombre' }];
  temp = [];
  rows = [];
  loadingIndicator = false;
  reorderable = true;
  messages = {
    emptyMessage: commonMessage.NO_EXISTE_INFO,
    totalMessage: commonMessage.REGISTROS_TOTALES,
  };
  confirmDialog: boolean;


  estadoFormulario: boolean;
  informacion = { llamado: true, id: 0 };
  recursoAuditoria: any;


  constructor(private recursosService: RecursosService,
    private moduloService: ModulosService,
    private notificationService: NotificationService,
    private privilegioService: PrivilegiosService,
    private reporteService: ReportesService,
    private datasourceService: FuentedatosService,
    private auditoriaService: RecursoAuditoriaService) {
    this.formRecursos = this.createForm();
    this.formRecursoExtentido = this.createFormReporte();
    this.formRecursoAuditoria = this.createformAuditoria();
  }

  ngOnInit() {
    this.actualizarListaRecursos();
    this.actualizarListaDatasources();
    this.actualizarListaModulos();
    this.actualizarListaReportes();
  }
  limpiarCriterio() {
    this.criterio = "";
  }

  //confimr dialog \\
  openModal(id) {
    if (id != null) {
      this.estadoFormulario = false;
      this.buscar(id, true);
      this.consultarRelaciones(id);
    }

  }
  confirm(): void {
    this.eliminar(this.recurso.id);
    this.nuevo();
    this.confirmDialog = false;
  }
  decline(): void {
    this.confirmDialog = false;
    this.nuevo();
  }
  // confimr dialog \\
  nuevo() {
    this.recurso = new Recurso;
    this.reporte = new Reporte;
  }

  actualizarListaModulos(): any {
    this.moduloService.listar(commonOptions.consultaEstadoEstandar).subscribe(response => {
      this.modulos.length = 0;
      this.modulos.push({ value: "", label: commonValues.SELECCIONE_MODULO_LABEL });
      for (let item of response.content) {
        this.modulos.push({ value: item.id, label: '[' + item.codigo + '] ' + item.nombre });
      }
    },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );

  }
  actualizarListaRecursos(): any {
    this.recursosService.listar(commonOptions.consultaEstadoEstandar).subscribe(
      response => {
        this.recursos.length = 0;
        this.recursos.push({ value: "", label: commonValues.NINGUNO_LABEL });
        for (let item of response.content) {
          if (this.recurso && this.recurso.id != item.id) {
            this.recursos.push({ value: item.id, label: '[' + item.codigo + '] ' + item.nombre });
          }
        }
      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );

  }
  actualizarListaReportes(): any {
    this.recursosService.listar({ tipo: commonValues.REPORTE_VALUE, estado: [commonValues.ACTIVO_VALUE, commonValues.INACTIVO_VALUE], page: 0, size: 1000 }).subscribe(
      response => {
        this.reportes.length = 0;
        this.reportes.push({ value: "", label: commonValues.SELECCIONE_REPORTE_LABEL, disabled: true });
        for (let item of response.content) {
          if (this.recurso && this.recurso.id != item.id) {
            this.reportes.push({ value: item.id, label: '[' + item.codigo + '] ' + item.nombre });
          }
        }
      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );

  }
  actualizarListaDatasources(): any {

    this.datasourceService.listar(commonOptions.consultaEstadoEstandar).subscribe(
      response => {
        this.datasources.length = 0;
        this.datasources.push({ value: "", label: commonValues.SELECCION_FUENTE_DATOS_LABEL });
        for (let item of response.content) {
          this.datasources.push({ value: item.id, label: '[' + item.codigo + '] ' + item.nombre });
        }
      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );

  }

  consultarRelaciones(id) {
    let arrayTemp = [];

    this.recursosService.listar({ estado: [commonValues.ACTIVO_VALUE, commonValues.INACTIVO_VALUE] }).subscribe(
      response => {
        console.log(response.content);
        for (let item of response.content) {
          if ((item.recursoPadre && item.recursoPadre.id == id) || item.idRecursoReporte == id) {
            arrayTemp.push(item);
          }
        }
        this.privilegioService.listar({ estado: [commonValues.ACTIVO_VALUE, commonValues.INACTIVO_VALUE] }).subscribe(
          response => {
            console.log(response.content);
            for (let item of response.content) {
              if (item.recurso.id == id) {
                arrayTemp.push(item);
              }
            }
            this.sePuedeEliminar = arrayTemp.length > 0;
            this.confirmDialog = true;
          },
          error => {
            console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
          }
        );

      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );

  }

  selectTab(moduleCode: string) {
    this.currentTab = moduleCode;
    window.scroll(0, 0);
  }

  createForm(): any {
    return {
      header: {
        label: commonValues.DATOS_BASICOS_LABEL,
      },
      fields: {
        id: {},
        row: {
          row: [
            {
              key: "codigo",
              label: commonValues.CODIGO_LABEL,
              typeElement: TypeElement.TEXT,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
              placeholder: commonMessage.INGRESE_CODIGO
            },
            {
              key: "nombre",
              label: commonValues.NOMBRE_LABEL,
              typeElement: TypeElement.TEXT,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
              placeholder: commonMessage.INGRESE_NOMBRE
            },
            {
              key: "estado",
              label: commonValues.ESTADO_LABEL,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesEstado,
              default: this.opcionesEstado[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            }
          ]
        },
        row2: {
          row: [
            {
              key: "modulo",
              label: commonValues.MODULO_LABEL,
              typeElement: TypeElement.SELECT,
              options: this.modulos,
              default: this.modulos[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {
              key: "recursoPadre",
              label: commonValues.RECURSO_LABEL,
              typeElement: TypeElement.SELECT,
              options: this.recursos,
              default: this.recursos[0]
            },
            {
              key: "nivelLog",
              label: commonValues.NIVEL_LOG_LABEL,
              typeElement: TypeElement.SELECT,
              options: this.opcionesLog,
              default: this.opcionesLog[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            }
          ]
        },
        row3: {
          row: [
            {
              key: "tipo",
              label: commonValues.TIPO_LABEL,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesTipo,
              default: this.opcionesTipo[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {
              key: "reporteRelacionado",
              label: commonValues.REPORTE_RELACIONADO_LABEL,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesParametro,
              default: this.opcionesParametro[1],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {
              key: "requiereParametros",
              label: commonValues.REQUIERE_PARAMETROS_LABEL,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesParametro,
              default: this.opcionesParametro[1],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            }
          ]
        },
        row4: {
          row: [
            {
              key: "idRecursoReporte",
              label: commonValues.REPORTE_LABEL,
              typeElement: TypeElement.SELECT,
              options: this.reportes,
              default: this.reportes[0]

            },
            {
              key: "recurso",
              label: commonValues.RUTA_RECURSO_LABEL,
              typeElement: TypeElement.TEXT

            },
            {
              key: "requiereAuditoria",
              label: commonValues.REQUIERE_AUDITORIA_LABEL,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesParametro,
              default: this.opcionesParametro[1],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            }
          ]
        },

        documentacion: {},
        fechaCreacion: {},
        fechaUltimaModificacion: {}
      }
    };

  }
  createFormReporte(): any {
    return {
      header: {
        label: commonValues.EXTENSION_TIPO_REPORTE_LABEL
      },
      fields: {
        id: {},
        row: {
          row: [
            {
              key: "idDatasource",
              label: commonValues.FUENTE_DATOS_LABEL,
              typeElement: TypeElement.SELECT,
              options: this.datasources,
              default: this.datasources[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {
              key: "consultaSql",
              label: commonValues.CONSULTA_SQL_LABEL,
              typeElement: TypeElement.TEXTAREA,
              placeholder: commonMessage.INGRESE_CONSULTA_SQL
            },
          ]
        },
        row2: {
          row: [
            {
              key: "origen",
              label: commonValues.ORIGEN_LABEL,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesOrigen,
              default: this.opcionesOrigen[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }

            },
            {
              key: "exportarPdf",
              label: commonValues.EXPORTAR_LABEL + " " + commonValues.PDF_VALUE,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesParametro,
              default: this.opcionesParametro[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {
              key: "exportarXls",
              label: commonValues.EXPORTAR_LABEL + " " + commonValues.XLS_VALUE,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesParametro,
              default: this.opcionesParametro[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            }
          ]
        },
        row3: {
          row: [
            {
              key: "exportarCsv",
              label: commonValues.EXPORTAR_LABEL + " " + commonValues.CSV_VALUE,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesParametro,
              default: this.opcionesParametro[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {
              key: "exportarHtml",
              label: commonValues.EXPORTAR_LABEL + " " + commonValues.HTML_VALUE,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesParametro,
              default: this.opcionesParametro[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {
              key: "exportarImg",
              label: commonValues.EXPORTAR_LABEL + " " + commonValues.IMG_VALUE,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesParametro,
              default: this.opcionesParametro[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            }
          ]
        }
      }
    };
  }

  createformAuditoria(): any {
    return {
      header: {
        label: commonValues.EXTENSION_AUDITORIA_LABEL,
      }, fields: {
        id: {},
        estado: {},
        row: {
          row: [
            {
              key: "auditaCreacion",
              label: commonValues.AUDITA_CREACION_LABEL,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesParametro,
              default: this.opcionesParametro[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {
              key: "auditaModificacion",
              label: commonValues.AUDITA_MODIFCACION_LABEL,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesParametro,
              default: this.opcionesParametro[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {
              key: "auditaEliminacion",
              label: commonValues.AUDITA_ELIMINACION_LABEL,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesParametro,
              default: this.opcionesParametro[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            }
          ]
        }
      }
    }
  }

  editando() {
    return !this.searching
  }
  consultando() {
    return this.searching
  }

  verTabla() {
    this.searching = true;
    this.disable = false;
    this.selectTab(commonValues.RECURSOS_VALUE);
    this.nuevo();
  }

  verFormulario(estado = false) {
    this.estadoFormulario = estado;
    this.searching = false;
    this.nuevo();
    this.suscritoAComponenteCodigo = false;
    this.pestanias = false;
    this.mostrarFormularioReporte(commonValues.FORMA_VALUE);
    this.mostrarFormularioAuditoria(commonValues.FALSE_VALUE);
    this.actualizarListaRecursos();
    this.actualizarListaDatasources();
    this.actualizarListaModulos();
    this.actualizarListaReportes();
  }
  get isInvalid() {
    if (this.formResource) {
      this.onChanges();
    }
    if (this.formResource) {

      if (this.formResourceExtend && !this.formResourceAuditory) {
        return this.formResourceExtend.form.invalid;
      }
      if (this.formResourceAuditory && !this.formResourceExtend) {
        return this.formResourceAuditory.form.invalid;
      }
      if (this.formResourceExtend && this.formResourceAuditory) {
        let x = this.formResourceAuditory.form.invalid || this.formResourceExtend.form.invalid;
        return x;
      }
      return this.formResource.form.invalid;
    }
    return true;
  }

  get desactivar() {
    return this.disable;
  }
  onChanges(): void {
    if (this.formResource && !this.suscritoAComponenteCodigo) {
      this.suscritoAComponenteCodigo = true;
      this.formResource.form.get('codigo').valueChanges.subscribe(val => {
        this.onKeyupCodigo(val);
      });
      this.formResource.form.get('tipo').valueChanges.subscribe(val => {
        this.mostrarFormularioReporte(val);
      });
      this.formResource.form.get('reporteRelacionado').valueChanges.subscribe(val => {
        this.habilitarAdicionarReporteRelacionado(val);
      });
      this.formResource.form.get('requiereAuditoria').valueChanges.subscribe(val => {
        this.mostrarFormularioAuditoria(val);
      });

    }
  }
  mostrarFormularioAuditoria(valor) {
    this.estadoAuditoria = valor == commonValues.TRUE_VALUE ? true : false;
  }

  habilitarAdicionarReporteRelacionado(valor) {
    if (valor == commonValues.FALSE_VALUE) {
      this.formResource.form.controls['idRecursoReporte'].setValue("");
      this.formResource.form.controls['idRecursoReporte'].disable();
    } else if (valor == commonValues.TRUE_VALUE && !this.estadoFormulario) {
      this.formResource.form.controls['idRecursoReporte'].enable();
    }
    console.log(valor);
  }

  mostrarFormularioReporte(valor) {
    this.estadoReporte = valor == commonValues.REPORTE_VALUE ? true : false;
  }

  onKeyupCodigo(valorCodigo) {
    let parametros = new Parametros;
    parametros.codigo = valorCodigo;
    parametros.estado = commonOptions.estadosGeneral;
    if ((valorCodigo && (this.recurso.id && valorCodigo != this.recurso.codigo)) || !this.recurso.id && valorCodigo) {
      this.recursosService.listar(parametros).subscribe(data => {

        data = data.content;
        this.codigoRecorded = data != null && data.length > 0;
        console.log(data);
        if (this.codigoRecorded) {
          this.formResource.form.controls['codigo'].setErrors({ 'codigoRecorded': true });
        } else {
          this.formResource.form.controls['codigo'].setErrors({ 'codigoRecorded': false });
          const errores = this.formResource.form.controls['codigo'].errors;
          delete errores.codigoRecorded;
          this.formResource.form.controls['codigo'].setErrors(Object.keys(errores).length === 0 ? null : errores);
        }
      },
        error => console.error(error))
    }
  }

  updateFilter(event) {
    let val = event.target.value.toLowerCase();
    let colsAmt = this.cols.length;
    let keys = ['codigo', 'nombre', 'modulo'];
    this.rows = this.temp.filter(function (item) {
      for (let i = 0; i < colsAmt; i++) {
        if (item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 || !val) {
          return true;
        }
      }
    });
    this.table.offset = 0;
  }
  updatePageSize(value) {

    this.controls.pageSize = parseInt(value, 10);
    this.table.limit = this.controls.pageSize;
    this.table.offset = 0;
    this.controls.curPage = 0;
    this.consultar();
  }
  setPage(event) {
    this.controls.curPage = event.offset;
    this.controls.pageSize = event.pageSize;
    this.consultar();
  }
  onSubmit() {
    this.recurso = this.formResource.form.value;
    this.recurso.documentacion = this.documentacion;
    this.recurso.estado = this.recurso.estado == "" ? commonValues.ACTIVO_VALUE : this.recurso.estado;
    this.recurso.tipo = this.recurso.tipo == "" ? commonValues.FORMA_VALUE : this.recurso.tipo;
    this.recurso.reporteRelacionado = this.recurso.reporteRelacionado == "" ? commonValues.FALSE_VALUE : this.recurso.reporteRelacionado;
    this.recurso.requiereParametros = this.recurso.requiereParametros == "" ? commonValues.FALSE_VALUE : this.recurso.requiereParametros;
    this.recurso.nivelLog = !this.recurso.nivelLog ? 0 : this.recurso.nivelLog;
    if (this.recurso.modulo) {
      let miModulo = new Modulo;
      miModulo.id = this.recurso.modulo;
      this.recurso.modulo = miModulo;
    } else {
      this.recurso.modulo = null;
    }
    if (this.recurso.recursoPadre) {
      let miRecursoPadre = new Recurso;
      miRecursoPadre.id = this.recurso.recursoPadre;
      this.recurso.recursoPadre = miRecursoPadre;
    } else {
      this.recurso.recursoPadre = null;
    }
    if (this.recurso.idRecursoReporte) {
      this.recurso.idRecursoReporte = this.recurso.idRecursoReporte;
    } else {
      this.recurso.idRecursoReporte = null;
    }
    if (this.recurso.fechaCreacion) {
      this.recurso.fechaUltimaModificacion = new Date();
    } else {
      this.recurso.fechaCreacion = new Date();
      this.recurso.fechaUltimaModificacion = new Date();
    }
    console.log(this.recurso);
    if ((this.recurso.reporteRelacionado == commonValues.TRUE_VALUE && this.recurso.idRecursoReporte != null) || this.recurso.reporteRelacionado == "FALSE") {
      this.recursosService.guardar(this.recurso).subscribe(
        response => {
          this.recurso = response;
          this.searching = true;

          if (this.recurso.tipo == commonValues.REPORTE_VALUE) {
            let ds: Datasource = new Datasource;
            ds.id = this.formResourceExtend.form.controls['idDatasource'].value;
            this.reporte = this.formResourceExtend.form.value;
            this.reporte.idDatasource = ds;
            this.reporte.id = response.id;
            this.reporteService.guardar(this.reporte).subscribe(
              response => {
                let x = response;
                console.log(x);
              },
              error => {
                console.log(commonMessage.ERROR_OPERACION);
              }
            );
          }
          if (this.recurso.requiereAuditoria == commonValues.TRUE_VALUE) {
            this.recursoAuditoria = this.formResourceAuditory.form.value;
            this.recursoAuditoria.estado = this.recurso.estado;
            this.recursoAuditoria.id = this.recurso.id;
            this.auditoriaService.guardar(this.recursoAuditoria).subscribe(
              response => {
                let x = response;
                console.log(x);
              },
              error => {
                console.log(commonMessage.ERROR_OPERACION);
              }
            );
          }
          this.actualizarListaRecursos();
          this.actualizarListaDatasources();
          this.actualizarListaModulos();
          this.actualizarListaReportes();
          this.nuevo();
          this.notificationService.successMessage(commonMessage.REGISTRO_SATISFACTORIO);
          this.consultar();
        },
        error => {
          this.notificationService.errorMessage(commonMessage.REGISTRO_NO_EXITOSO);
        }
      );

    } else {
      this.notificationService.warningMessage(commonMessage.DEBE_SELECCIONAR_REPORTE_RELACIONADO);
    }

  }
  consultar() {

    console.log(this.criterio)
    this.loadingIndicator = true;
    const x = new Parametros;
    x.size = this.controls.pageSize;
    x.page = this.controls.curPage;
    x.estado = this.estado.split(",");

    //x.id_modulo=5;
    if (this.criterio != "") {
      if (this.campo != "modulo") {
        if (this.criterio != commonValues.CRITERIO_VALUE) {
          if (this.campo == commonValues.CODIGO_VALUE) {
            x.codigo = this.criterio;
          } else if (this.campo == commonValues.NOMBRE_VALUE) {
            x.nombre = this.criterio;
          }
        }
      } else {
        console.log(this.criterio)
        x.id_modulo = parseInt(this.criterio);
      }


      //this.estado
      this.recursosService.listar(x).subscribe(
        response => {
          this.rows = response.content;
          this.temp = this.rows;
          this.loadingIndicator = false;
          this.controls.rowCount = response.totalElements;
          this.controls.curPage = response.number;
          console.log(response)
        },
        error => {
          console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
        }
      );
    }
  }

  visualizar(id) {

    this.buscar(id);
    this.verFormulario(true);
    this.disable = true;
    this.pestanias = true;

  }

  editar(id) {
    this.verFormulario();
    this.buscar(id);
    this.pestanias = true;
  }

  buscar(id, eliminar?) {
    this.informacion.id = id;
    this.recursosService.consultar(id).subscribe(
      response => {
        this.recurso = response;
        if (eliminar == null) {
          if (this.estadoFormulario) {
            this.formResource.form.disable();
          }
          this.formResource.form.setValue(response);
          this.formResource.form.controls['modulo'].setValue(response.modulo != null ? response.modulo.id : "");
          this.formResource.form.controls['recursoPadre'].setValue(response.recursoPadre != null ? response.recursoPadre.id : "");
          this.formResource.form.controls['idRecursoReporte'].setValue(response.idRecursoReporte != null ? response.idRecursoReporte : "");
          this.documentacion = response.documentacion;
          console.log(this.recurso);

          if (this.recurso.tipo == commonValues.REPORTE_VALUE) {
            this.reporteService.consultar(this.recurso.id).subscribe(
              response => {
                this.reporte = response;
                console.log(response);
                this.formResourceExtend.form.setValue(response);
                this.formResourceExtend.form.controls['idDatasource'].setValue(response.idDatasource != null ? response.idDatasource.id : 0);
                if (this.estadoFormulario) {
                  this.formResourceExtend.form.disable();
                }
              },
              error => {
                console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
              }
            );
          }
          if (this.recurso.requiereAuditoria == commonValues.TRUE_VALUE) {
            this.auditoriaService.consultar(this.recurso.id).subscribe(
              response => {
                this.recursoAuditoria = response;
                console.log(response);
                this.formResourceAuditory.form.setValue(response);
                if (this.estadoFormulario) {
                  this.formResourceAuditory.form.disable();
                }
              },
              error => {
                console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
              }
            );
          }

        }
      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );

  }

  eliminar(id) {
    this.recursosService.eliminar(id).subscribe(
      response => {
        this.notificationService.successMessage(commonMessage.REGISTRO_ELIMINADO);
        this.consultar();
      },
      error => {
        this.notificationService.errorMessage(commonMessage.REGISTRO_NO_ELIMINADO);
      }
    );

    this.consultar();
  }

  validarReferencias() {
    this.recursos.splice(0);
  }
}
