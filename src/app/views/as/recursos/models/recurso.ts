export class Recurso {
    constructor() {
    }
    id : number;

    estado : string;

    modulo : Modulo;

    codigo : string;

    nombre : string;

    recursoPadre : Recurso;

    tipo : string;

    recurso : string;

    nivelLog : number;

    fechaCreacion : Date;
    
    TipoBoolean : string;

    fechaUltimaModificacion : Date;

    reporteRelacionado : string;

    requiereParametros : string;

    documentacion : string;

    idRecursoReporte : number;
}
export class Modulo {
    constructor() {
    }
    id : number;
}
