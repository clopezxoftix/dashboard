export class Reporte {
    constructor() {
    }
   id : number;

   idDatasource : Datasource;

   origen : string;

   consultaSql : string;

   exportarPdf : string;

   exportarXls : string;

   exportarCsv : string;

   exportarHtml : string;

   exportarImg : string;


}
export class Datasource {
    constructor() {
    }
    id : number;
}