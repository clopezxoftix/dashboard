import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RecursosComponent } from "@app/views/as/recursos/recursos.component";

const routes: Routes = [

    {
        path: '', component: RecursosComponent , pathMatch: 'full'
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RecursosRoutingModule {}
