import { CommonModule } from '@angular/common';
import { GeneralFormComponentModule } from './../../../shared/components/general-form/general-form-component.module';

import { NgModule } from "@angular/core";

import { FormsModule } from '@angular/forms';
import { NgxDatatableModule, DataTableHeaderCellComponent } from '@swimlane/ngx-datatable';
import { RecursosComponent } from '@app/views/as/recursos/recursos.component';
import { RecursosRoutingModule } from '@app/views/as/recursos/recursos.routing.module';
import { PrivilegiosComponent } from '../privilegios/privilegios.component';


@NgModule({
    declarations: [ RecursosComponent,PrivilegiosComponent ],
    imports: [RecursosRoutingModule,GeneralFormComponentModule,CommonModule,FormsModule,NgxDatatableModule,DataTableHeaderCellComponent,PrivilegiosComponent],
    exports: [GeneralFormComponentModule]
})
export class RecursosModule {}
