import { NgModule } from "@angular/core";
import { AdministracionSeguridadRoutingModule } from "./administracion.seguridad.routing.module";

import { SharedModule } from "@app/shared/shared.module";
import { FuentedatosComponent } from "./fuentedatos/fuentedatos.component";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { ReactiveFormsModule } from '@angular/forms';
import { RecursosComponent } from '@app/views/as/recursos/recursos.component';
import { PrivilegiosComponent } from './privilegios/privilegios.component';
import { PerfilesComponent } from './perfiles/perfiles.component';
import { CuentasUsuarioComponent } from './cuentas-usuario/cuentas-usuario.component';
import { PickListModule } from 'primeng/picklist';
import { PicklistComponent } from '../picklist/picklist.component';
import { PrivilegiosViewComponent } from './privilegios-view/privilegios-view.component';
import { BlockUIModule } from 'ng-block-ui';
import { OpcionesMenuComponent } from "./opciones-menu/opciones-menu.component";
import { NivelSeguridadComponent } from "./nivel-seguridad/nivel-seguridad.component";


@NgModule({
    declarations: [
        NivelSeguridadComponent, 
        FuentedatosComponent, 
        RecursosComponent, 
        PrivilegiosComponent, 
        PerfilesComponent, 
        CuentasUsuarioComponent,
        PicklistComponent,
        PrivilegiosViewComponent,
        OpcionesMenuComponent
    ],
    imports: [
        AdministracionSeguridadRoutingModule,
        SharedModule,
        NgxDatatableModule, 
        ReactiveFormsModule,
        PickListModule,
        BlockUIModule.forRoot()


    ],
    exports: [
        NivelSeguridadComponent, 
        FuentedatosComponent, 
        RecursosComponent, 
        PrivilegiosComponent, 
        PerfilesComponent, 
        CuentasUsuarioComponent,
        OpcionesMenuComponent
    ],
    entryComponents: [
        NivelSeguridadComponent, 
        FuentedatosComponent, 
        RecursosComponent, 
        PrivilegiosComponent, 
        PerfilesComponent, 
        CuentasUsuarioComponent,
        OpcionesMenuComponent
    ]
})
export class AdministracionSeguridadModule { }
