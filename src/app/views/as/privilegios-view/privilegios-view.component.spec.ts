import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivilegiosViewComponent } from './privilegios-view.component';

describe('PrivilegiosViewComponent', () => {
  let component: PrivilegiosViewComponent;
  let fixture: ComponentFixture<PrivilegiosViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivilegiosViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivilegiosViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
