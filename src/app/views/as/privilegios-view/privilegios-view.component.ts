import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { commonValues } from '@commons/commonValues';
import { commonMessage } from '@commons/commonMessages';
import { PerfilCuentaService } from '@app/core/services/as/perfil-cuenta.service';
import { CuentaUsuario } from '../cuentas-usuario/models/cuentasUsuario';
import { PerfilCuenta } from '../perfiles/models/perfil-cuenta';
import { Perfil } from '../perfiles/models/perfil';
import { PerfilPrivilegioService } from '@app/core/services/as/perfil-privilegio.service';
import { Privilegio } from '../privilegios/models/privilegio';
import { common } from '@commons/common';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { GeneralFormComponent } from '@app/shared/components/general-form/general-form.component';
import { PrivilegiosService } from '@app/core/services/as/privilegios.service';
import { TypeElement } from '@app/shared/components/general-form/types/TypeElement';
import { RecursosService } from '@app/core/services/as/recursos.service';

@Component({
  selector: 'privilegios-view',
  templateUrl: './privilegios-view.component.html',
  styleUrls: ['./privilegios-view.component.css']
})
export class PrivilegiosViewComponent implements OnInit {

  @ViewChild(DatatableComponent, { 'static': false }) table: DatatableComponent;
  @ViewChild('formPrivileges', { 'static': false }) formPrivileges: GeneralFormComponent;

  perfilCuenta: Perfil;
  perfilesCuenta: Perfil[];
  privilegioCuenta: Privilegio;
  privilegiosAsociados: Privilegio[];
  cuenta: CuentaUsuario;
  loadingIndicator = false;
  privilegio: Privilegio = new Privilegio;
  formPrivilegios: any;
  viewForm = false;
  opcionesEstado = [{ value: "ACTIVO", label: "Activo" }, { value: "INACTIVO", label: "Inactivo" }];
  recursos = [{ value: "", label: "Seleccione un recurso" }];
  operaciones = [
    { value: "", label: "Seleccione un tipo" },
    { value: 1, label: "Creación" },
    { value: 2, label: "Actualización" },
    { value: 3, label: "Borrado" }
  ];
  opcionesTipo = [
    { value: "", label: "Seleccione un tipo" },
    { value: "CREACION", label: "Creación" },
    { value: "RECUPERACION", label: "Recuperación" },
    { value: "ACTUALIZACION", label: "Actualización" },
    { value: "BORRADO", label: "Borrado" },
    { value: "EJECUCION", label: "Ejecución" }
  ];

  lblPrivilegio = commonValues.PRIVILEGIOS_LABEL;
  lblCancelar = commonValues.CANCELAR_LABEL;
  lblCodigo = commonValues.CODIGO_LABEL;
  lablNombre = commonValues.NOMBRE_LABEL;
  lblRecurso = commonValues.RECURSO_LABEL;
  lblEstado = commonValues.ESTADO_LABEL;
  lblActivo = commonValues.ACTIVO_LABEL;
  lblInactivo = commonValues.INACTIVO_LABEL;
  lblAcciones = commonValues.ACCIONES_LABEL;
  lblFiltroTabla = commonValues.FILTRO_TABLA_LABEL;

  controls: any = {
    pageSize: 10,
    filter: '',
    rowCount: 0, //total elementos
    curPage: 0, //numero pagina
    selectedCount: 0,
    offset: 0
  }
  rows = [];
  cols = common.cols;
  temp = [];
  reorderable = true;
  messages = {
    emptyMessage: commonMessage.NO_EXISTE_INFO,
    totalMessage: commonMessage.REGISTROS_TOTALES,
  };

  constructor(
    private perfilCuentaService: PerfilCuentaService,
    private perfilPrivilegioService: PerfilPrivilegioService,
    private privilegioService: PrivilegiosService,
    private recursoService: RecursosService,
  ) {
    this.formPrivilegios = this.createForm();
  }

  @Input()
  set cuentaUsuario(cuenta: CuentaUsuario) {
    this.cuenta = cuenta;
    this.consultarPerfilCuenta(cuenta);
  }

  ngOnInit() {
    this.actualizarListaRecursos();
  }

  actualizarListaRecursos(): any {
    this.recursoService.listar(common.consultaEstadoEstandarPageable).subscribe(
      response => {
        this.recursos.length = 0;
        this.recursos.push({ value: "", label: commonValues.SELECCIONE_RECURSO_LABEL });
        for (let item of response.content) {
          this.recursos.push({ value: item.id, label: '[' + item.codigo + '] ' + item.nombre });
        }
      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );
  }
  nuevo() {
    this.privilegio = new Privilegio;
  }

  verTabla() {
    this.viewForm = false;
  }

  createForm(): any {
    return {
      header: {
        label: commonValues.DATOS_BASICOS_LABEL,
      },
      fields: {
        id: {},
        row: {
          row: [
            {
              key: "codigo",
              label: commonValues.CODIGO_LABEL,
              typeElement: TypeElement.TEXT,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
              placeholder: commonMessage.INGRESE_CODIGO
            },
            {
              key: "nombre",
              label: commonValues.NOMBRE_LABEL,
              typeElement: TypeElement.TEXT,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
              placeholder: commonMessage.INGRESE_NOMBRE
            },
            {
              key: "estado",
              label: commonValues.ESTADO_LABEL,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesEstado,
              default: this.opcionesEstado[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            }
          ]
        },
        row2: {
          row: [
            {
              key: "recurso",
              label: commonValues.RECURSO_LABEL,
              typeElement: TypeElement.SELECT,
              options: this.recursos,
              default: this.recursos[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {
              key: "operacion",
              label: commonValues.OPERACION_LABEL,
              typeElement: TypeElement.SELECT,
              options: this.operaciones,
              default: this.operaciones[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {
              key: "tipo",
              label: commonValues.TIPO_LABEL,
              typeElement: TypeElement.SELECT,
              options: this.opcionesTipo,
              default: this.opcionesTipo[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            }
          ]
        },
        documentacion: {
          label: commonValues.DOCUMENTACION_LABEL,
          typeElement: TypeElement.TEXTAREA
        }

      }
    }
  }

  consultarPerfilCuenta(cuenta: CuentaUsuario) {
    this.perfilesCuenta = new Array;
    this.perfilCuentaService.listar({ estado: [commonValues.ACTIVO_VALUE, commonValues.INACTIVO_VALUE], page: 0, size: 100, id_cuenta: cuenta.id }).subscribe(
      response => {
        for (let item of response.content) {
          this.perfilCuenta = new Perfil;
          this.perfilCuenta = item.perfil;
          this.perfilesCuenta.push(this.perfilCuenta);
        }
        this.consultarPerfilPrivilegio();
      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );
  }

  consultarPerfilPrivilegio() {
    if (this.perfilesCuenta && this.perfilesCuenta.length > 0) {
      this.privilegiosAsociados = new Array;
      for (let perfil of this.perfilesCuenta) {
        this.perfilPrivilegioService.listar({ estado: [commonValues.ACTIVO_VALUE, commonValues.INACTIVO_VALUE], page: 0, size: 100, id_perfil: perfil.id }).subscribe(
          response => {
            for (let item of response.content) {
              this.privilegioCuenta = new Privilegio;
              this.privilegioCuenta = item.privilegio;
              if (!this.privilegiosAsociados.find(valorE => valorE.id === this.privilegioCuenta.id)) {
                this.privilegiosAsociados.push(this.privilegioCuenta);
                this.controls.rowCount++;
              }
            }
            this.rows = this.privilegiosAsociados;
            this.rows = [...this.rows];
            this.temp = this.rows;
            this.loadingIndicator = false;
            this.controls.rowCount = this.rows.length - 1;
            this.controls.curPage = (this.rows.length - 1) / 10;
          },
          error => {
            console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
          }
        );
      }
    }
  }

  visualizar(id) {
    this.buscar(id);
    this.viewForm = true;
  }

  buscar(id) {
    this.privilegioService.consultar(id).subscribe(
      response => {
        this.formPrivileges.form.disable();
        this.privilegio = response;
        this.formPrivileges.form.setValue(response);
        this.formPrivileges.form.controls['operacion'].setValue(response.operacion != null ? response.operacion : "");
        this.formPrivileges.form.controls['recurso'].setValue(response.recurso != null ? response.recurso.id : "");
      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );

  }

  /**
* Description: Metodo para el filtro de la tabla de consulta
* responde al evento keyup del elemento ngx-dataTable
*
* @param {*} event Valor del input del filtro del dataTable
* @memberof ModulosComponent
*/
  updateFilter(event) {
    this.rows = this.privilegiosAsociados;
    let val = event.target.value.toLowerCase();
    let colsAmt = this.cols.length;
    let keys = [commonValues.CODIGO_VALUE, commonValues.NOMBRE_VALUE];
    this.rows = this.temp.filter(function (item) {
      for (let i = 0; i < colsAmt; i++) {
        if (item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 || !val) {
          return true;
        }
      }
    });
    this.table.offset = 0;
  }

  /**
   * Description: Metodo que ejecuta la paginacion del ngx-dataTable responde
   * al evento ngModelChange del componente select
   *
   * @param {*} value Valor del select del componente paginacion del dataTable
   * @memberof ModulosComponent
   */
  updatePageSize(value) {
    this.controls.pageSize = parseInt(value, 10);
    this.table.limit = this.controls.pageSize;
    this.table.offset = 0;
    this.controls.curPage = (this.rows.length - 1) / 10;
  }
}
