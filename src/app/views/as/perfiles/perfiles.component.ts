import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { PerfilPrivilegioService } from '@app/core/services/as/perfil-privilegio.service';
import { TypeElement } from '@app/shared/components/general-form/types/TypeElement';
import { GeneralFormComponent } from '@app/shared/components/general-form/general-form.component';
import { Perfil } from './models/perfil';
import { PerfilesService } from '@app/core/services/as/perfiles.service';
import { FuentedatosService } from '@app/core/services/as/fuentadatos.service';

import { commonOptions } from '@commons/commonOptions';
import { commonMessage } from '@commons/commonMessages';
import { commonValues } from '@commons/commonValues';
import { ParametrosPerfil } from './models/parametros-perfil';
import { NotificationService } from '@app/core/services';
import { Fuentedatos } from '../fuentedatos/models/fuentedatos';
import { PerfilCuentaService } from '@app/core/services/as/perfil-cuenta.service';
import { GenericObject } from '@app/views/picklist/models/genericObject';
import { CuentasUsuarioService } from '@app/core/services/as/cuentas-usuario.service';
import { PrivilegiosService } from '@app/core/services/as/privilegios.service';
import { Privilegio } from '../privilegios/models/privilegio';
import { CuentaUsuario } from '../cuentas-usuario/models/cuentasUsuario';
import { PicklistComponent } from '@app/views/picklist/picklist.component';
import { PerfilCuenta } from './models/perfil-cuenta';
import { PerfilPrivilegio } from './models/perfil-privilegio';
import { Persona } from '@app/views/pm/models/personaModel';
import { forkJoin } from 'rxjs';
import { NivelSeguridad } from '../nivel-seguridad/models/nivel-seguridad';
import { NivelSeguridadService } from '@app/core/services/as/nivel-seguridad.service';

@Component({
  selector: 'perfiles',
  templateUrl: './perfiles.component.html',
  styleUrls: ['./perfiles.component.css']
})
export class PerfilesComponent implements OnInit {

  @Input() datos: { llamado: boolean, id: number } = { llamado: false, id: 0 };
  @ViewChild(GeneralFormComponent, { 'static': false }) formPerfiles: GeneralFormComponent;
  @ViewChild(DatatableComponent, { 'static': false }) table: DatatableComponent;
  @ViewChild('pickCuentas', { 'static': false }) pickListCuentas: PicklistComponent;
  @ViewChild('pickPrivilegios', { 'static': false }) pickListPrivilegios: PicklistComponent;

  formPerfil: void;
  perfil: Perfil;
  opcionesEstado = [{ value: commonValues.ACTIVO_VALUE, label: commonValues.ACTIVO_LABEL }, { value: commonValues.INACTIVO_VALUE, label: commonValues.INACTIVO_LABEL }];
  fuentesdatos = [];
  nivelesSeguridad = [];
  cuentasUsuarioDisponibles: CuentaUsuario[];
  cuentasUsuarioAsociadas: CuentaUsuario[];
  privilegiosDisponibles: Privilegio[];
  privilegiosAsociados: Privilegio[];
  currentTab = commonValues.PERFIL_LABEL;
  criterio = "";
  estado = commonValues.ACTIVO_VALUE;
  campo = commonValues.CODIGO_VALUE;
  searching = true;
  disable: any;
  estadoFormulario: boolean;
  suscritoAcomponente = false;
  codigoRecorded: boolean;
  confirmDialog = false;
  sePuedeEliminar: boolean;
  editForm: boolean = false;
  arrayCuentasUsuarioAsociadas: GenericObject[];
  arrayCuentasUsuarioDisponibles: GenericObject[];
  arrayPrivilegiosAsociados: GenericObject[];
  arrayPrivilegiosDisponibles: GenericObject[];
  msgDeseaEliminar = commonMessage.DESEA_ELIMINAR_REGISTRO;
  msgNoSePuedeEliminar = commonMessage.NO_SE_PUEDE_ELIMINAR;
  lblEliminar = commonValues.ELIMINAR_LABEL;
  lblSi = commonValues.SI_LABEL;
  lblNo = commonValues.NO_LABEL;
  lblCancelar = commonValues.CANCELAR_LABEL;
  lblGuardar = commonValues.GUARDAR_LABEL;
  lblAgregar = commonValues.AGREGAR_LABEL;
  lblCriteriosBusqueda = commonValues.CRITERIOS_BUSQUEDA_LABEL;
  lblCriterioBusqueda = commonValues.CRITERIO_BUSQUEDA_LABEL;
  lblFiltroTabla = commonValues.FILTRO_TABLA_LABEL;
  lblCampo = commonValues.CAMPO_LABEL;
  lblCodigo = commonValues.CODIGO_LABEL;
  lblNombre = commonValues.NOMBRE_LABEL;
  lblEstado = commonValues.ESTADO_LABEL;
  lblActivo = commonValues.ACTIVO_LABEL;
  lblInactivo = commonValues.INACTIVO_LABEL;
  lblTodos = commonValues.TODOS_LABEL;
  lblBuscar = commonValues.BUSCAR_LABEL;
  lblAcciones = commonValues.ACCIONES_LABEL;
  lblPerfil = commonValues.PERFIL_LABEL;
  lblCuentas = commonValues.CUENTAS_LABEL;
  lblPrivilegios = commonValues.PRIVILEGIOS_LABEL;

  controls: any = {
    pageSize: 10,
    filter: '',
    rowCount: 0, //total elementos
    curPage: 0, //numero pagina
    selectedCount: 0,
    offset: 0
  }

  cols = [{ name: commonValues.CODIGO_VALUE }, { name: commonValues.NOMBRE_VALUE }];
  temp = [];
  rows = [];
  loadingIndicator = false;
  reorderable = true;
  messages = {
    emptyMessage: commonMessage.NO_EXISTE_INFO,
    totalMessage: commonMessage.REGISTROS_TOTALES,
  };

  constructor(
    private perfilService: PerfilesService,
    private dataSourceService: FuentedatosService,
    private nivelSeguridadService: NivelSeguridadService,
    private perfilPrivilegioService: PerfilPrivilegioService,
    private perfilCuentaService: PerfilCuentaService,
    private notificationService: NotificationService,
    private cuentaUsuarioService: CuentasUsuarioService,
    private privilegioService: PrivilegiosService
  ) {
    this.formPerfil = this.createForm();
    this.actualizarListaFuenteDatos();
    this.actualizarListaNiverSeguridad();
    this.listarCuentasUsuarioDisponibles();
    this.listarPivilegiosDisponibles();
    this.arrayCuentasUsuarioAsociadas = new Array;
    this.arrayPrivilegiosAsociados = new Array;
  }

  ngOnInit() {
  }

  createForm(): any {
    return {
      header: {
        label: commonValues.DATOS_BASICOS_LABEL
      },
      fields: {
        id: {},
        row: {
          row: [
            {
              key: "codigo",
              label: commonValues.CODIGO_LABEL,
              typeElement: TypeElement.TEXT,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
              placeholder: commonMessage.INGRESE_CODIGO
            },
            {
              key: "nombre",
              label: commonValues.NOMBRE_LABEL,
              typeElement: TypeElement.TEXT,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
              placeholder: commonMessage.INGRESE_NOMBRE
            },
            {
              key: "estado",
              label: commonValues.ESTADO_LABEL,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesEstado,
              default: this.opcionesEstado[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            }
          ]
        },
        row2: {
          row: [
            {
              key: "datasource",
              label: commonValues.FUENTE_DATOS_LABEL,
              typeElement: TypeElement.SELECT,
              options: this.fuentesdatos,
              default: this.fuentesdatos[0]
            },
            {
              key: "nivelSeguridad",
              label: commonValues.NIVEL_SEGURIDAD_LABEL,
              typeElement: TypeElement.SELECT,
              options: this.nivelesSeguridad,
              default: this.nivelesSeguridad[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            }
          ]
        },
        documentacion: {
          label: commonValues.DOCUMENTACION_LABEL,
          typeElement: TypeElement.TEXTAREA
        }
      }
    }
  }

  actualizarListaFuenteDatos(): any {
    this.dataSourceService.listar(commonOptions.consultaEstadoActivo).subscribe(
      response => {
        this.fuentesdatos.length = 0;
        this.fuentesdatos.push(commonOptions.seleccioneValorEstandar);
        for (let item of response.content) {
          this.fuentesdatos.push({ value: item.id, label: '[' + item.codigo + '] ' + item.nombre });
        }
      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );
  }

  actualizarListaNiverSeguridad(): any {
    this.nivelSeguridadService.listar(commonOptions.consultaEstadoEstandar).subscribe(
      response => {
        this.nivelesSeguridad.length = 0;
        this.nivelesSeguridad.push(commonOptions.seleccioneValorEstandar);
        for (let item of response.content) {
          this.nivelesSeguridad.push({ value: item.id, label: '[' + item.codigo + '] ' + item.nombre });
        }
      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );
  }

  listarCuentasUsuarioDisponibles() {
    this.arrayCuentasUsuarioDisponibles = new Array;
    this.cuentasUsuarioDisponibles = new Array;
    let cuenta: CuentaUsuario;
    this.cuentaUsuarioService.listar(commonOptions.consultaEstadoActivo).subscribe(
      response => {
        for (let item of response.content) {
          cuenta = new CuentaUsuario;
          cuenta = item;
          this.cuentasUsuarioDisponibles.push(cuenta);
          this.agregarItemPickListCUsuarioDisponible(cuenta);
        }
      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );
  }

  agregarItemPickListCUsuarioDisponible(cuentaUsario: CuentaUsuario) {
    let valor: GenericObject = new GenericObject;
    valor.id = cuentaUsario.id;
    valor.valorUno = "[" + cuentaUsario.cuentaUsuario + "]";
    valor.valorDos = cuentaUsario.persona.nombre + " " + cuentaUsario.persona.primerApellido + " " + cuentaUsario.persona.segundoApellido;
    valor.estado = cuentaUsario.estado;
    this.arrayCuentasUsuarioDisponibles.push(valor);
  }

  listarCuentasUsuarioAsociados() {
    this.arrayCuentasUsuarioAsociadas = new Array;
    this.cuentasUsuarioAsociadas = new Array;
    let cuenta: CuentaUsuario;
    this.perfilCuentaService.listar({ estado: ["ACTIVO", "INACTIVO"], page: 0, size: 100, id_perfil: this.perfil.id }).subscribe(
      response => {
        for (let item of response.content) {
          cuenta = new CuentaUsuario;
          cuenta = item.cuentaUsuario;
          this.cuentasUsuarioAsociadas.push(cuenta);
          this.agregarItemPickListCUsuarioAsociada(cuenta);
        }
      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );
  }

  agregarItemPickListCUsuarioAsociada(cuentaUsario: CuentaUsuario) {
    let valor: GenericObject = new GenericObject;
    let cuentaU: CuentaUsuario = cuentaUsario;
    let persona: Persona = cuentaU.persona;
    valor.id = cuentaU.id;
    valor.valorUno = "[" + cuentaU.cuentaUsuario + "]";
    valor.valorDos = persona.nombre + " " + persona.primerApellido + " " + persona.segundoApellido;
    valor.estado = cuentaU.estado;
    if (this.arrayCuentasUsuarioDisponibles && this.arrayCuentasUsuarioDisponibles.length > 0) {
      if (this.arrayCuentasUsuarioDisponibles.find(valorE => valorE.id === valor.id)) {
        this.arrayCuentasUsuarioDisponibles = this.arrayCuentasUsuarioDisponibles.filter(function (val) {
          return val.id !== valor.id;
        });
      }
    }
    this.arrayCuentasUsuarioAsociadas.push(valor);
  }

  listarPivilegiosDisponibles() {
    this.arrayPrivilegiosDisponibles = new Array;
    this.privilegiosDisponibles = new Array;
    let privilegio: Privilegio;
    this.privilegioService.listar(commonOptions.consultaEstadoActivo).subscribe(
      response => {
        for (let item of response.content) {
          privilegio = new Privilegio;
          privilegio = item;
          this.privilegiosDisponibles.push(privilegio);
          this.agregarItemPickListPrivilegioDisponible(privilegio);
        }
      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );
  }

  agregarItemPickListPrivilegioDisponible(privilegio: Privilegio) {
    let valor: GenericObject = new GenericObject;
    valor.id = privilegio.id;
    valor.valorUno = "[" + privilegio.codigo + "]";
    valor.valorDos = privilegio.nombre;
    valor.estado = privilegio.estado;
    this.arrayPrivilegiosDisponibles.push(valor);
  }

  listarPrivilegiosAsociados() {
    this.arrayPrivilegiosAsociados = new Array;
    this.privilegiosAsociados = new Array;
    let privilegio: Privilegio;
    this.perfilPrivilegioService.listar({ estado: commonOptions.estadosActivoInactivo, page: 0, size: 100, id_perfil: this.perfil.id }).subscribe(
      response => {
        for (let item of response.content) {
          privilegio = new Privilegio;
          privilegio = item.privilegio;
          this.privilegiosAsociados.push(privilegio);
          this.agregarItemPickListPrivilegioAsociado(privilegio);
        }
      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );
  }

  agregarItemPickListPrivilegioAsociado(privilegio: Privilegio) {
    let valor: GenericObject = new GenericObject;
    valor.id = privilegio.id;
    valor.valorUno = "[" + privilegio.codigo + "]";
    valor.valorDos = privilegio.nombre;
    valor.estado = privilegio.estado;
    if (this.arrayPrivilegiosDisponibles && this.arrayPrivilegiosDisponibles.length > 0) {
      if (this.arrayPrivilegiosDisponibles.find(valorE => valorE.id === valor.id)) {
        this.arrayPrivilegiosDisponibles = this.arrayPrivilegiosDisponibles.filter(function (val) {
          return val.id !== valor.id;
        });
      }
    }
    this.arrayPrivilegiosAsociados.push(valor);
  }


  selectTab(moduleCode: string) {
    this.currentTab = moduleCode
  }

  editando() {
    return !this.searching
  }

  consultando() {
    return this.searching
  }

  limpiarCriterio() {
    this.criterio = "";
  }

  verFormulario(estado: boolean) {
    this.estadoFormulario = estado;
    this.searching = false;
    this.nuevo();
    this.actualizarListaFuenteDatos();
    this.actualizarListaNiverSeguridad();
    this.listarCuentasUsuarioDisponibles();
    this.listarPivilegiosDisponibles();
    this.suscritoAcomponente = false;
    this.arrayCuentasUsuarioAsociadas = new Array;
    this.arrayPrivilegiosAsociados = new Array;
  }

  verTabla() {
    this.searching = true;
    this.disable = false;
    this.selectTab(commonValues.PERFIL_LABEL);
    this.nuevo();
  }

  visualizar(id) {
    this.verFormulario(false);
    this.buscar(id, true);
    this.disable = true;
  }

  nuevo(): any {
    this.currentTab = commonValues.PERFIL_LABEL;
    this.perfil = new Perfil;
  }

  editar(id) {
    this.verFormulario(true);
    this.buscar(id, false);
    this.editForm = true;
  }

  consultar() {
    this.loadingIndicator = true;
    const x = new ParametrosPerfil;
    x.size = this.controls.pageSize;
    x.page = this.controls.curPage;

    if (this.criterio != "") {
      if (this.estado != commonValues.TODOS_VALUE) {
        x.estado = this.estado.split(",");
      }
      if (this.criterio != commonValues.CRITERIO_VALUE) {
        if (this.campo == commonValues.CODIGO_VALUE) {
          x.codigo = this.criterio;
        } else if (this.campo == commonValues.NOMBRE_VALUE) {
          x.nombre = this.criterio;
        }
      }

      this.perfilService.listar(x).subscribe(
        response => {
          this.rows = response.content;
          this.temp = this.rows;
          this.loadingIndicator = false;
          this.controls.rowCount = response.totalElements;
          this.controls.curPage = response.number;
        },
        error => {
          console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
        }
      );
    }
    this.loadingIndicator = false;
  }

  onSubmit() {
    this.perfil = this.formPerfiles.form.value;
    this.perfil.estado = this.perfil.estado == "" ? commonValues.ACTIVO_VALUE : this.perfil.estado;
    let fuenteDato: Fuentedatos = new Fuentedatos;
    fuenteDato.id = this.formPerfiles.form.get('datasource').value ? this.formPerfiles.form.get('datasource').value : null;;
    let nivelSeguridad: NivelSeguridad = new NivelSeguridad;
    nivelSeguridad.id = this.formPerfiles.form.get('nivelSeguridad').value ? this.formPerfiles.form.get('nivelSeguridad').value : null;
    this.perfil.nivelSeguridad = nivelSeguridad.id ? nivelSeguridad : null;
    this.perfil.datasource = fuenteDato.id ? fuenteDato : null;
    this.perfilService.guardar(this.perfil).subscribe(
      response => {
        this.perfil = response;
        this.registrarPerfilCuenta(response);
        this.registrarPerfilPrivilegio(response);
        this.eliminarPerfilCuenta(response);
        this.eliminarPerfilPrivilegio(response);
        this.searching = true;
        this.nuevo();
        this.notificationService.successMessage(commonMessage.REGISTRO_SATISFACTORIO);
        this.consultar();
      },
      error => {
        this.notificationService.errorMessage(commonMessage.REGISTRO_NO_EXITOSO);
      }
    );
    this.consultar();
  }

  buscar(id, deshabilitar, eliminar?) {
    this.perfilService.consultar(id).subscribe(
      response => {
        this.perfil = response;
        if (eliminar == null) {
          this.formPerfiles.form.setValue(response);
          this.formPerfiles.form.controls['datasource'].setValue(response.datasource != null ? response.datasource.id : "");
          this.formPerfiles.form.controls['nivelSeguridad'].setValue(response.nivelSeguridad != null ? response.nivelSeguridad.id : "");
          if (deshabilitar) {
            this.formPerfiles.form.disable();
            this.desactivarPicksList(true);
          }
        }
        this.listarCuentasUsuarioAsociados();
        this.listarPrivilegiosAsociados();
      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );
  }

  eliminar(id) {
    this.perfilService.eliminar(id).subscribe(
      response => {
        this.notificationService.successMessage(commonMessage.REGISTRO_ELIMINADO);
        this.consultar();
      },
      error => {
        this.notificationService.errorMessage(commonMessage.REGISTRO_NO_ELIMINADO);
      }
    );
    this.consultar();
  }

  consultarRelaciones(id) {
    let arrayTemp = new Array;
    let arrayTemp2 = new Array;
    let arrayConcat = new Array;
    this.sePuedeEliminar = false;
    forkJoin(
      [
        this.perfilPrivilegioService.listar(commonOptions.consultaEstadoEstandarCompleto),
        this.perfilCuentaService.listar(commonOptions.consultaEstadoEstandarCompleto)
      ]
    ).subscribe(results => {
      arrayTemp = results[0].content;
      arrayTemp2 = results[1].content;
      arrayConcat = arrayTemp.concat(arrayTemp2);     
      if(arrayConcat != null && arrayConcat.length > 0){
        for (let item of arrayConcat){
          if (item && item.perfil && item.perfil.id == id) {
            this.sePuedeEliminar = true;
          }
        }
      }
      this.confirmDialog = true;
    }
    );
  }

  registrarPerfilCuenta(perfil: Perfil) {
    if (this.pickListCuentas.arrayFirst && this.pickListCuentas.arrayFirst != null && this.pickListCuentas.arrayFirst.length > 0) {
      if (this.cuentasUsuarioAsociadas && this.cuentasUsuarioAsociadas != null) {
        for (let item of this.cuentasUsuarioAsociadas) {
          this.pickListCuentas.arrayFirst = this.pickListCuentas.arrayFirst.filter(function (val) {
            return val.id !== item.id;
          });
        }
      }

      for (let item of this.pickListCuentas.arrayFirst) {
        let perfilCuenta: PerfilCuenta = new PerfilCuenta;
        let cuenta: CuentaUsuario = new CuentaUsuario;
        let perfilg: Perfil = new Perfil;
        perfilg.id = perfil.id;
        cuenta.id = item.id;
        perfilCuenta.perfil = perfilg;
        perfilCuenta.cuentaUsuario = cuenta;
        perfilCuenta.estado = perfilCuenta.estado ? perfilCuenta.estado : commonValues.ACTIVO_VALUE;
        if (this.editForm) {
          this.perfilCuentaService.editar(perfilCuenta).subscribe(
            response => {
            },
            error => {
              console.log(commonMessage.REGISTRO_NO_EXITOSO, error);
            }
          );
        } else {
          this.perfilCuentaService.guardar(perfilCuenta).subscribe(
            response => {
            },
            error => {
              console.log(commonMessage.REGISTRO_NO_EXITOSO, error);
            }
          );
        }
      }
    }
  }

  registrarPerfilPrivilegio(perfil: Perfil) {
    if (this.pickListPrivilegios.arrayFirst && this.pickListPrivilegios.arrayFirst != null && this.pickListPrivilegios.arrayFirst.length > 0) {
      if (this.privilegiosAsociados && this.privilegiosAsociados != null) {
        for (let item of this.privilegiosAsociados) {
          this.pickListPrivilegios.arrayFirst = this.pickListPrivilegios.arrayFirst.filter(function (val) {
            return val.id !== item.id;
          });
        }
      }

      for (let item of this.pickListPrivilegios.arrayFirst) {
        let perfilPrivilegio: PerfilPrivilegio = new PerfilPrivilegio;
        let privilegio: Privilegio = new Privilegio;
        let perfilg: Perfil = new Perfil;
        perfilg.id = perfil.id;
        privilegio.id = item.id;
        perfilPrivilegio.perfil = perfilg;
        perfilPrivilegio.privilegio = privilegio;
        perfilPrivilegio.estado = perfilPrivilegio.estado ? perfilPrivilegio.estado : commonValues.ACTIVO_VALUE;
        if (this.editForm) {
          this.perfilPrivilegioService.editar(perfilPrivilegio).subscribe(
            response => {
            },
            error => {
              console.log(commonMessage.REGISTRO_NO_EXITOSO, error);
            }
          );
        } else {
          this.perfilPrivilegioService.guardar(perfilPrivilegio).subscribe(
            response => {
            },
            error => {
              console.log(commonMessage.REGISTRO_NO_EXITOSO, error);
            }
          );
        }
      }
    }
  }

  eliminarPerfilCuenta(perfil: Perfil) {
    let perfilB: Perfil = perfil;
    let cuentasAsociadasIniciales: CuentaUsuario[] = this.cuentasUsuarioAsociadas;
    if (cuentasAsociadasIniciales && cuentasAsociadasIniciales.length > 0) {
      for (let item of cuentasAsociadasIniciales) {
        let encontrado = this.pickListCuentas.arraySecond.find(cuenta => cuenta.id === item.id);
        if (encontrado && encontrado != null) {
          this.perfilCuentaService.eliminarPorPerfilCuenta({ id_perfil: perfilB.id, id_cuenta: item.id }).subscribe(
            response => {
            },
            error => {
              this.notificationService.errorMessage(commonMessage.REGISTRO_NO_ELIMINADO);
            }
          );
        }
      }
    }
  }

  eliminarPerfilPrivilegio(perfil: Perfil) {
    let perfilB: Perfil = perfil;
    let privilegioAsociadosIniciales: Privilegio[] = this.privilegiosAsociados;
    if (privilegioAsociadosIniciales && privilegioAsociadosIniciales.length > 0) {
      for (let item of privilegioAsociadosIniciales) {
        let encontrado = this.pickListPrivilegios.arraySecond.find(privilegio => privilegio.id === item.id);
        if (encontrado && encontrado != null) {
          this.perfilPrivilegioService.eliminarPorPerfilPrivilegio({ id_perfil: perfilB.id, id_privilegio: item.id }).subscribe(
            response => {
            },
            error => {
              this.notificationService.errorMessage(commonMessage.REGISTRO_NO_ELIMINADO);
            }
          );
        }
      }
    }
  }

  openModal(id) {
    if (id != null) {
      this.buscar(id, false, true);
      this.consultarRelaciones(id);
    }
  }

  confirm(): void {
    this.eliminar(this.perfil.id);
    this.nuevo();
    this.confirmDialog = false;
  }

  decline(): void {
    this.confirmDialog = false;
    this.nuevo();
  }

  updateFilter(event) {
    let val = event.target.value.toLowerCase();
    let colsAmt = this.cols.length;
    let keys = [commonValues.CODIGO_VALUE, commonValues.NOMBRE_VALUE];
    this.rows = this.temp.filter(function (item) {
      for (let i = 0; i < colsAmt; i++) {
        if (item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 || !val) {
          return true;
        }
      }
    });
    this.table.offset = 0;
  }
  updatePageSize(value) {
    this.controls.pageSize = parseInt(value, 10);
    this.table.limit = this.controls.pageSize;
    this.table.offset = 0;
    this.controls.curPage = 0;
    this.consultar();
  }
  setPage(event) {
    this.controls.curPage = event.offset;
    this.controls.pageSize = event.pageSize;
    this.consultar();
  }

  get isInvalid() {
    if (this.formPerfiles) {
      this.onChanges();
    }
    return this.formPerfiles ? this.formPerfiles.form.invalid : true;
  }

  get desactivar() {
    return this.disable;
  }

  desactivarPicksList(val: boolean) {
    this.pickListCuentas.pickList.disabled = val;
    this.pickListCuentas.pickList.dragdrop = !val;
    this.pickListPrivilegios.pickList.disabled = val;
    this.pickListPrivilegios.pickList.dragdrop = !val;
  }

  onChanges(): void {
    if (this.formPerfiles && !this.suscritoAcomponente) {
      this.suscritoAcomponente = true;
      this.formPerfiles.form.get(commonValues.CODIGO_VALUE).valueChanges.subscribe(val => {
        this.onKeyupCodigo(val);

      });
    }
  }

  onKeyupCodigo(codigoPerfil) {
    if ((codigoPerfil && (this.perfil.id && codigoPerfil != this.perfil.codigo)) || !this.perfil.id && codigoPerfil) {
      this.perfilService.listar({ codigo: codigoPerfil, estado: ['ACTIVO,INACTIVO,ELIMINADO'] })
        .subscribe(data => {
          data = data.content;
          this.codigoRecorded = data != null && data.length > 0;
          if (this.codigoRecorded) {
            this.formPerfiles.form.controls[commonValues.CODIGO_VALUE].setErrors({ 'codigoRecorded': true });
          } else {
            this.formPerfiles.form.controls[commonValues.CODIGO_VALUE].setErrors({ 'codigoRecorded': false });
            const errores = this.formPerfiles.form.controls[commonValues.CODIGO_VALUE].errors;
            delete errores.codigoRecorded;
            this.formPerfiles.form.controls[commonValues.CODIGO_VALUE].setErrors(Object.keys(errores).length === 0 ? null : errores);
          }
        },
          error => console.error(error))
    }
  }
}
