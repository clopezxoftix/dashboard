import { Datasource } from "../../recursos/models/reporte";
import { NivelSeguridad } from "../../nivel-seguridad/models/nivel-seguridad";

export class Perfil {
    constructor() {
    }
    codigo: string;
    documentacion: string;
    estado: string;
    id: number;
    nombre: string;
    nivelSeguridad: NivelSeguridad;
    datasource: Datasource;
}