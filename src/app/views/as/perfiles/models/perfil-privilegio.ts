import { Perfil } from "./perfil";
import { CuentaUsuario } from "../../cuentas-usuario/models/cuentasUsuario";
import { Privilegio } from "../../privilegios/models/privilegio";

export class PerfilPrivilegio {
    constructor() {
    }
    id: number;
    perfil: Perfil;
    privilegio: Privilegio;
    estado: string;
}