import { Perfil } from "./perfil";
import { CuentaUsuario } from "../../cuentas-usuario/models/cuentasUsuario";

export class PerfilCuenta {
    constructor() {
    }
    id: number;
    perfil: Perfil;
    cuentaUsuario: CuentaUsuario;
    estado: string;
}