import { Component, OnInit, ViewChild } from '@angular/core';
import { CuentaUsuario } from './models/cuentasUsuario';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ParametrosCuentasUsuario } from './models/parametrosCuentasUsuario';
import { CuentasUsuarioService } from '@app/core/services/as/cuentas-usuario.service';
import { TypeElement } from '@app/shared/components/general-form/types/TypeElement';
import { GeneralFormComponent } from '@app/shared/components/general-form/general-form.component';
import { IdentificacionService } from '@app/core/services/pmt/identificacion.service';
import { PersonaService } from '@app/core/services/pmt/persona.service';
import { NivelSeguridadService } from '@app/core/services/as/nivel-seguridad.service';
import { IdiomaService } from '@app/core/services/irt/idioma.service';
import { RegionService } from '@app/core/services/irt/region.service';
import { commonOptions } from '@commons/commonOptions';
import { commonMessage } from '@commons/commonMessages';
import { NotificationService } from '@app/core/services';
import { Persona } from '@app/views/pm/models/personaModel';
import { forkJoin } from 'rxjs';
import { cuentasUsuariosConstants } from './cuentaUsuarioConstants/cuentaUsuarioConstants';
import { commonValues } from '@commons/commonValues';
import { GenericObject } from '@app/views/picklist/models/genericObject';
import { Perfil } from '../perfiles/models/perfil';
import { PerfilesService } from '@app/core/services/as/perfiles.service';
import { PerfilCuentaService } from '@app/core/services/as/perfil-cuenta.service';
import { PicklistComponent } from '@app/views/picklist/picklist.component';
import { PerfilCuenta } from '../perfiles/models/perfil-cuenta';
import { common } from '@commons/common';
import * as moment from 'moment'
import { ValidacionCuentaUsuario } from './models/validacionCuentaUsuarioDTO';
import { Idioma } from '@app/views/ir/idioma/models/idioma';
import { Region } from '@app/views/ir/region/models/region';
import { CuentaUsuarioVO } from './models/cuentaUsuarioVO';
import { NivelSeguridad } from '../nivel-seguridad/models/nivel-seguridad';

@Component({
  selector: 'cuentas-usuario',
  templateUrl: './cuentas-usuario.component.html',
  styleUrls: ['./cuentas-usuario.component.css']
})
export class CuentasUsuarioComponent implements OnInit {

  @ViewChild(DatatableComponent, { 'static': false }) table: DatatableComponent;
  @ViewChild('formCuentaUsuarios', { 'static': false }) formCuentaUsuarios: GeneralFormComponent;
  @ViewChild('formUsuarios', { 'static': false }) formUsuarios: GeneralFormComponent;
  @ViewChild('pickPerfiles', { 'static': false }) pickListPerfiles: PicklistComponent;

  formUsuario: any;
  formCuentaUsuario: any;
  campo = commonValues.CODIGO_VALUE;
  criterio = "";
  estado = commonValues.ACTIVO_VALUE;
  searching = true;
  disable = false;
  viewTabPrivilegios = false;
  viewForm: boolean = false;
  cuentaUsuario = new CuentaUsuario;
  cuentaUsuarioVO = new CuentaUsuarioVO;
  persona = new Persona;
  opcionesEstado = commonOptions.opcionesEstado;
  opcionesSiNo = commonOptions.opcionesSiNo;
  idPersona: any;
  regiones = [];
  idiomas = [];
  nivelesSeguridad = [];
  identificaciones = [];
  perfilesAsociados: Perfil[];
  perfilesDisponibles: Perfil[];
  arrayPerfilesAsociados: GenericObject[];
  arrayPerfilesDisponibles: GenericObject[];
  suscritoAcomponente = false;
  okGuardar: boolean;
  contraseniaConcide: boolean = false;
  cuentaRecorded: boolean;
  editForm: boolean = false;
  newForm: boolean = false;
  confirmDialog = false;
  editDialog = false;
  sePuedeEliminar: boolean;
  cuentaOk: boolean = false;
  consultarIdentificaciones: boolean = false;
  contraseniaValida: boolean = false;
  currentTab = cuentasUsuariosConstants.CUENTA_USUARIO;
  valorPaginado = 10;
  commonValues = commonValues;
  commonMessage = commonMessage;
  contraseniaAnteriorDynamic = { visible: false };
  fechaHoraCreacionDynamic = { visible: false };
  ultimoIngresoExitosoDynamic = { visible: false };
  ultimoCambioClaveDynamic = { visible: false };

  /* Variables relacionadas a la tabla de consulta */
  controls: any = {
    pageSize: 10,
    filter: '',
    rowCount: 0, //total elementos
    curPage: 0, //numero pagina
    selectedCount: 0,
    offset: 0
  }
  cols = commonOptions.cols;
  temp = [];
  rows = [];
  activo: any;
  loadingIndicator = false;
  reorderable = true;
  messages = {
    emptyMessage: commonMessage.NO_EXISTE_INFO,
    totalMessage: commonMessage.REGISTROS_TOTALES,
  };

  constructor(
    private cuentasUsuarioService: CuentasUsuarioService,
    private identificacionService: IdentificacionService,
    private personaService: PersonaService,
    private nivelesSeguridadService: NivelSeguridadService,
    private idiomaService: IdiomaService,
    private regionService: RegionService,
    private perfilService: PerfilesService,
    private perfilCuentaService: PerfilCuentaService,
    private notificationService: NotificationService
  ) {
    this.formCuentaUsuario = this.createFormCuenta();
    this.formUsuario = this.createFormUsuario();
    this.actualizarListaIdentificaciones();
    this.actualizarListaNivelesSeguridad();
    this.listarPerfilesDisponibles();
    this.actualizarListaIdioma();
    this.actualizarListaRegion();
  }

  ngOnInit() {
  }

  createFormUsuario(): any {
    return {
      header: {
        label: cuentasUsuariosConstants.INFORMACION_USUARIO
      },
      fields: {
        row: {
          row: [
            {
              key: "tipo_identificacion",
              label: cuentasUsuariosConstants.IDENTIFICACION_PERSONA,
              typeElement: TypeElement.SELECCIONAREGISTRO,
              service: this.personaService,
              inputsConsulta: common.estadoNombreOpciones,
              camposHeadersTabla: ["Identificacion", "Nombre", "Email"],
              camposTabla: ["identificacion", "nombreCompleto", "emailPrincipal"],
              parametrosDefecto: common.consultaEstadoActivo,
              camposInput: ["tipoIdentificacion", "identificacion"],
              listarEspecifico: true,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {},
            {}
          ]
        },
        row1: {
          row: [
            {
              key: "nombres",
              label: commonValues.NOMBRES_LABEL,
              typeElement: TypeElement.TEXT,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {
              key: "apellidos",
              label: commonValues.APELLIDOS_LABEL,
              typeElement: TypeElement.TEXT,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {
              key: "email",
              label: commonValues.CORREO_LABEL,
              typeElement: TypeElement.TEXT,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            }
          ]
        },
        id: {}
      }
    }

  }

  createFormCuenta(): any {
    return {
      header: {
        label: cuentasUsuariosConstants.INFORMACION_CUENTA
      },
      fields: {
        row: {
          row: [
            {
              key: "cuentaUsuario",
              label: commonValues.CUENTA_LABEL,
              typeElement: TypeElement.TEXT,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
              placeholder: commonMessage.INGRESE_NOMBRE_USUARIO
            },
            {
              key: "nivelSeguridad",
              label: commonValues.NIVEL_SEGURIDAD_LABEL,
              typeElement: TypeElement.SELECT,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
              options: this.nivelesSeguridad,
              default: this.nivelesSeguridad[0]
            },
            {
              key: "estado",
              label: commonValues.ESTADO_LABEL,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesEstado,
              default: this.opcionesEstado[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            }
          ]
        },
        row2: {
          row: [
            {
              key: "idioma",
              label: commonValues.IDIOMA_LABEL,
              typeElement: TypeElement.SELECCIONAREGISTRO,
              service: this.idiomaService,
              inputsConsulta: common.codigoNombreOpciones,
              camposHeadersTabla: ["Código", "Idioma"],
              camposTabla: ["codigo", "nombre"],
              parametrosDefecto: common.consultaEstadoActivo,
              camposInput: ["nombre"],
              listarEspecifico: false,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            },
            {
              key: "region",
              label: commonValues.REGION_LABLE,
              typeElement: TypeElement.SELECCIONAREGISTRO,
              service: this.regionService,
              inputsConsulta: common.codigoNombreOpciones,
              camposHeadersTabla: ["Código", "Región"],
              camposTabla: ["codigo", "nombre"],
              parametrosDefecto: common.consultaEstadoActivo,
              camposInput: ["nombre"],
              listarEspecifico: false
            },
            {
              key: "copiaCorreoAlterno",
              label: commonValues.COPIA_CORREO_ALTERNO_LABEL,
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.opcionesSiNo,
              default: this.opcionesSiNo[0],
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              }
            }
          ]
        },
        row3: {
          row: [
            {
              key: "clave",
              label: commonValues.CONTRASENIA_LABEL,
              typeElement: TypeElement.PASSWORD,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
              placeholder: commonMessage.VALOR_CONTRASENIA
            },
            {
              key: "repetir_contrasenia",
              label: commonValues.REPETIR_CONTRASENIA_LABEL,
              typeElement: TypeElement.PASSWORD,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
              placeholder: commonMessage.CONFIRMAR_CONTRASENIA
            },
            {
              key: "clave_anterior",
              label: commonValues.CONTRASENIA_ANTERIOR_LABEL,
              typeElement: TypeElement.PASSWORD,
              placeholder: commonMessage.VALOR_CONTRASENIA_ANTERIOR,
              dynamic: this.contraseniaAnteriorDynamic
            }
          ]
        },
        row4: {
          row: [
            {
              key: "fechaHoraCreacion",
              label: commonValues.FECHA_CREACION_LABEL,
              typeElement: TypeElement.TEXT,
              dynamic: this.fechaHoraCreacionDynamic
            },
            {
              key: "ultimoIngresoExitoso",
              label: commonValues.ULTIMO_INGRESO_EXITOSO_LABEL,
              typeElement: TypeElement.TEXT,
              dynamic: this.ultimoIngresoExitosoDynamic
            },
            {
              key: "ultimoCambioClave",
              label: commonValues.ULTIMO_CAMBIO_CLAVE,
              typeElement: TypeElement.TEXT,
              dynamic: this.ultimoCambioClaveDynamic
            }
          ]
        },
        documentacion: {
          label: commonMessage.DOCUMENTACION,
          typeElement: TypeElement.TEXTAREA

        },
        id: {},
        persona: {},
        fechaHoraCreacion: {},
        intentosFallidos: {},
        ultimoIngresoExitoso: {},
        obligaCambioClave: {},
        ultimoCambioClave: {},
        fechaHoraReactivacion: {},
        fechaVenciminetoClave: {},
        ldapDn: {}
      }
    }

  }

  actualizarListaIdentificaciones(): any {
    let criterioBusqueda = this.consultarIdentificaciones || this.editForm ? common.consultaEstadoIdentificacionInactivoPageable : common.consultaEstadoIdentificacionActivoPageable;
    this.identificacionService.listar(criterioBusqueda).subscribe(response => {
      this.identificaciones.length = 0;
      this.identificaciones.push({ value: "", label: commonValues.SELECCIONE_IDENTIFICACION_LABEL });
      for (let item of response.content) {
        this.identificaciones.push({ value: item.id, label: item.identificacion });
      }
    },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );
  }

  actualizarListaNivelesSeguridad(): any {
    let criterioBusqueda = this.consultarIdentificaciones || this.editForm ? common.consultaEstadoEstandarPageable : common.consultaEstadoActivoPageable;
    this.nivelesSeguridadService.listar(criterioBusqueda).subscribe(response => {
      this.nivelesSeguridad.length = 0;
      this.nivelesSeguridad.push({ value: "", label: commonValues.SELECCIONE_NIVEL_SEGURIDAD_LABEL });
      for (let item of response.content) {
        this.nivelesSeguridad.push({ value: item.id, label: "[" + item.codigo + "] " + item.nombre });
      }
    },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );
  }

  actualizarListaIdioma(): any {
    let criterioBusqueda = this.consultarIdentificaciones || this.editForm ? common.consultaEstadoEstandarPageable : common.consultaEstadoActivoPageable;
    this.idiomaService.listar(criterioBusqueda).subscribe(response => {
      this.idiomas.length = 0;
      this.idiomas.push({ value: "", label: commonValues.SELECCIONE_IDIOMA_LABEL });
      for (let item of response.content) {
        this.idiomas.push({ value: item.id, label: "[" + item.codigo + "] " + item.nombre });
      }
    },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );
  }

  actualizarListaRegion(): any {
    let criterioBusqueda = this.consultarIdentificaciones || this.editForm ? common.consultaEstadoEstandarPageable : common.consultaEstadoActivoPageable;
    this.regionService.listar(criterioBusqueda).subscribe(response => {
      this.regiones.length = 0;
      this.regiones.push({ value: "", label: commonValues.SELECCIONE_REGION_LABEL });
      for (let item of response.content) {
        this.regiones.push({ value: item.id, label: "[" + item.codigo + "] " + item.nombre });
      }
    },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );
  }

  consultarPersona(val: any) {
    let id_identificacion = val && val.pmtIdentificacion ? val.pmtIdentificacion.id : val.id;
    this.personaService.consultar(id_identificacion).subscribe(
      response => {
        this.persona = new Persona;
        this.persona = response;
        this.formUsuarios.form.controls['nombres'].setValue(this.persona.nombre ? this.persona.nombre : "");
        let apellido1 = this.persona.primerApellido ? this.persona.primerApellido : "";
        let apellido2 = this.persona.segundoApellido ? this.persona.segundoApellido : "";
        this.formUsuarios.form.controls['apellidos'].setValue(apellido1 + " " + apellido2);
        this.formUsuarios.form.controls['email'].setValue(this.persona.emailPrincipal ? this.persona.emailPrincipal : "");
      }
    );
  }

  listarPerfilesDisponibles() {
    this.arrayPerfilesDisponibles = new Array;
    this.perfilesDisponibles = new Array;
    let perfil: Perfil;
    this.perfilService.listar(common.consultaEstadoActivoPageable).subscribe(
      response => {
        for (let item of response.content) {
          perfil = new Perfil;
          perfil = item;
          this.perfilesDisponibles.push(perfil);
          this.agregarItemPickListPerfilDisponible(perfil);
        }
      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );
  }

  agregarItemPickListPerfilDisponible(perfil: Perfil) {
    let valor: GenericObject = new GenericObject;
    valor.id = perfil.id;
    valor.valorUno = "[" + perfil.codigo + "]";
    valor.valorDos = perfil.nombre;
    valor.estado = perfil.estado;
    this.arrayPerfilesDisponibles.push(valor);
  }

  listarPerfilesAsociados() {
    this.arrayPerfilesAsociados = new Array;
    this.perfilesAsociados = new Array;
    let perfil: Perfil;
    this.perfilCuentaService.listar({ estado: [commonValues.ACTIVO_VALUE, commonValues.INACTIVO_VALUE], page: 0, size: 100, id_cuenta: this.cuentaUsuarioVO.id }).subscribe(
      response => {
        for (let item of response.content) {
          perfil = new Perfil;
          perfil = item.perfil;
          this.perfilesAsociados.push(perfil);
          this.agregarItemPickListPerfilAsociado(perfil);
        }
      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );
  }

  agregarItemPickListPerfilAsociado(perfil: Perfil) {
    let valor: GenericObject = new GenericObject;
    let perfilA: Perfil = perfil;
    valor.id = perfilA.id;
    valor.valorUno = "[" + perfilA.codigo + "]";
    valor.valorDos = perfilA.nombre;
    valor.estado = perfilA.estado;
    if (this.arrayPerfilesDisponibles && this.arrayPerfilesDisponibles.length > 0) {
      if (this.arrayPerfilesDisponibles.find(valorE => valorE.id === valor.id)) {
        this.arrayPerfilesDisponibles = this.arrayPerfilesDisponibles.filter(function (val) {
          return val.id !== valor.id;
        });
      }
    }
    this.arrayPerfilesAsociados.push(valor);
  }

  selectTab(moduleCode: string) {
    this.currentTab = moduleCode
  }

  editando() {
    return !this.searching
  }

  consultando() {
    this.consultarIdentificaciones = false;
    return this.searching
  }

  verFormulario(edit: boolean) {
    this.editForm = edit;
    this.searching = false
    this.nuevo();
    this.actualizarListaIdentificaciones();
    this.actualizarListaNivelesSeguridad();
    this.actualizarListaIdioma();
    this.actualizarListaRegion();
    this.listarPerfilesDisponibles();
    this.arrayPerfilesAsociados = new Array;
    this.suscritoAcomponente = false;
  }

  verTabla() {
    this.searching = true;
    this.disable = false;
  }

  nuevo() {
    this.currentTab = cuentasUsuariosConstants.CUENTA_USUARIO;
    this.cuentaUsuario = new CuentaUsuario;
    this.cuentaUsuarioVO = new CuentaUsuarioVO;
    this.viewTabPrivilegios = false;
  }

  nuevoForm(){
    this.newForm = true;
    this.verFormulario(false);
  }

  editarForm(id) {
    this.newForm = false;
    this.cuentaOk = false;
    this.verFormulario(true);
    this.buscar(id, false);
    this.editForm = true;
    this.viewTabPrivilegios = true;
  }

  visualizar(id) {
    this.newForm = false;
    this.cuentaOk = false;
    this.consultarIdentificaciones = true;
    this.viewForm = true;
    this.verFormulario(false);
    this.buscar(id, true);
    this.disable = true;
    this.viewTabPrivilegios = true;
  }

  consultar() {
    this.loadingIndicator = true;
    const x = new ParametrosCuentasUsuario;
    x.size = this.controls.pageSize;
    x.page = this.controls.curPage;

    if (this.criterio != "") {
      if (this.estado != commonValues.TODOS_VALUE) {
        x.estado = this.estado.split(",");
      }
      if (this.criterio != "%") {
        if (this.campo == commonValues.CODIGO_VALUE) {
          x.codigo = this.criterio;
        } else if (this.campo == commonValues.NOMBRE_VALUE) {
          x.nombre = this.criterio;
        }
      }

      this.cuentasUsuarioService.listar(x).subscribe(
        response => {
          this.rows = response.content;          
          this.temp = this.rows;
          this.loadingIndicator = false;
          this.controls.rowCount = response.totalElements;
          this.controls.curPage = response.number;
        },
        error => {
          console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
        }
      );
    }
    this.loadingIndicator = false;
  }

  buscar(id, deshabilitar, eliminar?) {
    this.cuentasUsuarioService.consultar(id).subscribe(
      response => {
        this.cuentaUsuarioVO = response;
        this.persona = this.cuentaUsuarioVO.persona;
        this.cuentaOk = true;
        if (eliminar == null) {
          this.cargarFormularioCuentaUsuario();
          this.cargarFormularioUsuario();
          if (deshabilitar) {
            this.formCuentaUsuarios.form.disable();
            this.formUsuarios.form.disable();
            this.desactivarPicksList(true);
          }
        }
        this.listarPerfilesAsociados();
      },
      error => {
        this.notificationService.errorMessage(commonMessage.NO_SE_REALIZO_CONSULTA);
      }
    );
  }

  eliminar(id) {
    this.cuentasUsuarioService.eliminar(id).subscribe(
      response => {
        this.notificationService.successMessage(commonMessage.REGISTRO_ELIMINADO);
        this.consultar();
      },
      error => {
        this.notificationService.errorMessage(commonMessage.REGISTRO_NO_ELIMINADO);
      }
    );
    this.consultar();
  }

  consultarRelaciones(id) {
    let arrayTemp = new Array;
    this.sePuedeEliminar = false;
    forkJoin(
      [
        this.perfilCuentaService.listar(common.consultaEstadoEstandarCompletoPageable)
      ]
    ).subscribe(results => {
      arrayTemp = results[0].content;

      if (arrayTemp != null && arrayTemp.length > 0) {
        for (let item of arrayTemp) {
          if (item && item.cuentaUsuario && item.cuentaUsuario.id == id) {
            this.sePuedeEliminar = true;
          }
        }
      }
      this.confirmDialog = true;
    }
    );
  }

  cargarFormularioUsuario() {
    this.formUsuarios.form.controls['tipo_identificacion'].setValue(this.persona.pmtIdentificacion);
    this.formUsuarios.form.controls['nombres'].setValue(this.persona.nombre ? this.persona.nombre : "");
    let apellido1 = this.persona.primerApellido ? this.persona.primerApellido : "";
    let apellido2 = this.persona.segundoApellido ? this.persona.segundoApellido : "";
    this.formUsuarios.form.controls['apellidos'].setValue(apellido1 + " " + apellido2);
  }

  cargarFormularioCuentaUsuario() {
    let cuentaVer: CuentaUsuario = JSON.parse(JSON.stringify(this.cuentaUsuarioVO));
    cuentaVer.clave_anterior = "";
    cuentaVer.clave = "******";
    cuentaVer.repetir_contrasenia = "******";
    this.formCuentaUsuarios.form.setValue(cuentaVer);
    this.formCuentaUsuarios.form.controls['fechaHoraCreacion'].setValue(moment(this.cuentaUsuarioVO.fechaHoraCreacion).format("MM/DD/YYYY - HH:mm:ss A"));
    this.formCuentaUsuarios.form.controls['ultimoIngresoExitoso'].setValue(this.cuentaUsuarioVO.ultimoIngresoExitoso ? moment(this.cuentaUsuarioVO.ultimoIngresoExitoso).format("MM/DD/YYYY - HH:mm:ss A") : "N/A");
    this.formCuentaUsuarios.form.controls['ultimoCambioClave'].setValue(this.cuentaUsuarioVO.ultimoCambioClave ? moment(this.cuentaUsuarioVO.ultimoCambioClave).format("MM/DD/YYYY - HH:mm:ss A") : "N/A");
    this.formCuentaUsuarios.form.controls['nivelSeguridad'].setValue(this.cuentaUsuarioVO.nivelSeguridad.id);
    this.formCuentaUsuarios.form.controls['idioma'].setValue(this.cuentaUsuarioVO.idioma);
    this.formCuentaUsuarios.form.controls['region'].setValue(this.cuentaUsuarioVO.region ? this.cuentaUsuarioVO.region : "");

  }

  onSubmit() {
    if (!this.editForm) {
      this.guardar();
    } else {
      this.editDialog = true;
    }
  }

  guardar() {
    let persona: Persona = new Persona;
    persona.id = this.persona.id;
    let nivelSeguridad: NivelSeguridad = new NivelSeguridad;
    nivelSeguridad.id = this.formCuentaUsuarios.form.get('nivelSeguridad').value ? this.formCuentaUsuarios.form.get('nivelSeguridad').value : null;
    let idioma: Idioma = new Idioma;
    idioma.id = this.formCuentaUsuarios.form.get('idioma').value ? this.formCuentaUsuarios.form.get('idioma').value.id : null;
    let region: Region = this.formCuentaUsuarios.form.get('region').value ? this.formCuentaUsuarios.form.get('region').value : null;
    this.cuentaUsuario = this.formCuentaUsuarios.form.value;
    this.cuentaUsuario.estado = this.cuentaUsuario.estado == "" ? commonValues.ACTIVO_VALUE : this.cuentaUsuario.estado;
    this.cuentaUsuario.fechaHoraCreacion = new Date();
    this.cuentaUsuario.intentosFallidos = 0;
    this.cuentaUsuario.obligaCambioClave = commonValues.FALSE_VALUE;
    this.cuentaUsuario.persona = persona;
    this.cuentaUsuario.nivelSeguridad = nivelSeguridad;
    this.cuentaUsuario.idioma = idioma;
    this.cuentaUsuario.region = region;
    if (this.validarContraseniaSegunNIvelSeguridad(nivelSeguridad.id, this.cuentaUsuario.clave)) {
      this.guardarCuentaUsuario(this.cuentaUsuario);
      this.consultar();
    } else {
      this.notificationService.errorMessage(commonMessage.CONTRASENIA_NO_CUMPLE_CON_NIVEL_SEGURIDAD);
    }
  }

  editar() {
    let persona: Persona = new Persona;
    persona.id = this.persona.id;
    let nivelSeguridad: NivelSeguridad = new NivelSeguridad;
    nivelSeguridad.id = this.formCuentaUsuarios.form.get('nivelSeguridad').value;
    let cuentaUsuarioForm: CuentaUsuario = this.formCuentaUsuarios.form.value;
    let idioma: Idioma = new Idioma;
    idioma.id = this.formCuentaUsuarios.form.get('idioma').value ? this.formCuentaUsuarios.form.get('idioma').value.id : null;
    let region: Region = this.formCuentaUsuarios.form.get('region').value ? this.formCuentaUsuarios.form.get('region').value : null;
    cuentaUsuarioForm.estado = cuentaUsuarioForm.estado ? cuentaUsuarioForm.estado : commonValues.ACTIVO_VALUE;
    cuentaUsuarioForm.fechaHoraCreacion = this.cuentaUsuarioVO.fechaHoraCreacion;
    cuentaUsuarioForm.intentosFallidos = this.cuentaUsuarioVO.intentosFallidos;
    cuentaUsuarioForm.obligaCambioClave = this.cuentaUsuarioVO.obligaCambioClave;
    cuentaUsuarioForm.cuentaUsuario = this.cuentaUsuarioVO.cuentaUsuario;
    cuentaUsuarioForm.clave = null;
    cuentaUsuarioForm.persona = persona;
    cuentaUsuarioForm.nivelSeguridad = nivelSeguridad;
    cuentaUsuarioForm.idioma = idioma;
    cuentaUsuarioForm.region = region;
    this.cuentaUsuario = cuentaUsuarioForm;
    let claveAnterior: string = this.formCuentaUsuarios.form.get('clave_anterior').value;
    if (claveAnterior != "") {
      let validacionCuentaUsuario: ValidacionCuentaUsuario = new ValidacionCuentaUsuario();
      validacionCuentaUsuario.id = this.cuentaUsuario.id;
      validacionCuentaUsuario.clave = claveAnterior;
      this.cuentasUsuarioService.validarSimilitudClave(validacionCuentaUsuario).subscribe(
        response => {
          this.contraseniaValida = response;
          if (this.contraseniaValida) {
            let claveNueva = this.formCuentaUsuarios.form.get('clave').value;
            if (this.validarContraseniaSegunNIvelSeguridad(nivelSeguridad.id, claveNueva)) {
              this.cuentaUsuario.clave = claveNueva;
              this.editarCuentaUsuario(this.cuentaUsuario);
            } else {
              this.notificationService.errorMessage(commonMessage.CONTRASENIA_NO_CUMPLE_CON_NIVEL_SEGURIDAD);
            }
          } else {
            this.notificationService.warningMessage(commonMessage.CONTRASENIA_ANTERIOR_ERRONEA);
          }
        }
      );
    } else {
      if (this.validarContraseniaSegunNIvelSeguridad(nivelSeguridad.id, this.cuentaUsuario.clave)) {
        this.editarCuentaUsuario(this.cuentaUsuario);
      } else {
        this.notificationService.errorMessage(commonMessage.CONTRASENIA_NO_CUMPLE_CON_NIVEL_SEGURIDAD);
      }
    }
  }

  guardarCuentaUsuario(cuentaUsuario: CuentaUsuario) {
    this.cuentasUsuarioService.guardar(cuentaUsuario).subscribe(
      response => {
        this.cuentaUsuarioVO = response;
        this.registrarPerfilCuenta(response);
        if (this.editForm) {
          this.eliminarPerfilCuenta(response);
        }
        this.searching = true;
        this.nuevo();
        this.notificationService.successMessage(commonMessage.REGISTRO_SATISFACTORIO);
        this.consultar();
        this.contraseniaValida = false;
      },
      error => {
        this.notificationService.errorMessage(commonMessage.REGISTRO_NO_EXITOSO);
      }
    );
  }

  editarCuentaUsuario(cuentaUsuario: CuentaUsuario) {
    this.cuentasUsuarioService.editar(cuentaUsuario.id, cuentaUsuario).subscribe(
      response => {
        this.cuentaUsuarioVO = response;
        this.registrarPerfilCuenta(response);
        if (this.editForm) {
          this.eliminarPerfilCuenta(response);
        }
        this.searching = true;
        this.nuevo();
        this.notificationService.successMessage(commonMessage.REGISTRO_SATISFACTORIO);
        this.consultar();
        this.contraseniaValida = false;
      },
      error => {
        this.notificationService.errorMessage(commonMessage.REGISTRO_NO_EXITOSO);
      }
    );
  }

  validarContraseniaSegunNIvelSeguridad(idNivelSeguridad: number, contrasenia: string) {
    //TODO: VALIDAR CON MICROSERVICIO NIVELSEGURIDAD
    console.log(contrasenia);
    return true;
  }

  registrarPerfilCuenta(cuentaP: CuentaUsuarioVO) {
    if (this.pickListPerfiles.arrayFirst && this.pickListPerfiles.arrayFirst != null && this.pickListPerfiles.arrayFirst.length > 0) {
      if (this.perfilesAsociados && this.perfilesAsociados != null) {
        for (let item of this.perfilesAsociados) {
          this.pickListPerfiles.arrayFirst = this.pickListPerfiles.arrayFirst.filter(function (val) {
            return val.id !== item.id;
          });
        }
      }

      for (let item of this.pickListPerfiles.arrayFirst) {
        let perfilCuenta: PerfilCuenta = new PerfilCuenta;
        let cuenta: CuentaUsuario = new CuentaUsuario;
        let perfilg: Perfil = new Perfil;
        perfilg.id = item.id;
        cuenta.id = cuentaP.id;
        perfilCuenta.perfil = perfilg;
        perfilCuenta.cuentaUsuario = cuenta;
        perfilCuenta.estado = perfilCuenta.estado ? perfilCuenta.estado : commonValues.ACTIVO_VALUE;
        if (this.editForm) {
          this.perfilCuentaService.editar(perfilCuenta).subscribe(
            response => {
            },
            error => {
              console.log(commonMessage.REGISTRO_NO_EXITOSO, error);
            }
          );
        } else {
          this.perfilCuentaService.guardar(perfilCuenta).subscribe(
            response => {
            },
            error => {
              console.log(commonMessage.REGISTRO_NO_EXITOSO, error);
            }
          );
        }
      }
    }
  }

  eliminarPerfilCuenta(cuenta: CuentaUsuarioVO) {
    let cuentaB: CuentaUsuarioVO = cuenta;
    let perfilesAsociadosIniciales: Perfil[] = this.perfilesAsociados;
    if (perfilesAsociadosIniciales && perfilesAsociadosIniciales.length > 0) {
      for (let item of perfilesAsociadosIniciales) {
        let encontrado = this.pickListPerfiles.arraySecond.find(perfil => perfil.id === item.id);
        if (encontrado && encontrado != null) {
          this.perfilCuentaService.eliminarPorPerfilCuenta({ id_cuenta: cuentaB.id, id_perfil: item.id }).subscribe(
            response => {
            },
            error => {
              this.notificationService.errorMessage(commonMessage.REGISTRO_NO_ELIMINADO);
            }
          );
        }
      }
    }
  }

  limpiarCriterio() {
    this.criterio = "";
  }

  openModal(id) {
    if (id != null) {
      this.buscar(id, false, true);
      this.consultarRelaciones(id);
    }
  }

  confirm(): void {
    this.eliminar(this.cuentaUsuarioVO.id);
    this.nuevo();
    this.confirmDialog = false;
  }

  decline(): void {
    this.confirmDialog = false;
    this.nuevo();
  }

  confirmEdit(): void {
    this.editar();
    this.nuevo();
    this.editDialog = false;
  }

  declineEdit(): void {
    this.editDialog = false;
    this.nuevo();
  }

  /**
  * Description: Metodo para el filtro de la tabla de consulta
  * responde al evento keyup del elemento ngx-dataTable
  *
  * @param {*} event Valor del input del filtro del dataTable
  * @memberof ModulosComponent
  */
  updateFilter(event) {
    let val = event.target.value.toLowerCase();
    let colsAmt = this.cols.length;
    let keys = [commonValues.CODIGO_VALUE, commonValues.NOMBRE_VALUE];
    this.rows = this.temp.filter(function (item) {
      for (let i = 0; i < colsAmt; i++) {
        if (item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 || !val) {
          return true;
        }
      }
    });
    this.table.offset = 0;
  }

  /**
   * Description: Metodo que ejecuta la paginacion del ngx-dataTable responde
   * al evento ngModelChange del componente select
   *
   * @param {*} value Valor del select del componente paginacion del dataTable
   * @memberof ModulosComponent
   */
  updatePageSize(value) {
    this.controls.pageSize = parseInt(value, 10);
    this.table.limit = this.controls.pageSize;
    this.table.offset = 0;
    this.controls.curPage = 0;
    this.consultar();
  }

  setPage(event) {
    this.controls.curPage = event.offset;
    this.controls.pageSize = event.pageSize;
    this.consultar();
  }

  get isInvalid() {
    if (!this.suscritoAcomponente) {
      this.onChanges();
    }
    return (this.formUsuarios && this.formUsuarios.form.invalid) || (this.formCuentaUsuarios && this.formCuentaUsuarios.form.invalid);
  }

  get desactivar() {
    return this.disable;
  }

  desactivarPicksList(val: boolean) {
    this.pickListPerfiles.pickList.disabled = val;
    this.pickListPerfiles.pickList.dragdrop = !val;
  }

  onChanges(): void {
    if (this.formUsuarios && this.formUsuarios.form && !this.suscritoAcomponente) {
      this.suscritoAcomponente = true;
      this.formUsuarios.form.get('nombres').disable();
      this.formUsuarios.form.get('apellidos').disable();
      this.formUsuarios.form.get('email').disable();
      this.formUsuarios.form.get('tipo_identificacion').valueChanges.subscribe(val => {
        if (val) {
          this.consultarPersona(val);
        } else {
          this.formUsuarios.form.get('nombres').setValue("");
          this.formUsuarios.form.get('apellidos').setValue("");
          this.formUsuarios.form.get('email').setValue("");
        }
      });
    }
    if (this.formCuentaUsuarios && this.formCuentaUsuarios.form) {
      this.suscritoAcomponente = true;
      this.formCuentaUsuarios.form.get('fechaHoraCreacion').disable();
      this.formCuentaUsuarios.form.get('ultimoIngresoExitoso').disable();
      this.formCuentaUsuarios.form.get('ultimoCambioClave').disable();
      this.contraseniaAnteriorDynamic.visible = !this.editForm ? false : true;
      this.fechaHoraCreacionDynamic.visible = !this.newForm ? true : false;
      this.ultimoIngresoExitosoDynamic.visible = !this.newForm ? true : false;
      this.ultimoCambioClaveDynamic.visible = !this.newForm ? true : false;
      if (!this.editForm) {
        this.formCuentaUsuarios.form.get('clave_anterior').disable();
      } else {
        this.formCuentaUsuarios.form.get('cuentaUsuario').disable();
        this.formCuentaUsuarios.form.get('clave').disable();
        this.formCuentaUsuarios.form.get('repetir_contrasenia').disable();
        this.formCuentaUsuarios.form.get('clave').setValue("*****");
        this.formCuentaUsuarios.form.get('repetir_contrasenia').setValue("*****");
        this.formCuentaUsuarios.form.get('clave_anterior').valueChanges.subscribe(val => {
          if (val) {
            this.formCuentaUsuarios.form.get('clave').enable();
            this.formCuentaUsuarios.form.get('repetir_contrasenia').enable();
            this.formCuentaUsuarios.form.get('clave').setValue(null);
            this.formCuentaUsuarios.form.get('repetir_contrasenia').setValue(null);
          } else {
            this.formCuentaUsuarios.form.get('clave').disable();
            this.formCuentaUsuarios.form.get('repetir_contrasenia').disable();
          }
        });
      }
      this.formCuentaUsuarios.form.get('repetir_contrasenia').valueChanges.subscribe(val => {
        if (val != this.formCuentaUsuarios.form.get('clave').value) {
          this.formCuentaUsuarios.form.controls['repetir_contrasenia'].setErrors({ 'errorCoincidencia': true });
        }
        this.disable = val != this.formCuentaUsuarios.form.get('clave').value;
      }
      );

      this.formCuentaUsuarios.form.get('cuentaUsuario').valueChanges.subscribe(nombreCuenta => {
        this.onKeyupCuenta(nombreCuenta);
      }
      );
    }
  }

  onKeyupCuenta(nombreCuenta) {
    if ((nombreCuenta && (this.cuentaUsuarioVO.id && nombreCuenta != this.cuentaUsuarioVO.cuentaUsuario)) || !this.cuentaUsuarioVO.id && nombreCuenta) {
      this.cuentasUsuarioService.listar({ cuenta_usuario: nombreCuenta, estado: ['ACTIVO,INACTIVO,ELIMINADO'] })
        .subscribe(data => {
          data = data.content;
          this.cuentaRecorded = data != null && data.length > 0;
          if (this.cuentaRecorded) {
            this.formCuentaUsuarios.form.controls['cuentaUsuario'].setErrors({ 'cuentaRecorded': true });
          } else {
            this.formCuentaUsuarios.form.controls['cuentaUsuario'].setErrors({ 'cuentaRecorded': false });
            const errores = this.formCuentaUsuarios.form.controls['cuentaUsuario'].errors;
            delete errores.cuentaRecorded;
            this.formCuentaUsuarios.form.controls['cuentaUsuario'].setErrors(Object.keys(errores).length === 0 ? null : errores);
          }
        },
          error => console.error(error))
    }
  }

}
