
import { Persona } from "@app/views/pm/models/personaModel";
import { Idioma } from "@app/views/ir/idioma/models/idioma";
import { Region } from "@app/views/ir/region/models/region";
import { NivelSeguridad } from "../../nivel-seguridad/models/nivel-seguridad";

export class CuentaUsuarioVO{
    constructor(){}
    id: number;
    estado: string;
    persona: Persona;
    cuentaUsuario: string;
    fechaHoraCreacion: Date;
    nivelSeguridad: NivelSeguridad;
    intentosFallidos: number;
    ultimoIngresoExitoso: Date;
    obligaCambioClave: string;
    ultimoCambioClave: Date;
    fechaHoraReactivacion: Date;
    fechaVencimientoClave: Date;
    copiaCorreoAlterno: string;
    ldapDn: string;
    idioma: Idioma;
    region: Region;
    documentacion: string;
}