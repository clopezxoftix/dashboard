export const cuentasUsuariosConstants = {
    CUENTA_USUARIO: "CUENTA",
    INFORMACION_USUARIO: "Información de usuario",
    IDENTIFICACION_PERSONA: "Identificación de persona",
    INFORMACION_CUENTA: "Información de cuenta"
};