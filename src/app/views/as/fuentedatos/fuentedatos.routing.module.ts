import { FuentedatosComponent } from './fuentedatos.component';
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
    
    {
        path: '', component: FuentedatosComponent , pathMatch: 'full'
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FuentedatosRoutingModule {}