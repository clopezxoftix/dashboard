import { CommonModule } from '@angular/common';
import { NgModule } from "@angular/core";
import { FuentedatosRoutingModule } from './fuentedatos.routing.module';
import { FuentedatosComponent } from "./fuentedatos.component";
import { GeneralFormComponentModule } from '@app/shared/components/general-form/general-form-component.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule, DataTableHeaderCellComponent } from '@swimlane/ngx-datatable';
import { SharedModule } from '@app/shared/shared.module';
import { BlockUIModule } from 'ng-block-ui';


@NgModule({

imports: [
    FuentedatosRoutingModule,
    GeneralFormComponentModule,
    CommonModule,
    FormsModule,
    NgxDatatableModule,
    DataTableHeaderCellComponent,
    SharedModule,    
    BlockUIModule.forRoot(),
    ReactiveFormsModule
],
declarations: [ FuentedatosComponent ],
exports: [GeneralFormComponentModule]
})
export class FuentedatosModule {}