import { Component, OnInit, ViewChild, OnChanges } from '@angular/core';
import { commonValues } from '@commons/commonValues';
import { commonMessage } from '@commons/commonMessages';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { FuentedatosService } from '@app/core/services/as/fuentadatos.service';
import { NotificationService } from '@app/core/services/notification.service';
import { ParametrosFuenteDatos } from './models/parametros-fuente-datos';
import { Fuentedatos } from './models/fuentedatos';
import { GeneralFormComponent } from '@app/shared/components/general-form/general-form.component';
import { TypeCaseText } from '@app/shared/components/general-form/types/TypeCaseText';
import { TypePatternMask } from '@app/shared/components/general-form/types/TypePatternMask';
import { TypeElement } from '@app/shared/components/general-form/types/TypeElement';
import { ReportesService } from '@app/core/services/as/reportes.service';
import { PerfilesService } from '@app/core/services/as/perfiles.service';
import { RecursosService } from '@app/core/services/as/recursos.service';
import { commonOptions } from '@commons/commonOptions';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { FormBuilder, Validators } from '@angular/forms';



@Component({
  selector: 'fuentedatos',
  templateUrl: './fuentedatos.component.html',
  styleUrls: ['./fuentedatos.component.css']
})
export class FuentedatosComponent implements OnInit {

  @ViewChild(GeneralFormComponent, { 'static': false }) afFuenteDatos: GeneralFormComponent;
  @ViewChild(DatatableComponent, { 'static': false }) table: DatatableComponent;

  suscritoAComponenteCodigo: any = false;
  disable: any;
  formFuenteDatos: any;
  formCriterio: any;
  fuenteDatos: Fuentedatos;
  accion = "";
  @BlockUI('formConsulta') blockUIConsulta: NgBlockUI;
  @BlockUI('formRegistro') blockUIRegistro: NgBlockUI;

  /* Inicializacion de filtro de busqueda */
  campo = commonValues.CODIGO_VALUE;
  estado = commonValues.ACTIVO_VALUE;
  criterio = "";

  /*Variables */
  searching = true;
  criterioRequerido = false;
  suscrito = false;
  inicio = false;
  opcionesEstado = commonOptions.tiposEstado;
  opcionesJndi = commonOptions.opcionesJndi;
  estadoMensaje = "";
  codigoRecorded: any;

  //Labels vista
  lblConsulta = commonValues.CONSULTA_LABEL;
  lblFuentesDatos = commonValues.FUENTES_DATOS_LABEL;
  lblCampos = commonValues.CAMPOS_LABEL;
  lblEstado = commonValues.ESTADO_LABEL;
  lblActivo = commonValues.ACTIVO_LABEL;
  lblInactivo = commonValues.INACTIVO_LABEL;
  lblTodos = commonValues.TODOS_LABEL;
  lblCriterioBusqueda = commonValues.CRITERIO_BUSQUEDA_LABEL;
  lblBuscar = commonValues.BUSCAR_LABEL;
  lblAgregar = commonValues.AGREGAR_LABEL;
  lblFiltroTabla = commonValues.FILTRO_TABLA_LABEL;
  lblAcciones = commonValues.ACCIONES_LABEL;
  lblVer = commonValues.VER_LABEL;
  lblEditar = commonValues.EDITAR_LABEL;
  lblEliminar = commonValues.ELIMINAR_LABEL;
  lblSi = commonValues.SI_LABEL;
  lblNo = commonValues.NO_LABEL;
  lblCodigo = commonValues.CODIGO_LABEL;
  lblNombre = commonValues.NOMBRE_LABEL;
  lblUrl = commonValues.URL_LABEL;
  lblfuenteDatos = commonValues.FUENTE_DATOS;
  lblCancelar = commonValues.CANCELAR_LABEL;
  lblGuardar = commonValues.GUARDAR_LABEL;
  msgCriterioObligatorio = commonMessage.CRITERIO_OBLIGATORIO;
  msgDeseaEliminar = commonMessage.DESEA_ELIMINAR_REGISTRO;
  msgNoSePuedeEliminar = commonMessage.NO_SE_PUEDE_ELIMINAR;
  reporteValue = commonValues.REPORTE_VALUE;

  /* Variables relacionadas a la tabla de consulta */
  controls: any = {
    pageSize: 10,
    filter: '',
    rowCount: 0,
    curPage: 0,
    offset: 0
  }

  temp = [];
  rows = [];
  loadingIndicator = false;
  reorderable = true;
  messages = {
    emptyMessage: commonMessage.NO_EXISTE_INFO,
    totalMessage: commonMessage.REGISTROS_TOTALES,
  };

  confirmDialog = false;
  sePuedeEliminar: boolean;

  constructor(private fuenteService: FuentedatosService,
    private notificationService: NotificationService,
    private recursoService: RecursosService,
    private perfilService: PerfilesService,
    private reporteService: ReportesService,
    private formBuilder: FormBuilder) {

  }
  ngOnInit() {
    this.formCriterio = this.formBuilder.group({
      criterio: ['', Validators.required]
    });
    this.formFuenteDatos = this.createForm();
  }
/* Formulario */

  createForm(): any {
    return {
      header: {
        label: commonValues.DATOS_BASICOS_LABEL,
      },
      fields: {
        id: {},
        row: {
          row: [
            {
              key: "codigo",
              label: commonValues.CODIGO_LABEL,
              typeElement: { typeElement: TypeElement.FORMAT },
              textCase: TypeCaseText.UPPER_CASE,
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO,
                expression: TypePatternMask.getPattern(TypePatternMask.LETRAS_NUMEROS_UNDERSCORE)
              },
              placeholder: commonMessage.INGRESE_CODIGO,
              tooltip: commonMessage.CODIGO_IDENTIFICA_REGISTRO
            },
            {
              key: "nombre",
              label: commonValues.NOMBRE_LABEL,
              typeElement: { typeElement: TypeElement.TEXT },
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
              placeholder: commonMessage.INGRESE_NOMBRE
            },
            {
              key: "estado",
              label: commonValues.ESTADO_LABEL,
              typeElement: { typeElement: TypeElement.BUTTON_GROUP },
              options: this.opcionesEstado,
              default: this.opcionesEstado[0],
              dynamic: { visible: true }
            }
          ]
        },
        row2: {
          row: [
            {
              key: "driver",
              label: commonValues.DRIVER_LABEL,
              typeElement: { typeElement: TypeElement.TEXT },
              placeholder: commonMessage.INGRESE_DRIVER

            },
            {
              key: "url",
              label: commonValues.URL_LABEL,
              typeElement: { typeElement: TypeElement.TEXT },
              validation: {
                required: commonMessage.CAMPO_OBLIGATORIO
              },
              placeholder: commonMessage.INGRESE_URL
            },
            {
              key: "jndi",
              label: commonValues.JNDI_LABEL,
              typeElement: { typeElement: TypeElement.BUTTON_GROUP },
              options: this.opcionesJndi,
              default: this.opcionesJndi[0],
              dynamic: { visible: true }
            }
          ]
        },
        row3: {
          row: [
            {
              key: "usuario",
              label: commonValues.USUARIO_BABEL,
              typeElement: { typeElement: TypeElement.TEXT },
              placeholder: commonMessage.INGRESE_USUARIO
            },
            {
              key: "clave",
              label: commonValues.CLAVE_LABEL,
              typeElement: { typeElement: TypeElement.PASSWORD },
              placeholder: commonMessage.INGRESE_CLAVE

            },
            {
              key: "maxIdle",
              label: commonValues.MAX_IDLE,
              typeElement: { typeElement: TypeElement.FORMAT },
              validation: {
                expression: TypePatternMask.getPattern(TypePatternMask.NUMERO)
              },
              placeholder: commonMessage.INGRESE_CODIGO,
              tooltip: commonMessage.CODIGO_IDENTIFICA_REGISTRO
            }
          ]
        },
        row4: {
          row: [
            {
              key: "maxActive",
              label: commonValues.MAX_ACTIVE_LABEL,
              typeElement: { typeElement: TypeElement.FORMAT },
              validation: {
                expression: TypePatternMask.getPattern(TypePatternMask.NUMERO)
              },
              placeholder: commonMessage.INGRESE_VALOR,

            },
            {
              key: "maxWait",
              label: commonValues.MAX_WAIT_LABEL,
              typeElement: { typeElement: TypeElement.FORMAT },
              validation: {
                expression: TypePatternMask.getPattern(TypePatternMask.NUMERO)
              },
              placeholder: commonMessage.INGRESE_VALOR
            },
          ]
        },
        queryValidation: {
          label: commonValues.QUERY_VALIDATION_LABEL,
          typeElement: { typeElement: TypeElement.TEXTAREA }
        },
        documentacion: {
          label: commonValues.DOCUMENTACION_LABEL,
          typeElement: { typeElement: TypeElement.TEXTAREA }
        }
      }
    };
  }

  consultando() {
    return this.searching
  }

  editando() {
    return !this.searching
  }

  consultar(initPage: boolean) {
    if (this.formCriterio.invalid) {
      this.criterioRequerido = true;
      return;
    }
    this.criterioRequerido = false;
    if (this.criterio != "") {
      this.controls.filter = "";
      this.loadingIndicator = true;
      if (initPage) {
        this.controls.curPage = 0;
      }
      let params = new ParametrosFuenteDatos();
      if (this.campo == commonValues.CODIGO_VALUE) {
        params.codigo = this.criterio;
      } else {
        params.nombre = this.criterio;
      }
      params.size = this.controls.pageSize;
      params.page = this.controls.curPage;
      params.listaEstados = this.estado.split(",");
      this.fuenteService.listar(params).subscribe(
        response => {
          this.rows = response.content;
          this.temp = this.rows;
          this.loadingIndicator = false;
          this.controls.rowCount = response.totalElements;
          this.controls.curPage = response.number;
        },
        error => {
          this.loadingIndicator = false;
          console.log(error);
          this.notificationService.errorMessage(commonMessage.NO_SE_REALIZO_CONSULTA);
        }
      );
    }
  }

  updateFilter(event) {
    let val = event.target.value.toLowerCase();
    let colsAmt = 2;
    let keys = ['codigo', 'nombre'];
    this.rows = this.temp.filter(function (item) {
      for (let i = 0; i < colsAmt; i++) {
        if (item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 || !val) {
          return true;
        }
      }
    });
    this.table.offset = 0;
  }

  updatePageSize(value) {
    this.controls.pageSize = parseInt(value, 10);
    this.table.limit = this.controls.pageSize;
    this.controls.curPage = (this.controls.pageSize > this.controls.rowCount) ?
      0 : this.controls.curPage;
    this.consultar(false);
  }

  setPage(event) {
    this.controls.curPage = event.offset;
    this.controls.pageSize = event.pageSize;
    this.consultar(false);
  }

  limpiarCriterio() {
    this.criterio = "";
  }

  verTabla() {
    this.criterioRequerido = false;
    this.searching = true;
    this.disable = false;
  }

  verFormulario() {
    this.accion = commonValues.REGISTRAR_LABEL;
    this.searching = false;
    this.nuevo();
    this.suscritoAComponenteCodigo = false;
  }

  nuevo() {
    this.fuenteDatos = new Fuentedatos();
  }

  visualizar(id, deshabilitar) {
    this.verFormulario();
    this.buscar(id, deshabilitar, false);
  }

  buscar(id: number, deshabilitar: boolean, eliminar: boolean) {
    this.blockUIRegistro.start(commonMessage.CARGANDO + '...');
    this.fuenteService.consultar(id).subscribe(
      response => {
        this.fuenteDatos = response;
        if (!eliminar) {
          this.afFuenteDatos.form.disable();
          this.disable = true;
          this.accion = (!deshabilitar ? commonValues.EDITAR_LABEL : commonValues.VER_LABEL);
          if (!deshabilitar) {
            this.afFuenteDatos.form.enable();
            this.disable = false;
          }
          this.afFuenteDatos.form.setValue(response);
        } else {
          this.confirmDialog = true; commonMessage.CARGANDO +
            this.consultarRelaciones(id);
        }
        this.blockUIRegistro.stop();
      },
      error => {
        this.blockUIRegistro.stop();
        this.notificationService.errorMessage(commonMessage.NO_SE_REALIZO_CONSULTA);
      }
    );
  }

  get isInvalid() {
    if (this.afFuenteDatos) {
      this.onChanges();
    }
    return this.afFuenteDatos ? this.afFuenteDatos.form.invalid : true;
  }

  get desactivar() {
    return this.disable;
  }
  onChanges(): void {
    if (this.afFuenteDatos && !this.suscritoAComponenteCodigo) {
      this.suscritoAComponenteCodigo = true;
      this.afFuenteDatos.form.get('codigo').valueChanges.subscribe(val => {
        this.onKeyupCodigo(val);
      });
    }
  }

    onKeyupCodigo(valorCodigo:string) {
      if ((valorCodigo && (this.fuenteDatos.id && valorCodigo != this.fuenteDatos.codigo)) || !this.fuenteDatos.id && valorCodigo) {
        this.fuenteService.consultarCodigo(valorCodigo).subscribe(
          response => {
            if (response != null && response.id != this.fuenteDatos.id) {
              this.afFuenteDatos.form.controls['codigo'].setErrors({ 'codigoRecorded': true });
            } else {
              this.afFuenteDatos.form.controls['codigo'].setErrors({ 'codigoRecorded': false });
              const errores = this.afFuenteDatos.form.controls['codigo'].errors;
              delete errores.codigoRecorded;
              this.afFuenteDatos.form.controls['codigo'].setErrors(Object.keys(errores).length === 0 ? null : errores);
            }
          },
          error => {
            console.log(error);
            this.notificationService.errorMessage("Error al consultar código");
          }
        );
      }
    }
    
  onSubmit() {
    this.fuenteDatos = this.afFuenteDatos.form.value;
    this.fuenteDatos.estado = this.fuenteDatos.estado == "" ? commonValues.ACTIVO_VALUE : this.fuenteDatos.estado;
    this.fuenteService.guardar(this.fuenteDatos).subscribe(
      response => {
        this.fuenteDatos = response;
        this.searching = true;
        this.nuevo();
        this.notificationService.successMessage(commonMessage.REGISTRO_SATISFACTORIO);
        this.consultar(false);
      },
      error => {
        this.notificationService.errorMessage(commonMessage.REGISTRO_NO_EXITOSO);
      }
    );
  }

  eliminar(id) {
    this.blockUIConsulta.start(commonMessage.CARGANDO + '...');
    this.fuenteService.eliminar(id).subscribe(
      response => {
        this.consultar(false);
        this.notificationService.successMessage(commonMessage.REGISTRO_ELIMINADO);
        this.blockUIConsulta.stop();
      },
      error => {
        this.notificationService.errorMessage(commonMessage.REGISTRO_NO_ELIMINADO);
        this.blockUIConsulta.stop();
      }
    );
  }

  //confimr dialog
  openModal(id, estado) {
    this.estadoMensaje = estado;
    if (id != null) {
      this.buscar(id, false, true);
    }

  }

  confirm(): void {
    if (this.estadoMensaje == commonValues.ELIMINAR_LABEL) {
      this.eliminar(this.fuenteDatos.id);
      this.nuevo();
    }
    else if (this.estadoMensaje == commonValues.EDITAR_LABEL) {
      this.onSubmit();
    }

    this.confirmDialog = false;
  }

  decline(): void {

    if (this.estadoMensaje == commonValues.ELIMINAR_LABEL) {
      this.nuevo();
    }
    this.confirmDialog = false;
  }

  consultarRelaciones(id) {
    let arrayTemp = [];

    this.perfilService.listar(commonOptions.consultaEstadoEstandar).subscribe(
      response => {
        console.log(response.content);
        for (let item of response.content) {
          if ((item.fuenteDatos != null)) {
            if (item.fuenteDatos.id == id) {
              arrayTemp.push(item);
            }
          }
        }
        this.recursoService.listar(commonOptions.consultaEstadoEstandar).subscribe(
          response => {
            console.log(response.content);
            for (let item of response.content) {
              if (item.tipo == this.reporteValue) {
                this.reporteService.consultar(item.id).subscribe(
                  responseReporte => {
                    console.log(responseReporte);
                    if (responseReporte.idDatasource.id == id) {
                      arrayTemp.push(item);
                    }
                    this.sePuedeEliminar = arrayTemp.length > 0;
                    this.confirmDialog = true;
                  },
                  error => {
                    console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
                  }
                );
              }
            }
          },
          error => {
            console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
          }
        );
      },
      error => {
        console.log(commonMessage.NO_SE_PUDO_CONSULTAR, error);
      }
    );
  }
}