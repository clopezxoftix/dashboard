import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FuentedatosComponent } from './fuentedatos.component';

describe('FuentedatosComponent', () => {
  let component: FuentedatosComponent;
  let fixture: ComponentFixture<FuentedatosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FuentedatosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FuentedatosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
