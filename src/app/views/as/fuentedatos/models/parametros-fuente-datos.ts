export class ParametrosFuenteDatos {
  constructor() {
  }
  codigo: string;
  nombre: string;
  listaEstados: string[];
  page: number;
  size: number;
}
