export class Fuentedatos {
  constructor() {
  }
  id: number;
  codigo: string;
  nombre: string;
  estado: string;
  driver: string;
  url: string;
  usuario: string;
  clave: string;
  maxIdle: number;
  maxActive: number;
  maxWait: number;
  queryValidation: string;
  jndi: string;
  documentacion: string;
}
