import { TimepickerConfig } from "ngx-bootstrap";

export function getTimepickerConfig(): TimepickerConfig {
    return Object.assign(new TimepickerConfig(), {
        showSpinners : false
    });
  }