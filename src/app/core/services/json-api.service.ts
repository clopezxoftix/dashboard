import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { environment } from '@env/environment';
import {config} from '@app/config/smartadmin.config';
import {Observable} from "rxjs";
import { delay, map, catchError } from 'rxjs/operators';


@Injectable()
export class JsonApiService {

  constructor(private http: HttpClient) {}

  public fetch(url): Observable<any>{
    return this.http.get(this.getBaseUrl() + config.API_URL + url)
    .pipe(
      delay(100),
      map((data: any)=>(data.data|| data)),
      catchError(this.handleError)
    )
  }

  public fetchObjetivoEstartegico(url):Observable<any>{
    return this.http.get(environment.urlBase + environment.objetivoEstrategico + "/treeView/" + url).pipe(
      delay(100),
    ); 
  }

  public fetchObjetivoOperativo(url):Observable<any>{
    return this.http.get(environment.urlBase + environment.objetivoOperativo + "/treeView/" + url).pipe(
      delay(100),
    ); 
  }
/*
  public fetchDependencia(url):Observable<any>{
    return this.http.get(environment.urlBase+environment.dependencias+"/crear_arbol_raiz/"+url).pipe(
      delay(100),
    ); 
  }*/

  private getBaseUrl(){
    return location.protocol + '//' + location.hostname + (location.port ? ':'+location.port : '') + '/'
  }



  private handleError(error:any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }

}


