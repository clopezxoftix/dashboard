import { TestBed } from '@angular/core/testing';

import { ListaValoresService } from './lista-valores.service';

describe('ListaValoresService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListaValoresService = TestBed.get(ListaValoresService);
    expect(service).toBeTruthy();
  });
});
