import { TestBed } from '@angular/core/testing';

import { ObjetoNegocioService } from './objeto-negocio.service';

describe('ObjetoNegocioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ObjetoNegocioService = TestBed.get(ObjetoNegocioService);
    expect(service).toBeTruthy();
  });
});
