import { GeneralService } from './../general.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class ModulosService extends GeneralService {

constructor(public httpClient: HttpClient) {
  super(httpClient);
}

public guardar(modulos:any) {
  return super.post(environment.urlBase+environment.modulos, modulos, )
}

public editar(id: Number, modulos: any) {
  let url = environment.urlBase+environment.modulos + environment.slash + id
  return super.put(url, modulos)
}

public consultar(id: Number) {
  let url = environment.urlBase+environment.modulos + environment.slash + id
  return super.get(url)
}

public eliminar(id: Number) {
  let url = environment.urlBase+environment.modulos + environment.slash + id
  return super.delete(url)
}


public listar(parameters: any) {
  let url = environment.urlBase+environment.modulos
  return super.get(url, { params: parameters})
}

}
