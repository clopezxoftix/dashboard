import { TestBed } from '@angular/core/testing';

import { ObjetoAtributoService } from './objeto-atributo.service';

describe('ObjetoAtributoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ObjetoAtributoService = TestBed.get(ObjetoAtributoService);
    expect(service).toBeTruthy();
  });
});
