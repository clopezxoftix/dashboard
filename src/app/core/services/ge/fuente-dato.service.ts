import { Injectable } from '@angular/core';
import { GeneralService } from '../general.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';


@Injectable({
  providedIn: 'root'
})
export class FuenteDatoService extends GeneralService{

  constructor(protected http:HttpClient) {
    super(http);
   }

   public guardar(fuentes:any) {
    return super.post(environment.urlBase+environment.fuenteDato, fuentes )
  }
/*

  public guardarFuenteDatoCompleta(peticion:any){

    return super.post(environment.urlBase+environment.peticionFuenteDatos+environment.guardarFuenteDatosCompleta, peticion)


  }
  */
  /*

  public editarFuenteDatoCompleta(peticion:any){

    return super.put(environment.urlBase+environment.peticionFuenteDatos+environment.editarFuenteDatosCompleta, peticion)


  }
  */
  
  public editar( fuentes: any) {
    let url = environment.urlBase+environment.fuenteDato
    return super.put(url, fuentes)
  }
  
  public consultar(id: number) {
    let url = environment.urlBase+environment.fuenteDato + environment.slash + id
    return super.get(url)
  }
  
  public eliminar(id: number) {
    let url = environment.urlBase+environment.fuenteDato + environment.slash + id
    return super.delete(url)
  }
  
  public listar(fuentes: any) {
    let url = environment.urlBase+environment.fuenteDato
    return super.get(url, { params: fuentes})
  }

  public listarCodigo(parameters:any){
    let url = environment.urlBase + environment.fuenteDato + environment.slash + "codigo"
    return super.get(url,{params:parameters})
  }
}