import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { GeneralService } from '../general.service';

@Injectable({
  providedIn: 'root'
})
export class FuenteDatoAtributoService extends GeneralService {

  constructor(protected http:HttpClient) {
    super(http);
   }

   public guardar(fdAtributo:any) {
    return super.post(environment.urlBase+environment.fuenteDatoAtributo, fdAtributo )
  }
  
  public editar( fdAtributo: any) {
    let url = environment.urlBase+environment.fuenteDatoAtributo
    return super.put(url, fdAtributo)
  }
  
  public consultar(id: number) {
    let url = environment.urlBase+environment.fuenteDatoAtributo + environment.slash + id
    return super.get(url)
  }
  
  public eliminar(id: number) {
    let url = environment.urlBase+environment.fuenteDatoAtributo + environment.slash + id
    return super.delete(url)
  }
  
  public listar(fdAtributo: any) {
    let url = environment.urlBase+environment.fuenteDatoAtributo
    return super.get(url, { params: fdAtributo})
  }
/*
  public listarAtr(params: any) {
    let url = environment.urlBase+environment.fuenteDatoAtributo+environment.slash+environment.atributoFuenteDeDatos
    return super.get(url, { params: params})
  }
  */

  /*
  public consultarUltimoId() {
    let url = environment.urlBase+environment.fuenteDatoAtributo+environment.slash+environment.consultarUltimoId
    return super.get(url)
  }
  */

  public listarCodigo(parameters:any){
    let url = environment.urlBase + environment.fuenteDatoAtributo + environment.slash + "codigo_atr"
    return super.get(url,{params:parameters})
  }
}