import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GeneralService } from '../general.service';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class ObjetoNegocioService extends GeneralService {

  constructor(private http: HttpClient) {
    super(http);
   }

   public guardar(objeto: any) {
    return super.post(environment.urlBase + environment.objeto, objeto)
  }

  public editar(id: any, objeto: any) {
    let url = environment.urlBase + environment.objeto + environment.slash + id
    return super.put(url, objeto)
  }

  public consultar(id: number) {
    let url = environment.urlBase + environment.objeto + environment.slash + id
    return super.get(url)
  }
 
  public eliminar(id: number) {
    let url = environment.urlBase + environment.objeto + environment.slash + id
    return super.delete(url)
  }

  public listar(parameters: any) {
    let url = environment.urlBase + environment.objeto 
    return super.get(url, { params: parameters })
  }

  public listarCodigo(parameters: any) {
    let url = environment.urlBase + environment.objeto + environment.slash + environment.codigo
    return super.get(url, { params: parameters })
  }

  public consultarCodigo(codigo: any) {
    let url = environment.urlBase + environment.objeto + environment.slash + environment.codigo + environment.slash + codigo;
    return super.get(url);
  }
}
