import { TestBed } from '@angular/core/testing';

import { FuenteDatoAtributoService } from './fuente-dato-atributo.service';

describe('FuenteDatoAtributoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FuenteDatoAtributoService = TestBed.get(FuenteDatoAtributoService);
    expect(service).toBeTruthy();
  });
});
