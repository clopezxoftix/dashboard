import { Injectable } from '@angular/core';
import { GeneralService } from '../general.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { ListaAtributos } from '@app/views/ge/lista-valores/models/lista-atributos';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ListaAtributosService extends GeneralService {

  constructor(protected http: HttpClient) {
    super(http);
  }

  public guardar(listaAtributo: ListaAtributos): Observable<any> {
    return super.post(environment.urlBase + environment.listaAtributo, listaAtributo)
  }

  public editar(id: Number, listaAtributo: ListaAtributos): Observable<any> {
    let url = environment.urlBase + environment.listaAtributo + environment.slash + id
    return super.put(url, listaAtributo)
  }

  public consultar(id: number): Observable<any> {
    let url = environment.urlBase + environment.listaAtributo + environment.slash + environment.consultar + environment.slash + id
    return super.get(url)
  }

  public consultarId(id: number): Observable<any> {
    let url = environment.urlBase + environment.listaAtributo + environment.slash + id
    return super.get(url)
  }

  public listar(parameters: any) {
    let url = environment.urlBase + environment.listaAtributo + environment.slash + environment.consultar
    return super.get(url, { params: parameters })
  }

  public eliminar(id: Number): Observable<any> {
    let url = environment.urlBase + environment.listaAtributo + environment.slash + id
    return super.delete(url)
  }

  public guardarTodo(atributos: Array<any>) {
    let url = environment.urlBase + environment.listaAtributo + environment.slash + environment.guardarAtributo
    return super.post(url, atributos)
  }

  public bajarIndice(parameters: number) {
    let url = environment.urlBase + environment.listaAtributo + environment.slash + "bajar" + environment.slash + parameters
    return super.get(url)
  }

  public subirIndice(parameters: number) {
    let url = environment.urlBase + environment.listaAtributo + environment.slash + "subir" + environment.slash + parameters
    return super.get(url)
  }
}
