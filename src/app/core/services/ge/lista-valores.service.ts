import { Injectable } from '@angular/core';
import { GeneralService } from '../general.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { ListaValores } from '../../../views/ge/lista-valores/models/lista-valores';
import { Observable } from 'rxjs';
import { PathUtil } from '@app/shared/components/utils/path-util';

@Injectable({
  providedIn: 'root'
})
export class ListaValoresService extends GeneralService {

  constructor(public httpClient: HttpClient) {
    super(httpClient);
  }

  public guardar(listaValores: ListaValores): Observable<any> {
    return super.post(environment.urlBase + environment.listaValores, listaValores)
  }

  public editar(id: Number, listaValores: ListaValores): Observable<any> {
    let url = environment.urlBase + environment.listaValores + environment.slash + id
    return super.put(url, listaValores)
  }

  public consultar(id: Number): Observable<any> {
    let url = environment.urlBase + environment.listaValores + environment.slash + id
    return super.get(url)
  }

  consultarCodigo(codigo:string): Observable<any>{
    let url = environment.urlBase + environment.listaValores+ environment.slash+ environment.codigo+ environment.slash + codigo
    return super.get(url);
  }

  public eliminar(id: Number): Observable<any> {
    let url = environment.urlBase + environment.listaValores + environment.slash + id
    return super.delete(url)
  }

  public listar(parameters: any): Observable<any> {
    let path:string = PathUtil.getPathParams(parameters);
    let url = environment.urlBase + environment.listaValores;
    return super.get(url+path,)
  }


}
