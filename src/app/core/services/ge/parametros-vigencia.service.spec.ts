import { TestBed, inject } from '@angular/core/testing';

import { ParametrosVigenciaService } from './parametros-vigencia.service';

describe('ParametrosVigenciaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ParametrosVigenciaService]
    });
  });

  it('should be created', inject([ParametrosVigenciaService], (service: ParametrosVigenciaService) => {
    expect(service).toBeTruthy();
  }));
});
