import { Injectable } from '@angular/core';
import { GeneralService } from '../general.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { ListaElementos } from '@app/views/ge/lista-valores/models/lista-elementos';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ListaElementosService extends GeneralService {

  constructor(protected http: HttpClient) {
    super(http);
  }

  public guardar(listaElemento: ListaElementos): Observable<any> {
    return super.post(environment.urlBase + environment.listaElemento, listaElemento)
  }

  public editar(id: Number, listaEmento: ListaElementos): Observable<any> {
    let url = environment.urlBase + environment.listaElemento + environment.slash + id
    return super.put(url, listaEmento)
  }

  public consultar(id: number): Observable<any> {
    let url = environment.urlBase + environment.listaElemento + environment.slash + environment.consultar + environment.slash + id
    return super.get(url)
  }

  public consultarId(id: number): Observable<any> {
    let url = environment.urlBase + environment.listaElemento + environment.slash + id
    return super.get(url)
  }

  public listar(parameters: any) {
    let url = environment.urlBase + environment.listaElemento + environment.slash + environment.consultar
    return super.get(url, { params: parameters })
  }

  public eliminar(id: Number): Observable<any> {
    let url = environment.urlBase + environment.listaElemento + environment.slash + id
    return super.delete(url)
  }

  public guardarTodo(elementos: Array<ListaElementos>) {
    let url = environment.urlBase + environment.listaElemento + environment.slash + environment.guardarElemento
    return super.post(url, elementos)
  }
}
