import { Injectable } from '@angular/core';
import { GeneralService } from '../general.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class ParametrosService extends GeneralService{

  constructor(public httpClient: HttpClient) {
    super(httpClient);
   }

   public guardar(parametros: any) {
    return super.post(environment.urlBase + environment.parametros, parametros)
  }

  public editar(id: Number, parametros: any) {
    let url = environment.urlBase + environment.parametros + environment.slash + id
    return super.put(url, parametros)
  }

  public consultar(id: Number) {
    let url = environment.urlBase + environment.parametros + environment.slash + id
    return super.get(url)
  }

  public eliminar(id: Number) {
    let url = environment.urlBase + environment.parametros + environment.slash + id
    return super.delete(url)
  }

  public listar(parameters: any) {
    let url = environment.urlBase + environment.parametros
    return super.get(url, { params: parameters })
  }
}
