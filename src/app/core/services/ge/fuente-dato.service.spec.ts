import { TestBed } from '@angular/core/testing';

import { FuenteDatoService } from './fuente-dato.service';

describe('FuenteDatoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FuenteDatoService = TestBed.get(FuenteDatoService);
    expect(service).toBeTruthy();
  });
});
