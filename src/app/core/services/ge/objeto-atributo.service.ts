import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GeneralService } from '../general.service';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ObjetoAtributoService extends GeneralService {

  constructor(private http: HttpClient) {
    super(http);
  }
  public guardar(atributo: any) {
    return super.post(environment.urlBase + environment.atributo, atributo)
  }

  public editar(id: any, atributo: any) {
    let url = environment.urlBase + environment.atributo + environment.slash + id
    return super.put(url, atributo)
  }

  public consultar(id: number) {
    let url = environment.urlBase + environment.atributo + environment.slash + id
    return super.get(url)
  }

  public eliminar(id: number) {
    let url = environment.urlBase + environment.atributo + environment.slash + id
    return super.delete(url)
  }

  public listar(parameters: any) {
    let url = environment.urlBase + environment.atributo
    return super.get(url, { params: parameters })
  }

  public listarCodigo(parameters: any) {
    let url = environment.urlBase + environment.atributo + environment.slash + environment.codigo
    return super.get(url, { params: parameters })
  }

  public subirIndice(parameters: number) {
    let url = environment.urlBase + environment.atributo + environment.slash + environment.subir + environment.slash + parameters
    return super.get(url)
  }

  public bajarIndice(parameters: number) {
    let url = environment.urlBase + environment.atributo + environment.slash + environment.bajar + environment.slash + parameters
    return super.get(url)
  }

  public guardarTodos(atributos: Array<any>) {
    let url = environment.urlBase + environment.atributo + environment.slash + environment.guardar_todos
    return super.post(url, atributos);
  }

  public consultarCodigo(codigo: any) {
    let url = environment.urlBase + environment.atributo + environment.slash + environment.codigo + environment.slash + codigo;
    return super.get(url);
  }
}
