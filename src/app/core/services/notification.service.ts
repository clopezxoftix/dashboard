import {Injectable} from '@angular/core';

declare var $: any;

@Injectable()
export class NotificationService {

  constructor() {
  }

  smallBox(data, cb?) {
    $.smallBox(data, cb)
  }

  bigBox(data, cb?) {
    $.bigBox(data, cb)
  }

  smartMessageBox(data, cb?) {
    $.SmartMessageBox(data, cb)
  }

  errorMessage(message: string, cb?) {
    const parameters = {
      title: '',
      content: '<i class=\'fa fa-remove\'></i>',
      color: '#77021D',
      iconSmall: 'fa fa-thumbs-down bounce animated',
      timeout: 5000
    };
    parameters.title = message;
    $.smallBox(parameters, cb)
  }

  warningMessage(message: string, cb?) {
    const parameters = {
      title: '',
      content: '<i class=\'fa fa-remove\'></i>',
      color: '#FF9800',
      iconSmall: 'fa fa-thumbs-down bounce animated',
      timeout: 5000
    };
    parameters.title = message;
    $.smallBox(parameters, cb)
  }
  
  successMessage(message: string, cb?) {
    const parameters = {
      title: '',
      content: '<i class=\'fa fa-check\'></i>',
      color: '#739E73',
      iconSmall: 'fa fa-thumbs-up bounce animated',
      timeout: 5000
    };
    parameters.title = message;
    $.smallBox(parameters, cb)
  }

}
