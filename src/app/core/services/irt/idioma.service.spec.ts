import { TestBed, inject } from '@angular/core/testing';

import { IdiomaService } from './idioma.service';

describe('IdiomaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IdiomaService]
    });
  });

  it('should be created', inject([IdiomaService], (service: IdiomaService) => {
    expect(service).toBeTruthy();
  }));
});
