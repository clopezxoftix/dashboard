import { GeneralService } from './../general.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class RegionService extends GeneralService {

  constructor(public httpClient: HttpClient) {
    super(httpClient);
  }

  public guardar(region: any) {
    return super.post(environment.urlBase + environment.regiones, region);
  }

  public editar(id: Number, region: any) {
    let url = environment.urlBase + environment.regiones + environment.slash + id;
    return super.put(url, region);
  }

  public eliminar(id: Number) {
    let url = environment.urlBase + environment.regiones + environment.slash + id;
    return super.delete(url);
  }

  public consultar(id: Number) {
    let url = environment.urlBase + environment.regiones + environment.slash + id;
    return super.get(url);
  }

  public listar(parameters: any) {
    let url = environment.urlBase + environment.regiones;
    return super.get(url, { params: parameters });
  }
}
