import { GeneralService } from './../general.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment'

@Injectable({
  providedIn: 'root'
})
export class IdiomaService extends GeneralService {

  constructor(public httpClient: HttpClient) {
    super(httpClient);
  }

  public guardar(idioma: any) {
    return super.post(environment.urlBase + environment.idiomas, idioma);
  }

  public editar(id: Number, idioma: any) {
    let url = environment.urlBase + environment.idiomas + environment.slash + id;
    return super.put(url, idioma);
  }

  public eliminar(id: Number) {
    let url = environment.urlBase + environment.idiomas + environment.slash + id;
    return super.delete(url);
  }

  public consultar(id: Number) {
    let url = environment.urlBase + environment.idiomas + environment.slash + id;
    return super.get(url);
  }

  public listar(parameters: any) {
    let url = environment.urlBase + environment.idiomas;
    return super.get(url, { params: parameters });
  }
}
