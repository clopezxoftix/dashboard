import { Injectable } from '@angular/core';
import { GeneralService } from '../general.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class NivelesSeguridadService extends GeneralService{

  constructor(public httpClient: HttpClient) {
    super(httpClient);
   }

   public guardar(nivelesSeguridad: any) {
    return super.post(environment.urlBase + environment.nivelesSeguridad, nivelesSeguridad)
  }

  public editar(id: Number, nivelesSeguridad: any) {
    let url = environment.urlBase + environment.nivelesSeguridad + environment.slash + id
    return super.put(url, nivelesSeguridad)
  }

  public consultar(id: Number) {
    let url = environment.urlBase + environment.nivelesSeguridad + environment.slash + id
    return super.get(url)
  }

  public eliminar(id: Number) {
    let url = environment.urlBase + environment.nivelesSeguridad + environment.slash + id
    return super.delete(url)
  }

  public listar(parameters: any) {
    let url = environment.urlBase + environment.nivelesSeguridad
    return super.get(url, { params: parameters })
  }
}
