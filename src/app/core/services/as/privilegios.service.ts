import { Injectable } from '@angular/core';
import { GeneralService } from '../general.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class PrivilegiosService extends GeneralService {

  constructor(public httpClient : HttpClient) {
    super(httpClient);
  }
  
  public guardar(privilegios:any) {
    return super.post(environment.urlBase+environment.privilegios, privilegios, )
  }

  public editar(id: Number, privilegios: any) {
    let url = environment.urlBase+environment.privilegios + environment.slash + id
    return super.put(url, privilegios)
  }

  public consultar(id: Number) {
    let url = environment.urlBase+environment.privilegios + environment.slash + id
    return super.get(url)
  }

  public eliminar(id: Number) {
    let url = environment.urlBase+environment.privilegios + environment.slash + id
    return super.delete(url)
  }


  public listar(parameters: any) {
    let url = environment.urlBase+environment.privilegios
    return super.get(url, { params: parameters})
  }

}
