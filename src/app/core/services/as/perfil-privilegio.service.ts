import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { GeneralService } from '../general.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PathUtil } from '@app/shared/components/utils/path-util';
import { PerfilPrivilegio } from '@app/views/as/perfiles/models/perfil-privilegio';

@Injectable({
  providedIn: 'root'
})
export class PerfilPrivilegioService extends GeneralService {

  constructor(public httpClient: HttpClient) {
    super(httpClient);
  }

  public guardar(perfilPrivilegio: PerfilPrivilegio): Observable<any>  {
    return super.post(environment.urlBase + environment.perfilPrivilegio, perfilPrivilegio)
  }

  public editar(perfilPrivilegio: PerfilPrivilegio): Observable<any> {
    let url = environment.urlBase + environment.perfilPrivilegio + environment.slash + environment.editarPerfilPrivilegio;
    return super.post(url, perfilPrivilegio)
  }

  public consultar(id: Number): Observable<any> {
    let url = environment.urlBase + environment.perfilPrivilegio + environment.slash + id
    return super.get(url)
  }

  public eliminar(id: Number): Observable<any> {
    let url = environment.urlBase + environment.perfilPrivilegio + environment.slash + id
    return super.delete(url)
  }

  public eliminarPorPerfilPrivilegio(parameters: any) {
    let url = environment.urlBase + environment.perfilPrivilegio + environment.slash + environment.eliminarPerfilPrivilegio;
    return super.delete(url, { params: parameters });
  }

  public listar(parameters: any): Observable<any> {
    let path:string = PathUtil.getPathParams(parameters);
    let url = environment.urlBase + environment.perfilPrivilegio
    return super.get(url+path,)
  }
}
