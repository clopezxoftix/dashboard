import { Injectable } from '@angular/core';
import { GeneralService } from '../general.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class ReportesService extends GeneralService {

  constructor(public httpClient:HttpClient) {
    super(httpClient);
   }
   
   public guardar(reportes:any) {
    return super.post(environment.urlBase+environment.reportes, reportes, )
  }

  public editar(id: Number, reportes: any) {
    let url = environment.urlBase+environment.reportes + environment.slash + id
    return super.put(url, reportes)
  }

  public consultar(id: Number) {
    let url = environment.urlBase+environment.reportes + environment.slash + id
    return super.get(url)
  }

  public eliminar(id: Number) {
    let url = environment.urlBase+environment.reportes + environment.slash + id
    return super.delete(url)
  }


  public listar(parameters: any) {
    let url = environment.urlBase+environment.reportes
    return super.get(url, { params: parameters})
  }
}

