import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { GeneralService } from '../general.service';
import { HttpClient } from '@angular/common/http';
import { PerfilCuenta } from '@app/views/as/perfiles/models/perfil-cuenta';
import { Observable } from 'rxjs';
import { PathUtil } from '@app/shared/components/utils/path-util';

@Injectable({
  providedIn: 'root'
})
export class PerfilCuentaService extends GeneralService {

  constructor(public httpClient: HttpClient) {
    super(httpClient);
  }

  public guardar(perfilCuenta: PerfilCuenta): Observable<any>  {
    return super.post(environment.urlBase + environment.perfilCuenta, perfilCuenta)
  }

  public editar(perfilCuenta: PerfilCuenta): Observable<any> {
    let url = environment.urlBase + environment.perfilCuenta + environment.slash + environment.editarPerfilCuenta;
    return super.post(url, perfilCuenta)
  }

  public consultar(id: Number): Observable<any> {
    let url = environment.urlBase + environment.perfilCuenta + environment.slash + id
    return super.get(url)
  }

  public eliminar(id: Number): Observable<any> {
    let url = environment.urlBase + environment.perfilCuenta + environment.slash + id
    return super.delete(url)
  }

  public eliminarPorPerfilCuenta(parameters: any) {
    let url = environment.urlBase + environment.perfilCuenta + environment.slash + environment.eliminarPerfilCuenta;
    return super.delete(url, { params: parameters });
  }

  public listar(parameters: any): Observable<any> {
    let path:string = PathUtil.getPathParams(parameters);
    let url = environment.urlBase + environment.perfilCuenta
    return super.get(url+path,)
  }
}
