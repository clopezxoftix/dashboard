import { TestBed, inject } from '@angular/core/testing';

import { PerfilPrivilegioService } from './perfil-privilegio.service';

describe('PerfilPrivilegioService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PerfilPrivilegioService]
    });
  });

  it('should be created', inject([PerfilPrivilegioService], (service: PerfilPrivilegioService) => {
    expect(service).toBeTruthy();
  }));
});
