import { TestBed, inject } from '@angular/core/testing';

import { NivelesSeguridadService } from './niveles-seguridad.service';

describe('NivelesSeguridadService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NivelesSeguridadService]
    });
  });

  it('should be created', inject([NivelesSeguridadService], (service: NivelesSeguridadService) => {
    expect(service).toBeTruthy();
  }));
});
