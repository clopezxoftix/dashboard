import { Injectable } from '@angular/core';
import { GeneralService } from '../general.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '@env/environment';
import { CuentaUsuario } from '@app/views/as/cuentas-usuario/models/cuentasUsuario';

@Injectable({
  providedIn: 'root'
})
export class CuentasUsuarioService extends GeneralService {

  constructor(public httpClient: HttpClient) {
    super(httpClient);
  }

  public guardar(cuentasUsuario: any) {
    return super.post(environment.urlBase + environment.cuentasUsuario, cuentasUsuario)
  }

  public editar(id: Number, cuentasUsuario: any) {
    let url = environment.urlBase + environment.cuentasUsuario + environment.slash + id
    return super.put(url, cuentasUsuario)
  }

  public consultar(id: Number) {
    let url = environment.urlBase + environment.cuentasUsuario + environment.slash + id
    return super.get(url)
  }

  public eliminar(id: Number) {
    let url = environment.urlBase + environment.cuentasUsuario + environment.slash + id
    return super.delete(url)
  }

  public listar(parameters: any) {
    let url = environment.urlBase + environment.cuentasUsuario
    return super.get(url, { params: parameters })
  }

  public consultarPorNivelSeguridad(parameters: any) {
    let url = environment.urlBase + environment.cuentasUsuario + environment.slash + "nivel-seguridad"
    return super.get(url, { params: parameters })
  }

  public validarSimilitudClave(validacionCuentaUsuario: any) {
    let url = environment.urlBase + environment.cuentasUsuario + environment.slash + "validar-clave";
    return super.post(url, validacionCuentaUsuario);
  }

}
