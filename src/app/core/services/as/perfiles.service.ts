import { Injectable } from '@angular/core';
import { GeneralService } from '../general.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { PathUtil } from '@app/shared/components/utils/path-util';
import { Perfil } from '@app/views/as/perfiles/models/perfil';

@Injectable({
  providedIn: 'root'
})
export class PerfilesService extends GeneralService {

  constructor(public httpClient: HttpClient) {
    super(httpClient);
  }

  public guardar(perfiles: Perfil): Observable<any> {
    return super.post(environment.urlBase + environment.perfiles, perfiles)
  }

  public editar(id: Number, perfiles: Perfil): Observable<any> {
    let url = environment.urlBase + environment.perfiles + environment.slash + id
    return super.put(url, perfiles)
  }

  public consultar(id: Number): Observable<any> {
    let url = environment.urlBase + environment.perfiles + environment.slash + id
    return super.get(url)
  }

  public eliminar(id: Number): Observable<any> {
    let url = environment.urlBase + environment.perfiles + environment.slash + id
    return super.delete(url)
  }

  public listar(parameters: any): Observable<any> {
    let path: string = PathUtil.getPathParams(parameters);
    let url = environment.urlBase + environment.perfiles
    return super.get(url + path)
  }
}
