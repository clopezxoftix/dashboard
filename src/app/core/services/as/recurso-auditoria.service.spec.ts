import { TestBed, inject } from '@angular/core/testing';

import { RecursoAuditoriaService } from './recurso-auditoria.service';

describe('RecursoAuditoriaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RecursoAuditoriaService]
    });
  });

  it('should be created', inject([RecursoAuditoriaService], (service: RecursoAuditoriaService) => {
    expect(service).toBeTruthy();
  }));
});
