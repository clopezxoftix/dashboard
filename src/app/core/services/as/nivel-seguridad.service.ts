import { Injectable } from '@angular/core';
import { GeneralService } from '../general.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { NivelSeguridad } from '../../../views/as/nivel-seguridad/models/nivel-seguridad';
import { Observable } from 'rxjs';
import { PathUtil } from '@app/shared/components/utils/path-util';

@Injectable({
  providedIn: 'root'
})
export class NivelSeguridadService extends GeneralService{

  constructor(public httpClient: HttpClient) {
    super(httpClient);
   }

   public guardar(nivelSeguridad: NivelSeguridad):Observable<any> {
    return super.post(environment.urlBase + environment.nivelesSeguridad, nivelSeguridad)
  }

  public editar(id: Number, nivelSeguridad:NivelSeguridad):Observable<any> {
    let url = environment.urlBase + environment.nivelesSeguridad + environment.slash + id
    return super.put(url, nivelSeguridad)
  }

  public consultar(id: Number): Observable<any> {
    let url = environment.urlBase + environment.nivelesSeguridad + environment.slash + id
    return super.get(url)
  }

  consultarCodigo(codigo:string): Observable<any>{
    let url = environment.urlBase + environment.nivelesSeguridad+ environment.slash+ environment.codigo+ environment.slash + codigo
    return super.get(url);
  }

  public eliminar(id: Number): Observable<any> {
    let url = environment.urlBase + environment.nivelesSeguridad + environment.slash + id
    return super.delete(url)
  }

  public listar(parameters: any): Observable<any>{
    let path:string = PathUtil.getPathParams(parameters);
    let url = environment.urlBase + environment.nivelesSeguridad
    return super.get(url, { params: parameters })
  }
}
