import { TestBed, inject } from '@angular/core/testing';

import { FuentedatosService } from './fuentadatos.service';

describe('FuentedatosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FuentedatosService]
    });
  });

  it('should be created', inject([FuentedatosService], (service: FuentedatosService) => {
    expect(service).toBeTruthy();
  }));
});
