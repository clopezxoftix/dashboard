import { Injectable } from '@angular/core';
import { GeneralService } from '../general.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class RecursoAuditoriaService extends GeneralService {

  constructor(public httpClient:HttpClient) {
    super(httpClient);
   }
   
   public guardar(recursoAuditoria:any) {
    return super.post(environment.urlBase+environment.recursoAuditoria, recursoAuditoria, )
  }

  public editar(id: Number, recursoAuditoria: any) {
    let url = environment.urlBase+environment.recursoAuditoria + environment.slash + id
    return super.put(url, recursoAuditoria)
  }

  public consultar(id: Number) {
    let url = environment.urlBase+environment.recursoAuditoria + environment.slash + id
    return super.get(url)
  }

  public eliminar(id: Number) {
    let url = environment.urlBase+environment.recursoAuditoria + environment.slash + id
    return super.delete(url)
  }


  public listar(parameters: any) {
    let url = environment.urlBase+environment.recursoAuditoria
    return super.get(url, { params: parameters})
  }
}
