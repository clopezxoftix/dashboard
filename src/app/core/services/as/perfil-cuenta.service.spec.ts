import { TestBed, inject } from '@angular/core/testing';

import { PerfilCuentaService } from './perfil-cuenta.service';

describe('PerfilCuentaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PerfilCuentaService]
    });
  });

  it('should be created', inject([PerfilCuentaService], (service: PerfilCuentaService) => {
    expect(service).toBeTruthy();
  }));
});
