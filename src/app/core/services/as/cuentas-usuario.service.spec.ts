import { TestBed, inject } from '@angular/core/testing';

import { CuentasUsuarioService } from './cuentas-usuario.service';

describe('CuentasUsuarioService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CuentasUsuarioService]
    });
  });

  it('should be created', inject([CuentasUsuarioService], (service: CuentasUsuarioService) => {
    expect(service).toBeTruthy();
  }));
});
