import { Injectable } from '@angular/core';
import { GeneralService } from '../general.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class RecursosService extends GeneralService {

  constructor(public httpClient:HttpClient) {
    super(httpClient);
   }

   public guardar(recursos:any) {
    return super.post(environment.urlBase+environment.recursos, recursos, )
  }

  public editar(id: Number, recursos: any) {
    let url = environment.urlBase+environment.recursos + environment.slash + id
    return super.put(url, recursos)
  }

  public consultar(id: Number) {
    let url = environment.urlBase+environment.recursos + environment.slash + id
    return super.get(url)
  }

  public eliminar(id: Number) {
    let url = environment.urlBase+environment.recursos + environment.slash + id
    return super.delete(url)
  }


  public listar(parameters: any) {
    let url = environment.urlBase+environment.recursos
    return super.get(url, { params: parameters})
  }
}
