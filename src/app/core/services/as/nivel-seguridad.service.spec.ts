import { TestBed, inject } from '@angular/core/testing';
import { NivelSeguridadService } from './nivel-seguridad.service';

describe('NivelesSeguridadService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NivelSeguridadService]
    });
  });

  it('should be created', inject([NivelSeguridadService], (service: NivelSeguridadService) => {
    expect(service).toBeTruthy();
  }));
});
