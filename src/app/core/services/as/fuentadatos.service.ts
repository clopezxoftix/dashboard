import { Injectable } from '@angular/core';
import { GeneralService } from '../general.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { Fuentedatos } from '../../../views/as/fuentedatos/models/fuentedatos';
import { PathUtil } from '@app/shared/components/utils/path-util';

@Injectable({
  providedIn: 'root'
})
export class FuentedatosService extends GeneralService {

  constructor(public httpClient: HttpClient) { 
    super(httpClient);
  }

  public guardar(fuenteDatos: Fuentedatos): Observable<any> {
    return super.post(environment.urlBase + environment.fuentedatos, fuenteDatos)
  }

  public editar(id: Number, fuenteDatos:  Fuentedatos): Observable<any> {
    let url = environment.urlBase + environment.fuentedatos + environment.slash + id
    return super.put(url, fuenteDatos)
  }

  public consultar(id: Number): Observable<any> {
    let url = environment.urlBase + environment.fuentedatos + environment.slash + id
    return super.get(url)
  }

  consultarCodigo(codigo:string): Observable<any>{
    let url = environment.urlBase + environment.fuentedatos+ environment.slash+ environment.codigo+ environment.slash + codigo
    return super.get(url);
  }


  public eliminar(id: Number): Observable<any> {
    let url = environment.urlBase + environment.fuentedatos + environment.slash + id
    return super.delete(url)
  }

  public listar(parameters: any): Observable<any> {
    let path:string = PathUtil.getPathParams(parameters);
    let url = environment.urlBase + environment.fuentedatos;
    return super.get(url+path,)
  }

}
