import { Directive, ElementRef, Renderer } from "@angular/core";
import { ControlValueAccessor } from "@angular/forms";

@Directive({
    selector:
    'input[type=radio][formControlName],input[type=radio][formControl],input[type=radio][ngModel]',
    host: {'(change)': 'onChange($event.target.value)', '(blur)': 'onTouched()'},
    
})
export class RadioControlValueAccessor implements ControlValueAccessor {
  [x: string]: any;
    onChange = (_) => {};
    onTouched = () => {};

    constructor(private renderer: Renderer, private elementRef: ElementRef) {}

    writeValue(value: any): void {
        this._renderer.setElementProperty(this._elementRef, 'checked', value == this._elementRef.nativeElement.value);
    }
    registerOnChange(fn: (_: any) => {}): void { this.onChange = fn; }
    registerOnTouched(fn: () => {}): void { this.onTouched = fn; }
}
