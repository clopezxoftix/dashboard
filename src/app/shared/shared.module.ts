import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";




import { BootstrapModule } from "@app/shared/bootstrap.module";



import { GeneralFormComponentModule } from "./components/general-form/general-form-component.module";
import { GeneralFormComponent } from "./components/general-form/general-form.component";
import { UserModule } from "./components/user/user.module";
import { I18nModule } from "./components/i18n/i18n.module";
import { UtilsModule } from "./components/utils/utils.module";
import { PipesModule } from "./pipes/pipes.module";
import { SmartadminFormsLiteModule } from "./components/forms/smartadmin-forms-lite.module";
import { SmartProgressbarModule } from "./components/ui/smart-progressbar/smart-progressbar.module";
import { InlineGraphsModule } from "./components/graphs/inline/inline-graphs.module";
import { SmartadminWidgetsModule } from "./components/widgets/smartadmin-widgets.module";
import { ChatModule } from "./components/chat/chat.module";
import { StatsModule } from "./components/stats/stats.module";
import { VoiceControlModule } from "./components/voice-control/voice-control.module";
import { CalendarComponentsModule } from "./components/calendar/calendar-components.module";
import { MenuBarModule } from "./components/menu-bar/menu-bar.module";
import { TabsModule, BsDatepickerModule } from "ngx-bootstrap";
import { AdministracionSeguridadModule } from "@app/views/as/administracion.seguridad.module";
import { SmartadminLayoutModule } from "@app/views/layout";
import { RadioControlValueAccessor } from "@app/shared/directives/RadioControlValueAccessor";
import { SmartadminInputModule } from "./components/forms/input/smartadmin-input.module";
import { UiDatepickerDirective } from "./components/forms/input/ui-datepicker.directive";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,

    SmartadminLayoutModule,
    BootstrapModule,
    GeneralFormComponentModule,
    TabsModule,
    BsDatepickerModule.forRoot()
  ],
  declarations: [RadioControlValueAccessor],
  exports: [
    MenuBarModule,
    CommonModule,
    FormsModule,
    RouterModule,
    UserModule,
    SmartadminLayoutModule,
    BootstrapModule,
    I18nModule,
    UtilsModule,
    PipesModule,
    SmartadminFormsLiteModule,
    SmartProgressbarModule,
    InlineGraphsModule,
    SmartadminWidgetsModule,
    ChatModule,
    StatsModule,
    VoiceControlModule,
    CalendarComponentsModule,
    TabsModule,
    GeneralFormComponent,
    RadioControlValueAccessor

  ]
})
export class SharedModule {}
