import { Component, OnInit, Input } from "@angular/core";
import { FormGroup } from "@angular/forms";

@Component({
  selector: "general-form-row",
  templateUrl: "./general-form-row.component.html"
})
export class GeneralFormRowComponent implements OnInit {
  readonly COLUMNS_TOTAL = 12;
  @Input()
  fields: any[];
  @Input()
  form: FormGroup;
  get columns() {
    return "col-" + this.COLUMNS_TOTAL / (this.fields ? this.fields.length : 1);
  }
  constructor() {}

  ngOnInit() {}

  isAnArray(field) {
    return Array.isArray(field);
  }
}
