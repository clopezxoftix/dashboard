export enum TypeCaseText {
    UPPER_CASE = "upper",
    LOWER_CASE = "lower",
    DEFAULT_CASE = "default"
}