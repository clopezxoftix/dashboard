export enum TypeValidators {
  REQUIRED = "required",
  EMAIL = "email",
  MIN = "min",
  MAX = "max",
  EQUAL_TO = "equalTo"
}
