export abstract class TypePatternMask {

    public static LETRAS_NUMEROS_UNDERSCORE = "(\\w|[áéíóúÁÉÍÓÚñÑ])";
    public static NUMERO = "\\d";
    public static LETRAS = "[a-zA-ZáéíóúÁÉÍÓÚñÑ]";
    public static DECIMALES = "\\d*[\.|,]\\d*";
    public static MASK = "0*";
    public static getPattern(pattern: string) {
        return {
            mask: this.MASK,
            pattern: { '0': { pattern: new RegExp(pattern) } }
        }
    }

}