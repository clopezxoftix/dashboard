export enum TypeButtonClass {
    DEFAULT = 'btn-default',
    PRIMARY = 'btn-primary',
    SUCCESS = 'btn-success',
    DANGER = 'btn-danger'
}