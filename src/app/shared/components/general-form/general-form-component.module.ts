import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { GeneralFormComponent } from "./general-form.component";
import { GeneralFormFieldComponent } from "./general-form-field/general-form-field.component";
import { GeneralFormRowComponent } from "./general-form-row/general-form-row.component";
import { UtilsModule } from "../utils/utils.module";
import { BsDatepickerModule, TimepickerModule, PopoverModule } from "ngx-bootstrap";
import { SampleComponent } from './sample/sample.component';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { AccordionModule } from "primeng/accordion";
import { CalendarModule } from "primeng/calendar";
import { SeleccionarRegistroComponent } from './seleccionar-registro/seleccionar-registro.component';
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { TableModule } from 'primeng/table';
import { NgxMaskModule } from 'ngx-mask'
import { SmartadminInputModule } from '@app/shared/components/forms/input/smartadmin-input.module';
import { SliderModule } from 'primeng/slider';
import { RatingModule } from 'primeng/rating';
import { InputSwitchModule } from 'primeng/inputswitch';
import { MultiSelectModule } from 'primeng/multiselect';
import {SpinnerModule} from 'primeng/spinner';
import {ToggleButtonModule} from 'primeng/togglebutton';

@NgModule({
  imports: [CommonModule,
    FormsModule,
    ReactiveFormsModule,
    UtilsModule,
    AccordionModule,
    CalendarModule,
    NgxMyDatePickerModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    PopoverModule.forRoot(),
    FontAwesomeModule,
    TableModule,
    NgxMaskModule.forRoot(),
    SmartadminInputModule,
    SliderModule,
    RatingModule,
    InputSwitchModule,
    MultiSelectModule,
    SpinnerModule,
    ToggleButtonModule
  ],
  declarations: [
    GeneralFormComponent,
    GeneralFormFieldComponent,
    GeneralFormRowComponent,
    SampleComponent,
    SeleccionarRegistroComponent
  ],
  exports: [
    GeneralFormComponent,
    GeneralFormFieldComponent,
    GeneralFormRowComponent,
    SampleComponent
  ],
  providers: []
})
export class GeneralFormComponentModule { }
