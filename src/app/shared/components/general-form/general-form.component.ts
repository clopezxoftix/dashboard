import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators
} from "@angular/forms";
import { TypeValidators } from "./types/TypeValidators";
import { CustomValidators } from "./validators/CustomValidators";

@Component({
  selector: "general-form",
  templateUrl: "./general-form.component.html",
  styleUrls: ['./general-form.component.css']
})
export class GeneralFormComponent implements OnInit {
  //@Input("parameters")
  _parameters: any;
  config = {};
  @Input('parameters')
  set parameters(value: any) {
    this._parameters = value;
  }
  get parameters() {
    return this._parameters;
  }
  @Output()
  onSubmitEvent = new EventEmitter();
  formControled: FormGroup;
  fields = [];
  tokenForm: any;
  token = new Date().getTime().toString();

  constructor() { }

  ngOnInit() {
    this.loadFormGroup();
  }

  generateToken() {
    this.tokenForm = new Date().getTime().toString();
  }

  private loadFormGroup() {
    this.parameters = this.parameters || {};
    if (this.parameters.fields) {
      for (let key in this.parameters.fields) {
        let field = this.parameters.fields[key];
        if (field.row) {
          field.row.forEach(fieldRow => {
            if (Array.isArray(fieldRow)) {
              fieldRow.forEach(fieldRowSingle => {
                this.initFormControl(fieldRowSingle.key, fieldRowSingle)
              })
            } else {
              this.initFormControl(fieldRow.key, fieldRow)
            }
          });
        } else if (Array.isArray(field)) {
          field.forEach(fieldRowSingle => {
            this.initFormControl(fieldRowSingle.key, fieldRowSingle)
          })
        } else {
          this.initFormControl(key, field)
          field.key = key;
        }

        this.fields.push(field);
      }
    }

    this.formControled = new FormGroup(this.config);
  }

  initFormControl(key, field) {
    this.config[key] = new FormControl("",
      this.getArrayValidations(field.validation)
    );
    field.token = this.token;
  }

  private getArrayValidations(validation) {
    validation = validation || {};
    let validations = [];
    for (let key in validation) {
      switch (key) {
        case TypeValidators.REQUIRED:
          validations.push(Validators.required);
          break;
        case TypeValidators.EMAIL:
          validations.push(Validators.email);
          break;
        case TypeValidators.MAX:
          validations.push(Validators.max(validation[key].number));
          break;
        case TypeValidators.MIN:
          validations.push(Validators.min(validation[key].number));
          break;
        case TypeValidators.EQUAL_TO:
          validations.push(CustomValidators.equalTo(validation[key].to));
          break;
        default:
          break;
      }
    }
    return validations;
  }

  private onSubmit() {
    this.onSubmitEvent.emit(this.formControled.value);
  }

  get form() {
    return this.formControled;
  }

  radioDisable(key, value) {
    const idRadio = this.token + '-' + key + "-" + value;
    const query: any = document.getElementById(idRadio);
    query.disabled = true;
  }

  public clear() {
    this.formControled.reset()
  }

  isAnArray(field) {
    return Array.isArray(field);
  }
}
