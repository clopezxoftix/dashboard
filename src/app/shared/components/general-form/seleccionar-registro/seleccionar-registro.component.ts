import { Component, OnInit, Input, forwardRef, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { faEraser, faSearch } from '@fortawesome/free-solid-svg-icons';
import { TypeElement } from '../types/TypeElement';
import { GeneralFormComponent } from '../general-form.component';
import { commonValues } from '../../../../../commons/commonValues';

@Component({
  selector: 'seleccionar-registro',
  templateUrl: './seleccionar-registro.component.html',
  styleUrls: ['./seleccionar-registro.component.css'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => SeleccionarRegistroComponent),
    multi: true
  }]
})
export class SeleccionarRegistroComponent implements OnInit, ControlValueAccessor, Validators {

  @ViewChild(GeneralFormComponent, { 'static': false }) formularioFiltro: GeneralFormComponent;

  /**tabla primeng */
  datasource = new Array<any>();

  totalRecords: number;

  cols = new Array<any>();

  loading: boolean = false;
  /** fin tabla */

  faEraser = faEraser;
  faSearch = faSearch;

  @Input() servicio: any;

  @Input() parametrosBusqueda = new Array<string>();

  @Input() parametrosTabla = new Array<string>();

  @Input() parametrosHeadersTabla = new Array<string>();

  @Input() parametrosInput = new Array<string>();

  @Input() listarEspecifico = false;

  _parametrosDefecto;
  params: any = { 'size': 5, 'page': 0 }


  form: any;
  typeElementText = { typeElement: TypeElement.TEXT };

  @Input('parametrosDefecto')
  set parametrosDefecto(value: any) {
    this._parametrosDefecto = value;

  }

  get parametrosFiltrar() {
    return this._parametrosDefecto;
  }

  listaTemp = new Array<any>();
  elementoSeleccionado: any;

  texto = "";

  estructura = {
    pag: 1,
    pagTotal: 1,
    totalRegistros: 0
  }

  value: any = "";
  isDisabled: boolean;
  onChange = (_: any) => { };
  onTouch = () => { };


  writeValue(obj: any): void {
    if (obj) {
      this.value = obj || '';
    } else {
      this.value = '';
    }
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }
  constructor() {

  }

  ngOnInit() {
    this.form = this.crearFormFiltrol();
    this.parametrosTabla.forEach(element => {
      this.cols.push({ field: element, header: this.parametrosHeadersTabla[this.parametrosTabla.indexOf(element)] })
    });
  }

  /**metodos tabla */
  loadCarsLazy(event) {
    this.params['page'] = event.first / event.rows;
    if (this.listaTemp.length != 0) {
      this.servicioBuscar();
    }
  }

  /** fin metodos tabla */

  textoElemento(valor) {
    let mensaje = "";
    if (this.parametrosInput.length > 0) {
      this.parametrosInput.forEach(element => {
        if (valor[element] != null) {
          if (element == "codigo") {
            mensaje = mensaje + "[" + valor[element] + "] ";
          } else {
            mensaje = mensaje + " " + valor[element];
          }
        }
      });
    }
    return mensaje;
  }

  buscar() {
    this.params['size'] = 5;
    this.params['page'] = 0;
    this.servicioBuscar();
  }

  buscarTextoCriterio() {

    this.parametrosBusqueda.forEach(element => {
      if (this.formularioFiltro.form.get(element).value != "") {
        this.params[element] = this.formularioFiltro.form.get(element).value;
      } else {
        delete this.params[element];
      }
    });
    this.servicioBuscar();

  }

  buscarTexto() {
    this.loading = true;
    this.buscarTextoCriterio();
  }
  servicioBuscar() {
    let parametros = { ... this.params, ...this.parametrosFiltrar }
    if (!this.listarEspecifico) {
      this.servicio.listar(parametros).subscribe(data => {
        this.listaTemp = data.content;
        this.datasource = data.content;
        this.estructura.pagTotal = data.totalPages;
        this.estructura.pag = data.pageable.pageNumber + 1;
        this.totalRecords = data.totalElements;
        this.loading = false;
      });
    } else {
      this.servicio.listarEspecifico(parametros).subscribe(data => {
        this.listaTemp = data.content;
        this.datasource = data.content;
        this.estructura.pagTotal = data.totalPages;
        this.estructura.pag = data.pageable.pageNumber + 1;
        this.totalRecords = data.totalElements;
        this.loading = false;
      });
    }
  }

  asignar(obj) {
    this.elementoSeleccionado = obj;
    this.value = obj;
    this.onTouch();
    this.onChange(this.value);
  }

  limpiar() {
    this.elementoSeleccionado = "";
    this.value = "";
    this.onTouch();
    this.onChange(this.value);
    this.listaTemp = new Array<any>();
  }

  crearFormFiltrol() {
    let temp = [];
    this.parametrosBusqueda.forEach(element => {
      temp.push({
        key: element,
        label: element.charAt(0).toUpperCase() + element.slice(1),
        placeholder: commonValues.CRITERIO_LABEL + " " + element,
        typeElement: this.typeElementText
      })
    });

    return {
      fields: {
        rowOne: {
          row: temp
        }
      }
    }
  }

}