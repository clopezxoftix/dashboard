import { Component, OnInit, ViewChild } from '@angular/core';
import { TypeElement } from '../types/TypeElement';
import { TypeButton } from '../types/TypeButton';
import { TypeButtonClass } from '../types/TypeButtonClass';
import { GeneralFormComponent } from '../general-form.component';

@Component({
  selector: 'sample',
  templateUrl: './sample.component.html',
  styleUrls: ['./sample.component.css']
})
export class SampleComponent implements OnInit {

  config: any;
  options = [{value:"1", label: "Item 1"}, {value:"2", label: "Item 2"}]
  optionsEstado = [{value:"S", label: "SI"}, {value:"N", label: "NO"}]

  @ViewChild(GeneralFormComponent, { 'static': false }) generalFormComponent : GeneralFormComponent
  constructor() {

  }

  ngOnInit() {
    this.buildForm()
  }

  buildForm() {
    this.config = {
      header: {
        label: "Datos Generales",
      },
      buttons: [
        {
          label: "Enviar",
          type: TypeButton.SUBMIT,
          typeClass: TypeButtonClass.SUCCESS
        }
      ],
      fields: {
        id:{},
        row: {
          row: [
            {
              key: "textbox",
              label: "Caja de texto",
              typeElement: TypeElement.TEXT,
              validation: {
                required: "El campo es obligatorio"
              },
              placeholder:"ingrese un codigo"
            },
            {
              key: "fecha",
              label: "Fecha",
              typeElement: TypeElement.DATE,
              validation: {
                required: "El campo es obligatorio"
              },
              placeholder:"ingrese un codigo",
              config: {}
            },
            {
              key: "hora",
              label: "Hora",
              typeElement: TypeElement.TIME,
              config: {}
            },
            {
              key: "estado",
              label: "Estado",
              typeElement: TypeElement.RADIO,
              options: this.options,
              default:this.options[0]

            }
          ]
        },
        row1: {
          row: [
            {
              key: "tipoIdentificacion",
              label: "Tipo de identificación",
              typeElement: TypeElement.SELECT,
              options: this.options,
              default:this.options[0]
            },
            {
              key: "password",
              label: "Contraseña",
              typeElement: TypeElement.PASSWORD,
              validation: {
                required: "El campo es obligatorio"
              },
              placeholder:"Contraseña"
            },
            {
              key: "cantidad",
              label: "Cantidad",
              typeElement: TypeElement.NUMBER,
              validation: {
                required: "El campo es obligatorio"
              },
              placeholder:"Cantidad"
            },
            [
              {
                key: "aceptado",
                label: "Aceptado",
                typeElement: TypeElement.CHECKBOX,
                validation: {
                  required: "El campo es obligatorio"
                }
              },
              {
                key: "negado",
                label: "Negado",
                typeElement: TypeElement.CHECKBOX,
                validation: {
                  required: "El campo es obligatorio"
                }
              }
            ],
          ]
        },
        row2: {
          row: [
            {
              key: "status",
              label: "Estado",
              typeElement: TypeElement.BUTTON_GROUP,
              options: this.options,
              default:this.options[0]
            },
            {
              key: "dateRange",
              label: "Rango de fecha",
              typeElement: TypeElement.DATE_RANGE
            },
          ]
        },
        separator: {
          typeElement: TypeElement.SEPARATOR
        },
        documentacion:{
          label: "Documentación",
          typeElement: TypeElement.TEXTAREA

        },
      }
    };
  }

  onSubmit(event) {
    console.log(event)
    this.generalFormComponent.formControled.disable()
  }

}
