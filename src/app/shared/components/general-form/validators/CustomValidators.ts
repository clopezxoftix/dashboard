import { AbstractControl, ValidatorFn } from "@angular/forms";

export class CustomValidators {
  static equalTo(to: string): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const field = control.parent ? control.parent.controls[to] : null;

      if (!field) return null;

      const isTheSame = field.value === control.value;
      if (isTheSame) {
        if (field.errors ? field.errors.equalTo : false) {
          delete field.errors.equalTo;
          delete field.errors.to;
          field.setErrors(
            Object.keys(field.errors).length === 0 ? null : field.errors
          );
        }
      }
      return isTheSame
        ? null
        : { equalTo: { value: control.value }, to: { value: field.value } };
    };
  }
}
