import { Component, OnInit, Input } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { TypeElement } from '../types/TypeElement';
import { TypeValidators } from "../types/TypeValidators";
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { TypeCaseText } from "../types/TypeCaseText";

@Component({
  selector: "general-form-field",
  templateUrl: "./general-form-field.component.html",
  styleUrls: ['./general-form-field.component.css']
})
export class GeneralFormFieldComponent implements OnInit {
  typeElement = TypeElement;
  typeValidators = TypeValidators;
  @Input()
  field: any;
  @Input()
  form: FormGroup;

  style = "width: 50px; height: 50px;";
  active = "";
  keyDownValue = "";
  rangeValues: number[] = [20, 80];
  defaultStart: number = 3;
  defaultValueSpinner: number = 0;
  stepValue: number = 1;
  minSpinner: number = 0;
  maxSpinner: number = 10;
  dateTime: Date = new Date();
  dateMinTime: Date = new Date();
  dateMaxTime: Date = new Date();
  date8: Date;
  stepSlider: number = 1;
  minSlider: number = 0;
  maxSlider: number = 100;

  formTime: FormGroup;

  myOptions: INgxMyDpOptions = {
    // other options...
    dateFormat: 'dd/mm/yyyy',
    selectorHeight: '260px',
    selectorWidth: '290px',
  };
  myDate: [null]

  _tokenForm: String;

  datePickerConfig: any;
  readonly DEFAULT_THEME_DATEPICKER = 'theme-dark-blue';
  @Input('tokenForm')
  set tokenForm(value: String) {
    this._tokenForm = value;
  }

  get isInvalid() {
    return this.form.controls[this.field.key].invalid;
  }
  get isTouched() {
    return this.form.controls[this.field.key].touched;
  }
  get isDirty() {
    return this.form.controls[this.field.key].dirty;
  }

  hasErrorAndHasMessage(typeValidator: TypeValidators) {
    return this.hasError(typeValidator) &&
      this.field.validation ? this.field.validation[typeValidator] : false
  }

  hasError(idValidation: string) {
    return this.form.controls[this.field.key].hasError(idValidation);
  }

  getMessage(property: string) {
    let message: String = "";
    let prop = this.field.validation[property];
    if (typeof prop === 'string') {
      message = prop;
    } else if (typeof prop === 'object') {
      if (prop.message) message = prop.message;
    }
    return message;
  }
  setDate(): void {
    // Set today date using the patchValue function
    let date = new Date();
    this.form.patchValue({
      myDate: {
        date: {
          year: date.getFullYear(),
          month: date.getMonth() + 1,
          day: date.getDate()
        }
      }
    });
  }

  clearDate(): void {
    // Clear the date using the patchValue function
    this.form.patchValue({ myDate: null });
  }

  constructor() {
    this.dateMinTime.setHours(0);
    this.dateMinTime.setMinutes(0);
    this.dateMaxTime.setHours(23);
    this.dateMaxTime.setMinutes(59);

    this.formTime = new FormGroup({
      myControl: new FormControl(new Date())
    });
  }

  ngOnInit() {
    this.datePickerConfig = Object.assign({ containerClass: this.DEFAULT_THEME_DATEPICKER }, this.field.config);
    switch (this.field.typeElement) {
      case TypeElement.RADIO:
        this.listenerChangesForX(this.field.key, (key, val) => this.setValueRadioView(key, val))
        break;
      case TypeElement.CHECKBOX:
        this.listenerChangesForX(this.field.key, (key, val) => this.setValueCheckboxView(key, val))
        break;
    }
  }

  listenerChangesForX(key: string, callback) {
    this.form.controls[key].valueChanges.subscribe(val => callback(key, val))
  }


  setValueRadio(field, value) {
    this.form.controls[field.key].setValue(value);
  }

  setValueRadioView(key, value) {
    this.setValueElementView(key, value)
  }

  setValueCheckbox(event, key) {
    this.form.controls[key].setValue(event.target.checked);
  }

  setValueCheckboxView(key, value) {
    this.setValueElementView(key, value)
  }

  setValueElementView(key, value) {
    const id = this.field.token + '-' + key + (this.field.typeElement === TypeElement.RADIO ? "-" + value : '');
    const query: any = document.getElementById(id);
    if (query) {
      query.checked = value;
    }
  }

  setActive(field: any, option: any) {
    this.setDefaultValue(field);
    return this.form.controls[field.key].value == option.value ? "active" : null;
  }

  setDefaultValue(field: any) {
    if (field.default && this.form.controls[field.key].value == "") {
      this.form.controls[field.key].setValue(field.default.value);
    }
  }

  setFirstValue(field, form) {
    if (!this.form.controls[field.key].value || this.form.controls[field.key].value == "") {
      this.form.controls[field.key].setValue(form.controls[field.key].value ? form.controls[field.key].value.toUpperCase() : "");
    }
  }

  onKeyUp(field) {
    if (field.textCase == TypeCaseText.UPPER_CASE) {
      this.form.controls[field.key].setValue(this.form.controls[field.key].value.toUpperCase());
    } else if (field.textCase == TypeCaseText.LOWER_CASE) {
      this.form.controls[field.key].setValue(this.form.controls[field.key].value.toLowerCase());
    }
    return true;
  }

  validarInput(field: any) {
    let typeElement = this.getTypeElement(field);
    if (typeElement == this.typeElement.TIME || typeElement == this.typeElement.DATE || typeElement == this.typeElement.DATE_RANGE) {
      if (isNaN(Date.parse(this.form.controls[field.key].value))) {
        this.form.controls[field.key].setValue("");
      }
    }
    return typeElement === this.typeElement.TEXT
      || typeElement === this.typeElement.PASSWORD
      || typeElement === this.typeElement.NUMBER
      || typeElement === this.typeElement.COLOR;
  }

  getTypeElement(field: any) {
    return field.typeElement && field.typeElement.typeElement ? field.typeElement.typeElement : field.typeElement;
  }

  // setConfiguration(field) {
  //   if (field.configuration.range) {
  //     this.rangeValues = field.configuration.valor ? field.configuration.valor : this.rangeValues;
  //   } else {
  //     this.valor = field.configuration.valor ? field.configuration.valor : this.valor;      
  //   }
  //   return field.configuration ? true : false;
  // }
}
