import { NgModule } from "@angular/core";
import { MenuBarComponent } from "./menu-bar.component";

@NgModule({
    declarations: [MenuBarComponent],
    imports: [],
    exports: [MenuBarComponent]
})
export class MenuBarModule {}